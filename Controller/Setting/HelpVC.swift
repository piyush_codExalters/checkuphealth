//
//  HelpVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit

class HelpCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTitleName: LabelMedium!
    @IBOutlet weak var lblDesc: LabelRegular!
    @IBOutlet weak var imageIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias : 6)

    }
}


class HelpVC: ParentViewController {
    var cellHeightsDictionary = NSMutableDictionary()
    @IBOutlet weak var lblEmail: LabelRegular!
    
    var arryaHelipList = [HelipListName]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getHelplist()
        if isWorkingCountryUK(){
            lblEmail.text = "customercare@checkuphealth.co.uk"
        }else{
            lblEmail.text = "customercare@checkuphealth.co.uk"
        }
    }
    

   

}
extension HelpVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height = cellHeightsDictionary[indexPath] as? NSNumber
        if height != nil {
            return CGFloat(Double(truncating: height ?? 0.0))
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let titleView = view as! UITableViewHeaderFooterView
        titleView.textLabel?.text =  arryaHelipList[section].CategoryName//titleView.textLabel?.text?.lowercased()
        titleView.backgroundView?.backgroundColor = .white
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       return arryaHelipList[section].CategoryName
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arryaHelipList[indexPath.section].arryaHelipList[indexPath.row].isSect{
            arryaHelipList[indexPath.section].arryaHelipList[indexPath.row].isSect = false
        }else{
            arryaHelipList[indexPath.section].arryaHelipList[indexPath.row].isSect = true
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaHelipList[section].arryaHelipList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaHelipList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Expendation") as! HelpCell
        cell.lblTitleName.text = arryaHelipList[indexPath.section].arryaHelipList[indexPath.row].vQuestion
        if arryaHelipList[indexPath.section].arryaHelipList[indexPath.row].isSect{
            cell.imageIcon.image = #imageLiteral(resourceName: "ic_neavigationbar_bg")
            cell.lblDesc.text = arryaHelipList[indexPath.section].arryaHelipList[indexPath.row].vAnswer
        }else{
            cell.imageIcon.image = #imageLiteral(resourceName: "ic_plus")
            cell.lblDesc.text = ""
        }
        return cell
    }
}
extension HelpVC{
    func getHelplist(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "getHelplist.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    self.arryaHelipList.append(HelipListName(dic: obj as! NSDictionary))
                                }
                            }
                            self.tableView.reloadData()
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
