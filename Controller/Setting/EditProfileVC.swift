//
//  EditProfileVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 23/04/21.
//

import UIKit
import SKCountryPicker
import DropDown
class EditBasicList: NSObject {
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Basic Info"
        
        var t1 = SingIn()
        t1.placeholder = "First Name"
        t1.keybord = .default
        t1.isSecure = false
        t1.strkey1 = "fname"
        
        
        var t2 = SingIn()
        t2.placeholder = "Last Name"
        t2.keybord = .default
        t2.isSecure = false
        t2.strkey1 = "lname"
        
        
        var t3 = SingIn()
        t3.placeholder = "Date of Birth"
        t3.keybord = .default
        t3.image = #imageLiteral(resourceName: "ic_calender")
        t3.isSecure = false
        
        
        
        var t6 = SingIn()
        t6.placeholder = "Sex"
        t6.keybord = .default
        t6.isSecure = false
        t6.image = #imageLiteral(resourceName: "ic_dropdown")
        
        
        var t16 = SingIn()
        t16.placeholder = "Registered Address"
        t16.keybord = .default
        t16.placeholder1 = "Address Line 1"
        
        var t17 = SingIn()
        t17.keybord = .default
        t17.placeholder1 = "Area (Optional)"
        
        var t18 = SingIn()
        t18.keybord = .default
        t18.placeholder1 = "Town/City"
        
        
        var t22 = SingIn()
        t22.placeholder1 = "District(Optional)"
        t22.keybord = .default
        
        var t19 = SingIn()
        t19.keybord = .default
        t19.placeholder1 = "State/Province/Region (Optional)"
        
        var t20 = SingIn()
        t20.keybord = .default
        t20.placeholder1 = "Country"
        
        
        var t21 = SingIn()
        t21.keybord = .default
        t21.placeholder1 = "Postcode"
        
        
        
        var t62 = SingIn()
        t62.placeholder = "GP Practice Name"
        t62.keybord = .default
        t62.strkey1 = "practice_name"
        t62.placeholder1 = "GP Practice Name"
        
        var t63 = SingIn()
        t63.placeholder = "GP Practice Email"
        t63.keybord = .default
        t63.strkey1 = "practice_email"
        t63.placeholder1 = "GP Practice Email"

        
        
        
        var t57 = SingIn()
        t57.placeholder = "GP Practice Address"
        t57.keybord = .default
        t57.strkey1 = "practice_addressline1"
        t57.placeholder1 = "Address Line 1"
        
        
        
        var t58 = SingIn()
        t58.placeholder = ""
        t58.keybord = .default
        t58.strkey1 = "practice_addressline2"
        t58.placeholder1 = "Address Line 2"
        
        
        
        var t59 = SingIn()
        t59.placeholder = ""
        t59.keybord = .default
        t59.strkey1 = "practice_city"
        t59.placeholder1 = "Town/City"
        
        
        
        var t60 = SingIn()
        t60.placeholder = ""
        t60.keybord = .default
        t60.strkey1 = "practice_postcode"
        t60.placeholder1 = "Postcode"
        
        
        
        var t61 = SingIn()
        t61.placeholder = ""
        t61.keybord = .numberPad
        t61.strkey1 = "practice_telephoneno"
        t61.placeholder1 = "Telephone no"
        
        
        
        var t4 = SingIn()
        t4.placeholder = "Ethnicity"
        t4.keybord = .default
        t4.isSecure = false
        t4.strkey1 = "ethnicity"
        t4.placeholder1 = "Ethnicity"
        
        
        var t56 = SingIn()
        t56.placeholder = "Patient ID"
        t56.keybord = .default
        t56.isSecure = false
        t56.strkey1 = ""
        
        allFields.append(t0)
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
        allFields.append(t6)
        
        allFields.append(t16)
        allFields.append(t17)
        allFields.append(t18)
        allFields.append(t22)
        allFields.append(t19)
        allFields.append(t20)
        allFields.append(t21)
        
        allFields.append(t62)
        allFields.append(t63)
        allFields.append(t57)
        allFields.append(t58)
        allFields.append(t59)
        allFields.append(t60)
        allFields.append(t61)
        allFields.append(t4)
        allFields.append(t56)
    }
    
    
}
class EditContactList: NSObject {
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    
    func prepareDate(){
        var t5 = SingIn()
        t5.placeholder = "Contact Info"
        
        var t9 = SingIn()
        t9.placeholder = "Email Address"
        t9.keybord = .emailAddress
        t9.strValue = ""
        t9.isSecure = false
        t9.strkey1 = "email"
        
        var t7 = SingIn()
        t7.placeholder = "Mobile Number"
        let country = CountryManager.shared.currentCountry
        t7.strkey = country!.countryCode
        t7.strcode = (country?.dialingCode)!
        t7.image1 = country!.flag
        t7.keybord = .numberPad
        t7.isSecure = false
        t7.strkey1 = "mobile"
        t7.strkey2 = "countrycode"
        
        
        var t8 = SingIn()
        t8.placeholder = "Consultation Address"
        t8.keybord = .default
        t8.image = #imageLiteral(resourceName: "ic_navigation_aro")
        t8.isSecure = false
        t8.strValue = "Manage Addresses"
        
        allFields.append(t5)
        allFields.append(t9)
        allFields.append(t7)
        allFields.append(t8)
    }
}

class EditOrganizationList: NSObject {
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
        
    }
    
    func prepareDate(){
        var t10 = SingIn()
        t10.placeholder = "Organization Info"
        
        var t11 = SingIn()
        t11.placeholder = "Your Organization Code (Optional)"
        t11.keybord = .default
        t11.isSecure = false
        t11.strkey1 = "vOrganisationCode"
        
        allFields.append(t10)
        allFields.append(t11)
    }
}
class EditOtherList: NSObject {
    
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t12 = SingIn()
        t12.placeholder = "Other Info"
        
        var t13 = SingIn()
        t13.placeholder = "Height"
        t13.keybord = .default
        t13.image = #imageLiteral(resourceName: "ic_dropdown")
        t13.isSecure = false
        
        
        var t14 = SingIn()
        t14.placeholder = "Weight"
        t14.keybord = .default
        t14.image = #imageLiteral(resourceName: "ic_dropdown")
        t14.isSecure = false
        
        
        var t15 = SingIn()
        t15.placeholder = "Blood Group"
        t15.keybord = .default
        t15.image = #imageLiteral(resourceName: "ic_dropdown")
        t15.isSecure = false
        
        
        var t16 = SingIn()
        t16.placeholder = "National l'd No."
        t16.keybord = .default
        t16.isSecure = false
        t16.strkey1 = "NIN"
        
        
        var t17 = SingIn()
        t17.placeholder = "Occupation"
        t17.keybord = .default
        t17.isSecure = false
        t17.strkey1 = "occupation"
        
        
        var t18 = SingIn()
        t18.placeholder = "Birth Certificate No."
        t18.keybord = .default
        t18.isSecure = false
        t18.strkey1 = "BCN"
        
        
        var t19 = SingIn()
        t19.placeholder = "l am"
        t19.keybord = .default
        t19.image = #imageLiteral(resourceName: "ic_dropdown")
        t19.isSecure = false
        
        
        var t20 = SingIn()
        t20.placeholder = "Country"
        t20.keybord = .default
        t20.image = #imageLiteral(resourceName: "ic_dropdown")
        t20.isSecure = false
        
        
        var t21 = SingIn()
        t21.placeholder = "Is Advantage Health Taken"
        
        var t22 = SingIn()
        t22.placeholder = "SMS Consent"
        
        
        allFields.append(t12)
        allFields.append(t13)
        allFields.append(t14)
        allFields.append(t15)
        allFields.append(t17)
        allFields.append(t16)
        allFields.append(t18)
        allFields.append(t19)
        allFields.append(t20)
        allFields.append(t21)
        allFields.append(t22)
        
    }
    
    
}
class EditProfileVC: ParentViewController, addressDelegete {
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewVerification: UIView!
    @IBOutlet weak var btndeletedClicked: deltedButton!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    var fieldData = EditOtherList()
    var basicList = EditBasicList()
    var contactList = EditContactList()
    var organizationList = EditOrganizationList()
    var imagePicker = UIImagePickerController()
    var imgUser : UIImage?
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var ayyaHight = [String]()
    var ayyaWeight = [String]()
    var ayyaBlood = ["A+", "B+","AB+", "O+", "A-", "B-","AB-","O-"]
    var city = ["Zimbabwe", "United Kingdom"]
    var arryaEthnicity = ["White", "Mixed/ multiple ethnic","Asian/ Asian British","Black/ African/ Caribbean/ African British","Other ethnic groups"]
    var arryaGender = ["Male", "Female","Not listed","Prefer not to say"]
    var arryaIam = ["Non-diabetic", "Type 2 diabetes","Type 1 diabetes","Children w/type 1 diabetes"]
    var cityCode = ["ZW","GB"]
    var isEmailVerdilation = "0"
    var nOrganizationId = "0"
    let dropHight = DropDown()
    let dropWeight = DropDown()
    let dropBlood = DropDown()
    let dropCity = DropDown()
    let dropEthnicity = DropDown()
    let dropGender = DropDown()
    let dropIam = DropDown()
    var strAppointment = "Enter your comments"
    var selectAddres : AddressListName!
    var isLoading = true
    override func viewDidLoad() {
        super.viewDidLoad()
        getHight()
        getWeight()
        viewVerification.makeCornerRound(redias : 6)
        viewProfile.makeCornerRound(redias : 6)
        imageProfile.makeCornerRound(redias : 6)
        viewBottom.frame.size.height = 111 * _widthRatio
        
        viewTop.frame.size.height = 203 * _widthRatio
        datefrmenter.dateFormat = "dd-MM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = NSDate()
        var components = DateComponents()
        components.calendar = calendar
        components.year = -100
        let minDate = calendar.date(byAdding: components, to: currentDate as Date)!
        
        components.year = -10
        let maxDate = calendar.date(byAdding: components, to: currentDate as Date)!
        
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        
        //   btndeletedClicked.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isLoading{
            getuserprofile()
        }
        if !isLoading{
            self.isLoading = true
        }
    }
    
    @IBAction func btnInfoClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Organization Code", message: "If you are registering via your organization, please enter your organization code here.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        basicList.allFields[3].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 3, section: 0))as! ProfileCell
        cell.textEmail.text = basicList.allFields[3].strValue
        getupdate(key : "dob",value: basicList.allFields[3].strValue)
        
    }
    @IBAction func btnMaleClicked(_ sender: Any) {
        fieldData.allFields[4].strkey = "1"
        tableView.reloadData()
    }
    @IBAction func btnFemaleClicekd(_ sender: Any) {
        fieldData.allFields[4].strkey = "2"
        tableView.reloadData()
    }
    @IBAction func btnContyCodeClikced(_ sender: Any) {
        _ = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.contactList.allFields[2].strkey = country.countryCode // IN
            self.contactList.allFields[2].strcode = country.dialingCode! // +91
            self.isLoading = false
            self.tableView.reloadData()
        }
        
    }
    @IBAction func btnsenderLink(_ sender: Any) {
        resendverificationlink()
    }
    
    @IBAction func btnCameraClicked(_ sender: Any) {
        // openPickerCamera()
    }
    
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if fieldData.allFields[sender.tag].strkey == "0"{
            fieldData.allFields[sender.tag].strkey = "1"
        }else{
            fieldData.allFields[sender.tag].strkey = "0"
        }
        if sender.tag == 9{
            if fieldData.allFields[sender.tag].strkey == "1"{
                getupdate(key : "IsAdvantageHealthTaken",value: "true")
            }else{
                getupdate(key : "IsAdvantageHealthTaken",value: "false")
            }
        }else{
            getupdate(key : "isSMSconsent",value: fieldData.allFields[sender.tag].strkey)
        }
        tableView.reloadData()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter your comments"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 20)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Enter your comments"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strAppointment
            }
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        strAppointment = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    
    func getAddress(str: String,address: AddressListName, index: Int) {
        // selectAddres = address
        // getupdateAddres(key : "location",value: str)
    }
    
    @IBAction func btnVerifation(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "IdentityVerificationVC")  as! IdentityVerificationVC
        // mapVc1.delegatesAddress1 = self
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    
    @IBAction func btnDeletedClicked(_ sender: Any) {
        showAlert(title: "Delete Account", msgString: "Are you sure to delete your CheckupHealth Account?", Ohk: "OK", cancal: "Cancel") {
            self.deleteaccount()
        }
        
    }
    
    
}
extension EditProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return basicList.allFields.count
        }else if section == 1{
            return contactList.allFields.count
        }else if section == 2{
            return organizationList.allFields.count
        }else{
            return fieldData.allFields.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if  indexPath.row == 12 || indexPath.row == 13 || indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16 || indexPath.row == 17 || indexPath.row == 18{
                let alertController = UIAlertController(title:"Edit " + basicList.allFields[indexPath.row].placeholder1, message: "", preferredStyle: UIAlertController.Style.alert)
                alertController.addTextField { [self] (textField : UITextField!) -> Void in
                    textField.placeholder = basicList.allFields[indexPath.row].placeholder
                    textField.text = basicList.allFields[indexPath.row].strValue
                }
                let saveAction = UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    if String.validateStringValue(str: firstTextField.text){
                        self.showFailMessage(message : "Please enter required fields.")
                    }else{
                        let str = firstTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                        getupdate(key : basicList.allFields[indexPath.row].strkey1,value: str)
                    }
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                    (action : UIAlertAction!) -> Void in })
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            if indexPath.row == 4{
                getgender()
                dropGender.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[4].strValue = arryaGender[index]
                    getupdate(key : "gender",value: fieldData.allFields[4].strValue)
                    
                }
                self.view.endEditing(true)
                self.dropGender.show()
            }
            
        }else if indexPath.section == 1{
            if indexPath.row == 1 || indexPath.row == 2 {
                let alertController = UIAlertController(title:"Edit " + contactList.allFields[indexPath.row].placeholder, message: "", preferredStyle: UIAlertController.Style.alert)
                alertController.addTextField { [self] (textField : UITextField!) -> Void in
                    textField.placeholder = contactList.allFields[indexPath.row].placeholder
                    textField.text = contactList.allFields[indexPath.row].strValue
                }
                let saveAction = UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    if String.validateStringValue(str: firstTextField.text){
                        self.showFailMessage(message : "Please enter required fields.")
                    }
                    //                    else if firstTextField.text!.count <= 9 {
                    //                        return showFailMessage(message: "Please enter valid contact number")
                    //                    }
                    else{
                        
                        let str = firstTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        if indexPath.row == 2{
                            getupdateMobile(key : contactList.allFields[indexPath.row].strkey1,value:  str)
                            return
                        }
                        getupdate(key : contactList.allFields[indexPath.row].strkey1,value: str)
                    }
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                    (action : UIAlertAction!) -> Void in })
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            if indexPath.row == 3{
                let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressVC")  as! AddressVC
                // mapVc1.delegatesAddress1 = self
                navigationController?.pushViewController(mapVc1, animated: true)
            }
        }else if indexPath.section == 2{
            if indexPath.row == 1 {
                let alertController = UIAlertController(title:"Edit " + organizationList.allFields[indexPath.row].placeholder, message: "", preferredStyle: UIAlertController.Style.alert)
                alertController.addTextField { [self] (textField : UITextField!) -> Void in
                    textField.placeholder = organizationList.allFields[indexPath.row].placeholder
                    textField.text = organizationList.allFields[indexPath.row].strValue
                }
                let saveAction = UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    if String.validateStringValue(str: firstTextField.text){
                        self.showFailMessage(message : "Please enter required fields.")
                    }else{
                        let str = firstTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                        getupdate(key : organizationList.allFields[indexPath.row].strkey1,value: str)
                    }
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                    (action : UIAlertAction!) -> Void in })
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }else{
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6{
                let alertController = UIAlertController(title:"Edit " + fieldData.allFields[indexPath.row].placeholder, message: "", preferredStyle: UIAlertController.Style.alert)
                alertController.addTextField { [self] (textField : UITextField!) -> Void in
                    textField.placeholder = fieldData.allFields[indexPath.row].placeholder
                    textField.text = fieldData.allFields[indexPath.row].strValue
                }
                let saveAction = UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    if String.validateStringValue(str: firstTextField.text){
                        self.showFailMessage(message : "Please Enter required fields.")
                    }else{
                        let str = firstTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                        getupdate(key : fieldData.allFields[indexPath.row].strkey1,value: str)
                    }
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                    (action : UIAlertAction!) -> Void in })
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
            if indexPath.row == 1{
                getdropHight()
                dropHight.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[indexPath.row].strValue = ayyaHight[index]
                    getupdate(key : "height",value: fieldData.allFields[indexPath.row].strValue)
                }
                self.view.endEditing(true)
                self.dropHight.show()
            }
            if indexPath.row == 2{
                getdropWeight()
                dropWeight.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[indexPath.row].strValue = ayyaWeight[index]
                    getupdate(key : "weight",value: fieldData.allFields[indexPath.row].strValue)
                }
                self.view.endEditing(true)
                self.dropWeight.show()
            }
            
            if indexPath.row == 3{
                getdropBlood()
                dropBlood.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[indexPath.row].strValue = ayyaBlood[index]
                    getupdate(key : "bloodgroup",value: fieldData.allFields[indexPath.row].strValue)
                }
                self.view.endEditing(true)
                self.dropBlood.show()
            }
            if indexPath.row == 7{
                getIAm()
                dropIam.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[indexPath.row].strValue = arryaIam[index]
                    getupdate(key : "p_type",value: "\(index + 1)")
                }
                self.view.endEditing(true)
                self.dropIam.show()
            }
            if indexPath.row == 8{
                getdropCity()
                dropCity.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[indexPath.row].strValue = city[index]
                    _userDefault.set(cityCode[index], forKey: "Language")
                    _userDefault.synchronize()
                    if isWorkingCountryUK(){
                        var arrVc = _appDelegator.tab.viewControllers
                        if arrVc!.count == 4{
                            return
                        }
                        arrVc?.remove(at: 1)
                        _appDelegator.tab.viewControllers = arrVc
                    }else{
                        let nav = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NavSettingVC") as! NavSettingVC
                        nav.navigationController?.isNavigationBarHidden = true
                        let vc =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "QuestionsVC") as! QuestionsVC
                        nav.viewControllers = [vc]
                        vc.navigationController?.isNavigationBarHidden = true
                        var arrVC = _appDelegator.tab.viewControllers
                        if arrVC!.count == 5{
                            return
                        }
                        arrVC?.insert(nav, at: 1)
                        
                        _appDelegator.tab.viewControllers = arrVC
                        (_appDelegator.tab.tabBar.items)?[1].title = "Question"
                        (_appDelegator.tab.tabBar.items)?[1].image = UIImage(named: "ic_questions_active")
                        
                        
                    }
                    
                    tableView.reloadData()
                }
                self.view.endEditing(true)
                self.dropCity.show()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 40
            }
            if isWorkingCountryUK(){
                
                
                if indexPath.row == 6 || indexPath.row == 8 || indexPath.row == 9{
                    return 0
                }
                if indexPath.row == 20{
                    return 90 * _widthRatio
                }
                if indexPath.row == 19{
                    return 90 * _widthRatio
                }
            }else{
                if indexPath.row == 11{
                    return 0
                }
                if indexPath.row == 12 || indexPath.row == 13 || indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16 || indexPath.row == 17 || indexPath.row == 18{
                    return 0
                }
            }
            if indexPath.row == 20 || indexPath.row == 1{
                return 90 * _widthRatio
            }
            return 75 * _widthRatio
        }
        if indexPath.section == 1{
            if indexPath.row == 0{
                return 40
            }
            if indexPath.row == 3 || indexPath.row == 1{
                return 90 * _widthRatio
            }
            return 75 * _widthRatio
        }
        if indexPath.section == 2{
            if indexPath.row == 0{
                return 40
            }
            return 90 * _widthRatio
        }else{
            if isWorkingCountryUK(){
                if indexPath.row == 5 || indexPath.row == 6{
                    return 0
                }
            }
            if indexPath.row == 0{
                return 40
            }
            if indexPath.row == 8{
                return 0
            }
            if indexPath.row == 1{
                return 90 * _widthRatio
            }
            if indexPath.row == 9 || indexPath.row == 10{
                if indexPath.row == 9{
                    let Lang = _userDefault.value(forKey: "Language") as? String
                    if Lang == nil || Lang == "ZW"{
                        return 50 * _widthRatio
                    }else{
                        return 0
                    }
                }
                return 50 * _widthRatio
            }
            return 75 * _widthRatio
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = basicList.allFields[indexPath.row].placeholder
                cell.btnViewAll.isHidden = true
                return cell
                
            }
            if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = basicList.allFields[indexPath.row].keybord
                
                cell.textEmail.text = basicList.allFields[indexPath.row].strValue
                cell.lblName.text = basicList.allFields[indexPath.row].placeholder
                cell.imageCalander.image = basicList.allFields[indexPath.row].image
                cell.textEmail.isUserInteractionEnabled = basicList.allFields[indexPath.row].isSecure
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                return cell
            }
            if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = basicList.allFields[indexPath.row].keybord
                cell.textEmail.text = basicList.allFields[indexPath.row].strValue
                cell.lblName.text = basicList.allFields[indexPath.row].placeholder
                cell.textEmail.isUserInteractionEnabled = basicList.allFields[indexPath.row].isSecure
                cell.imageCalander.image = basicList.allFields[indexPath.row].image
                dropGender.anchorView = cell.textEmail
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = basicList.allFields[indexPath.row].keybord
            cell.textEmail.text = basicList.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = basicList.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = basicList.allFields[indexPath.row].isSecure
            cell.btnLink.isHidden = true
            cell.imageVerifation.isHidden = true
            cell.textEmail.placeholder = basicList.allFields[indexPath.row].placeholder1
            
            if indexPath.row == 1{
                cell.viewConstretTop.constant = 5
                cell.viewConstretBottom.constant = -5
            }else if indexPath.row == 20{
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = 5
            }else{
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
            }
            return cell
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = contactList.allFields[indexPath.row].placeholder
                cell.btnViewAll.isHidden = true
                return cell
                
            }
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = contactList.allFields[indexPath.row].keybord
                cell.textEmail.text = contactList.allFields[indexPath.row].strValue
                cell.textEmail.inputView = nil
                cell.lblName.text = contactList.allFields[indexPath.row].placeholder
                cell.textEmail.isUserInteractionEnabled = contactList.allFields[indexPath.row].isSecure
                cell.btnLink.isHidden = true
                cell.viewConstretTop.constant = 5
                cell.viewConstretBottom.constant = -5
                if isEmailVerdilation == "0"{
                    cell.btnLink.isHidden = false
                    cell.imageVerifation.isHidden = true
                }else{
                    cell.btnLink.isHidden = true
                    cell.imageVerifation.isHidden = false
                }
                let FormattedText1 = NSMutableAttributedString()
                FormattedText1
                    .unlink("Resend Verification Link")
                cell.btnLink.setAttributedTitle(FormattedText1, for: .normal)
                return cell
            }
            if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "custTextConact") as! LoginCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = contactList.allFields[indexPath.row].keybord
                cell.textEmail.text = contactList.allFields[indexPath.row].strValue
                cell.textEmail.isUserInteractionEnabled = contactList.allFields[indexPath.row].isSecure
                cell.lblName.text = contactList.allFields[indexPath.row].placeholder
                cell.lblCountryCode.text = "\(contactList.allFields[indexPath.row].strcode)"
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                cell.viewBg.makeCornerRound(redias : 6)
                
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = contactList.allFields[indexPath.row].keybord
            cell.textEmail.text = contactList.allFields[indexPath.row].strValue
            cell.lblName.text = contactList.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = contactList.allFields[indexPath.row].isSecure
            cell.imageCalander.image = contactList.allFields[indexPath.row].image
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = 5
            return cell
            
        } else if indexPath.section == 2{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = organizationList.allFields[indexPath.row].placeholder
                if nOrganizationId == "0"{
                    cell.btnViewAll.isHidden = true
                }else{
                    cell.btnViewAll.isHidden = false
                }
                let FormattedText1 = NSMutableAttributedString()
                FormattedText1
                    .unlink("Unlink")
                cell.btnViewAll.setAttributedTitle(FormattedText1, for: .normal)
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Origation") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = organizationList.allFields[indexPath.row].keybord
            cell.textEmail.text = organizationList.allFields[indexPath.row].strValue
            cell.lblName.text = organizationList.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = organizationList.allFields[indexPath.row].isSecure
            cell.viewConstretTop.constant = 5
            cell.viewConstretBottom.constant = 5
            cell.textEmail.inputView = nil
            return cell
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = fieldData.allFields[indexPath.row].placeholder
                cell.btnViewAll.isHidden = true
                return cell
                
            }
            if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 7 || indexPath.row == 8{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
                cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
                cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
                cell.textEmail.isUserInteractionEnabled = fieldData.allFields[indexPath.row].isSecure
                cell.imageCalander.image = fieldData.allFields[indexPath.row].image
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                if indexPath.row == 1{
                    cell.viewConstretTop.constant = 5
                    cell.viewConstretBottom.constant = -5
                }
                if indexPath.row == 4{
                    dropGender.anchorView = cell.textEmail
                }
                if indexPath.row == 5{
                    dropEthnicity.anchorView = cell.textEmail
                }
                if indexPath.row == 1{
                    dropHight.anchorView = cell.textEmail
                }
                if indexPath.row == 2{
                    dropWeight.anchorView = cell.textEmail
                }
                if indexPath.row == 3{
                    dropBlood.anchorView = cell.textEmail
                }
                if indexPath.row == 7{
                    dropIam.anchorView = cell.textEmail
                }
                if indexPath.row == 8{
                    dropCity.anchorView = cell.textEmail
                }
                return cell
            }
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
                cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
                cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
                cell.textEmail.isUserInteractionEnabled = fieldData.allFields[indexPath.row].isSecure
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                cell.imageVerifation.isHidden = true
                
                cell.btnLink.isHidden = true
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
            cell.lblisAdvatigate.text = fieldData.allFields[indexPath.row].placeholder
            cell.btnCheck.tag = indexPath.row
            if isWorkingCountryUK(){
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = 5
            }else{
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                if indexPath.row == 10 {
                    cell.viewConstretBottom.constant = 5
                }
            }
            
            if fieldData.allFields[indexPath.row].strkey == "0"{
                cell.btnCheck.isSelected = false
            }else{
                cell.btnCheck.isSelected = true
            }
            
            return cell
            
        }
        
    }
    
    func getHight(){
        for inch in 1...96 {
            let cm = Float(inch) * 2.54
            let ft = inch / 12
            let inchFt = inch % 12
            ayyaHight.append("\(Int(ft)) ft \(Int(inchFt)) in (\(inch) in | \(cm.roundedFlaot(toPlaces: 2)) cm) ")
        }
    }
    
    
    func getWeight(){
        for i in 1...150 {
            let stone = Float(i) * 0.157473
            let lbs = Float(i) * 2.2
            ayyaWeight.append("\(lbs.roundedFlaot(toPlaces: 2)) lb (\(i) kg | \(stone.roundedFlaot(toPlaces: 2)) st)")
        }
    }
    func getdropHight(){
        dropHight.dataSource = ayyaHight
        dropHight.direction = .bottom
        dropHight.bottomOffset = CGPoint(x: 0, y:(dropHight.anchorView?.plainView.bounds.height)!)
        dropHight.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropWeight(){
        dropWeight.dataSource = ayyaWeight
        dropWeight.direction = .bottom
        dropWeight.bottomOffset = CGPoint(x: 0, y:(dropWeight.anchorView?.plainView.bounds.height)!)
        dropWeight.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropBlood(){
        dropBlood.dataSource = ayyaBlood
        dropBlood.direction = .bottom
        dropBlood.bottomOffset = CGPoint(x: 0, y:(dropBlood.anchorView?.plainView.bounds.height)!)
        dropBlood.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropCity(){
        dropCity.dataSource = city
        dropCity.direction = .bottom
        dropCity.bottomOffset = CGPoint(x: 0, y:(dropCity.anchorView?.plainView.bounds.height)!)
        dropCity.width = 263 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    func getdropethnicity(){
        dropEthnicity.dataSource = arryaEthnicity
        dropEthnicity.direction = .bottom
        dropEthnicity.bottomOffset = CGPoint(x: 0, y:(dropEthnicity.anchorView?.plainView.bounds.height)!)
        dropEthnicity.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getgender(){
        dropGender.dataSource = arryaGender
        dropGender.direction = .bottom
        dropGender.bottomOffset = CGPoint(x: 0, y:(dropGender.anchorView?.plainView.bounds.height)!)
        dropGender.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getIAm(){
        dropIam.dataSource = arryaIam
        dropIam.direction = .bottom
        dropIam.bottomOffset = CGPoint(x: 0, y:(dropIam.anchorView?.plainView.bounds.height)!)
        dropIam.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
}
extension EditProfileVC{
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        imageProfile.kf.setImage(with: URL(string:dataname.getStringValue(key: "patient_profile_image")), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                                        }
                                        basicList.allFields[1].strValue = dataname.getStringValue(key: "patient_first_name")
                                        basicList.allFields[2].strValue = dataname.getStringValue(key: "patient_last_name")
                                        basicList.allFields[3].strValue = dataname.getStringValue(key: "patient_dob")
                                        basicList.allFields[4].strValue = dataname.getStringValue(key: "patient_gender")
                                        
                                        
                                        basicList.allFields[5].strValue = dataname.getStringValue(key: "patient_addressline1")
                                        basicList.allFields[6].strValue = dataname.getStringValue(key: "patient_area")
                                        basicList.allFields[7].strValue = dataname.getStringValue(key: "patient_city")
                                        basicList.allFields[8].strValue = dataname.getStringValue(key: "patient_district")
                                        basicList.allFields[9].strValue = dataname.getStringValue(key: "patient_state")
                                        basicList.allFields[10].strValue = dataname.getStringValue(key: "patient_country")
                                        basicList.allFields[11].strValue = dataname.getStringValue(key: "patient_postcode")
                                        
                                        basicList.allFields[12].strValue = dataname.getStringValue(key: "patient_practice_name")
                                        basicList.allFields[13].strValue = dataname.getStringValue(key: "patient_practice_email")
                                        basicList.allFields[14].strValue = dataname.getStringValue(key: "patient_practice_addressline1")
                                        basicList.allFields[15].strValue = dataname.getStringValue(key: "patient_practice_addressline2")
                                        basicList.allFields[16].strValue = dataname.getStringValue(key: "patient_practice_city")
                                        basicList.allFields[17].strValue = dataname.getStringValue(key: "patient_practice_postcode")
                                        basicList.allFields[18].strValue = dataname.getStringValue(key: "patient_practice_telephoneno")
                                        basicList.allFields[19].strValue = dataname.getStringValue(key: "patient_ethnicity")
                                        basicList.allFields[20].strValue = dataname.getStringValue(key: "vPatientId")

                                        
                                        contactList.allFields[1].strValue = dataname.getStringValue(key: "patient_email_address")
                                        contactList.allFields[2].strValue = dataname.getStringValue(key: "patient_phone")
                                        contactList.allFields[2].strcode = dataname.getStringValue(key: "patient_countrycode")
                                        contactList.allFields[3].strValue = "Manage Address"
                                        
                                        
                                        organizationList.allFields[1].strValue = dataname.getStringValue(key: "vOrganizationName")
                                        if fieldData.allFields[1].strValue == ""{
                                            nOrganizationId = "0"
                                        }else{
                                            nOrganizationId = "1"
                                        }
                                        
                                        fieldData.allFields[1].strValue = dataname.getStringValue(key: "patient_height")
                                        fieldData.allFields[2].strValue = dataname.getStringValue(key: "patient_weight")
                                        fieldData.allFields[3].strValue = dataname.getStringValue(key: "patient_blood")
                                        fieldData.allFields[4].strValue = dataname.getStringValue(key: "patient_occupation")
                                        
                                        fieldData.allFields[5].strValue = dataname.getStringValue(key: "patient_NIN")
                                        fieldData.allFields[6].strValue = dataname.getStringValue(key: "patient_BCN")
                                        if dataname.getStringValue(key: "patient_type") == "1"{
                                            fieldData.allFields[7].strValue = arryaIam[0]
                                        }else if dataname.getStringValue(key: "patient_type") == "2"{
                                            fieldData.allFields[7].strValue = arryaIam[1]
                                        }else if dataname.getStringValue(key: "patient_type") == "3"{
                                            fieldData.allFields[7].strValue = arryaIam[2]
                                        }else if dataname.getStringValue(key: "patient_type") == "4"{
                                            fieldData.allFields[7].strValue = arryaIam[3]
                                        }
                                        
                                        let Lang = _userDefault.value(forKey: "Language") as? String
                                        if Lang == nil || Lang == "ZW"{
                                            fieldData.allFields[8].strValue = city[0]
                                        }else{
                                            fieldData.allFields[8].strValue = city[1]
                                        }
                                        
                                        isEmailVerdilation = dataname.getStringValue(key: "isEmailVerified")
                                        fieldData.allFields[9].strkey = dataname.getStringValue(key: "IsAdvantageHealthTaken")
                                        fieldData.allFields[10].strkey = dataname.getStringValue(key: "isSMSconsent")
                                        if dataname.getIntValue(key: "isVerified") == 2{
                                            viewVerification.isHidden = true
                                            viewTop.frame.size.height = 203 * _widthRatio
                                            
                                        }else{
                                            viewVerification.isHidden = false
                                            viewTop.frame.size.height = 299 * _widthRatio
                                            
                                        }
                                        
                                        tableView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getupdate(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getuserprofile()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getupdateMobile(key : String,value: String){
        var newMobile = value
        var strLanguage = ""
        if let obj = _userDefault.value(forKey: "Language") as? String{
            strLanguage = obj
        }
        //        if newMobile.prefix(1) == "0"{
        //            let result1 = String(newMobile.dropFirst())    // "ello"
        //            newMobile = result1
        //        }
        
        
        let Mobile =  contactList.allFields[2].strcode + "," + newMobile
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(Mobile)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        
                                        let otp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                                        otp.isUpdateNumber = true
                                        otp.strOTP = dataname.getStringValue(key: "otp_code")
                                        
                                        
                                        
                                        otp.vMobileNumber = newMobile
                                        otp.vMobileConctroy = contactList.allFields[2].strcode
                                        self.navigationController?.pushViewController(otp, animated: true)
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getupdateAddres(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\",\"value\":\"\(selectAddres.country!)\",\"key\":\"country\",\"value\":\"\(selectAddres.state!)\",\"key\":\"state\",\"value\":\"\(selectAddres.city!)\",\"key\":\"city\",\"value\":\"\(selectAddres.postcode!)\",\"key\":\"postcode\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getuserprofile()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func resendverificationlink(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "resendverificationlink.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getuserprofile()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func deleteaccount(){
        
        let parameters = "{\"data\":{\"patientid\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "deleteaccount.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                
                                if Response.getIntValue(key: "success") == 1{
                                    let loginViewController = UIStoryboard.init(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                                    _appDelegator.window?.rootViewController = loginViewController
                                    _userDefault.set(nil, forKey: "userInfo")
                                    _userDefault.synchronize()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        imageProfile.image = image
        imgUser = image
        let imageResized = image.resizeWith(percentage: 1)
        let base64 = imageResized?.toBase64()
        getupdate(key : "image",value: "\(base64!)")
        
        dismiss(animated:true, completion: nil)
    }
    
    
    func openPickerCamera(){
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        let action2 = UIAlertAction(title: "Open Gallery", style: .default) { (action) in
            self.openLibrary()
        }
        let action3 = UIAlertAction(title: "Close", style: .cancel) { (action) in
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}
