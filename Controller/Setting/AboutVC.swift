//
//  PrivayVC.swift
//  PickApp
//
//  Created by codExalters1 on 01/01/20.
//  Copyright © 2020 codExalters. All rights reserved.
//

import UIKit
import WebKit
class AboutVC: ParentViewController, WKNavigationDelegate, UIWebViewDelegate{
    
    @IBOutlet weak var lblHeader: LabelMedium!
    @IBOutlet weak var webView: WKWebView!
    
  //  @IBOutlet weak var web: UIWebView!
    var strHeader = "About Us"
    @IBOutlet weak var textdescrpition: JPWidthTextView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeader.text = strHeader
        if strHeader == "About Us"{
            getAbout()
        }else if strHeader == "Terms & Conditions"{
            getTerms()
        }else if strHeader == "Privacy Policy"{
            getPrivacy()
        }else if strHeader == "Disclaimer"{
            getDisclaimer()
        }
    

    }
    
    
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        if navigationType == UIWebView.NavigationType.linkClicked {
            
                UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
    

    func getAbout(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"pageId\":\"76\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "page.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSDictionary{
                                let headerString = "<head><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></head>"

                                self.webView.loadHTMLString(headerString + data.getStringValue(key: "page_desc"), baseURL: nil)
                                

                            }
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    
    func getTerms(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"pageId\":\"79\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "page.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSDictionary{
                                self.webView.loadHTMLStringWithMagic(content:data.getStringValue(key: "page_desc"),baseURL:nil)

                            }
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func getPrivacy(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"pageId\":\"85\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "page.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSDictionary{
                                self.webView.loadHTMLStringWithMagic(content:data.getStringValue(key: "page_desc"),baseURL:nil)

                            }
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func getDisclaimer(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"pageId\":\"87\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "page.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSDictionary{
                              //  self.webView.loadHTMLString(data.getStringValue(key: "page_desc"), baseURL: nil)
                                self.webView.loadHTMLStringWithMagic(content:data.getStringValue(key: "page_desc"),baseURL:nil)
                            }
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    
   
}
extension WKWebView {

    /// load HTML String same font like the UIWebview
    ///
    //// - Parameters:
    ///   - content: HTML content which we need to load in the webview.
    ///   - baseURL: Content base url. It is optional.
    func loadHTMLStringWithMagic(content:String,baseURL:URL?){
        let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
        loadHTMLString(headerString + content, baseURL: baseURL)
    }
}
