//
//  SwitchAccountVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/09/21.
//

import UIKit
class SwitchAccountCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var btnDeleted: UIButton!
    @IBOutlet weak var lblName: LabelRegular!
    @IBOutlet weak var imageProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if let _ = imageProfile{
            imageProfile.makeCornerRoun(redias: 8)
            btnDeleted.makeRound()

        }
    }
}


class SwitchAccountVC: ParentViewController {

    
    var ArrayParentList = [ParentList]()
    @IBOutlet weak var btnBackNavigation: UIButton!
    
    var isSetting  = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action:
             #selector(handleRefresh(_:)),
                                  for: UIControl.Event.valueChanged)

        myColView.addSubview(refreshControl)

        if isSetting{
            btnBackNavigation.isHidden = false
        }else{
            btnBackNavigation.isHidden = true
            getFirebaseAnalytics(screenName : getEventName.SELECT_PROFILE,isPatient_id : true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getChildProfiles(loader: true)
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getChildProfiles(loader: false)
    }
    
    
    @IBAction func btnContuneClicked(_ sender: Any) {
        for obj in ArrayParentList {
            if obj.isSected == true{
                if obj.nDocStatus == 1{
                    getSwitchedAccountSession(nId : obj.id, nUserType: obj.nUserType)
                }else if obj.nDocStatus == 0{
                    showFailMessage(message : "Profile is under approval!")

                }else if obj.nDocStatus == 2{
                    showFailMessage(message : "Profile is rejected!")
                }else{
                    showFailMessage(message : "Profile is under approval!")
                }
                
            }
        }
        
    }
    
    
    
    @IBAction func btnDeletedClicked(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            deleteChildProfile(parent : ArrayParentList[sender.tag].id)
        }
        
    }
    
}
extension SwitchAccountVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == ArrayParentList.count{
            
            let vc = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "NewProfileVC") as! NewProfileVC
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }else{
            for obj in ArrayParentList {
                obj.isSected = false
            }
            ArrayParentList[indexPath.row].isSected = true
            
            myColView.reloadData()
        }
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
       
        return ArrayParentList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Switch", for: indexPath) as! SwitchAccountCell
        if indexPath.row == ArrayParentList.count{
            cell.lblName.text = "Add New"
            cell.imageProfile.kf.setImage(with: URL(string:""), placeholder: #imageLiteral(resourceName: "ic_add_img_withbg"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
            cell.btnDeleted.tag = indexPath.row
            cell.btnDeleted.isHidden = true
            cell.imageProfile.makeCornerRoundBorderwithoutShadow(redias: 8, Color: UIColor.clear)
            return cell
        }else{
            cell.btnDeleted.tag = indexPath.row
            cell.lblName.text = ArrayParentList[indexPath.row].patient_first_name + " " + ArrayParentList[indexPath.row].patient_last_name
            cell.imageProfile.kf.setImage(with: URL(string:ArrayParentList[indexPath.row].patient_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
            let patient =  _userDefault.value(forKey: "OldPatientId")
            if ArrayParentList[indexPath.row].isSected{
                cell.imageProfile.makeCornerRoundBorderwithoutShadow1(redias: 8, Color: UIColor.colorchaupOrange())
            }else{
                cell.imageProfile.makeCornerRoundBorderwithoutShadow(redias: 8, Color: UIColor.clear)
            }
            
            if patient as! String != _currentUser.patient_id{
                cell.btnDeleted.isHidden = true
            }else{
                cell.btnDeleted.isHidden = false

            }
            if indexPath.row == 0{
                cell.btnDeleted.isHidden = true
            }
            
        }
       
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: _screenSize.width / 3, height: 164 * _widthRatio)

    }
    
    
}
extension SwitchAccountVC{
    
    
    func getChildProfiles(loader:Bool){
       let patient =  _userDefault.value(forKey: "OldPatientId")
        let parameters = "{\"data\":{\"nParentId\":\"\(patient!)\"}}"
        if loader{
            showCentralSpinner()
        }
        KPWebCall.call.postRequestApiClinet(relPath: "getChildProfiles.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    ArrayParentList.removeAll()
                                    if let dataname = Response["data"] as? NSDictionary{
                                        if let parnet = dataname["parent"] as? NSDictionary{
                                            ArrayParentList.append(ParentList(dic: parnet))
                                        }
                                        if let parnet = dataname["children"] as? NSArray{
                                            for obj in parnet {
                                                
                                                ArrayParentList.append(ParentList(dic: obj as! NSDictionary))
                                            }
                                        }
                                        if let _ = _currentUser{
                                            for obj in ArrayParentList {
                                                if _currentUser.patient_id == "\(obj.id!)"{
                                                    obj.isSected = true
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    self.showResponseMessage(dict: Response)
                                }
                                myColView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func getSwitchedAccountSession(nId : Int, nUserType : Int){
        
        
        let uuid = UUID().uuidString
        var strPush = ""
            if let push = _userDefault.value(forKey: "vpushToken") as? String{
                  strPush = push
            }
        let parameters = "{\"data\":{\"nChildId\":\"\(nId)\",\"fcm_token\":\"\(strPush)\", \"macAddress\":\"\(uuid)\",\"nDeviceLoginType\":\"2\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getSwitchedAccountSession.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                           
                            if let dataname = Response["data"] as? NSDictionary{
                                let pinnew = _currentUser.nPin
                              
                                _currentUser = UserDetail(dic: dataname)
                                KPWebCall.call.setClientApiKey(token: _currentUser.vApiToken)
                                _userDefault.set(nUserType, forKey: "nUserType")
                                _currentUser.patient_id = "\(nId)"
                                _currentUser.nPin = pinnew
                                

                                print (self.getSaveData(dataname : dataname))
                                _userDefault.set(self.getSaveData(dataname : dataname), forKey: "userInfo")
                                _userDefault.synchronize()
                                
                                
                                if isSetting{
                                    let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                                    self.navigationController?.pushViewController(vclogin, animated: true)
                                }else{
                                    if  !_userDefault.bool(forKey: "Guidelines"){
                                        _userDefault.set(true, forKey: "Guidelines")
                                        _userDefault.synchronize()
                                        let vclogin = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GuidelinesVC") as! GuidelinesVC
                                        self.navigationController?.pushViewController(vclogin, animated: true)
                                        
                                    }else{
                                        let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                                        self.navigationController?.pushViewController(vclogin, animated: true)
                                        
                                    }
                                }
                                
                                
                                
                                
                               
                            }
                        
                           
                                
                        }else{
                            self.showResponseMessage(dict: Response)
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func deleteChildProfile(parent : Int){
        let parameters = "{\"data\":{\"nChildId\":\"\(parent)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "deleteChildProfile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                           
                            getChildProfiles(loader: true)
                                
                        }
                        self.showResponseMessage(dict: Response)
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
