//
//  SettingVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit
import Firebase
import FirebaseDynamicLinks
class SettingCell: ConstrainedTableViewCell {
    
    
    @IBOutlet weak var lblNAme: LabelMedium!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias: 5)

    }
    
}


class SettingVC: ParentViewController {
    @IBOutlet weak var lblVersion: LabelMedium!

    var arryaMenuIcon = [#imageLiteral(resourceName: "ic_profile"),
                         #imageLiteral(resourceName: "ic_switch_profile"),
                         #imageLiteral(resourceName: "ic_creditusage"),
                         #imageLiteral(resourceName: "ic_notification"),
                         #imageLiteral(resourceName: "ic_walletusage"),
                         #imageLiteral(resourceName: "ic_transcation"),
                         #imageLiteral(resourceName: "ic_changepass"),
                         #imageLiteral(resourceName: "ic_changeapplockpin"),
                         #imageLiteral(resourceName: "ic_aboutus"),
                         #imageLiteral(resourceName: "ic_support"),
                         #imageLiteral(resourceName: "ic_termsconditions"),
                         #imageLiteral(resourceName: "ic_privacypolicy"),
                         #imageLiteral(resourceName: "ic_disclaimer"),
                         #imageLiteral(resourceName: "ic_share"),
                         #imageLiteral(resourceName: "ic_rate"),
                         #imageLiteral(resourceName: "ic_help")]
    
    var arryaMenuName = ["My Profile","Switch Profile", "Credit Usage", "Notifications","Wallet Usage", "Transactions", "Change Password", "Change App Lock Pin","About Us","Support","Terms & Conditions","Privacy Policy","Disclaimer","Share","Rate CheckUp Health","Help","Logout"]

    override func viewDidLoad() {
        super.viewDidLoad()
        if let info = Bundle.main.infoDictionary{
            let currentVersion = info["CFBundleShortVersionString"] as? String
            lblVersion.text = "Version " +  currentVersion!
        }else{
            lblVersion.text = ""
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getuserprofile()
    }
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "About"{
            let vc = segue.destination as! AboutVC
            vc.strHeader = sender as! String
        }
    }
}
extension SettingVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.performSegue(withIdentifier: "Profile", sender: nil)
        }else if indexPath.row == 1{
            self.performSegue(withIdentifier: "SwitchAccount", sender: nil)
        }else if indexPath.row == 2{
            self.performSegue(withIdentifier: "Created", sender: nil)
        }else if indexPath.row == 3{
            self.performSegue(withIdentifier: "Notifiation", sender: nil)
        }else if indexPath.row == 4{
            self.performSegue(withIdentifier: "Wallet", sender: nil)
        }else if indexPath.row == 5{
            self.performSegue(withIdentifier: "Tranction", sender: nil)
        }else if indexPath.row == 6{
            self.performSegue(withIdentifier: "Change", sender: nil)
        }else if indexPath.row == 7{
            self.performSegue(withIdentifier: "Pin", sender: nil)
        }else if indexPath.row == 8{
            self.performSegue(withIdentifier: "About", sender: "About Us")
        }else if indexPath.row == 9{
            self.performSegue(withIdentifier: "Suport", sender: nil)
        }else if indexPath.row == 10{
            self.performSegue(withIdentifier: "About", sender: "Terms & Conditions")
        }else if indexPath.row == 11{
            self.performSegue(withIdentifier: "About", sender: "Privacy Policy")
        }else if indexPath.row == 12{
            self.performSegue(withIdentifier: "About", sender: "Disclaimer")
        }
        
        else if indexPath.row == 13{ // share
            
            let strReferral = _userDefault.value(forKey: "patient_referralcode") as! String
            
            guard let link = URL(string: "https://dl.checkuphealth.co.uk/referral=" + strReferral) else { return }
            
            let dynamicLinksDomainURIPrefix = "https://dl.checkuphealth.co.uk"
            
            let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
            
            
            
            linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.checkup.patient")
            linkBuilder?.iOSParameters?.appStoreID = "1568467711"
            linkBuilder?.iOSParameters?.minimumAppVersion? = "2.0"

            
            linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.checkup.patient")
            linkBuilder?.androidParameters?.minimumVersion = 4

            
            guard let longDynamicLink = linkBuilder?.url else { return }
          
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
             // guard let url = url, error != nil else { return }
              print("The short URL is: \(url!)")
                
                let items = ["Download CheckUp Health app now \n\(url!)"]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                self.present(ac, animated: true)
            }
            
          
            
        }
        
        else if indexPath.row == 14{ // Rate us
            guard let url = URL(string: "https://apps.apple.com/us/app/checkup-health-at-home/id1568467711") else {
                 return
            }
            if UIApplication.shared.canOpenURL(url) {
                 UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else if indexPath.row == 15{
            self.performSegue(withIdentifier: "Help", sender: nil)
        }else if indexPath.row == 16{
            
            showAlert(title: "Logout" , msgString: "Are you sure to Logout?" , Ohk: "OK" , cancal: "Cancel" ) { [self] in
                getLogout()
               
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            if _currentUser.vOrganizationName == ""{
                return 0
            }
        }
        if indexPath.row == 6 || indexPath.row == 7{
            if  _userDefault.value(forKey: "nUserType") as! Int == 1 ||  _userDefault.value(forKey: "nUserType") as! Int == 2{
                return 0
            }
        }
        return 60 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaMenuName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 16{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Logout") as! SettingCell
            cell.lblNAme.text = arryaMenuName[indexPath.row]
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! SettingCell
        cell.lblNAme.text = arryaMenuName[indexPath.row]
        cell.imageIcon.image = arryaMenuIcon[indexPath.row]

        return cell
    }
    
    
    
    
}
extension SettingVC{
    func getLogout(){
        var strPush = ""
              if let push = _userDefault.value(forKey: "vpushToken") as? String{
                  strPush = push
              }
        let parameters = "{\"data\":{\"fcm_token\":\"\(strPush)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "logout.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                               
                                if Response.getIntValue(key: "success") == 1{
                                    let loginViewController = UIStoryboard.init(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                                    _appDelegator.window?.rootViewController = loginViewController
                                    _userDefault.set(nil, forKey: "userInfo")
                                    _userDefault.synchronize()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        
                                        _currentUser.vOrganizationName = dataname.getStringValue(key: "vOrganizationName")
                                        
                                        tableView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
