//
//  ChangePasswordVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit
struct changePassword {
    var image : UIImage?
    var image1 : UIImage?
    var title : String = ""
    var placeholder : String = ""
    var strValue : String = ""
    var isSecure = false
    var strkey: String = ""
    var keybord = UIKeyboardType.default
}
//Second Class Create
class Change: NSObject {
    var vChange: [changePassword] = []
    override init() {
        super.init()
        prepareDate()
        
        
    }
    //Function arry ti appand
    func prepareDate(){
        var t1 = changePassword()
        t1.placeholder = "Old Password"
        t1.title = "Old Password"
        t1.isSecure = true
        t1.image = #imageLiteral(resourceName: "ic_eye_close")
        t1.image1 = #imageLiteral(resourceName: "ic_eye_open")
        t1.strkey = "1"
        t1.keybord = .numbersAndPunctuation
        
        var t2 = changePassword()
        t2.placeholder = "New Password"
        t2.title = "New Password"
        t2.image = #imageLiteral(resourceName: "ic_eye_close")
        t2.image1 = #imageLiteral(resourceName: "ic_eye_open")
        t2.strkey = "1"
        t2.isSecure = true
        t2.keybord = .numbersAndPunctuation
        
        var t3 = changePassword()
        t3.placeholder = "Confirm Password"
        t3.title = "Confirm Password"
        t3.image = #imageLiteral(resourceName: "ic_eye_close")
        t3.image1 = #imageLiteral(resourceName: "ic_eye_open")
        t3.strkey = "1"
        t3.isSecure = true
        t3.keybord = .numbersAndPunctuation
        
        vChange.append(t1)
        vChange.append(t2)
        vChange.append(t3)
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: vChange[0].strValue){
            return(false,"Please Enter Old Password")
        }else  if String.validateStringValue(str: vChange[1].strValue){
            return(false,"Please Enter New Password")
        }else  if String.validateStringValue(str: vChange[2].strValue){
            return(false,"Please Confirm Password")
        }else if vChange[1].strValue != vChange[2].strValue{
            return(false,"Your New Password doesn't match")
        }
        return(true,"")
        
    }
}
class ChangePasswordVC: ParentViewController {
    var vchage = Change()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func btnShowPasswordClicked(_ sender: UIButton) {
            if sender.isSelected{
                vchage.vChange[sender.tag].strkey = "1"
            }else{
                vchage.vChange[sender.tag].strkey = "0"
            }
            tableView.reloadData()
    }

    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        vchage.vChange[sender.tag].strValue = sender.text!
    }
    @IBAction func btnChangePasswordClicked(_ sender: Any) {
        if vchage.validation().0{
            addchangepassword()
        }else{
            showFailMessage(message: vchage.validation().1)
        }
    }

}
extension ChangePasswordVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }
        if indexPath.row == 2{
            return 90 * _widthRatio
        }
        return 75 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vchage.vChange.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Password") as! ProfileCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = vchage.vChange[indexPath.row].keybord
        cell.textEmail.text = vchage.vChange[indexPath.row].strValue
        cell.lblName.text = vchage.vChange[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = vchage.vChange[indexPath.row].isSecure
        cell.btnCheck.tag = indexPath.row
        if indexPath.row == 0{
            cell.viewConstretTop.constant = 5
            cell.viewConstretBottom.constant = -5
        }else if indexPath.row == 2{
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = 5
        }else{
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
        }
        if vchage.vChange[indexPath.row].strkey == "1"{
            cell.textEmail.isSecureTextEntry = true
            cell.btnCheck.setImage(vchage.vChange[indexPath.row].image1, for: .normal)
        }else{
            cell.textEmail.isSecureTextEntry = false
            cell.btnCheck.isSelected = true
            cell.btnCheck.setImage(vchage.vChange[indexPath.row].image, for: .normal)
        }
            return cell
                
    }
    
    
    
    
}
extension ChangePasswordVC{
    func addchangepassword(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"old_password\":\"\(vchage.vChange[0].strValue)\",\"new_password\":\"\(vchage.vChange[1].strValue)\",\"retype_password\":\"\(vchage.vChange[2].strValue)\"}}"
            showCentralSpinner()
            KPWebCall.call.postRequestApiClinet(relPath: "changepassword.php", param: parameters) { (response) in
                self.hideCentralSpinner()
                if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                        if let Response = response as? NSDictionary{
                            if Response.getIntValue(key: "success") == 1{
                                self.navigationController?.popViewController(animated: true)
                            }
                            self.showResponseMessage(dict: Response)
                         }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                }
            }
    }
}
