//
//  NotifationVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit

class NotifationVC: ParentViewController {
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewSMS: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var btnswitchEdit: UISwitch!
    @IBOutlet weak var btnswitchSMS: UISwitch!
    @IBOutlet weak var btnswithEmail: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBG.makeCornerRound(redias: 5)
        viewSMS.makeCornerRound(redias: 5)
        viewEmail.makeCornerRound(redias: 5)
        getuserprofile()
    }
    
    @IBAction func btnSwitchUpdateCliceked(_ sender: UISwitch) {
        if sender.isOn {
            getupdate(key : "isSendNotification",value: "1")
        }else{
            getupdate(key : "isSendNotification",value: "0")
        }
    }
    
    
    @IBAction func btnSwitchUpdateEmail(_ sender: UISwitch) {
        if sender.isOn {
            getupdate(key : "isSendEmail",value: "1")
        }else{
            getupdate(key : "isSendEmail",value: "0")
        }
    }
    
    
    @IBAction func btnSwitchSMS(_ sender: UISwitch) {
        if sender.isOn {
            getupdate(key : "isSendSMS",value: "1")
        }else{
            getupdate(key : "isSendSMS",value: "0")
        }
    }
    
    
    
    func getupdate(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getuserprofile()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        if dataname.getStringValue(key: "isSendNotification") == "0"{
                                            btnswitchEdit.isOn = false
                                        }else{
                                            btnswitchEdit.isOn = true
                                            
                                        }
                                        if dataname.getStringValue(key: "isSendEmail") == "0"{
                                            btnswithEmail.isOn = false
                                        }else{
                                            btnswithEmail.isOn = true
                                            
                                        }
                                        if dataname.getStringValue(key: "isSendSMS") == "0"{
                                            btnswitchSMS.isOn = false
                                        }else{
                                            btnswitchSMS.isOn = true
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}


