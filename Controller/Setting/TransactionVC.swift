//
//  TransactionVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit

class TransactionVC: ParentViewController {
    
    var arryaTransaction = [TransactionHistory]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getusertransactionhistory()
       
    }
    

   
}
extension TransactionVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Vc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDetailsVC")  as! AppointmentDetailsVC
        navigationController?.pushViewController(Vc1, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaTransaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
        cell.imageDr.kf.setImage(with: URL(string:arryaTransaction[indexPath.row].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        cell.viewRs.makeCornerRoundClear(redias: 5)
        cell.lblDate.text = arryaTransaction[indexPath.row].dCreatedDate
        cell.lblRs.text = "£" +  arryaTransaction[indexPath.row].dAmount
        cell.lblDrName.text = arryaTransaction[indexPath.row].doctor_first_name + " " + arryaTransaction[indexPath.row].doctor_last_name
            return cell
        
        
    }
    
    
    
    
}
extension TransactionVC{
    func getusertransactionhistory(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "getusertransactionhistory.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        self.arryaTransaction.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    self.arryaTransaction.append(TransactionHistory(dic: obj as! NSDictionary))
                                }
                            }
                            self.tableView.reloadData()
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
