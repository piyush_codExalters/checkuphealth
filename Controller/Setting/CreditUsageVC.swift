//
//  CreditUsageVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit
class CreditUsageCell: ConstrainedTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}


class CreditUsageVC: ParentViewController {
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblGB: LabelSemiBold!
    @IBOutlet weak var lblCurenry: LabelMedium!

    var craditTransactionHistory :  CraditTransactionHistory!
    var appoinmentID = ""
    var isFor = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTop.frame.size.height = 165 * _widthRatio
        viewBG.makeCornerRoundClear(redias: 5)
        getuserwallethistory()
    }
   


   

}
extension CreditUsageVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDetailsVC")  as! AppointmentDetailsVC
        mapVc1.appoinmentID = craditTransactionHistory.arryaWalletHistory[indexPath.row].appointment_id
        mapVc1.isFor = craditTransactionHistory.arryaWalletHistory[indexPath.row].nAppointmentFor
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = craditTransactionHistory{
            return craditTransactionHistory.arryaWalletHistory.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
        cell.imageDr.kf.setImage(with: URL(string:craditTransactionHistory.arryaWalletHistory[indexPath.row].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        cell.viewRs.makeCornerRoundClear(redias: 5)
        
        cell.lblDate.text = craditTransactionHistory.arryaWalletHistory[indexPath.row].dCreatedDate
        cell.lblRs.text = craditTransactionHistory.arryaWalletHistory[indexPath.row].currency_vSymbol +  craditTransactionHistory.arryaWalletHistory[indexPath.row].dAmount
        cell.lblDrName.text = craditTransactionHistory.arryaWalletHistory[indexPath.row].doctor_first_name + " " + craditTransactionHistory.arryaWalletHistory[indexPath.row].doctor_last_name

            return cell
        
        
    }
    
    
    
    
}
extension CreditUsageVC{
    func getuserwallethistory(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "getuserwallethistory.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.craditTransactionHistory = nil
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSDictionary{
                                craditTransactionHistory = CraditTransactionHistory(dic: data)
                                if craditTransactionHistory.Credit == ""{
                                    craditTransactionHistory.Credit = "0.0"
                                }
                                if isWorkingCountryUK(){
                                    lblCurenry.text = "GBP"
                                }else{
                                    lblCurenry.text = "USD"
                                }
                                self.lblGB.text = craditTransactionHistory.Credit
                            }
                            self.tableView.reloadData()
                        }else{
                            
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
