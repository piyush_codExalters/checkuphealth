//
//  NewProfileVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/09/21.
//

import UIKit
import DropDown
import CropViewController

protocol addressDelegeteNew {
    func getAddress(str: String,address: AddressListName, index: Int)
}
class ChildProfileList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t1 = SingIn()
        t1.placeholder = "First Name"
        t1.keybord = .default
        
        var t2 = SingIn()
        t2.placeholder = "Last Name"
        t2.keybord = .default
        
        
        var t4 = SingIn()
        t4.placeholder = "Ethnicity"
        t4.keybord = .default
        t4.image = #imageLiteral(resourceName: "ic_dropdown")
        
    
        
        var t3 = SingIn()
        t3.placeholder = "Date of Birth"
        t3.keybord = .default
        t3.image = #imageLiteral(resourceName: "ic_calender")
        
        
        var t6 = SingIn()
        t6.placeholder = "Sex"
        t6.keybord = .default
        t6.image = #imageLiteral(resourceName: "ic_dropdown")
        
        var t10 = SingIn()
        t10.placeholder = "GP Practice Name"
        t10.keybord = .default
        
        var t122 = SingIn()
        t122.placeholder = "GP Practice Email"
        t122.keybord = .emailAddress

        
        var t11 = SingIn()
        t11.placeholder = "GP Practice Address"
        t11.keybord = .default
        t11.placeholder1 = "Address Line 1"
        
        var t12 = SingIn()
        t12.placeholder1 = "Address Line 2"
        t12.keybord = .default
        
        var t13 = SingIn()
        t13.placeholder1 = "Town/City"
        t13.keybord = .default
        
        var t14 = SingIn()
        t14.placeholder1 = "Postcode"
        t14.keybord = .default
        
        var t15 = SingIn()
        t15.placeholder1 = "Telephone no"
        t15.keybord = .numberPad
        
      
      
        
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t4)
        allFields.append(t3)
        allFields.append(t6)
        
        allFields.append(t10)
        allFields.append(t122)

        allFields.append(t11)
        allFields.append(t12)
        allFields.append(t13)
        allFields.append(t14)
        allFields.append(t15)
        
       
      
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please Enter First Name")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please Enter Last Name")
        }else if String.validateStringValue(str: allFields[4].strValue){
            return(false,"Please select Birth date")
        }
        return(true,"")
    }
}
class NewLinkList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t4 = SingIn()
        t4.placeholder = "Email or Mobile number"
        t4.isSecure = false
        t4.keybord = .emailAddress
        t4.strValue = ""
        
        var t5 = SingIn()
        t5.placeholder = "Password"
        t5.isSecure = true
        t5.strValue = ""
        
        
        allFields.append(t4)
        allFields.append(t5)
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Enter Valid Email id or Mobile Number")
        }else  if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Password can't be blank")
        }
        return(true,"")
    }
}
class NewProfileVC: ParentViewController, addressDelegeteNew {
   
    @IBOutlet weak var viewCertificate: UIView!
    @IBOutlet weak var imageCertificate: UIImageView!
    @IBOutlet weak var viewPassport: UIView!
    @IBOutlet weak var imagePassport: UIImageView!

    @IBOutlet weak var btnCertificate: UIButton!
    @IBOutlet weak var btnPassport: UIButton!
    
    @IBOutlet weak var btnContiue: UIButton!
    @IBOutlet weak var segmentItem: UISegmentedControl!
    @IBOutlet weak var tableLink: UITableView!
    @IBOutlet weak var tableChild: UITableView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    
    var indexrNumber = 0
    let dropEthnicity = DropDown()
    let dropGender = DropDown()
    var selectAddres : AddressListName!
    var isImage = 0
    var fieldData = NewLinkList()
    var fieldData1 = ChildProfileList()
    var imagePicker = UIImagePickerController()
    var imgUser : UIImage?
    
    var imgUserCertificate : UIImage?

    var imgUserPassport : UIImage?

    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var arryaEthnicity = ["White", "Mixed/ multiple ethnic","Asian/ Asian British","Black/ African/ Caribbean/ African British","Other ethnic groups"]
    var arryaGender = ["Male", "Female","Not listed","Prefer not to say"]
    var indexa = -1
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if indexrNumber == 0{
            tableChild.isHidden = false
            tableLink.isHidden = true
            
        }else{
            tableChild.isHidden = true
            tableLink.isHidden = false
        }
        segmentItem.setupSegment()
        btnContiue.makeBorderRound(redias: 8)
        
        imageCertificate.makeCornerRound(redias : 6)
        imagePassport.makeCornerRound(redias : 6)
        viewPassport.makeCornerRound(redias : 6)
        viewCertificate.makeCornerRound(redias : 6)

        btnCertificate.makeRound()
        btnPassport.makeRound()

        btnCertificate.isHidden = true
        btnPassport.isHidden = true
        
        fieldData1.allFields[4].strValue = arryaGender[0]
        fieldData1.allFields[2].strValue = arryaEthnicity[0]
        viewProfile.makeCornerRound(redias : 6)
        viewTop.frame.size.height = 203 * _widthRatio
        viewBottom.frame.size.height = 368 * _widthRatio
        datefrmenter.dateFormat = "dd-MM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        imageProfile.makeCornerRound(redias : 6)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageCertificate.isUserInteractionEnabled = true
        imageCertificate.addGestureRecognizer(tapGestureRecognizer)
        
        
        let tapGestureRecognize1r = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        imagePassport.isUserInteractionEnabled = true
        imagePassport.addGestureRecognizer(tapGestureRecognize1r)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        isImage = 1
        openPickerCamera()
    }
    
    @objc func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer)
    {
        isImage = 2
        openPickerCamera()
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData1.allFields[3].strValue = datefrmenter.string(from: sender.date)
        let cell = tableChild.cellForRow(at: IndexPath(row: 3, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData1.allFields[3].strValue
    }
    
    @IBAction func btnCameraClicked(_ sender: Any) {
        isImage = 0
        openPickerCamera()
    }
    
    @IBAction func btnActionPassport(_ sender: Any) {
        imagePassport.image = #imageLiteral(resourceName: "ic_add_img_withbg")
        imgUserPassport = nil
        btnPassport.isHidden = true
    }
    
    
    @IBAction func btnActionCertifation(_ sender: Any) {
        imageCertificate.image = #imageLiteral(resourceName: "ic_add_img_withbg")
        imgUserCertificate = nil
        btnCertificate.isHidden = true
    }
    
    
    @IBAction func newEditText(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!

    }
    
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
       
        fieldData1.allFields[sender.tag].strValue = sender.text!
    }
    @IBAction func btnLoginClicekd(_ sender: Any) {
        if fieldData.validation().0{
            
            setProfileAsChild()
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
    }
    
    
    @IBAction func btnContunClicekd(_ sender: Any) {
        if fieldData1.validation().0{
            if imgUser == nil{
                showFailMessage(message : "Please choose profile image")
                return
            }
            if imgUserCertificate == nil{
                showFailMessage(message : "Please add Birth certificate!")
                return
            }
            if imgUserPassport == nil{
                showFailMessage(message : "Please add passport!")
                return
            }
            
            addChildProfile()
        }else{
            showFailMessage(message : fieldData1.validation().1)
        }
    }
    
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentItem.changeUnderlinePosition()
        switch segmentItem.selectedSegmentIndex {
        case 0:
            indexrNumber = 0
            tableChild.isHidden = false
            tableLink.isHidden = true
            
        default:
            indexrNumber = 1
            tableChild.isHidden = true
            tableLink.isHidden = false
        }
    }
    func getAddress(str: String,address: AddressListName, index: Int) {
        fieldData1.allFields[3].strValue = str
        selectAddres = address
        indexa = index
        tableChild.reloadData()
    }
    
}
extension NewProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableChild{
            if indexPath.row == 4{
                getgender()
                dropGender.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData1.allFields[4].strValue = arryaGender[index]
                    self.tableChild.reloadData()
                    
                }
                self.view.endEditing(true)
                self.dropGender.show()
            }
            if indexPath.row == 2{
                getdropethnicity()
                dropEthnicity.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData1.allFields[2].strValue = arryaEthnicity[index]
                    self.tableChild.reloadData()
                }
                self.view.endEditing(true)
                self.dropEthnicity.show()
            }
            
           
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableLink{
            return 60 * _widthRatio
        }
        if indexPath.row == 0{
            return 90 * _widthRatio
        }
        
        if !isWorkingCountryUK(){
            if indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 || indexPath.row == 11{
                return 0
            }
            if indexPath.row == 5{
                return 90 * _widthRatio
            }
        }
        
        if indexPath.row == 11{
            return 90 * _widthRatio
        }
        return 80 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableLink{
           return fieldData.allFields.count
        }
        return fieldData1.allFields.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if tableView == tableLink{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custText") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            return cell
          }
        
        
            if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
                cell.textEmail.tag = indexPath.row
                cell.textEmail.keyboardType = fieldData1.allFields[indexPath.row].keybord
                cell.textEmail.text = fieldData1.allFields[indexPath.row].strValue
                cell.lblName.text = fieldData1.allFields[indexPath.row].placeholder
                cell.textEmail.isSecureTextEntry = fieldData1.allFields[indexPath.row].isSecure
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                cell.imageCalander.image = fieldData1.allFields[indexPath.row].image
                if indexPath.row == 3{
                    if #available(iOS 13.4, *) {
                        datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                        datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                        datePickerView.preferredDatePickerStyle = .wheels
                        datePickerView.preferredDatePickerStyle = .wheels
                        cell.textEmail.inputView = datePickerView
                    }else{
                        cell.textEmail.inputView = datePickerView
                    }
                    cell.textEmail.isUserInteractionEnabled = true
                    cell.textEmail.placeholder = ""
                }else if indexPath.row == 4{
                    cell.textEmail.isUserInteractionEnabled = false
                    cell.textEmail.placeholder = ""
                    dropGender.anchorView = cell.textEmail
                }else if indexPath.row == 2{
                    cell.textEmail.isUserInteractionEnabled = false
                    cell.textEmail.placeholder = ""
                    dropEthnicity.anchorView = cell.textEmail
                }
                
                return cell
            }
            
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData1.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData1.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.textEmail.placeholder = fieldData1.allFields[indexPath.row].placeholder1
            cell.lblName.text = fieldData1.allFields[indexPath.row].placeholder
            cell.textEmail.isSecureTextEntry = fieldData1.allFields[indexPath.row].isSecure
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
                cell.viewConstretBottom.constant = -5
            }else if indexPath.row == 11{
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = 5
            }else{
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
            }
            if !isWorkingCountryUK(){
                if indexPath.row == 4{
                    cell.viewConstretTop.constant = -5
                    cell.viewConstretBottom.constant = 5

                }
            }
            return cell
            
        
        
    }
    
    func getdropethnicity(){
        dropEthnicity.dataSource = arryaEthnicity
        dropEthnicity.direction = .bottom
        dropEthnicity.bottomOffset = CGPoint(x: 0, y:(dropEthnicity.anchorView?.plainView.bounds.height)!)
        dropEthnicity.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getgender(){
        dropGender.dataSource = arryaGender
        dropGender.direction = .bottom
        dropGender.bottomOffset = CGPoint(x: 0, y:(dropGender.anchorView?.plainView.bounds.height)!)
        dropGender.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    
}
extension NewProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        if isImage == 0{
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.customAspectRatio = CGSize(width: 1, height: 1)
        }
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropViewController, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropViewController, animated: true)
        }
    }
    
    
    
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            // 'image' is the newly cropped version of the original image
        if isImage == 0{
            imageProfile.image = image
            imgUser = image
        }else if isImage == 1{
            imageCertificate.image = image
            imgUserCertificate = image
            btnCertificate.isHidden = false
        }else{
            imagePassport.image = image
            imgUserPassport = image
            btnPassport.isHidden = false

        }
        
        dismiss(animated:true, completion: nil)

    }
  
    
    
    
    func openPickerCamera(){
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        let action2 = UIAlertAction(title: "Open Gallery", style: .default) { (action) in
            self.openLibrary()
        }
        let action3 = UIAlertAction(title: "Close", style: .cancel) { (action) in
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.modalPresentationStyle = .fullScreen
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}
extension NewProfileVC{
    func setProfileAsChild(){
        let patient =  _userDefault.value(forKey: "OldPatientId")

        let parameters = "{\"data\":{\"nParentId\":\"\(patient!)\",\"email\":\"\(fieldData.allFields[0].strValue)\",\"password\":\"\(fieldData.allFields[1].strValue)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "setProfileAsChild.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                            getFirebaseAnalytics(screenName : getEventName.LINK_CHILD_PROFILE,isPatient_id : true)
                        _ = self.navigationController?.popViewController(animated: true)
                                
                        }
                        self.showResponseMessage(dict: Response)
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func addChildProfile(){
        
        var dic = [String:Any]()
        
        let patient =  _userDefault.value(forKey: "OldPatientId")

        dic["nParentId"] = patient
        dic["firstname"] = fieldData1.allFields[0].strValue
        dic["lastname"] = fieldData1.allFields[1].strValue
        dic ["ethnicity"] = fieldData1.allFields[2].strValue
        dic["dob"] = fieldData1.allFields[3].strValue
        dic["gender"] = fieldData1.allFields[4].strValue
        dic["vCountryCode"] = _currentUser.patient_countrycode
        dic["practice_name"] = fieldData1.allFields[5].strValue
        dic["practice_email"] = fieldData1.allFields[6].strValue
        dic["practice_addressline1"] = fieldData1.allFields[7].strValue
        dic["practice_addressline2"] = fieldData1.allFields[8].strValue
        dic["practice_city"] = fieldData1.allFields[9].strValue
        dic["practice_postcode"] = fieldData1.allFields[10].strValue
        dic["practice_telephoneno"] = fieldData1.allFields[11].strValue
        if let _ = imgUser{
            let imageResized = imgUser!.resizeWith(percentage: 1)
            let base64 = imageResized?.toBase64()
            dic["patient_profile_image"] = base64
        }
        if let _ = imgUserPassport{
                    let imageResized = imgUserPassport!.resizeWith(percentage: 1)
                    let base64 = imageResized?.toBase64()
                    dic["vBirthCertificatePath"] = base64
                }
                if let _ = imgUserCertificate{
                    let imageResized = imgUserCertificate!.resizeWith(percentage: 1)
                    let base64 = imageResized?.toBase64()
                    dic["vGuardianCertificatePath"] = base64
                }
      //  dic["location"] = selectAddres.address

    
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addChildProfile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                            getFirebaseAnalytics(screenName : getEventName.ADD_CHILD_PROFILE,isPatient_id : true)
                        _ = self.navigationController?.popViewController(animated: true)
                                
                        }
                        self.showResponseMessage(dict: Response)
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
