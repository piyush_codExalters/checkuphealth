//
//  NewPinVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit

class NewPinVC: ParentViewController {
    
    @IBOutlet weak var btnSave: LoginButton!
    @IBOutlet weak var textFour: JPWidthTextField!
    @IBOutlet weak var textThree: JPWidthTextField!
    @IBOutlet weak var textTwo: JPWidthTextField!
    @IBOutlet weak var textONe: JPWidthTextField!
    @IBOutlet weak var lblError: LabelSemiBold!
    @IBOutlet weak var lblsetTitlel: LabelSemiBold!
    var arryaNumber = ["1","2","3","4","5","6","7","8","9","","0",""]
    var setOld = ""
    var setPin = ""
    var setAgenPin = ""
    var isAgen = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblsetTitlel.text = "Enter Your Old Pin"
        textFour.makeCornecscrRoun1(redias : 4)
        textThree.makeCornecscrRoun1(redias : 4)
        textTwo.makeCornecscrRoun1(redias : 4)
        textONe.makeCornecscrRoun1(redias : 4)
        lblError.text = " "
        btnSave.isHidden = true
    }
    @IBAction func btnSaveClicekd(_ sender: Any) {
        getupdate(key : "nPin",value: setAgenPin)
        
      

    }
    
    @IBAction func btnNumberClickrd(_ sender: UIButton) {
        lblError.text = ""
        if sender.tag == 9{
            return
        }
        if sender.tag == 11{
            if !String.validateStringValue(str: textFour.text){
                textFour.text = ""
            }else if !String.validateStringValue(str: textThree.text){
                textThree.text = ""
            }else if !String.validateStringValue(str: textTwo.text){
                textTwo.text = ""
            }else if !String.validateStringValue(str: textONe.text){
                textONe.text = ""
            }
            return
        }
        
        if String.validateStringValue(str: textONe.text){
            textONe.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textTwo.text){
            textTwo.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textThree.text){
            textThree.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textFour.text){
            textFour.text = arryaNumber[sender.tag]
            if isAgen == 2{
                setAgenPin = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
                if setPin != setAgenPin{
                    lblError.text = "Wrong Pin"
                    textFour.text = ""
                    textThree.text = ""
                    textTwo.text = ""
                    textONe.text = ""
                    return
                }else{
                    lblError.text = " "
                    btnSave.isHidden = false
                }
            }else if isAgen == 1{
                setPin = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
                textFour.text = ""
                textThree.text = ""
                textTwo.text = ""
                textONe.text = ""
                lblsetTitlel.text = "Re enter Your New Pin"
                isAgen = 2
            }else{
                setOld = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
                if setOld != _currentUser.nPin{
                    lblError.text = "Wrong old pin"
                    textFour.text = ""
                    textThree.text = ""
                    textTwo.text = ""
                    textONe.text = ""
                    return
                }
                textFour.text = ""
                textThree.text = ""
                textTwo.text = ""
                textONe.text = ""
                lblsetTitlel.text = "Enter your New Pin"
                isAgen = 1
            }
        }
        
    }
    
}
extension NewPinVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryaNumber.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 11{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CancelIcon", for: indexPath) as! PinCollCell
            cell.btnNumber.tag = indexPath.row
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Number", for: indexPath) as! PinCollCell
        cell.lbblNumber.text = arryaNumber[indexPath.row]
        cell.btnNumber.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 3 , height: 84 * _widthRatio)
    }
    
}

extension NewPinVC{
    func getupdate(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    let data = _userDefault.value(forKey: "userInfo") as? NSDictionary
                                    _currentUser = UserDetail(dic: data!)
                                    _currentUser.nPin = value
                                    _userDefault.set(self.getSaveData(dataname : data!), forKey: "userInfo")
                                    _userDefault.synchronize()
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
