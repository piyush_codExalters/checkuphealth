//
//  SupportVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit
class SupportList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t1 = SingIn()
        t1.placeholder = "Full Name"
        t1.keybord = .default
        
        
        var t4 = SingIn()
        t4.placeholder = "Email"
        t4.isSecure = false
        t4.keybord = .emailAddress
        t4.strValue = ""
        
        var t5 = SingIn()
        t5.placeholder = "Message"
        t5.isSecure = true
        t5.keybord = .default
        
        allFields.append(t1)
        allFields.append(t4)
        allFields.append(t5)
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please Enter your Name")
        }else  if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please Enter Email Address")
        }else if !allFields[1].strValue.isValidEmailAddress(){
            return(false,"Please Enter Valid Email Address")
        }else  if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please Enter your Query")
        }else  if "Enter your Message" == allFields[2].strValue{
            return(false,"Please Enter your Query")
        }
        return(true,"")
        
    }
    
}
class SupportVC: ParentViewController, UITextViewDelegate {
    var strAppointment = "Enter your Message"
    var fieldData = SupportList()
    @IBOutlet weak var viewBottom: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBottom.frame.size.height = 95 * _widthRatio
        fieldData.allFields[0].strValue = _currentUser.patient_first_name + " " + _currentUser.patient_last_name
        fieldData.allFields[1].strValue = _currentUser.patient_email_address
        tableView.reloadData()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter your Message"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Enter your Message"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strAppointment
            }
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[2].strValue = textView.text!
        strAppointment = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        if fieldData.validation().0{
            addsupport()
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }
    
    
    
    
}
extension SupportVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 150 * _widthRatio
        }
        if indexPath.row == 0{
            return 90 * _widthRatio
        }
        return 75 * _widthRatio
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = 5
            cell.textAreaNAme.tag = indexPath.row
            fieldData.allFields[indexPath.row].strValue = strAppointment
            cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
            return cell
            
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.inputView = nil
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
        if indexPath.row == 0{
            cell.viewConstretTop.constant = 5
            cell.viewConstretBottom.constant = -5
        }else{
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
        }
        return cell
    }
    
    
}
extension SupportVC{
    func addsupport(){
        let parameters = "{\"data\":{\"nPatientId\":\"\(_currentUser.patient_id!)\",\"vPatientName\":\"\(fieldData.allFields[0].strValue)\",\"vEmail\":\"\(fieldData.allFields[1].strValue)\",\"vMessage\":\"\(fieldData.allFields[2].strValue)\"}}"
            showCentralSpinner()
            KPWebCall.call.postRequestApiClinet(relPath: "addsupport.php", param: parameters) { (response) in
                self.hideCentralSpinner()
                if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                        if let Response = response as? NSDictionary{
                            if Response.getIntValue(key: "success") == 1{
                                self.navigationController?.popViewController(animated: true)
                            }
                            self.showResponseMessage(dict: Response)
                         }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                }
            }
    }
}
