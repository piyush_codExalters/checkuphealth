//
//  PhotoCameraVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 12/03/22.
//

import UIKit
import AVFoundation
import CropViewController

class PhotoCameraVC: ParentViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CropViewControllerDelegate{

    var imagePickers:UIImagePickerController?

    @IBOutlet weak var customCameraView: UIView!

    @IBOutlet weak var btnpickPhoto: UIButton!

    
    var isType = 1
    var vIdProofImagePath : UIImage?
    var vSelfieImagePath : UIImage?
    var vResidenceProofImagePath : UIImage?
    var cameraType = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnpickPhoto.makeRound()
        addCameraInView(type : isType)

    }
    
    func addCameraInView(type : Int){

        imagePickers = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickers?.delegate = self
            imagePickers?.sourceType = .camera
            if isType == 1{
                imagePickers?.cameraDevice = .front
            }else{
                imagePickers?.cameraDevice = .rear
            }
            //add as a childviewcontroller
            addChild(imagePickers!)

            // Add the child's View as a subview
            self.customCameraView.addSubview((imagePickers?.view)!)
            imagePickers?.view.frame = customCameraView.bounds
            imagePickers?.allowsEditing = false
            imagePickers?.showsCameraControls = false
            imagePickers?.view.autoresizingMask = [.flexibleWidth,  .flexibleHeight]
            }
        }

        @IBAction func cameraButtonPressed(_ sender: Any) {

          
          imagePickers?.takePicture()

             
        }
   

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropViewController, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropViewController, animated: true)
        }
    }
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            // 'image' is the newly cropped version of the original image
        if cameraType == 1{
            vSelfieImagePath = image
        }
        if cameraType == 2{
            vIdProofImagePath = image
        }
        if cameraType == 3{
            vResidenceProofImagePath = image
        }
        dismiss(animated:true, completion: nil)
        _ = self.navigationController?.popViewController(animated: true)

        
        if cameraType == 1{
            showAlert1(title: _appName, msgString: "Now Take a picture of document!", Ohk: "OK") {
                self.addCameraInView(type : 2)
            }
        }

    }
    
}
