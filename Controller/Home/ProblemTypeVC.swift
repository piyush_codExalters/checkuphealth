//
//  ProblemTypeVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/05/21.
//

import UIKit
import DropDown


class ProblemCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblName: LabelSemiBold!
    @IBOutlet weak var btnYES: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var botom: NSLayoutConstraint!
    @IBOutlet weak var top: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias : 6)

    }
}

class ProblemTypeVC: ParentViewController, UITextViewDelegate {
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var textDrop: JPWidthTextField!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var viewOtherInfo: UIView!
    
    @IBOutlet weak var textAreaOtherInfo: JPWidthTextView!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var hightTextBox: NSLayoutConstraint!
    
    @IBOutlet weak var lblNoteAdd: LabelRegular!
    @IBOutlet weak var lblstNote: LabelRegular!
    
    let dropCity = DropDown()
    var ProblemID = [String]()
    var ProblemName = [String]()
    var ProblemNote = [String]()

    var arryaProblem = [ProblemList]()
    var arryaProblemQuestionList = [ProblemQuestionList]()
    var daleget : AppointmentConfirmationVC!
    var indext = 0
    var strProblemNote = "Please provide more information."
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewTop.frame.size.height = 185 * _widthRatio
        viewBottom.isHidden = true
        viewBottom.frame.size.height = 250 * _widthRatio
        hightTextBox.constant = 105
        lblNoteAdd.isHidden = true
        lblstNote.isHidden = true
        
        viewText.makeCustomRound(radius:6,bc:.black)
        viewBg.makeCornerRound(redias : 6)
        self.tableView.contentInset.bottom = 80 * _widthRatio
        
        dropCity.anchorView = textDrop
        getProblemList()
        viewOtherInfo.makeCustomRound(radius:6,bc:.black)
        textAreaOtherInfo.text = strProblemNote
    }
    @IBAction func btnDropDownClicked(_ sender: Any) {
        dropCity.selectionAction = { [unowned self] (index: Int, item: String) in
            self.textDrop.text! = ProblemName[index]
            
            hightTextBox.constant = 105
            viewTop.frame.size.height = 185 * _widthRatio
            lblNoteAdd.isHidden = true
            lblstNote.isHidden = true
            if self.ProblemNote[index] != ""{
                self.lblNoteAdd.text = ProblemNote[index]
                self.lblNoteAdd.sizeToFit()
               let highy = self.lblNoteAdd.frame.height
                hightTextBox.constant = 130 + highy
                viewTop.frame.size.height = 200 + highy
                lblNoteAdd.isHidden = false
                lblstNote.isHidden = false
            }
            indext = index
            getOcquestionlist(nProblemId: ProblemID[index])
        }
        self.view.endEditing(true)
        self.dropCity.show()
    }

    
    @IBAction func btnYesClicked(_ sender: UIButton) {
        arryaProblemQuestionList[sender.tag].isYes = true
        tableView.reloadData()
    }
    
    @IBAction func btnNOClicek(_ sender: UIButton) {
        arryaProblemQuestionList[sender.tag].isYes = false
        tableView.reloadData()
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if strProblemNote == "Please provide more information."{
            strProblemNote = ""
        }
        
        daleget.getProbem(problemName: ProblemName[indext], problemId: ProblemID[indext], Quration: arryaProblemQuestionList, problemNote : strProblemNote)
    }
    
    
     func getdropCity(){
         //Delivry.removeAll()
         for obj in arryaProblem {
            ProblemID.append(obj.nProblemId)
            ProblemName.append(obj.vName)
            ProblemNote.append(obj.vNote)
         }
         dropCity.dataSource = ProblemName
         dropCity.direction = .bottom
         dropCity.bottomOffset = CGPoint(x: 0, y:(dropCity.anchorView?.plainView.bounds.height)!)
         dropCity.width = 340 * _widthRatio
         DropDown.appearance().textColor = UIColor.black
         //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
         DropDown.appearance().backgroundColor = UIColor.white
         DropDown.appearance().selectionBackgroundColor = UIColor.white
         DropDown.appearance().cellHeight = 40 * _widthRatio
         
     }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please provide more information."
            textView.textColor = UIColor.lightGray
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Please provide more information."{
                textView.text = nil
            }else{
                textView.text = strProblemNote
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        strProblemNote = textView.text!
    }
}

extension ProblemTypeVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaProblemQuestionList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arryaProblemQuestionList[section].isYes == true{
            if arryaProblemQuestionList[section].vDesc == ""{
                return 1
            }
            return 2
        }else{
            if arryaProblemQuestionList[section].vDesc1 == ""{
                return 1
            }
            return 2
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Problem") as! ProblemCell
            cell.lblName.text = "\(arryaProblemQuestionList[indexPath.section].vQuestion!)\n"
            cell.btnNo.tag = indexPath.section
            cell.btnYES.tag = indexPath.section
            cell.btnNo.layer.cornerRadius = cell.btnNo.frame.height * _widthRatio / 2
            cell.btnYES.layer.cornerRadius = cell.btnNo.frame.height * _widthRatio / 2
            cell.btnYES.layer.backgroundColor = UIColor.white.cgColor
            cell.btnNo.layer.backgroundColor = UIColor.white.cgColor

            if arryaProblemQuestionList[indexPath.section].isYes == true{
                cell.top.constant = 5
                cell.botom.constant = -5
                if arryaProblemQuestionList[indexPath.section].vDesc == ""{
                    cell.botom.constant = 5
                }
                cell.btnNo.layer.borderColor = UIColor.colorRed().cgColor
                cell.btnNo.layer.borderWidth = 1 * _widthRatio
                cell.btnNo.layer.backgroundColor = UIColor.white.cgColor
                cell.btnNo.setTitleColor(UIColor.colorRed(), for: .normal)

                cell.btnYES.layer.backgroundColor = UIColor.colorchaupOrange().cgColor
                cell.btnYES.setTitleColor(UIColor.white, for: .normal)
            }else{
                cell.top.constant = 5
                cell.botom.constant = -5
                if arryaProblemQuestionList[indexPath.section].vDesc1 == ""{
                    cell.botom.constant = 5
                }
                cell.btnYES.layer.borderColor = UIColor.colorchaupOrange().cgColor
                cell.btnYES.layer.borderWidth = 1 * _widthRatio
                cell.btnYES.layer.backgroundColor = UIColor.white.cgColor
                cell.btnYES.setTitleColor(UIColor.colorchaupOrange(), for: .normal)
                
                cell.btnNo.layer.backgroundColor = UIColor.colorRed().cgColor
                cell.btnNo.setTitleColor(UIColor.white, for: .normal)
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "desc") as! ProblemCell
        if arryaProblemQuestionList[indexPath.section].isYes{
            cell.lblName.text = arryaProblemQuestionList[indexPath.section].vDesc
        }else{
            cell.lblName.text = arryaProblemQuestionList[indexPath.section].vDesc1
        }
        return cell
       
    }
    
    
}


extension ProblemTypeVC{
    func getProblemList(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "getProblemList.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaProblem.removeAll()
                              //  self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                            for str in data {
                                                arryaProblem.append(ProblemList(dic: str as! NSDictionary))

                                            }
                                        
                                    }
                                    
                                   getdropCity()
                                    self.textDrop.text! = ProblemName[0]
                                    hightTextBox.constant = 105
                                    viewTop.frame.size.height = 185 * _widthRatio
                                    lblNoteAdd.isHidden = true
                                    lblstNote.isHidden = true
                                    if self.ProblemNote[0] != ""{
                                        self.lblNoteAdd.text = ProblemNote[0]
                                        self.lblNoteAdd.sizeToFit()
                                       let highy = self.lblNoteAdd.frame.height
                                        hightTextBox.constant = 130 + highy
                                        viewTop.frame.size.height = 200 + highy
                                        lblNoteAdd.isHidden = false
                                        lblstNote.isHidden = false
                                    }
                                    
                                    getOcquestionlist(nProblemId: ProblemID[0])
                                }else{
                                   
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func getOcquestionlist(nProblemId: String){
        
        var dic = [String:Any]()
        // dic["memberId"] = _currentUser.patient_id!
        dic["nProblemId"] = nProblemId
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getOcquestionlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaProblemQuestionList.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        if let obj = data.value(forKey: "Question") as? NSArray{
                                            for str in obj {
                                                arryaProblemQuestionList.append(ProblemQuestionList(dic: str as! NSDictionary))

                                            }
                                        }
                                    }
                                    viewBottom.isHidden = false
                                }else{
                                    viewBottom.isHidden = true
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
extension String {
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
}
