//
//  ConfirmPaymentVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 17/05/21.
//

import UIKit

import FBSDKCoreKit


class ConfirmPaymentCollCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var lblDesc: LabelRegular!
    @IBOutlet weak var lblPaly: LabelSemiBold!
    @IBOutlet weak var lblTitle: LabelMedium!
    @IBOutlet weak var viewBG: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}



class ConfirmPaymentCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblCureny: LabelMedium!
    @IBOutlet weak var lblPaymnet: LabelSemiBold!
    @IBOutlet weak var lblDescount: LabelRegular!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var hightApply: NSLayoutConstraint!
    @IBOutlet weak var textPromocode: UITextField!
    @IBOutlet weak var btnCheckWallet: UIButton!
    @IBOutlet weak var btnPresonalWalltet: UIButton!
    @IBOutlet weak var lblPresanal: LabelMedium!
    @IBOutlet weak var hightBottom: NSLayoutConstraint!
    @IBOutlet weak var hightTop: NSLayoutConstraint!
    @IBOutlet weak var collcetionSubcription: UICollectionView!
    
    weak var confirmPayment : ConfirmPaymentVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let _ = viewBg{
            viewBg.makeCornerRound(redias : 6)
            
        }
        if reuseIdentifier == "Cupon"{
            btnApply.makeCornerRound(redias : 6)
            btnApply.layer.borderColor = UIColor.colorchaupOrange().cgColor
            btnApply.layer.borderWidth = 1 * _widthRatio
            btnApply.layer.backgroundColor = UIColor.white.cgColor
            btnApply.setTitleColor(UIColor.colorchaupOrange(), for: .normal)
            textPromocode.makeCustomRound(radius:6,bc:.black)
            
        }
        
        
    }
}


class ConfirmPaymentVC: ParentViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnContinue: submitButton!
    
    
    
    var isCHeck = "0"
    var nOrganizationId = "0"
    var usedCredits  : Float = 0.0
    var arryaProblemQuestionList = [ProblemQuestionList]()
    var docterDetailes : DocterDetailes!
    var subcription : Subcription!
    var strTime = ""
    var strdate = ""
    var fieldData = AppointmentList()
    var appointmentType = ""
    var selectAddres : AddressListName!
    var deliverylist : Deliverylist!
    var pharmacies : Pharmacies!
    var uploadfile : Data?
    var imgUploadfile : UIImage?
    var problemId = ""
    var isPersnal = ""
    var isOrganisation = ""
    var nurseDetailes : NurseDetailes!
    var isDoctor = false
    var isEmail = "0"
    var isPhone = "0"
    var amountPayableByWorldPay : Float = 0.0
    var organisationId = ""
    var organisationWalletAmount : Float = 0.0
    var personalWallet : Float = 0.0
    var cupanid = ""
    var cupStr = ""
    var Discount : Float = 0.0
    var walletAmountUsed : Float = 0.0
    var dCapValue : Float = 0.0
    var dValue : Float = 0.0
    var nType : Float = 0.0
    var isSubcription = 0
    var referralList = ""
    var strAppointment = ""
    var strProblemNote = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getuserwallet()
        getFirebaseAnalytics(screenName : getEventName.SELECT_PACKAGE,isPatient_id : true)
    }
    
    @IBAction func btnCheckClicked(_ sender: Any) {
        if personalWallet > 0 || organisationWalletAmount > 0{
            if isCHeck == "0"{
                isCHeck = "1"
                if personalWallet > 0{
                    isPersnal = "1"
                }else if organisationWalletAmount > 0{
                    isOrganisation = "1"
                }else if personalWallet > 0 || organisationWalletAmount > 0{
                    isPersnal = "1"
                    isOrganisation = "0"
                }
            }else{
                isCHeck = "0"
                isOrganisation = "0"
                isPersnal = "0"
            }
        }
        calculateAmountPayable(dValue:dValue,dCapValue: dCapValue, Type: Int(nType))
        tableView.reloadData()
        
    }
    
    @IBAction func btnPersnalClicekd(_ sender: UIButton) {
        if isCHeck == "1"{
            if sender.tag == 3{
                if personalWallet > 0{
                    isPersnal = "1"
                    isOrganisation = "0"
                }
            }
            if sender.tag == 4{
                if organisationWalletAmount > 0{
                    isOrganisation = "1"
                    isPersnal = "0"
                }
            }
        }
        calculateAmountPayable(dValue:dValue,dCapValue: dCapValue, Type: Int(nType))
        tableView.reloadData()
    }
    
    
    @IBAction func textPromocodeEdit(_ sender: UITextField) {
        cupStr = sender.text!
        
    }
    
    @IBAction func btnApplyClicekd(_ sender: Any) {
        
        if String.validateStringValue(str: cupStr){
            return showFailMessage(message: "Enter promo code you have")
        }else{
            applyCouponcode(vCouponCode: cupStr)
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        cupStr = textField.text!
        cupanid = ""
        calculateAmountPayable(dValue:dValue,dCapValue: dCapValue, Type: Int(nType))
        tableView.reloadData()
        return true
    }
    @IBAction func btnContiueClicked(_ sender: Any) {
        if subcription == nil{
            return
        }
        if subcription.arryaActivesubscription.count > 0{
            
            if isWorkingCountryUK() {
                saveappointment()
            } else {
                if isDoctor{
                    saveappointment()
                }else{
                    savenurseappointment()
                }
            }
            
        }else{
            if amountPayableByWorldPay > 0.00{
                let cardVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CardDetailsVC")  as! CardDetailsVC
                cardVC.isCHeck = isCHeck
                cardVC.nOrganizationId = nOrganizationId
                cardVC.usedCredits  = usedCredits
                cardVC.arryaProblemQuestionList = arryaProblemQuestionList
                cardVC.docterDetailes  = docterDetailes
                cardVC.subcription  = subcription
                cardVC.strTime = strTime
                cardVC.strdate = strdate
                cardVC.fieldData = fieldData
                cardVC.appointmentType = appointmentType
                cardVC.selectAddres = selectAddres
                cardVC.deliverylist = deliverylist
                cardVC.pharmacies = pharmacies
                cardVC.uploadfile = uploadfile
                cardVC.imgUploadfile = imgUploadfile
                cardVC.problemId = problemId
                cardVC.isPersnal = isPersnal
                cardVC.isOrganisation = isOrganisation
                cardVC.nurseDetailes = nurseDetailes
                cardVC.isDoctor = isDoctor
                cardVC.isEmail = isEmail
                cardVC.isPhone = isPhone
                cardVC.amountPayableByWorldPay = amountPayableByWorldPay
                cardVC.organisationId = organisationId
                cardVC.organisationWalletAmount = organisationWalletAmount
                cardVC.personalWallet = personalWallet
                cardVC.cupanid = cupanid
                cardVC.cupStr = cupStr
                cardVC.Discount = Discount
                cardVC.walletAmountUsed = walletAmountUsed
                cardVC.dCapValue = dCapValue
                cardVC.dValue = dValue
                cardVC.nType = nType
                cardVC.isSubcription = isSubcription
                cardVC.referralList = referralList
                cardVC.strAppointment = strAppointment
                cardVC.strProblemNote = strProblemNote
                navigationController?.pushViewController(cardVC, animated: true)
            }else{
                saveappointment()
            }
        }
        
        
        
    }
    
    
}

extension ConfirmPaymentVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Packege
        if indexPath.row == 0{
            return 211 * _widthRatio
        }
        if let _ = subcription{
            if subcription.arryaActivesubscription.count > 0{
                // Details
                if indexPath.row == 1{
                    return 105 * _widthRatio
                }
            }
            
        }
        // Promocode
        if indexPath.row == 1{
            return 120 * _widthRatio
        }
        // checkbox
        if indexPath.row == 2{
            return 65 * _widthRatio
        }
        // walletBox
        if indexPath.row == 3{
            //            if personalWallet <= 0.00 {
            //                return 0 * _widthRatio
            //            }else{
            return 50 * _widthRatio
            //   }
        }
        if indexPath.row == 4{
            if nOrganizationId.isEmpty || nOrganizationId == "0"{
                return 0
            }else{
                if organisationWalletAmount <= 0.00 {
                    return 0 * _widthRatio
                }else{
                    return 50 * _widthRatio
                }
            }
        }
        // Total
        if indexPath.row == 5{
            return 150 * _widthRatio
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = subcription{
            if subcription.arryaActivesubscription.count > 0{
                return 2
            }else{
                return 6
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Subscripation") as! ConfirmPaymentCell
            if let _ = subcription{
                if subcription.arryaActivesubscription.count > 0{
                    cell.lblPresanal.text = "Your Active Package"
                }else{
                    cell.lblPresanal.text = "Select Your Package"
                }
                cell.confirmPayment = self
                cell.collcetionSubcription.reloadData()
            }
            return cell
        }
        if let _ = subcription{
            if subcription.arryaActivesubscription.count > 0{
                
                if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! ConfirmPaymentCell
                    cell.lblCureny.text = subcription.arryaActivesubscription[0].nNoOfConsultantRemaining
                    cell.lblPresanal.text = subcription.arryaActivesubscription[0].dExpiredDate
                    
                    return cell
                }
            }
        }
        
        
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cupon") as! ConfirmPaymentCell
            if cupanid != ""{
                cell.lblDescount.text = "Discount of " + CheckupCurrey() + " " + "\(Discount.roundedFlaot(toPlaces: 1))" + " Applied."
            }
            if String.validateStringValue(str: cupStr){
                cell.lblDescount.isHidden = true
                cell.textPromocode.textColor = .black
                cell.hightApply.constant = 82 * _widthRatio
            }else{
                cell.lblDescount.isHidden = false
                cell.textPromocode.textColor = UIColor.init(hexString: "1C47A4")
                cell.hightApply.constant = 0 * _widthRatio
            }
            return cell
        }
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Wallet") as! ConfirmPaymentCell
            if isCHeck == "0"{
                cell.btnCheckWallet.isSelected = false
            }else{
                cell.btnCheckWallet.isSelected = true
            }
            return cell
        }
        if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckWalletAmont") as! ConfirmPaymentCell
            cell.lblPaymnet.text = CheckupCurrey() + " " + "\(personalWallet.roundedFlaot(toPlaces: 2))"
            cell.lblPresanal.text = "Use Personal Wallet"
            cell.btnPresonalWalltet.tag = indexPath.row
            if isPersnal == "1"{
                cell.btnPresonalWalltet.isSelected = true
            }else{
                cell.btnPresonalWalltet.isSelected = false
            }
            if nOrganizationId.isEmpty || nOrganizationId == "0"{
                cell.hightBottom.constant = 5
            }else{
                if organisationWalletAmount <= 0.00 {
                    cell.hightBottom.constant = 5
                }else{
                    cell.hightBottom.constant = -5
                }
            }
            cell.hightTop.constant = 5
            return cell
        }
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckWalletAmont") as! ConfirmPaymentCell
            cell.lblPaymnet.text = CheckupCurrey() + " " + "\(organisationWalletAmount.roundedFlaot(toPlaces: 2))"
            cell.lblPresanal.text = "Use Organisation Wallet"
            
            cell.btnPresonalWalltet.tag = indexPath.row
            if isOrganisation == "1"{
                cell.btnPresonalWalltet.isSelected = true
            }else{
                cell.btnPresonalWalltet.isSelected = false
            }
            cell.hightTop.constant = -5
            cell.hightBottom.constant = 5
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PayAmount") as! ConfirmPaymentCell
        cell.lblCureny.text = CheckupCurrey()
        if subcription.arryaActivesubscription.count > 0{
            cell.lblPaymnet.text = "\(amountPayableByWorldPay.roundedFlaot(toPlaces: 1))"
        }
        if subcription.arryaSubscription.count > 0{
            cell.lblPaymnet.text = "\(amountPayableByWorldPay.roundedFlaot(toPlaces: 1))"
        }
        return cell
        
        
    }
    
    
}
extension ConfirmPaymentCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        confirmPayment.isSubcription = indexPath.row
        
        if let _ = confirmPayment.subcription{
            if confirmPayment.subcription.arryaActivesubscription.count > 0{
                return
            }else{
                confirmPayment.amountPayableByWorldPay = Float(confirmPayment.subcription.arryaSubscription[indexPath.row].dAmount)!
                confirmPayment.btnContinue.setTitle("Continue", for: .normal)
            }
        }
        if !String.validateStringValue(str: confirmPayment.cupStr){
            confirmPayment.applyCouponcode(vCouponCode: confirmPayment.cupStr)
        }
        confirmPayment.calculateAmountPayable(dValue:confirmPayment.dValue,dCapValue: confirmPayment.dCapValue, Type: Int(confirmPayment.nType))
        confirmPayment.tableView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = confirmPayment{
            if let _ = confirmPayment.subcription{
                if confirmPayment.subcription.arryaActivesubscription.count > 0{
                    return confirmPayment.subcription.arryaActivesubscription.count
                }
                if confirmPayment.subcription.arryaSubscription.count > 0{
                    return confirmPayment.subcription.arryaSubscription.count
                }
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if confirmPayment.subcription.arryaActivesubscription.count > 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Packege", for: indexPath) as! ConfirmPaymentCollCell
            if let _ = confirmPayment{
                if let _ = confirmPayment.subcription{
                    cell.lblTitle.text = confirmPayment.subcription.arryaActivesubscription[indexPath.row].vTitle
                    cell.lblPaly.text = confirmPayment.CheckupCurrey() + " " + confirmPayment.subcription.arryaActivesubscription[indexPath.row].dAmount
                    cell.lblDesc.text =  confirmPayment.subcription.arryaActivesubscription[indexPath.row].nNoOfConsultant + " Consultations"
                    if confirmPayment.isSubcription == indexPath.row{
                        cell.viewBG.makeCornerRoundBorder(redias : 8,Color : .blue)
                    }else{
                        cell.viewBG.makeCornerRoundBorder(redias : 8,Color : .white)
                    }
                }
                
            }
            return cell
            
            
            
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Packege", for: indexPath) as! ConfirmPaymentCollCell
        if let _ = confirmPayment{
            if let _ = confirmPayment.subcription{
                cell.lblTitle.text = confirmPayment.subcription.arryaSubscription[indexPath.row].vTitle
                cell.lblPaly.text = confirmPayment.CheckupCurrey() + " " + confirmPayment.subcription.arryaSubscription[indexPath.row].dAmount
                cell.lblDesc.text =  confirmPayment.subcription.arryaSubscription[indexPath.row].nNoOfConsultant + " Consultations"
                if confirmPayment.isSubcription == indexPath.row{
                    cell.viewBG.makeCornerRoundBorder(redias : 8,Color : .blue)
                }else{
                    cell.viewBG.makeCornerRoundBorder(redias : 8,Color : .white)
                }
            }
            
        }
        return cell
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 148 * _widthRatio, height: 138 * _widthRatio)
    }
}
extension ConfirmPaymentVC{
    
    func saveappointment(){
        
        var dic = [String:Any]()
        var email = ""
        var phone = ""
        
        // Email OR mobile number verifier
        if isEmail == "1"{
            email = "1"
        }
        if isPhone == "1"{
            if email == ""{
                phone = "2"
            }else{
                phone = ",2"
            }
        }
        
        if let _ = subcription{
            if subcription.arryaActivesubscription.count > 0{
                dic["nSubscriptionId"] = subcription.arryaActivesubscription[isSubcription].id
                dic["nSubscriptionlogId"] = subcription.arryaActivesubscription[isSubcription].nSubscriptionlogId
                
            }else{
                dic["nSubscriptionId"] = subcription.arryaSubscription[isSubcription].id
                dic["nSubscriptionlogId"] = ""
            }
        }
        /* if let _ = pharmacies {
         dic["pharmacyId"] = pharmacies.id
         dic["pharmacyName"] = pharmacies.name
         }else{
         dic["pharmacyId"] = ""
         dic["pharmacyName"] = ""
         }
         if let _ = deliverylist {
         dic["deliveryId"] = deliverylist.id
         dic["deliveryName"] = deliverylist.name
         }else{
         dic["deliveryId"] = ""
         dic["deliveryName"] = ""
         }
         */
        if let _ = selectAddres {
            dic["addressId"] = selectAddres.address_id
            dic["postcode"] = selectAddres.postcode
        }else{
            dic["addressId"] = ""
            dic["postcode"] = ""
        }
        if (cupanid != "") {
            dic["dUseWalletAmount"] = usedCredits
        }else{
            if let _ = subcription{
                if subcription.arryaActivesubscription.count > 0{
                    dic["dUseWalletAmount"] = "0"//subcription.arryaActivesubscription[isSubcription].dAmount
                }else{
                    dic["dUseWalletAmount"] = subcription.arryaSubscription[isSubcription].dAmount
                }
            }
        }
        if referralList != ""{
            dic["nReferralId"] = referralList
        }else{
            dic["nReferralId"] =  ""
        }
        if isOrganisation == "1"{
            dic["nWalletType"] = "1" // Organisation 1
            
        }else if isPersnal == "1"{
            dic["nWalletType"] = "2" // Persnal 2
            
        }else{
            dic["nWalletType"] = "" // not Use
        }
        if uploadfile != nil{
            dic["vReferralFile"] = "\(uploadfile!.base64EncodedString())"
            dic["referralFileFormat"] = fieldData.allFields[4].strValue.getPathExtension()
        }
        if imgUploadfile != nil{
            let imageResized = imgUploadfile!.resizeWith(percentage: 0.25)
            let base64 = imageResized?.toBase64()
            dic["referralFileFormat"] = "jpg"
            dic["vReferralFile"] = "\(base64!)"
        }
        if isWorkingCountryUK(){
            var arryaQuest = [String]()
            dic["nProblemId"] = problemId
            for obj in arryaProblemQuestionList {
                var str = ""
                if obj.isYes{
                    str = "1"
                }else{
                    str = "0"
                }
                arryaQuest.append("\(obj.id!),\(str)")
            }
            dic["vQuestion"] = arryaQuest
        }
        dic["vNote"] = strProblemNote
        dic["vPreferredContactMethod"] = email + phone
        dic["doctor_id"] = docterDetailes.doctor_id
        dic["patient_id"] = _currentUser.patient_id
        dic["booking_date"] = strdate
        dic["booking_time"] = strTime
        dic["description"] = strAppointment
        dic["appointment_type"] = appointmentType
        dic["payment_method"] = "CREDIT"
        dic["payment_orderCode"] = ""
        dic["payment_amount"] = amountPayableByWorldPay
        dic["payment_currencyCode"] = ""
        dic["payment_expiryMonth"] = ""
        dic["payment_expiryYear"] = ""
        dic["payment_cardType"] = ""
        dic["payment_maskedCardNumber"] = ""
        dic["transaction_id"] = ""
        dic["currency_vSymbol"] = _currency.currency_vSymbol
        dic["currency_conversionRate"] = _currency.currency_conversionRate
        dic["payment_charge"] = amountPayableByWorldPay
        dic["paymentStatus"] = "SUCCESS"
        dic["nCouponId"] = cupanid
        dic["dDiscountPrice"] = Discount.roundedFlaot(toPlaces: 1)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "saveappointment.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    let parameters = [
                                        AppEvents.ParameterName("Name").rawValue: "\(docterDetailes.doctor_first_name!)" + " " +  "\(docterDetailes.doctor_last_name!)",
                                        AppEvents.ParameterName("Type").rawValue: "doctor",
                                        AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                                        AppEvents.ParameterName("Timeslot").rawValue: "\(self.strdate)" + "\(self.strTime)",
                                        AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                                        AppEvents.ParameterName("Appointment ID").rawValue: "\(Response.getIntValue(key: "AppointmentId"))",
                                        AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                                        AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                                        AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
                                        
                                    ] as [String : Any]
                                    AppEvents.logEvent(.init("Appointment Booked"), parameters: parameters)
                                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDoneVC")  as! AppointmentDoneVC
                                    vc.docterDetailes = docterDetailes
                                    vc.isDoctor = isDoctor
                                    vc.strTime = strTime
                                    vc.strdate = strdate
                                    vc.imageProfile = docterDetailes.doctor_profile_image
                                    vc.FullName = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                                    if isWorkingCountryUK() {
                                        vc.Specialist = docterDetailes.doctor_specialization
                                    } else {
                                        vc.Specialist = docterDetailes.doctor_specialization
                                    }
                                    navigationController?.pushViewController(vc, animated: true)
                                }else{
                                    self.showResponseMessage(dict: Response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func savenurseappointment(){
        
        var dic = [String:Any]()
        dic["nurse_id"] = nurseDetailes.nurse_id
        dic["patient_id"] = _currentUser.patient_id
        dic["booking_date"] = strdate
        dic["booking_time"] = strTime
        dic["nurse_appointment_type"] = appointmentType
        if let _ = selectAddres{
            dic["addressId"] = selectAddres.address_id
        }else{
            dic["addressId"] = ""
        }
        dic["description"] = strAppointment
        dic["payment_method"] = " "
        dic["payment_orderCode"] = " "
        dic["payment_amount"] = " "
        dic["payment_currencyCode"] = " "
        dic["payment_expiryMonth"] = " "
        dic["payment_expiryYear"] = " "
        dic["payment_cardType"] = " "
        dic["payment_maskedCardNumber"] = " "
        dic["transaction_id"] = " "
        dic["currency_vSymbol"] = _currency.currency_vSymbol
        dic["currency_conversionRate"] = "1.0"
        dic["payment_charge"] = nurseDetailes.nurse_fees
        if (cupanid != "") {
            dic["dUseWalletAmount"] = usedCredits
        }else{
            if let _ = subcription{
                if subcription.arryaActivesubscription.count > 0{
                    dic["dUseWalletAmount"] = subcription.arryaActivesubscription[isSubcription].dAmount
                }else{
                    dic["dUseWalletAmount"] = subcription.arryaSubscription[isSubcription].dAmount
                }
            }
        }
        dic["paymentStatus"] = " "
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "savenurseappointment.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    let parameters = [
                                        AppEvents.ParameterName("Name").rawValue: "\(nurseDetailes.nurse_first_name!)" + " " +  "\(nurseDetailes.nurse_last_name!)",
                                        AppEvents.ParameterName("Type").rawValue: "nurse",
                                        AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                                        AppEvents.ParameterName("Timeslot").rawValue: "\(self.strdate)" + "\(self.strTime)",
                                        AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                                        AppEvents.ParameterName("Appointment ID").rawValue: "\(Response.getIntValue(key: "AppointmentId"))",
                                        AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                                        AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                                        AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
                                        
                                    ] as [String : Any]
                                    AppEvents.logEvent(.init("Appointment Booked"), parameters: parameters)
                                    self.performSegue(withIdentifier: "Book", sender: nil)
                                    
                                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDoneVC")  as! AppointmentDoneVC
                                    vc.docterDetailes = docterDetailes
                                    vc.isDoctor = isDoctor
                                    vc.strTime = strTime
                                    vc.strdate = strdate
                                    if isDoctor{
                                        vc.imageProfile = docterDetailes.doctor_profile_image
                                        vc.FullName = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                                        if isWorkingCountryUK() {
                                            vc.Specialist = docterDetailes.doctor_specialization
                                        } else {
                                            vc.Specialist = docterDetailes.doctor_specialization
                                        }
                                    }else{
                                        vc.imageProfile = nurseDetailes.nurse_profile_image
                                        vc.FullName = nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
                                        vc.Specialist = nurseDetailes.nurse_specialization
                                    }
                                    navigationController?.pushViewController(vc, animated: true)
                                    
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getuserwallet(){
        var dic = [String:Any]()
        
        dic["id"] = _currentUser.patient_id
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserwallet.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        
                                        nOrganizationId = data.getStringValue(key: "nOrganizationId")
                                        if data.getStringValue(key: "dWalletAmount") != ""{
                                            organisationWalletAmount = data.getFloatValue(key: "dWalletAmount")
                                        }
                                        if data.getStringValue(key: "dPersonalWallet") != ""{
                                            personalWallet = data.getFloatValue(key: "dPersonalWallet")
                                        }
                                        getSubscriptionlist()
                                        tableView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func applyCouponcode(vCouponCode: String){
        
        var dic = [String:Any]()
        dic["nPatientId"] = _currentUser.patient_id!
        dic["vCouponCode"] = vCouponCode
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "applyCouponcode.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        cupanid = data.getStringValue(key: "id")
                                        // ApplyPromcode(dValue:data.getFloatValue(key: "dValue"),dCapValue: data.getFloatValue(key: "dCapValue"), Type: data.getIntValue(key: "nType"))
                                        dValue = data.getFloatValue(key: "dValue")
                                        dCapValue = data.getFloatValue(key: "dCapValue")
                                        nType = Float(data.getIntValue(key: "nType"))
                                        calculateAmountPayable(dValue:dValue,dCapValue: dCapValue, Type: Int(nType))
                                        tableView.reloadData()
                                    }
                                    
                                }else{
                                    cupStr = ""
                                    self.showResponseMessage(dict: Response)
                                }
                                
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func getSubscriptionlist(){
        
        var dic = [String:Any]()
        dic["nPatientId"] = _currentUser.patient_id!
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getSubscriptionlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        
                                        subcription = Subcription(dic: data as! NSDictionary)
                                    }
                                    if subcription.arryaActivesubscription.count > 0{
                                        btnContinue.setTitle("Continue", for: .normal)
                                        
                                    }else{
                                        amountPayableByWorldPay = Float(subcription.arryaSubscription[0].dAmount)!
                                        btnContinue.setTitle("Continue", for: .normal)
                                    }
                                    
                                }
                                tableView.reloadData()
                                //  self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func calculateAmountPayable(dValue:Float,dCapValue: Float, Type: Int) {
        
        amountPayableByWorldPay = Float(subcription.arryaSubscription[isSubcription].dAmount)!//actual fee of doctor/nurse
        walletAmountUsed = 0.00
        Discount = 0.00
        
        if (cupanid != "") {
            
            if Type == 1 {
                //amount
                Discount = dValue
            }else if Type == 2 {
                //percentage
                //capvalue
                
                var tempDiscount: Float = 0.0
                
                if (dCapValue == 0) {
                    tempDiscount = Float(subcription.arryaSubscription[isSubcription].dAmount)! * (dValue) / 100.00
                } else {
                    tempDiscount = Float(subcription.arryaSubscription[isSubcription].dAmount)! * (dValue) / 100.00
                    if (tempDiscount > dCapValue) {
                        tempDiscount = dCapValue
                    }
                }
                Discount = tempDiscount // discount amount
            }else if Type == 3{
                //free
                Discount = Float(subcription.arryaSubscription[isSubcription].dAmount)!
            }
            
            
            if (Discount > Float(subcription.arryaSubscription[isSubcription].dAmount)!) {
                Discount = Float(subcription.arryaSubscription[isSubcription].dAmount)!
                amountPayableByWorldPay = 0.00
            } else {
                amountPayableByWorldPay = Float(subcription.arryaSubscription[isSubcription].dAmount)! - Discount
            }
            usedCredits = amountPayableByWorldPay
        }
        
        if isCHeck == "1" {
            
            
            if isPersnal == "1" {
                
                if (personalWallet >= amountPayableByWorldPay) {
                    walletAmountUsed = amountPayableByWorldPay
                    
                    amountPayableByWorldPay = 0.00
                } else {
                    walletAmountUsed = personalWallet
                    
                    amountPayableByWorldPay -= walletAmountUsed
                }
            }
            
            if isOrganisation == "1" {
                
                if (organisationWalletAmount >= amountPayableByWorldPay) {
                    walletAmountUsed = amountPayableByWorldPay
                    amountPayableByWorldPay = 0.00
                } else {
                    walletAmountUsed = organisationWalletAmount
                    amountPayableByWorldPay -= walletAmountUsed
                }
            }
            
            
            
        }
        
        
        if (amountPayableByWorldPay > 0.00) {
            btnContinue.setTitle("Continue", for: .normal)
            
        } else {
            btnContinue.setTitle("Continue", for: .normal)
        }
        
        
    }
}
