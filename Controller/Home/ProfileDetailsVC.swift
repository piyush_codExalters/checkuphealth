//
//  ProfileDetailsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 22/04/21.
//

import UIKit
import MapKit
class ProfileDetailsVC: ParentViewController {
    @IBOutlet weak var lblTitleHeader: LabelMedium!
    var strHeader = ""
    var arrData: [[String]] = []
    var serviceDetails : ServiceDetails!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitleHeader.text = serviceDetails.provider_first_name + " " + serviceDetails.provider_last_name
        if serviceDetails.monday.count > 0{
            arrData.append(["Monday","\(serviceDetails.monday[0]) to \(serviceDetails.monday[1])"])
        }
        if serviceDetails.tuesday.count > 0{
            arrData.append(["Tuesday","\(serviceDetails.tuesday[0]) to \(serviceDetails.tuesday[1])"])
        }
        if serviceDetails.wednesday.count > 0{
            arrData.append(["Wednesday","\(serviceDetails.wednesday[0]) to \(serviceDetails.wednesday[1])"])
        }
        if serviceDetails.thursday.count > 0{
            arrData.append(["Thursday","\(serviceDetails.thursday[0]) to \(serviceDetails.thursday[1])"])
        }
        if serviceDetails.friday.count > 0{
            arrData.append(["Friday","\(serviceDetails.friday[0]) to \(serviceDetails.friday[1])"])
        }
        if serviceDetails.saturday.count > 0{
            arrData.append(["Saturday","\(serviceDetails.saturday[0]) to \(serviceDetails.saturday[1])"])

        }

    }
    

    @IBAction func btnCallClicked(_ sender: UIButton) {
        callNumber(phoneNumber: serviceDetails.provider_phone)
    }

}
extension ProfileDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 108 * _widthRatio
            }else{
                return 44 * _widthRatio
            }
        }else if indexPath.section == 1{
            
            if indexPath.row == 1 || indexPath.row == 3{
                return UITableView.automaticDimension
            }else if indexPath.row == 2{
                return 158 * _widthRatio
            }
            if indexPath.row == 0{
                return 30 * _widthRatio
            }
            return 25 * _widthRatio
        }else if indexPath.section == 2{
            return 35 * _widthRatio
        }else{
            return 72 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }else if section == 1{
            return 5
        }else if section == 2{
            return arrData.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Doctor") as! DrDetailsCell
                cell.imageDr.kf.setImage(with: URL(string:serviceDetails.provider_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                }
                cell.lblDrName.text = serviceDetails.provider_first_name + " " + serviceDetails.provider_last_name
                cell.lblMenuName.text = ""
                cell.lblAddres.text = serviceDetails.provider_Street
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = "About Doctor"
            
            return cell

        }else if indexPath.section == 1{
             if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "About Doctor:"
                cell.constraintTop.constant = 5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = serviceDetails.provider_overview.html2String
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Map") as! DrDetailsCell
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                let annotation = MKPointAnnotation()
                let centerCoordinate = CLLocationCoordinate2D(latitude: serviceDetails.provider_latitude, longitude: serviceDetails.provider_longitude)
                annotation.coordinate = centerCoordinate
                let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: cell.latDelta, longitudeDelta: cell.longDelta)
                let region:MKCoordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2DMake(serviceDetails.provider_latitude, serviceDetails.provider_longitude) , span: span)
                
                cell.mapLocation.setRegion(region, animated: true)
                cell.mapLocation.addAnnotation(annotation)
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                let t1 = serviceDetails.provider_Street + ", " +  serviceDetails.provider_city
                let t2 = ", " + serviceDetails.provider_state
                let t3 = ", " +  serviceDetails.provider_country
                cell.lblDesc.text = t1 + t2 + t3
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Practical Hours:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }
            
            

        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeZone") as! DrDetailsCell
            if indexPath.row == arrData.count - 1{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
            }else{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
            }
            if NSDate().dayOfTheWeek() == arrData[indexPath.row][0]{
                cell.lblDay.textColor = UIColor.colorchaupOrange()
            }else{
                cell.lblDay.textColor = UIColor.black
            }
            cell.lblDay.text = arrData[indexPath.row][0]
            cell.lblTime.text = arrData[indexPath.row][1]
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
            return cell
        }
       
    }
    
    
    
    
}
