//
//  QuestionDetailsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/04/21.
//

import UIKit

class QuestionDetailsVC: ParentViewController {
    @IBOutlet weak var btnBookAppontment: submitButton!
    var questionList : QuestionList!
    var questionDetaails : QuestionList!
    var QuestionId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBookAppontment.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        doctorReplyList()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "Chat"{
            let vc = segue.destination as! ChatVC
            vc.answer = (sender as! AnswerLIst)
            vc.qID = questionDetaails.questionId
        }
    }
    
    
    @IBAction func btnBookApponmation(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DrAndNuresListVC")  as! DrAndNuresListVC
        mapVc1.isDoctor = true
        mapVc1.isQustion = true
        mapVc1.questionId = questionDetaails.questionId
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    
    @IBAction func btnAskQution(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AskQuestionVC")  as! AskQuestionVC
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
}
extension QuestionDetailsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            return
        }
        performSegue(withIdentifier: "Chat", sender: questionDetaails.arryaAnswer[indexPath.row])
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = questionDetaails{
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 38 * _widthRatio
            }
            return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }
        return questionDetaails.arryaAnswer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Date") as! QuestionsCell
                cell.lblDate.text = questionDetaails.questionDate
                cell.constraintTop.constant = 5
                cell.constraintBottom.constant = -5
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Descption") as! QuestionsCell
            if questionDetaails.arryaAnswer.count > 0{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
            }else{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
            }
            cell.lblDate.text = questionDetaails.question
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Docter") as! QuestionsCell
        
        cell.constraintTop.constant = -5
        cell.constraintBottom.constant = -5
        cell.lblDocterName.text = questionDetaails.arryaAnswer[indexPath.row].answerDoctorName
        cell.imageDocter.kf.setImage(with: URL(string:questionDetaails.arryaAnswer[indexPath.row].answerDoctorImage), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        cell.lblDate.text = questionDetaails.arryaAnswer[indexPath.row].answer_title
        cell.lblReplied.text = questionDetaails.arryaAnswer[indexPath.row].answerBy
        if questionDetaails.arryaAnswer.count == indexPath.row + 1{
            cell.constraintBottom.constant = 5
        }
        return cell
    }
    
    
    
    
    
}
extension QuestionDetailsVC{
    func doctorReplyList(){
        
        var dic = [String:Any]()
        if let _ = questionList{
            QuestionId = questionList.questionId
        }
        dic["questionId"] = QuestionId
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "doctorReplyList.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.questionDetaails = nil
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    btnBookAppontment.isHidden = false
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        questionDetaails = QuestionList(dic: data)
                                    }
                                }else{
                                    btnBookAppontment.isHidden = true
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    
    
}
