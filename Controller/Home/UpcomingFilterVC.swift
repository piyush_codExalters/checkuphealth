//
//  UpcomingFilterVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 22/04/21.
//

import UIKit
class UpcomingFilterCell: ConstrainedTableViewCell {
    @IBOutlet weak var imageRadio: UIImageView!
    @IBOutlet weak var lblName: LabelMedium!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var hightTop: NSLayoutConstraint!
    @IBOutlet weak var hightBottom: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
class UpcomingFilterVC: ParentViewController {

    @IBOutlet weak var viewBg: UIView!
    var arryakey = ["all","upcoming","completed","cancel","dna"]
    var arryaNAme = ["All","Upcoming","Completed","Cancel","DNA"]
    var isIsected = 0
    var dalegate : AppointmentsVC!
    var str = "all"
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias : 6)
        isIsected = dalegate.IndexNumbrt
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func btnApplyClicked(_ sender: Any) {
        dalegate.getFilter(str: str, Index: isIsected)
    }
    
    @IBAction func btnReset(_ sender: Any) {
        dalegate.getFilter(str: "all", Index: 0)
    }
    

}

extension UpcomingFilterVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isIsected = indexPath.row
        str = arryakey[indexPath.row]
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! UpcomingFilterCell
        
        if isIsected == indexPath.row{
            cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_fill")
        }else{
            cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_unfill")
        }
        
            cell.lblName.text = arryaNAme[indexPath.row]
        return cell
    }
    
    
    
    
}
