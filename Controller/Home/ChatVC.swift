//
//  ChatVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 26/04/21.
//

import UIKit
import IQKeyboardManagerSwift
class ChatCell: ConstrainedTableViewCell {
    @IBOutlet weak var lblChat: LabelMedium!
    @IBOutlet weak var lblDate: LabelRegular!
    @IBOutlet weak var viewBg: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRoundxs(redias : 8)

    }

}
class ChatVC: ParentViewController, UITextViewDelegate {

    
    @IBOutlet weak var lblDesc: LabelRegular!
    @IBOutlet weak var lblProfileName: LabelMedium!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var textChat: JPWidthTextView!
    @IBOutlet weak var textHight: NSLayoutConstraint!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var isKeybordHight: NSLayoutConstraint!
    var answer : AnswerLIst!
    var ChatList : Chat!
    var cellHeightsDictionary = NSMutableDictionary()
    let textViewMaxHeight: CGFloat = 100
    var strPlaceholder = "Type a Message..." 
    var qID = ""
    var isFirstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        lblProfileName.text = answer.answerDoctorName
        textChat.text = strPlaceholder
        textChat.textColor = UIColor.lightGray
        IQKeyboardManager.shared.enable = false
        viewText.makeCornerRound(redias: 20)
        imageProfile.makeCornerRoundxs(redias : 5)
        imageProfile.kf.setImage(with: URL(string:answer.answerDoctorImage), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        conversation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.shared.enable = true
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            isKeybordHight.constant = keyboardSize.height
           // self.tableView.reloadData()
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        isKeybordHight.constant = 20
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = strPlaceholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == strPlaceholder{
                textView.text = nil
            }else{
                textView.text = textChat.text
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView)
    {
        textChat.text = textView.text
    }
  
    @IBAction func btnSendClicked(_ sender: Any) {
        if !String.validateStringValue(str: textChat.text) && textChat.text != "Type a Message..."{
        addAnswer(Massage : textChat.text)
         }
    }
    
}
extension ChatVC: UITableViewDelegate, UITableViewDataSource{


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height = cellHeightsDictionary[indexPath] as? NSNumber
        if height != nil {
            return CGFloat(Double(truncating: height ?? 0.0))
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = ChatList{
            return ChatList.arryaAnswer.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ChatList.arryaAnswer[indexPath.row].answerFor == "Patient"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! ChatCell
            cell.lblChat.text = ChatList.arryaAnswer[indexPath.row].answer_title
            cell.lblDate.text = ChatList.arryaAnswer[indexPath.row].answerTime

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ChatCell
            cell.lblChat.text = ChatList.arryaAnswer[indexPath.row].answer_title
            cell.lblDate.text = ChatList.arryaAnswer[indexPath.row].answerTime

            return cell
        }
    
    }
}
extension ChatVC{
    func conversation(){
        
        var dic = [String:Any]()
        dic["answerBy"] = answer.answerBy
        dic["doctorId"] = answer.answerDoctorId
        dic["questionId"] = qID
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "conversation.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.ChatList = nil
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        ChatList = Chat(dic: data)
                                    }
                                    lblDesc.text = ChatList.doctorSpe
                                }
                                self.tableView.reloadData()
                                if self.isRowVisible(){
                                    self.scrollToBottomOfChat()
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
           
        }
    }
    func addAnswer(Massage : String){
        
        var dic = [String:Any]()
        dic["answerBy"] = "Patient"
        dic["doctorId"] = answer.answerDoctorId
        dic["questionId"] = qID
        dic["patientId"] = _currentUser.patient_id
        dic["answer"] = Massage
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "answer.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    conversation()
                                    textChat.text = ""
                                    lblDesc.text = ChatList.doctorSpe
                                }
                               
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func scrollToBottomOfChat(){
        if ChatList.arryaAnswer.count == 0{
            return
        }
        
        let indexPath = IndexPath(row: ChatList.arryaAnswer.count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        
    }
    
    func isRowVisible() -> Bool
    {
        let inde = tableView.indexPathsForVisibleRows
        if isFirstTime{
            isFirstTime = false
            return true
        }
        for ind in inde!{
            if ind.row == ChatList.arryaAnswer.count - 1{
                return true
            }
        }
        return false
    }
}
