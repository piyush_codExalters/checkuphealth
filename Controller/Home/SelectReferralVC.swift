//
//  SelectReferralVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 20/04/21.
//

import UIKit
import MobileCoreServices
import CropViewController

class SelectReferralCell: ConstrainedTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
class SelectReferralVC: ParentViewController {

    @IBOutlet weak var lblFileName: LabelSemiBold!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var viewBg: UIView!
    var imagePicker = UIImagePickerController()
    var documnet : Data?
    var forment = ""
    var deleget : AppointmentConfirmationVC!
    var DoctorId = ""
    var isIsected = -1
    var indext = 0
    var refatr = ""
    var arrayReferralList = [ReferralList]()
    var imgUploadfile : UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewTop.frame.size.height = 164 * _widthRatio
        viewBg.makeCornerRound(redias : 6)
        viewText.makeCornecscrRoun1(redias : 6)
        getReferrallist()
    }
    
    @IBAction func btnUploadFileClicked(_ sender: Any) {
    
        openPickerCamera()
        
    }
 
    @IBAction func btnContinueClicked(_ sender: Any) {
        if arrayReferralList.count > 0{
            if refatr == "" && documnet == nil{
                return showFailMessage(message: "Please select any one file")
            }
            
        }
        if refatr == ""{
            if documnet == nil && imgUploadfile == nil{
                return showFailMessage(message: "upload attach File to Referral")
            }
            deleget.getFile(uploadFile: documnet, forment: forment, referral: refatr, imageURL: imgUploadfile)
        }else{
            deleget.getFile2(forment: arrayReferralList[isIsected].vReferralFile, referral: refatr)
        }
    }
    

}
extension String {
    func getPathExtension() -> String {
        return (self as NSString).pathExtension
    }
}
extension SelectReferralVC: UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        do {
            let DOc = try Data(contentsOf: myURL)
            print(DOc.count.byteSize)
            let fileSize = Double(DOc.count / 1048576) //Convert in to MB
            print("File size in MB: ", fileSize)
            if fileSize <= 2.0{
                documnet = DOc
                forment = "\(myURL.lastPathComponent)"
               // print(String(data: DOc, encoding: .utf8)!)
                lblFileName.text = "\(myURL.lastPathComponent)"
                
            }else{
                showFailMessage(message: "Max file size should be 2 mb only.")
            }
        } catch {
            print(error)
            return
        }
        
        
        print("import result : \(myURL)")
    }
    
    
    func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension Int {
    var byteSize: String {
        return ByteCountFormatter().string(fromByteCount: Int64(self))
    }
}
extension SelectReferralVC: UIImagePickerControllerDelegate, CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropViewController, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropViewController, animated: true)
        }
    }
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            // 'image' is the newly cropped version of the original image
        forment = "Referral.jpg"
        imgUploadfile = image
        lblFileName.text = "Referral.jpg"

        tableView.reloadData()

        dismiss(animated:true, completion: nil)

    }
    
    
    func openPickerCamera(){
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)

        
        let action1 = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        let action2 = UIAlertAction(title: "Open Gallery", style: .default) { (action) in
            self.openLibrary()
        }
        let action4 = UIAlertAction(title: "icloud", style: .default) { (action) in
            self.openIcould()
        }
        
        let action3 = UIAlertAction(title: "Close", style: .cancel) { (action) in
            
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action4)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openIcould(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypePNG),String(kUTTypeJPEG),"com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document","public.text"], in: .import)
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}
extension SelectReferralVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isIsected = indexPath.row
        refatr = arrayReferralList[indexPath.row].id
        tableView.reloadData()
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayReferralList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "Time") as! SelectPharmacyCell
        cell.lblPharmacyName.text = arrayReferralList[indexPath.row].DoctorName
        if isIsected == indexPath.row{
            cell.btnRedio.image  = #imageLiteral(resourceName: "ic_radio_fill")
        }else{
            cell.btnRedio.image  = #imageLiteral(resourceName: "ic_radio_unfill")
        }
        cell.btnAction.tag = indexPath.row
      


        return cell
    }
    
}
extension SelectReferralVC{
    func getReferrallist(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"nDoctorId\":\"\(DoctorId)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getReferrallist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        self.arrayReferralList.removeAll()
                        if Response.getIntValue(key: "success") == 1{
                            self.tableView.backgroundView = nil
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    self.arrayReferralList.append(ReferralList(dic: obj as! NSDictionary))
                                }
                            }
                            self.tableView.reloadData()
                        }else{
                           
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}


