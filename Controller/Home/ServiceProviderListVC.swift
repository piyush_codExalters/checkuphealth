//
//  ServiceProviderListVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 21/04/21.
//

import UIKit

class ServiceProviderListVC: ParentViewController, UITextFieldDelegate {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var textSearch: JPWidthTextField!
    @IBOutlet weak var lblTitleHeader: LabelMedium!
    var strHeader = ""
    var serviceProvideList : ServiceProvideList!
    var arryaServiceProvideListSpeciality = [ServiceProvideListSpeciality]()
    var search = [ServiceProvideListSpeciality]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias: 5)
        lblTitleHeader.text = "Specialities"
        getServiceProviderSpeciality()
    }
    
    @IBAction func textEditSearch(_ sender: UITextField) {
        arryaServiceProvideListSpeciality = search.filter { $0.category_name.localizedCaseInsensitiveContains(sender.text!) }
        if sender.text == ""{
            arryaServiceProvideListSpeciality = search
        }
        self.myColView.reloadData()
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        arryaServiceProvideListSpeciality = search
        self.myColView.reloadData()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "List"{
            let vc = segue.destination as! ServiceListVC
            vc.serviceProvideListSpeciality = (sender as! ServiceProvideListSpeciality)
            vc.serviceProvideList = serviceProvideList
        }
        
    }
    
}
extension ServiceProviderListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "List", sender: arryaServiceProvideListSpeciality[indexPath.row])
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryaServiceProvideListSpeciality.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! MyPhpCollCell
        cell.lblTitleName.text = arryaServiceProvideListSpeciality[indexPath.row].category_name
        cell.imageMenuIcon.kf.setImage(with: URL(string:arryaServiceProvideListSpeciality[indexPath.row].category_image), placeholder: #imageLiteral(resourceName: "ic_placeholder_logo"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 413.5 * _widthRatio / 2 , height: 170)
        
    }
}
extension ServiceProviderListVC{
    func getServiceProviderSpeciality(){
        
        var dic = [String:Any]()
        dic["category_id"] = serviceProvideList.category_id
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getServiceProviderSpeciality.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaServiceProvideListSpeciality.removeAll()
                                self.myColView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for str in data {
                                            arryaServiceProvideListSpeciality.append(ServiceProvideListSpeciality(dic: str as! NSDictionary))
                                            //
                                        }
                                        search = arryaServiceProvideListSpeciality
                                    }
                                }else{
                                    self.myColView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.myColView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
