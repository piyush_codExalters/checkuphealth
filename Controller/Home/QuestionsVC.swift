//
//  QuestionsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit

class QuestionsCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var lblReplied: LabelMedium!
    @IBOutlet weak var imageDocter: UIImageView!
    @IBOutlet weak var lblDocterName: LabelSemiBold!
    @IBOutlet weak var lblDate: LabelMedium!
    @IBOutlet weak var viewBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        viewBg.makeCornerRound(redias: 5)
        if reuseIdentifier == "Docter"{
            imageDocter.makeCornerRoundxs(redias : 5)
        }
    }
}
class QuestionsVC: ParentViewController {

    var arryQuestionList = [QuestionList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset.bottom = 65 * _widthRatio

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        questionlist()
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "Question"{
            let vc = segue.destination as! QuestionDetailsVC
            vc.questionList = (sender as! QuestionList)
        }
    }
    
    
    @IBAction func btnAskClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AskQuestionVC")  as! AskQuestionVC
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    

}
extension QuestionsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "Question", sender: arryQuestionList[indexPath.section])

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryQuestionList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 1{
            if indexPath.row == 0{
                return 38 * _widthRatio
            }
            return UITableView.automaticDimension
        }
        if indexPath.row == 0{
            return 38 * _widthRatio
        }
        if indexPath.row == 2{
            return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arryQuestionList[section].arryaAnswer.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Date") as! QuestionsCell
                cell.lblDate.text = arryQuestionList[indexPath.section].questionDate
                cell.constraintTop.constant = 5
                cell.constraintBottom.constant = -5
                return cell
        }
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Descption") as! QuestionsCell
            if arryQuestionList[indexPath.section].arryaAnswer.count > 0{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
            }else{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
            }
            cell.lblDate.text = arryQuestionList[indexPath.section].question
            return cell
        }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Docter") as! QuestionsCell
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = 5
            cell.lblDocterName.text = arryQuestionList[indexPath.section].arryaAnswer[indexPath.row - 2].answerDoctorName

        cell.imageDocter.kf.setImage(with: URL(string:arryQuestionList[indexPath.section].arryaAnswer[indexPath.row - 2].answerDoctorImage), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
            cell.lblDate.text = arryQuestionList[indexPath.section].arryaAnswer[indexPath.row - 2].answer_title
            cell.lblReplied.text = "\(arryQuestionList[indexPath.section].arryaAnswer[indexPath.row - 2].totalAnswer!) Replied"
            return cell
    }
    
    
    
    
}
extension QuestionsVC{
    func questionlist(){
        
        var dic = [String:Any]()
        dic["patientId"] = _currentUser.patient_id
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "questionlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryQuestionList.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for str in data {
                                            arryQuestionList.append(QuestionList(dic: str as! NSDictionary))
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
