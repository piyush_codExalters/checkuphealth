//
//  BookInformationVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 24/06/2022.
//

import UIKit
extension StringProtocol {
    var html2AttStr: NSAttributedString? {
        try? NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)

    }
}
class BookInformationVC: ParentViewController {
    @IBOutlet weak var textinfromtion: UITextView!

    let str = """
<p><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;">CheckUp is not for emergencies. Call 999 now if you have any of these:</span></strong></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Signs of a heart attack -</strong> pain like a very tight band, heavy weight or squeezing in the centre of your chest</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Signs of a stroke -</strong> face dropping on one side, can&rsquo;t hold both arms up, difficulty speaking</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Tried to end your life -</strong> by taking something or harming yourself</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Severe difficulty breathing -</strong> not being able to get words out, choking or gasping</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Heavy bleeding &nbsp;-</strong> spraying, pouring or enough to make a puddle</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Severe injuries -</strong> after a serious accident</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Seizure (fit) -</strong> someone is shaking or jerking because of a fit, or is unconscious (can&rsquo;t be woken up)</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Sudden, rapid swelling -</strong> of the lips, mouth, throat or tongue</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;"><strong>Deaf people can use 18000 to contact 999 using text relay.</strong></span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;">I have none of above</span></p>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px;">I confirm that I live, or will be located in England, Scotland, Wales, Northern Ireland at the time of my appointment.</span></p>
""".html2AttStr
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textinfromtion.attributedText = str
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BookTimeVC")  as! BookTimeVC
        navigationController?.pushViewController(mapVc1, animated: true)
       
    }
    
}
