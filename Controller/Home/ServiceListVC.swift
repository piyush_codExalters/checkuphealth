//
//  ServiceListVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 21/04/21.
//

import UIKit

class ServiceListVC: ParentViewController, UITextFieldDelegate {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var textSearch: JPWidthTextField!
    @IBOutlet weak var lblTitleHeader: LabelMedium!
    var strHeader = ""
    var serviceProvideList : ServiceProvideList!
    var serviceProvideListSpeciality : ServiceProvideListSpeciality!
    var arryaServiceDetails = [ServiceDetails]()
    var search = [ServiceDetails]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias: 5)
        if let _ = serviceProvideList{
            if serviceProvideList.category_name == "Doctors"{
                lblTitleHeader.text = "General Practitioner"
            }else{
                lblTitleHeader.text = serviceProvideList.category_name
            }
        }else{
            lblTitleHeader.text = serviceProvideList.category_name
        }
        getServiceProviderList()
    }
    
    @IBAction func textEditSearch(_ sender: UITextField) {
        arryaServiceDetails = search.filter { $0.provider_first_name .localizedCaseInsensitiveContains(sender.text!) || $0.provider_last_name .localizedCaseInsensitiveContains(sender.text!)}
        if sender.text == ""{
            arryaServiceDetails = search
        }
        self.tableView.reloadData()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        arryaServiceDetails = search
        self.tableView.reloadData()
        return true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Details"{
            let vc = segue.destination as! ProfileDetailsVC
            vc.serviceDetails = (sender as! ServiceDetails)
        }
        
    }
    @IBAction func btnCallClicked(_ sender: UIButton) {
        callNumber(phoneNumber: arryaServiceDetails[sender.tag].provider_phone)
    }
    
    
}
extension ServiceListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
            performSegue(withIdentifier: "Details", sender: arryaServiceDetails[indexPath.section])
            
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else{
            return 72 * _widthRatio
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaServiceDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
            cell.lblDrName.text = arryaServiceDetails[indexPath.section].provider_first_name + " " + arryaServiceDetails[indexPath.section].provider_last_name
            cell.imageDr.kf.setImage(with: URL(string:arryaServiceDetails[indexPath.section].provider_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
           let t1 = arryaServiceDetails[indexPath.section].provider_Street + ", " +  arryaServiceDetails[indexPath.section].provider_city
            let t2 = ", " +  arryaServiceDetails[indexPath.section].provider_state
            let t3 = ", " +  arryaServiceDetails[indexPath.section].provider_country
            cell.lblDate.text =  t1 + t2 + t3
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
            cell.btnCall.tag = indexPath.section
            return cell
        }
        
        
        
    }
    
    
    
    
}
extension ServiceListVC{
    func getServiceProviderList(){
        
        var dic = [String:Any]()
        if let _ = serviceProvideList{
            dic["category_id"] = serviceProvideList.category_id
        }else{
            dic["specialty_id"] = serviceProvideListSpeciality.specialityId
            dic["category_id"] = serviceProvideList.category_id
        }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getServiceProviderList.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaServiceDetails.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for str in data {
                                            arryaServiceDetails.append(ServiceDetails(dic: str as! NSDictionary))
                                        }
                                        search = arryaServiceDetails
                                    }
                                }else{
                                    self.myColView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    

}
