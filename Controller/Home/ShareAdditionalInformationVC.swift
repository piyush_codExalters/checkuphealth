//
//  ShareAdditionalInformationVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 26/04/21.
//

import UIKit
class AdditionalList: NSObject {
    
    
    
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t1 = SingIn()
        t1.title = "Do you have any previously diagnosed conditions??"
        t1.strValue1 = "Yes"
        t1.strValue2 = "No"
        t1.strkey = "0"
        t1.placeholder = "Enter condition or symptom"
        
        t1.strValue3 = "Have this"
        t1.strValue4 = "No longer have this"
        t1.strkey1 = "0"
        t1.placeholder1 = "Additional Notes"

        t1.keybord = .default
        
        var t2 = SingIn()
        t2.title = "Do you take any Medications?"
        t2.strValue1 = "Yes"
        t2.strValue2 = "No"
        t2.strkey = "0"
        t2.placeholder = "Enter Medication"
        t2.keybord = .default
        
        
        var t3 = SingIn()
        t3.title = "Do you have any allergies?"
        t3.strValue1 = "Yes"
        t3.strValue2 = "No"
        t3.strkey = "0"
        t3.placeholder = "Enter Allergy"
        t3.keybord = .default
        
        
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
    }
}
class ShareAdditionalInformationVC: ParentViewController, UITextViewDelegate {
    
    
    var fieldData = AdditionalList()
    
    var deleget : AskQuestionVC!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnYesClicked(_ sender: UIButton) {
        if sender.tag == 10{
            fieldData.allFields[0].strkey1 = "1"
            tableView.reloadData()
        }else{
            fieldData.allFields[sender.tag].strkey = "1"
            tableView.reloadData()
        }
    }
    @IBAction func btnNoClicekd(_ sender: UIButton) {
        if sender.tag == 10{
            fieldData.allFields[0].strkey1 = "0"
            tableView.reloadData()
        }else{
            fieldData.allFields[sender.tag].strkey = "0"
            tableView.reloadData()
        }
       
    }
    
    @IBAction func btnEditTExt(_ sender: UITextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[0].strcode = textView.text!
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        deleget.getShareAddition(information: fieldData)
    }
    
    
}
extension ShareAdditionalInformationVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if fieldData.allFields[indexPath.section].strkey == "1"{
                if indexPath.row == 0{
                    return UITableView.automaticDimension
                }
                if indexPath.row == 1 || indexPath.row == 3{
                    return 60 * _widthRatio
                }
                if indexPath.row == 2{
                    return 90 * _widthRatio
                }
                return 150 * _widthRatio
                
            }else{
                if indexPath.row == 0{
                    return UITableView.automaticDimension
                }
                if indexPath.row == 1{
                    return 60 * _widthRatio
                }
                return 0
                
            }
        }else{
            if indexPath.row == 0{
                return 30 * _widthRatio
            }
            if fieldData.allFields[indexPath.section].strkey == "1"{
                if indexPath.row == 1{
                    return 60 * _widthRatio
                }else{
                    return 90 * _widthRatio
                }
            }else{
                if indexPath.row == 1{
                    return 60 * _widthRatio
                }else{
                    return 0 * _widthRatio
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }else{
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = fieldData.allFields[indexPath.section].title
                return cell
                
            }
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Gender") as! ProfileCell
                cell.lblName.text = ""
                cell.btnMale.setTitle("  " + fieldData.allFields[indexPath.section].strValue1, for: .normal)
                cell.btnFemale.setTitle("  " + fieldData.allFields[indexPath.section].strValue2, for: .normal)
                cell.btnMale.setTitle("  " + fieldData.allFields[indexPath.section].strValue1, for: .selected)
                cell.btnFemale.setTitle("  " + fieldData.allFields[indexPath.section].strValue2, for: .selected)
                cell.btnMale.tag = indexPath.section
                cell.btnFemale.tag = indexPath.section
                cell.btnMale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
                cell.btnFemale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
                if fieldData.allFields[indexPath.section].strkey == "1"{
                    cell.btnMale.isSelected = true
                    cell.btnFemale.isSelected = false
                    cell.viewConstretTop.constant = 5
                    cell.viewConstretBottom.constant = -5
                }else{
                    cell.btnMale.isSelected = false
                    cell.btnFemale.isSelected = true
                    cell.viewConstretTop.constant = 5
                    cell.viewConstretBottom.constant = 5
                }
                return cell
            }
            if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Gender") as! ProfileCell
                cell.lblName.text = ""
                cell.btnMale.setTitle("  " + fieldData.allFields[indexPath.section].strValue3, for: .normal)
                cell.btnFemale.setTitle("  " + fieldData.allFields[indexPath.section].strValue4, for: .normal)
                cell.btnMale.setTitle("  " + fieldData.allFields[indexPath.section].strValue3, for: .selected)
                cell.btnFemale.setTitle("  " + fieldData.allFields[indexPath.section].strValue4, for: .selected)
                cell.btnMale.tag = 10
                cell.btnFemale.tag = 10
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                cell.btnMale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
                cell.btnFemale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
                if fieldData.allFields[indexPath.section].strkey1 == "1"{
                    cell.btnMale.isSelected = true
                    cell.btnFemale.isSelected = false
                }else{
                    cell.btnMale.isSelected = false
                    cell.btnFemale.isSelected = true
                }
                return cell
            }
            
            if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
                cell.textEmail.tag = indexPath.section
                cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
                cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
                cell.textEmail.inputView = nil
                cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
            cell.lblName.text = fieldData.allFields[indexPath.section].strcode
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant  = 5
            cell.textAreaNAme.tag = indexPath.section
            cell.textAreaNAme.text = fieldData.allFields[indexPath.section].strcode
            return cell
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = fieldData.allFields[indexPath.section].title
                return cell
                
            }
            
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Gender") as! ProfileCell
                cell.lblName.text = ""
                cell.btnMale.setTitle("  " + fieldData.allFields[indexPath.section].strValue1, for: .normal)
                cell.btnFemale.setTitle("  " + fieldData.allFields[indexPath.section].strValue2, for: .normal)
                cell.btnMale.setTitle("  " + fieldData.allFields[indexPath.section].strValue1, for: .selected)
                cell.btnFemale.setTitle("  " + fieldData.allFields[indexPath.section].strValue2, for: .selected)
                cell.btnMale.tag = indexPath.section
                cell.btnFemale.tag = indexPath.section
                cell.btnMale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
                cell.btnFemale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
                if fieldData.allFields[indexPath.section].strkey == "1"{
                    cell.btnMale.isSelected = true
                    cell.btnFemale.isSelected = false
                    cell.viewConstretTop.constant = 5
                    cell.viewConstretBottom.constant = -5
                }else{
                    cell.btnMale.isSelected = false
                    cell.btnFemale.isSelected = true
                    cell.viewConstretTop.constant = 5
                    cell.viewConstretBottom.constant = 5
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
                cell.textEmail.tag = indexPath.section
                cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
                cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
                cell.textEmail.inputView = nil
                cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = 5
                return cell
            }
            
        }
        
        
    }
    
    
}


