//
//  DRListVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 17/05/21.
//

import UIKit
protocol getFilter {
    func getFilterName(strSpecial: String, strGende: String,indSpecial: Int, indGende: Int)
}
class DRListVC: ParentViewController, getFilter {
    @IBOutlet weak var lblTitleHeader: LabelMedium!

    var strDate = ""
    var strDay = ""
    var strTime = ""
    var isDoctor = true
    var arryaDocterDetailes = [DocterDetailes]()
    var arryaNurseDetailes = [NurseDetailes]()
    var search = [DocterDetailes]()
    var strSpecialsd = 0
    var strGendesd = 0
    var drFilterList = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getFirebaseAnalytics(screenName : getEventName.SELECT_DOCTOR,isPatient_id : true)
        getdoctorlisttimeslotwise(booking_day: strDay,booking_date: strDate,timecurent: strTime)
    }
    
    @IBAction func btnBookClicked(_ sender: UIButton) {
                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ConsultationTypeVC")  as! ConsultationTypeVC
                   mapVc1.isDoctor = true
                   mapVc1.docterDetailes = arryaDocterDetailes[sender.tag]
                   mapVc1.strDay = strDay
                   mapVc1.strTime = strTime
                   mapVc1.strDate = strDate
                navigationController?.pushViewController(mapVc1, animated: true)
    }
    
   
    @IBAction func btnFilter(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "FilterDoctorVC")  as! FilterDoctorVC
        
        mapVc1.isSpecialInterestIsected = strSpecialsd
        mapVc1.isGenderIsected = strGendesd
        mapVc1.daleget = self
        mapVc1.arryaspectization = drFilterList
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    func getFilterName(strSpecial: String, strGende: String,indSpecial: Int, indGende: Int){
        strSpecialsd = indSpecial
        strGendesd = indGende
        arryaDocterDetailes = search
        if strSpecial == "" && strGende == ""{
            arryaDocterDetailes = search
       }else if strSpecial == "" && strGende != ""{
            arryaDocterDetailes = search.filter { $0.doctor_gender == (strGende) }
        }else if strSpecial != "" && strGende == ""{
            arryaDocterDetailes = search.filter {$0.doctor_specialisation_interest.localizedCaseInsensitiveContains(strSpecial) }
        }else{
            arryaDocterDetailes = search.filter { $0.doctor_gender  == (strGende) && $0.doctor_specialisation_interest.localizedCaseInsensitiveContains(strSpecial) }
        }
        self.tableView.reloadData()
        _ = self.navigationController?.popViewController(animated: true)

    }

}
extension DRListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
//        if indexPath.row == 0{
//            if isDoctor{
//                if arryaDocterDetailes.count > indexPath.section{
//                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DrDetailsVC")  as! DrDetailsVC
//                    mapVc1.isDoctor = true
//                    mapVc1.docterDetailes = arryaDocterDetailes[indexPath.section]
//                    navigationController?.pushViewController(mapVc1, animated: true)
//                }else{
//                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DrDetailsVC")  as! DrDetailsVC
//                    mapVc1.isDoctor = false
//                    mapVc1.nurseDetailes = arryaNurseDetailes[indexPath.section - arryaDocterDetailes.count]
//                    navigationController?.pushViewController(mapVc1, animated: true)
//                }
//
//            }else{
//                performSegue(withIdentifier: "Details", sender: arryaNurseDetailes[indexPath.section])
//
//            }
//        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return arryaDocterDetailes.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio

        }else{
            return 72 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
            cell.imageDr.kf.setImage(with: URL(string:arryaDocterDetailes[indexPath.section].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
                cell.lblExp.text = arryaDocterDetailes[indexPath.section].doctor_specialisation_interest
                cell.lblFree.text = _currency.currency_vSymbol + "\(arryaDocterDetailes[indexPath.section].doctor_fees!)"
                cell.lblDrName.text = arryaDocterDetailes[indexPath.section].doctor_first_name + " " + arryaDocterDetailes[indexPath.section].doctor_last_name
           
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
        cell.btnBook.tag = indexPath.section
        return cell
    }
    
    
    
    
}

extension DRListVC{
    func getdoctorlisttimeslotwise(booking_day: String,booking_date: String,timecurent: String){
        
        var dic = [String:Any]()
        dic["booking_day"] = booking_day
        dic["booking_date"] = booking_date
        dic["booking_time"] = timecurent
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getdoctorlisttimeslotwise_New.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaDocterDetailes.removeAll()
                             //   self.nurseDetailes = nil
                                drFilterList.removeAll()
                                
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data {
                                            let newObj = obj as! NSDictionary
                                            var arryaDrSpe = [String]()
                                            arryaDocterDetailes.append(DocterDetailes(dic: obj as! NSDictionary))
                                            arryaDrSpe = newObj.getStringValue(key: "doctor_specialisation_interest").components(separatedBy: ",")
                                            for new in arryaDrSpe {
                                                drFilterList.append(new)
                                            }
                                        }
                                        drFilterList = Array(Set(drFilterList))
                                        drFilterList.append("Any")
                                        drFilterList.reverse()
                                        search = arryaDocterDetailes
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
