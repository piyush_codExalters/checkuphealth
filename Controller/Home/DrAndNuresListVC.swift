//
//  DrAndNuresListVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 16/04/21.
//

import UIKit
class DrAndNuresListCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imageDr: UIImageView!
    @IBOutlet weak var btnBook: submitButton!
    @IBOutlet weak var btnCall: addButton!
    @IBOutlet weak var lblRs: LabelMedium!
    @IBOutlet weak var viewRs: UIView!
    @IBOutlet weak var viewConstretBottom: NSLayoutConstraint!
    @IBOutlet weak var lblActive: LabelSemiBold!
    @IBOutlet weak var lblDate: LabelMedium!
    @IBOutlet weak var lblDrName: LabelSemiBold!
    @IBOutlet weak var lblRequestModule: LabelRegular!
    @IBOutlet weak var lblExp: LabelMedium!
    @IBOutlet weak var lblFree: LabelSemiBold!
    @IBOutlet weak var widhtCall: NSLayoutConstraint!
    
    @IBOutlet weak var lblDateTime: LabelRegular!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias: 5)
        if reuseIdentifier == "DrDetails"{
            imageDr.makeCornerRoundxs(redias : 5)
        }

    }
}
class DrAndNuresListVC: ParentViewController {
    
    @IBOutlet weak var lblTitleHeader: LabelMedium!
    var strHeader = ""
    var isDoctor = false
    var doctorspeciality : Doctorspeciality!
    var nursespeciality : Doctorspeciality!
    var arryaDocterDetailes = [DocterDetailes]()
    var arryaNurseDetailes = [NurseDetailes]()
    var isQustion = false
    var questionId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if isDoctor{
                if isQustion{
                    lblTitleHeader.text = "Question List"
                    answerDoctorlist()
                }else{
                    lblTitleHeader.text = doctorspeciality.specialityTitle
                    doctorlist(str : doctorspeciality.specialityId)
                }
            
        }else{
            lblTitleHeader.text = nursespeciality.specialityTitle
            nurselist(str : nursespeciality.specialityId)
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Details"{
            let vc = segue.destination as! DrDetailsVC
            if isDoctor{
                vc.docterDetailes = (sender as! DocterDetailes)
            }else{
                vc.nurseDetailes = (sender as! NurseDetailes)
            }
            vc.isDoctor = isDoctor
        }
        if segue.identifier == "Consultation"{
            let vc = segue.destination as! ConsultationTypeVC
            if isDoctor{
                vc.docterDetailes = (sender as! DocterDetailes)
            }else{
                vc.nurseDetailes = (sender as! NurseDetailes)
            }
            vc.isDoctor = isDoctor
        }
    }

    @IBAction func btnBookClicked(_ sender: UIButton) {
        if isDoctor{
            if arryaDocterDetailes.count > sender.tag{
                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ConsultationTypeVC")  as! ConsultationTypeVC
                mapVc1.isDoctor = true
                mapVc1.docterDetailes = arryaDocterDetailes[sender.tag]
                navigationController?.pushViewController(mapVc1, animated: true)
            }else{
                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ConsultationTypeVC")  as! ConsultationTypeVC
                mapVc1.isDoctor = false
                mapVc1.nurseDetailes = arryaNurseDetailes[sender.tag - arryaDocterDetailes.count]
                navigationController?.pushViewController(mapVc1, animated: true)
            }
        }else{
            performSegue(withIdentifier: "Consultation", sender: arryaNurseDetailes[sender.tag])

        }
    }
    
    @IBAction func btnCallClikced(_ sender: UIButton) {
        if isDoctor{
            callNumber(phoneNumber: arryaDocterDetailes[sender.tag].doctor_phone)
        }else{
            callNumber(phoneNumber: arryaNurseDetailes[sender.tag].nurse_phone)
        }
        
    }
    
}

extension DrAndNuresListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isWorkingCountryUK(){
            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ConfirmPaymentVC")  as! ConfirmPaymentVC
            //mapVc1.sreFress = arryaDocterDetailes[indexPath.section].doctor_fees
            navigationController?.pushViewController(mapVc1, animated: true)
            return
        }
        if indexPath.row == 0{
            if isDoctor{
                if arryaDocterDetailes.count > indexPath.section{
                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DrDetailsVC")  as! DrDetailsVC
                    mapVc1.isDoctor = true
                    mapVc1.docterDetailes = arryaDocterDetailes[indexPath.section]
                    navigationController?.pushViewController(mapVc1, animated: true)
                }else{
                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DrDetailsVC")  as! DrDetailsVC
                    mapVc1.isDoctor = false
                    mapVc1.nurseDetailes = arryaNurseDetailes[indexPath.section - arryaDocterDetailes.count]
                    navigationController?.pushViewController(mapVc1, animated: true)
                }

            }else{
                performSegue(withIdentifier: "Details", sender: arryaNurseDetailes[indexPath.section])

            }
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isDoctor{
            return arryaDocterDetailes.count + arryaNurseDetailes.count
        }else{
            return arryaNurseDetailes.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio

        }else{
            return 72 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
            if isDoctor{
                if arryaDocterDetailes.count > indexPath.section{
                    cell.imageDr.kf.setImage(with: URL(string:arryaDocterDetailes[indexPath.section].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                    }
                    if isWorkingCountryUK() {
                        cell.lblExp.text = arryaDocterDetailes[indexPath.section].doctor_specialintrest
                        } else {
                        cell.lblExp.text = arryaDocterDetailes[indexPath.section].doctor_specialization
                        }
                    cell.lblFree.text = _currency.currency_vSymbol + "\(arryaDocterDetailes[indexPath.section].doctor_fees!)"
                    cell.lblDrName.text = arryaDocterDetailes[indexPath.section].doctor_first_name + " " + arryaDocterDetailes[indexPath.section].doctor_last_name
                }else{
                    cell.imageDr.kf.setImage(with: URL(string:arryaNurseDetailes[indexPath.section - arryaDocterDetailes.count].nurse_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                    }
                    cell.lblExp.text = arryaNurseDetailes[indexPath.section  - arryaDocterDetailes.count].nurse_specialization
                    cell.lblFree.text = _currency.currency_vSymbol + "\(arryaNurseDetailes[indexPath.section  - arryaDocterDetailes.count].nurse_fees!)"
                    cell.lblDrName.text = arryaNurseDetailes[indexPath.section  - arryaDocterDetailes.count].nurse_first_name + " " + arryaNurseDetailes[indexPath.section  - arryaDocterDetailes.count].nurse_last_name
                }
               
            }else{
                cell.imageDr.kf.setImage(with: URL(string:arryaNurseDetailes[indexPath.section].nurse_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                }
                cell.lblExp.text = arryaNurseDetailes[indexPath.section].nurse_specialization
                cell.lblFree.text = _currency.currency_vSymbol + "\(arryaNurseDetailes[indexPath.section].nurse_fees!)"
                cell.lblDrName.text = arryaNurseDetailes[indexPath.section].nurse_first_name + " " + arryaNurseDetailes[indexPath.section].nurse_last_name
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
        if isWorkingCountryUK() {
            cell.btnCall.isHidden = true
        } else {
            cell.btnCall.isHidden = false
        }
            if isDoctor{
                if arryaDocterDetailes[indexPath.section].isListingpurpose == "1"{
                    cell.btnBook.isHidden = true
                    cell.widhtCall.constant = 150

                }else{
                    cell.btnBook.isHidden = false
                    cell.widhtCall.constant = 18

                }
            }else{
                if arryaNurseDetailes[indexPath.section].isListingpurpose == "1"{
                    cell.btnBook.isHidden = true
                    cell.widhtCall.constant = 150

                }else{
                    cell.btnBook.isHidden = false
                    cell.widhtCall.constant = 18

                }
            }
            
       
        cell.btnCall.tag = indexPath.section
        cell.btnBook.tag = indexPath.section
        return cell
    }
    
    
    
    
}
extension DrAndNuresListVC{
    
    func doctorlist(str : String){
        let parameters = "{\"data\":{\"spaciality_id\":\"\(str)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "doctorlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaDocterDetailes.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaDocterDetailes.append(DocterDetailes(dic: obj as! NSDictionary))
                                }
                            }
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func nurselist(str : String){
        let parameters = "{\"data\":{\"spaciality_id\":\"\(str)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "nurselist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaNurseDetailes.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaNurseDetailes.append(NurseDetailes(dic: obj as! NSDictionary))
                                }
                            }
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func answerDoctorlist(){
        
        var dic = [String:Any]()
        dic["questionId"] = questionId
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "answerDoctorlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaDocterDetailes.removeAll()
                                self.arryaNurseDetailes.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        if let docter = data.value(forKey: "DoctorList") as? NSArray{
                                            for obj in docter{
                                                arryaDocterDetailes.append(DocterDetailes(dic: obj as! NSDictionary))
                                            }
                                        }
                                        if let data = data.value(forKey: "NurseList") as? NSArray{
                                            for obj in data{
                                                arryaNurseDetailes.append(NurseDetailes(dic: obj as! NSDictionary))
                                            }
                                        }
                                    }

                                    
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
