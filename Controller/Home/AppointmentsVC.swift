//
//  AppointmentsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit
protocol dalegetFilter {
    func getFilter(str : String,Index : Int)
}
class AppointmentsVC: ParentViewController, dalegetFilter {
   
    @IBOutlet weak var lblTitleNAme: LabelSemiBold!
    

    @IBOutlet weak var lblFilter: LabelSemiBold!
    var arryaAppointmenthistory = [Appointmenthistory]()
    var strFlter = "all"
    var IndexNumbrt = 0
    var appoinmentID = ""
    var isFor = ""
    var isCall = false
    override func viewDidLoad() {
        super.viewDidLoad()
        strFlter = "all"
        let FormattedText1 = NSMutableAttributedString()
        FormattedText1
            .viewAll("Filter")
        self.lblFilter.attributedText = FormattedText1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isCall{
            strFlter = "all"
            IndexNumbrt = 0
        }
        if isCall{
            isCall = false
        }
        appointmenthistory()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Filter"{
            let vc = segue.destination as! UpcomingFilterVC
            vc.dalegate = self
        }
        if segue.identifier == "Details"{
            let vc = segue.destination as! AppointmentDetailsVC
            vc.appoinmentID = appoinmentID
            vc.isFor = isFor
        }
    }
    
    
    @IBAction func btnFilted(_ sender: Any) {
        performSegue(withIdentifier: "Filter", sender: nil)

    }
    func getFilter(str: String, Index: Int) {
        strFlter = str
        IndexNumbrt = Index
        appointmenthistory()
        self.dismiss(animated: true, completion: nil)
    }

}
extension AppointmentsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appoinmentID = arryaAppointmenthistory[indexPath.row].appointment_id
        isFor = arryaAppointmenthistory[indexPath.row].appointment_for
        isCall = true
        performSegue(withIdentifier: "Details", sender: arryaAppointmenthistory[indexPath.row].appointment_id)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 117 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arryaAppointmenthistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "status") as! HomeCell
        cell.lblDocterName.text = arryaAppointmenthistory[indexPath.row].doctor_first_name + " " + arryaAppointmenthistory[indexPath.row].doctor_last_name
        
        if isWorkingCountryUK() {
            cell.lblInformation.text = arryaAppointmenthistory[indexPath.row].doctor_specialisation_interest + "\n" + arryaAppointmenthistory[indexPath.row].doctor_experience_clinic_name
        } else {
            cell.lblInformation.text = arryaAppointmenthistory[indexPath.row].doctor_specialization + "\n" + arryaAppointmenthistory[indexPath.row].doctor_experience_clinic_name
        }
        cell.imagePesant.kf.setImage(with: URL(string:arryaAppointmenthistory[indexPath.row].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        cell.lblStatus.text = arryaAppointmenthistory[indexPath.row].status
        return cell
        
    }
    
    
    
    
}
extension AppointmentsVC{
    func appointmenthistory(){
        showCentralSpinner()
        lblTitleNAme.text = strFlter.capitalized
        let parameters = "{\"data\":{\"patient_id\":\"\(_currentUser.patient_id!)\",\"type\":\"\(strFlter)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "appointmenthistory.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaAppointmenthistory.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaAppointmenthistory.append(Appointmenthistory(dic: obj as! NSDictionary))
                                }
                            }
                            if arryaAppointmenthistory.count == 0{
                                self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "No appointments to show!", image : nil)
                            }
                            
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
