//
//  CardDetailsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 17/05/21.
//

import UIKit
import WebKit
import FBSDKCoreKit

class CardDetailsVC: ParentViewController , WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate{
    
    @IBOutlet weak var webCardShow: WKWebView!
    
    
    var isCHeck = "0"
    var nOrganizationId = "0"
    var usedCredits  : Float = 0.0
    var arryaProblemQuestionList = [ProblemQuestionList]()
    var docterDetailes : DocterDetailes!
    var subcription : Subcription!
    var strTime = ""
    var strdate = ""
    var fieldData = AppointmentList()
    var appointmentType = ""
    var selectAddres : AddressListName!
    var deliverylist : Deliverylist!
    var pharmacies : Pharmacies!
    var uploadfile : Data?
    var imgUploadfile : UIImage?
    var problemId = ""
    var isPersnal = ""
    var isOrganisation = ""
    var nurseDetailes : NurseDetailes!
    var isDoctor = false
    var isEmail = "0"
    var isPhone = "0"
    var amountPayableByWorldPay : Float = 0.0
    var organisationId = ""
    var organisationWalletAmount : Float = 0.0
    var personalWallet : Float = 0.0
    var cupanid = ""
    var cupStr = ""
    var Discount : Float = 0.0
    var walletAmountUsed : Float = 0.0
    var dCapValue : Float = 0.0
    var dValue : Float = 0.0
    var nType : Float = 0.0
    var isSubcription = 0
    var referralList = ""
    var strAppointment = ""
    var strProblemNote = ""
    var nToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webCardShow.load(NSURLRequest(url: NSURL(string: _worldpaybaseUrl)! as URL) as URLRequest)
        webCardShow.navigationDelegate = self
        webCardShow.uiDelegate = self
        webCardShow.scrollView.delegate = self
        getFirebaseAnalytics(screenName : getEventName.ADD_CARD_DETAILS,isPatient_id : true)
    }
  
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       
        
        if let currentURL = webView.url?.absoluteString{
            if currentURL != _worldpaybaseUrl{
                 print(currentURL)
                let fileArray = currentURL.components(separatedBy: "?token=")
                var finalFileName = fileArray.last
                nToken = finalFileName!
                print(finalFileName!)
                saveappointment()
            }else{
                hideCentralSpinner()
            }
        }
    }
                                                                                                                                                                                      
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showCentralSpinner()
    }
   
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
          scrollView.pinchGestureRecognizer?.isEnabled = true
    }
    
}

extension CardDetailsVC{
  
    func saveappointment(){
        
        var dic = [String:Any]()
        var email = ""
        var phone = ""
        
        // Email OR mobile number verifier
        if isEmail == "1"{
            email = "1"
        }
        if isPhone == "1"{
            if email == ""{
                phone = "2"
            }else{
                phone = ",2"
            }
        }
        
        if let _ = subcription{
            if subcription.arryaActivesubscription.count > 0{
                dic["nSubscriptionId"] = subcription.arryaActivesubscription[isSubcription].id
                dic["nSubscriptionlogId"] = subcription.arryaActivesubscription[isSubcription].nSubscriptionlogId
            }else{
                dic["nSubscriptionId"] = subcription.arryaSubscription[isSubcription].id
                dic["nSubscriptionlogId"] = ""
            }
        }
        /* if let _ = pharmacies {
         dic["pharmacyId"] = pharmacies.id
         dic["pharmacyName"] = pharmacies.name
         }else{
         dic["pharmacyId"] = ""
         dic["pharmacyName"] = ""
         }
         if let _ = deliverylist {
         dic["deliveryId"] = deliverylist.id
         dic["deliveryName"] = deliverylist.name
         }else{
         dic["deliveryId"] = ""
         dic["deliveryName"] = ""
         }
         */
        if let _ = selectAddres {
            dic["addressId"] = selectAddres.address_id
            dic["postcode"] = selectAddres.postcode
        }else{
            dic["addressId"] = ""
            dic["postcode"] = ""
        }
        if referralList != ""{
            dic["nReferralId"] = referralList
        }else{
            dic["nReferralId"] =  ""
        }
        if isOrganisation == "1"{
            dic["nWalletType"] = "1" // Organisation 1
            dic["dUseWalletAmount"] = walletAmountUsed.roundedFlaot(toPlaces: 1)
        }else if isPersnal == "1"{
            dic["nWalletType"] = "2" // Persnal 2
            dic["dUseWalletAmount"] = walletAmountUsed.roundedFlaot(toPlaces: 1)
        }else{
            dic["nWalletType"] = "" // not Use
            dic["dUseWalletAmount"] = "0"
        }
        if uploadfile != nil{
            dic["vReferralFile"] = "\(uploadfile!.base64EncodedString())"
            dic["referralFileFormat"] = fieldData.allFields[4].strValue.getPathExtension()
        }
        if imgUploadfile != nil{
            let imageResized = imgUploadfile!.resizeWith(percentage: 0.25)
            let base64 = imageResized?.toBase64()
            dic["referralFileFormat"] = "jpg"
            dic["vReferralFile"] = "\(base64!)"
        }
        if isWorkingCountryUK(){
            var arryaQuest = [String]()
            dic["nProblemId"] = problemId
            for obj in arryaProblemQuestionList {
                var str = ""
                if obj.isYes{
                    str = "1"
                }else{
                    str = "0"
                }
                arryaQuest.append("\(obj.id!),\(str)")
            }
            dic["vQuestion"] = arryaQuest
        }
        dic["token"] = nToken
        dic["vNote"] = strProblemNote
        dic["payment_amount"] = subcription.arryaSubscription[isSubcription].dAmount
        dic["vPreferredContactMethod"] = email + phone
        dic["doctor_id"] = docterDetailes.doctor_id
        dic["patient_id"] = _currentUser.patient_id
        dic["booking_date"] = strdate
        dic["booking_time"] = strTime
        dic["description"] = strAppointment
        dic["appointment_type"] = appointmentType
        dic["payment_method"] = "CREDIT"
        dic["payment_orderCode"] = ""
        dic["payment_currencyCode"] = ""
        dic["payment_expiryMonth"] = ""
        dic["payment_expiryYear"] = ""
        dic["payment_cardType"] = ""
        dic["payment_maskedCardNumber"] = ""
        dic["transaction_id"] = ""
        dic["currency_vSymbol"] = _currency.currency_vSymbol
        dic["currency_conversionRate"] = _currency.currency_conversionRate
        dic["payment_charge"] = amountPayableByWorldPay.roundedFlaot(toPlaces: 1)
        dic["paymentStatus"] = "SUCCESS"
        dic["nCouponId"] = cupanid
        dic["dDiscountPrice"] = Discount.roundedFlaot(toPlaces: 1)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "saveappointment.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getFirebaseAnalytics(screenName : getEventName.APPOITMENT_BOOKED,isPatient_id : true)
                                    let parameters = [
                                        AppEvents.ParameterName("Name").rawValue: "\(docterDetailes.doctor_first_name!)" + " " +  "\(docterDetailes.doctor_last_name!)",
                                        AppEvents.ParameterName("Type").rawValue: "doctor",
                                        AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                                        AppEvents.ParameterName("Timeslot").rawValue: "\(self.strdate)" + "\(self.strTime)",
                                        AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                                        AppEvents.ParameterName("Appointment ID").rawValue: "\(Response.getIntValue(key: "AppointmentId"))",
                                        AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                                        AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                                        AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
                                        
                                    ] as [String : Any]
                                    AppEvents.logEvent(.init("Appointment Booked"), parameters: parameters)
                                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDoneVC")  as! AppointmentDoneVC
                                    vc.docterDetailes = docterDetailes
                                    vc.isDoctor = isDoctor
                                    vc.strTime = strTime
                                    vc.strdate = strdate
                                    vc.imageProfile = docterDetailes.doctor_profile_image
                                    vc.FullName = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                                    if isWorkingCountryUK() {
                                        vc.Specialist = docterDetailes.doctor_specialization
                                    } else {
                                        vc.Specialist = docterDetailes.doctor_specialization
                                    }
                                    navigationController?.pushViewController(vc, animated: true)
                                }else{
                                    self.showResponseMessage(dict: Response)
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}
