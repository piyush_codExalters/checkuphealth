//
//  SerachDoctorVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 16/04/21.
//

import UIKit
class SerachDrAndNurseCell: ConstrainedTableViewCell {
    
    
    @IBOutlet weak var coectionHight: NSLayoutConstraint!
    @IBOutlet weak var collectionNAme: UICollectionView!
    weak var serach : SerachDrAndNurseVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}


class SerachDrAndNurseVC: ParentViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var lblTitlSerach: LabelMedium!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var textSearch: JPWidthTextField!
    
    
    var strSerach = ""
    var isDoctor = false
    var arryaDoctorspeciality = [Doctorspeciality]()
    var arryaNursespeciality = [Doctorspeciality]()
    var arryaDocterDetailes = [DocterDetailes]()
    var arryaNurseDetailes = [NurseDetailes]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias: 5)
        if isDoctor{
            lblTitlSerach.text = "Search Doctor"
            doctorspeciality()
        }else{
            lblTitlSerach.text = "Search Nurse"
            nursespeciality()
        }
    }
    
    @IBAction func textEditSearch(_ sender: UITextField) {
        strSerach = sender.text!
        if String.validateStringValue(str: strSerach){
            tableView.reloadData()
            return
        }
        if isDoctor{
            searchdoctor(str : strSerach)
        }else{
            searchnurse(str : strSerach)
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        strSerach = textField.text!
        if String.validateStringValue(str: strSerach){
            tableView.reloadData()
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "List"{
            let vc = segue.destination as! DrAndNuresListVC
            if isDoctor{
                vc.doctorspeciality = (sender as! Doctorspeciality)
            }else{
                vc.nursespeciality = (sender as! Doctorspeciality)
            }
            vc.isDoctor = isDoctor
        }
        if segue.identifier == "Details"{
            let vc = segue.destination as! DrDetailsVC
            if isDoctor{
                vc.docterDetailes = (sender as! DocterDetailes)
            }else{
                vc.nurseDetailes = (sender as! NurseDetailes)
            }
            vc.isDoctor = isDoctor
        }
        if segue.identifier == "Consultation"{
            let vc = segue.destination as! ConsultationTypeVC
            if isDoctor{
                vc.docterDetailes = (sender as! DocterDetailes)
            }else{
                vc.nurseDetailes = (sender as! NurseDetailes)
            }
            vc.isDoctor = isDoctor
        }
    }
    
    @IBAction func btnBookApponmentVC(_ sender: UIButton) {
        if isDoctor{
            performSegue(withIdentifier: "Consultation", sender: arryaDocterDetailes[sender.tag])

        }else{
            performSegue(withIdentifier: "Consultation", sender: arryaNurseDetailes[sender.tag])

        }
    }
    
    @IBAction func btnCallClikced(_ sender: UIButton) {
        
        if isDoctor{
            callNumber(phoneNumber: arryaDocterDetailes[sender.tag].doctor_phone)
        }else{
            callNumber(phoneNumber: arryaNurseDetailes[sender.tag].nurse_phone)
        }
    }
    
    
    
}

extension SerachDrAndNurseVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if String.validateStringValue(str: strSerach){
            return
        }
        if indexPath.row == 0{
            if isWorkingCountryUK(){
                return
            }
            if isDoctor{
                performSegue(withIdentifier: "Details", sender: arryaDocterDetailes[indexPath.section])

            }else{
                performSegue(withIdentifier: "Details", sender: arryaNurseDetailes[indexPath.section])

            }
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if String.validateStringValue(str: strSerach){
            return 1
        }
        if isDoctor{
            return arryaDocterDetailes.count
        }else{
            return arryaNurseDetailes.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if String.validateStringValue(str: strSerach){
            return UITableView.automaticDimension
        }
        
        if indexPath.row == 0{
            return 90 * _widthRatio

        }else{
            return 72 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if String.validateStringValue(str: strSerach){
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if String.validateStringValue(str: strSerach){
            let cell = tableView.dequeueReusableCell(withIdentifier: "List") as! SerachDrAndNurseCell
            cell.serach = self
            cell.collectionNAme.reloadData()
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.coectionHight.constant = cell.collectionNAme.collectionViewLayout.collectionViewContentSize.height
            cell.collectionNAme.reloadData()
            return cell
        }
        
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
            if isDoctor{
                cell.imageDr.kf.setImage(with: URL(string:arryaDocterDetailes[indexPath.section].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                }
                cell.lblExp.text = arryaDocterDetailes[indexPath.section].doctor_specialization
                cell.lblFree.text = _currency.currency_vSymbol + "\(arryaDocterDetailes[indexPath.section].doctor_fees!)"
                cell.lblDrName.text = arryaDocterDetailes[indexPath.section].doctor_first_name + " " + arryaDocterDetailes[indexPath.section].doctor_last_name
            }else{
                cell.imageDr.kf.setImage(with: URL(string:arryaNurseDetailes[indexPath.section].nurse_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                }
                cell.lblExp.text = arryaNurseDetailes[indexPath.section].nurse_specialization
                cell.lblFree.text = _currency.currency_vSymbol + "\(arryaNurseDetailes[indexPath.section].nurse_fees!)"
                cell.lblDrName.text = arryaNurseDetailes[indexPath.section].nurse_first_name + " " + arryaNurseDetailes[indexPath.section].nurse_last_name
            }
           
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
        cell.btnCall.tag = indexPath.section
        cell.btnBook.tag = indexPath.section
        if isDoctor{
            if arryaDocterDetailes[indexPath.section].isListingpurpose == "1"{
                cell.btnBook.isHidden = true
                cell.widhtCall.constant = 150

            }else{
                cell.btnBook.isHidden = false
                cell.widhtCall.constant = 18

            }
        }else{
            if arryaNurseDetailes[indexPath.section].isListingpurpose == "1"{
                cell.btnBook.isHidden = true
                cell.widhtCall.constant = 150

            }else{
                cell.btnBook.isHidden = false
                cell.widhtCall.constant = 18

            }
        }
        return cell
    }
    
    
    
    
}




extension SerachDrAndNurseCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if serach.isDoctor{
            serach.performSegue(withIdentifier: "List", sender: serach.arryaDoctorspeciality[indexPath.row])
        }else{
            serach.performSegue(withIdentifier: "List", sender: serach.arryaNursespeciality[indexPath.row])
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if serach.isDoctor{
            return serach.arryaDoctorspeciality.count
        }else{
            return serach.arryaNursespeciality.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! MyPhpCollCell
        if serach.isDoctor{
            cell.lblTitleName.text = serach.arryaDoctorspeciality[indexPath.row].specialityTitle
            cell.imageMenuIcon.kf.setImage(with: URL(string:serach.arryaDoctorspeciality[indexPath.row].specialityImage), placeholder: #imageLiteral(resourceName: "ic_placeholder_logo"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
        }else{
            cell.lblTitleName.text = serach.arryaNursespeciality[indexPath.row].specialityTitle
            cell.imageMenuIcon.kf.setImage(with: URL(string:serach.arryaNursespeciality[indexPath.row].specialityImage), placeholder: #imageLiteral(resourceName: "ic_placeholder_logo"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
        }
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 413.5 * _widthRatio / 2 , height: 170)
        
    }
}
extension SerachDrAndNurseVC{
    func doctorspeciality(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "doctorspeciality.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaDoctorspeciality.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data {
                                            arryaDoctorspeciality.append(Doctorspeciality(dic: obj as! NSDictionary))
                                        }
                                    }
                               
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func nursespeciality(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "nursespeciality.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaNursespeciality.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data {
                                            arryaNursespeciality.append(Doctorspeciality(dic: obj as! NSDictionary))
                                        }
                                    }
                               
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func searchdoctor(str : String){
        let parameters = "{\"data\":{\"search\":\"\(str)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "searchdoctor.php", param: parameters) { (response) in
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaDocterDetailes.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaDocterDetailes.append(DocterDetailes(dic: obj as! NSDictionary))
                                }
                            }
                        }else{
                         //   self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func searchnurse(str : String){
        let parameters = "{\"data\":{\"search\":\"\(str)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "searchnurse.php", param: parameters) { (response) in
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaNurseDetailes.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaNurseDetailes.append(NurseDetailes(dic: obj as! NSDictionary))
                                }
                            }
                        }else{
                         //   self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
