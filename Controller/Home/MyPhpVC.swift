//
//  MyPhpVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit
class MyPhpCollCell : ConstrainedCollectionViewCell{
    
    @IBOutlet weak var lblDate: LabelMedium!
    @IBOutlet weak var imageMenuIcon: UIImageView!
    @IBOutlet weak var lblTitleName: LabelRegular!
    @IBOutlet weak var viewBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "Menu"{
            viewBg.makeCornerRound(redias: 5)

        }
    }

}

class MyPhpCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var hightCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionMenu: UICollectionView!
    @IBOutlet weak var viewBg: UIView!
    
    weak var myphp : MyPhpVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "Request"{
            viewBg.makeCornerRound(redias: 5)

        }
    }
    
}



class MyPhpVC: ParentViewController {

    var arryaMenuIcon = [#imageLiteral(resourceName: "ic_healthconditions"),#imageLiteral(resourceName: "ic_medications"),#imageLiteral(resourceName: "ic_allergies-adverse"),#imageLiteral(resourceName: "ic_vaccinations"),#imageLiteral(resourceName: "ic_medicalreport"),#imageLiteral(resourceName: "ic_sergery_procedure"),#imageLiteral(resourceName: "ic_lifestylehabits"),#imageLiteral(resourceName: "ic_familyhistory"),#imageLiteral(resourceName: "ic_emergencynumbers"),#imageLiteral(resourceName: "ic_management")]
    var arryaMenuName = ["Health Conditions", "Medications", "Allergies/Adverse Reaction", "Vaccinations", "Medical Report / Photos", "Surgery/Procedure", "Lifestyle Habits", "Family History", "Emergency Numbers", "Consultation Notes"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
extension MyPhpVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "CheckPHRRequestVC")  as! CheckPHRRequestVC
            self.navigationController?.pushViewController(mapVc1, animated: true)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Request") as! MyPhpCell
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Menu") as! MyPhpCell
        cell.myphp = self
        cell.frame = tableView.bounds
        cell.layoutIfNeeded()
        cell.hightCollection.constant = cell.collectionMenu.collectionViewLayout.collectionViewContentSize.height
        cell.collectionMenu.reloadData()
        return cell
    }
    
    
    
    
}
extension MyPhpCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "HealthConditionsVC")  as! HealthConditionsVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 1{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "MedicationVC")  as! MedicationVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 2{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AllergiesAdverseReactionVC")  as! AllergiesAdverseReactionVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 3{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "VaccinationsVC")  as! VaccinationsVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 4{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "MedicalReportListVC")  as! MedicalReportListVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 5{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "SurgeryProcedureVC")  as! SurgeryProcedureVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 6{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "LifestyleHabitsVC")  as! LifestyleHabitsVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 7{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "FamilyHistoryVC")  as! FamilyHistoryVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 8{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "EmergencyVC")  as! EmergencyVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
        if indexPath.row == 9{
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "ManagmnetVC")  as! ManagmnetVC
            myphp.navigationController?.pushViewController(mapVc1, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return myphp.arryaMenuName.count
      
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! MyPhpCollCell
            cell.imageMenuIcon.image = myphp.arryaMenuIcon[indexPath.row]
            cell.lblTitleName.text = myphp.arryaMenuName[indexPath.row]
            return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 413.5 * _widthRatio / 2 , height: 136)
       
    }
}
