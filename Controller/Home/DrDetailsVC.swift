//
//  DrDetailsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 16/04/21.
//

import UIKit
import MapKit
class DrDetailsCell: ConstrainedTableViewCell, CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var lblMenuName: LabelMedium!
    @IBOutlet weak var lblAddres: LabelMedium!
    @IBOutlet weak var lblTime: LabelSemiBold!
    @IBOutlet weak var lblDay: LabelSemiBold!
    @IBOutlet weak var mapLocation: MKMapView!
    @IBOutlet weak var lblDesc: LabelSemiBold!
    @IBOutlet weak var lblTitles: LabelSemiBold!
    @IBOutlet weak var lblDrName: LabelSemiBold!
    @IBOutlet weak var imageDr: UIImageView!
    @IBOutlet weak var btnDownload: CustAdd!
    @IBOutlet weak var btnGP: UISwitch!
    
    @IBOutlet weak var HightLabel: NSLayoutConstraint!
    @IBOutlet weak var labTableView: UITableView!
    
    @IBOutlet weak var testName: LabelSemiBold!
    @IBOutlet weak var lblPrice: LabelSemiBold!
    @IBOutlet weak var lblTat: LabelSemiBold!
    
    
    var managmnet : ManagmnetVC!
    
    
    var latDelta:CLLocationDegrees = 0.20
    var longDelta:CLLocationDegrees = 0.20
    override func awakeFromNib() {
        super.awakeFromNib()
        if let _ = imageDr{
            imageDr.makeCornerRoundxs(redias : 5)
        }
        if let _ = viewBg{
            viewBg.makeCornerRound(redias: 5)
        }
    }
}

class DrDetailsVC: ParentViewController {
    @IBOutlet weak var lblTitleHeader: LabelMedium!
    var strHeader = ""
    var arrData: [[String]] = []
    var isDoctor = false
    var docterDetailes : DocterDetailes!
    var nurseDetailes : NurseDetailes!

    override func viewDidLoad() {
        super.viewDidLoad()
        if isDoctor{
            lblTitleHeader.text = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
            if docterDetailes.monday.count > 0{
                arrData.append(["Monday","\(docterDetailes.monday[0]) to \(docterDetailes.monday[1])"])
            }
            if docterDetailes.tuesday.count > 0{
                arrData.append(["Tuesday","\(docterDetailes.tuesday[0]) to \(docterDetailes.tuesday[1])"])
            }
            if docterDetailes.wednesday.count > 0{
                arrData.append(["Wednesday","\(docterDetailes.wednesday[0]) to \(docterDetailes.wednesday[1])"])
            }
            if docterDetailes.thursday.count > 0{
                arrData.append(["Thursday","\(docterDetailes.thursday[0]) to \(docterDetailes.thursday[1])"])
            }
            if docterDetailes.friday.count > 0{
                arrData.append(["Friday","\(docterDetailes.friday[0]) to \(docterDetailes.friday[1])"])
            }
            if docterDetailes.saturday.count > 0{
                arrData.append(["Saturday","\(docterDetailes.saturday[0]) to \(docterDetailes.saturday[1])"])

            }
        }else{
            lblTitleHeader.text = nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
            if nurseDetailes.monday.count > 0{
                arrData.append(["Monday","\(nurseDetailes.monday[0]) to \(nurseDetailes.monday[1])"])
            }
            if nurseDetailes.tuesday.count > 0{
                arrData.append(["Tuesday","\(nurseDetailes.tuesday[0]) to \(nurseDetailes.tuesday[1])"])
            }
            if nurseDetailes.wednesday.count > 0{
                arrData.append(["Wednesday","\(nurseDetailes.wednesday[0]) to \(nurseDetailes.wednesday[1])"])
            }
            if nurseDetailes.thursday.count > 0{
                arrData.append(["Thursday","\(nurseDetailes.thursday[0]) to \(nurseDetailes.thursday[1])"])
            }
            if nurseDetailes.friday.count > 0{
                arrData.append(["Friday","\(nurseDetailes.friday[0]) to \(nurseDetailes.friday[1])"])
            }
            if nurseDetailes.saturday.count > 0{
                arrData.append(["Saturday","\(nurseDetailes.saturday[0]) to \(nurseDetailes.saturday[1])"])
            }
        }
        
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Consultation"{
            let vc = segue.destination as! ConsultationTypeVC
            if isDoctor{
                vc.docterDetailes = docterDetailes
            }else{
                vc.nurseDetailes = nurseDetailes
            }
            vc.isDoctor = isDoctor
        }
    }
    
    @IBAction func btnBookClicked(_ sender: UIButton) {
        performSegue(withIdentifier: "Consultation", sender: nil)
    }
    
    @IBAction func btnCallCliced(_ sender: Any) {
        if isDoctor{
            callNumber(phoneNumber: docterDetailes.doctor_phone)
        }else{
            callNumber(phoneNumber: nurseDetailes.nurse_phone)
        }
        
    }
    
    
}
extension DrDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 108 * _widthRatio
            }else{
                return 44 * _widthRatio
            }
        }else if indexPath.section == 1{
            
            if indexPath.row == 7 || indexPath.row == 9 || indexPath.row == 11{
                return UITableView.automaticDimension
            }else if indexPath.row == 10{
                return 158 * _widthRatio
            }
            if indexPath.row == 0{
                return 30 * _widthRatio
            }
            return 25 * _widthRatio
        }else if indexPath.section == 2{
            return 30 * _widthRatio
        }else{
            return 72 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }else if section == 1{
            return 13
        }else if section == 2{
            return arrData.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Doctor") as! DrDetailsCell
                if isDoctor{
                    cell.imageDr.kf.setImage(with: URL(string:docterDetailes.doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                    }
                    cell.lblDrName.text = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                    if isWorkingCountryUK() {
                        cell.lblMenuName.text = docterDetailes.doctor_specialintrest
                        } else {
                        cell.lblMenuName.text = docterDetailes.doctor_specialization
                        }
                    cell.lblAddres.text = docterDetailes.doctor_city
                }else{
                    cell.imageDr.kf.setImage(with: URL(string:nurseDetailes.nurse_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                    }
                    cell.lblDrName.text = nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
                    cell.lblMenuName.text = nurseDetailes.nurse_specialization
                    cell.lblAddres.text = nurseDetailes.nurse_city
                }
                
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = "About Doctor"
            
            return cell

        }else if indexPath.section == 1{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Charges:"
                cell.constraintTop.constant = 5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                if isDoctor{
                    cell.lblDesc.text = _currency.currency_vSymbol + "\(docterDetailes.doctor_fees!)"
                }else{
                    cell.lblDesc.text = _currency.currency_vSymbol + "\(nurseDetailes.nurse_fees!)"
                }
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Working Days:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                if isDoctor{
                    cell.lblDesc.text = docterDetailes.doctor_working_day
                }else{
                    cell.lblDesc.text = nurseDetailes.nurse_working_day!
                }
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Experience:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                if isDoctor{
                    cell.lblDesc.text = docterDetailes.experience + " Years"
                }else{
                    cell.lblDesc.text = nurseDetailes.experience + " Years"
                }
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 6{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "More Details about Doctor:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                if isDoctor{
                    cell.lblDesc.text = docterDetailes.doctor_overview.html2String

                }else{
                    cell.lblDesc.text = nurseDetailes.nurse_overview.html2String
                }
                
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 8{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                if isWorkingCountryUK() {
                    cell.lblTitles.text = "Special Interest:"
                    } else {
                        cell.lblTitles.text = "Specialties:"
                    }
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 9{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                if isDoctor{
                    if isWorkingCountryUK() {
                        cell.lblDesc.text = docterDetailes.doctor_specialintrest
                        } else {
                        cell.lblDesc.text = docterDetailes.doctor_specialization
                        }
                }else{
                    if isWorkingCountryUK() {
                        cell.lblDesc.text = nurseDetailes.nurse_specialization
                        } else {
                        cell.lblDesc.text = nurseDetailes.nurse_specialization
                        }
                }
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 10{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Map") as! DrDetailsCell
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                if isDoctor{
                    let annotation = MKPointAnnotation()
                    let centerCoordinate = CLLocationCoordinate2D(latitude: docterDetailes.latitude, longitude: docterDetailes.longitude)
                    annotation.coordinate = centerCoordinate
                    let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: cell.latDelta, longitudeDelta: cell.longDelta)
                    let region:MKCoordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2DMake(docterDetailes.latitude, docterDetailes.longitude) , span: span)
                    
                    cell.mapLocation.addAnnotation(annotation)
                }else{
                    let annotation = MKPointAnnotation()
                    let centerCoordinate = CLLocationCoordinate2D(latitude: nurseDetailes.nurse_latitude, longitude:nurseDetailes.nurse_longitude)
                    annotation.coordinate = centerCoordinate
                    let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: cell.latDelta, longitudeDelta: cell.longDelta)
                    let region:MKCoordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2DMake(nurseDetailes.nurse_latitude, nurseDetailes.nurse_longitude) , span: span)
                    cell.mapLocation.addAnnotation(annotation)
                }
                
                return cell
            }else if indexPath.row == 11{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                if isDoctor{
                    cell.lblDesc.text = docterDetailes.doctor_street + "," + docterDetailes.doctor_province + "," + docterDetailes.doctor_city + "," + docterDetailes.doctor_state + "," + docterDetailes.doctor_country
                }else{
                    cell.lblDesc.text = nurseDetailes.nurse_province + "," + nurseDetailes.nurse_province + "," + nurseDetailes.nurse_city + "," + nurseDetailes.nurse_state + "," + nurseDetailes.nurse_country
                }
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Practical Hours:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }
            
            

        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeZone") as! DrDetailsCell
            if indexPath.row == arrData.count - 1{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
            }else{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
            }
            if NSDate().dayOfTheWeek() == arrData[indexPath.row][0]{
                cell.lblDay.textColor = UIColor.colorchaupOrange()
            }else{
                cell.lblDay.textColor = UIColor.black
            }
            cell.lblDay.text = arrData[indexPath.row][0]
            cell.lblTime.text = arrData[indexPath.row][1]
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
            if isDoctor{
                if docterDetailes.isListingpurpose == "1"{
                    cell.btnBook.isHidden = true
                    cell.widhtCall.constant = 150

                }else{
                    cell.btnBook.isHidden = false
                    cell.widhtCall.constant = 18

                }
            }else{
                if nurseDetailes.isListingpurpose == "1"{
                    cell.btnBook.isHidden = true
                    cell.widhtCall.constant = 150

                }else{
                    cell.btnBook.isHidden = false
                    cell.widhtCall.constant = 18

                }
            }
            return cell
        }
       
    }
    
    
    
    
}
extension NSDate {

    func dayOfTheWeek() -> String? {
        let weekdays = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ]

        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        let components: NSDateComponents = calendar.components(.weekday, from: self as Date) as NSDateComponents
        return weekdays[components.weekday - 1]
    }
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
