//
//  HomeVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit
import MessageUI
import AVFoundation
import Network
import Firebase
import FirebaseCrashlytics

class HomeCollCell : ConstrainedCollectionViewCell{
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblInformation: LabelRegular!
    @IBOutlet weak var ImageMenu: UIImageView!
    @IBOutlet weak var lblTitleInforation: LabelSemiBold!
    @IBOutlet weak var lblDscrption: LabelSemiBold!
    
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var hightBannerImage: NSLayoutConstraint!
    @IBOutlet weak var widhtBannerImage: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let _ = viewBg{
            viewBg.makeCornerRound(redias: 5)
        }
        
        if reuseIdentifier == "INforamtion"{
            ImageMenu.makeCornerRoundxs(redias : 5)
        }
    }
}
class HomeCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var collectionInforamtion: UICollectionView!
    @IBOutlet weak var lblDateTime: LabelSemiBold!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblStatus: LabelSemiBold!
    @IBOutlet weak var lblDocterName: LabelSemiBold!
    @IBOutlet weak var imagePesant: UIImageView!
    @IBOutlet weak var lblService: LabelMedium!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var collectionMenu: UICollectionView!
    @IBOutlet weak var ImageChange: UIImageView!
    @IBOutlet weak var lblInformation: LabelSemiBold!
    @IBOutlet weak var lblPlaceHoder: LabelSemiBold!
    @IBOutlet weak var collectionLike: UICollectionView!
    
    @IBOutlet weak var collectionBanner: UICollectionView!
    @IBOutlet weak var pageBanner: UIPageControl!
    
    
    weak var home : HomeVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      //  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        
        if reuseIdentifier == "sevice" || reuseIdentifier == "status" || reuseIdentifier == "Contact"{
            viewBG.makeCornerRound(redias: 5)
        }
        if reuseIdentifier == "status"{
            imagePesant.makeCornerRoundxs(redias : 5)
        }
        if reuseIdentifier == "Contact"{
            lblDateTime.text = "Mon - Fri 9am - 5pm\nSat: 9am - 2pm\nSun: Closed"
            let FormattedText = NSMutableAttributedString()
            FormattedText
                .Open("Mon - Fri 9am - 5pm; Sat: 9am - 2pm;\n")
                .Cloase("Sun: Closed")
            self.lblDateTime.attributedText = FormattedText
            let FormattedText1 = NSMutableAttributedString()
            FormattedText1
                .viewAll("info@checkuphealth.io")
            btnEmail.setAttributedTitle(FormattedText1, for: .normal)
            let FormattedText2 = NSMutableAttributedString()
            FormattedText2
                .viewAll("+263 715042307")
            btnCall.setAttributedTitle(FormattedText2, for: .normal)
        }
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
            
            if let coll  = collectionBanner {
                for cell in coll.visibleCells {
                    let indexPath: IndexPath? = coll.indexPath(for: cell)
                    if ((indexPath?.row)!  < home.homeList.arrayBanner.count - 1){
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                        
                        coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    }
                    else{
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                        coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    }
                    
                }
            }
            
        }
}
class HomeVC: ParentViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var viewIDProof: UIView!
    
    var arrMenu = ["Doctor", "Monitor Health at Home", "Chat with us"]
    var atyaMenuImage  = [#imageLiteral(resourceName: "ic_home_doctor"),#imageLiteral(resourceName: "ic_home_moniterhealth"),#imageLiteral(resourceName: "ic_home_chat")]
    
    
    
    
    var homeList : HomeList!
    var arryaAppointmenthistory = [Appointmenthistory]()
    var isAppointmeDetails = false
    var appoinmentID = ""
    var isFor = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        viewIDProof.isHidden = true
        self.tableView.contentInset.bottom = 80 * _widthRatio
        viewIDProof.makeCornerRound(redias: 10)
        Crashlytics.crashlytics().setCustomValue("\(_currentUser.patient_id ?? "no id found")", forKey: "userId")
      //  fatalError()

        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: break // The user has previously granted access to the camera.
            
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                }
            
            case .denied: // The user has previously denied access.
                return

            case .restricted: // The user can't grant access due to restrictions.
                return
         default:
            break
        }

        
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            print("Permission granted")
        case AVAudioSession.RecordPermission.denied:
            print("Pemission denied")
        case AVAudioSession.RecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
         default:
            break
        }
      
       
        if isAppointmeDetails{
            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDetailsVC")  as! AppointmentDetailsVC
            mapVc1.appoinmentID = appoinmentID
            mapVc1.isFor = isFor
            navigationController?.pushViewController(mapVc1, animated: true)

            
        }
        getFirebaseAnalytics(screenName : getEventName.HOME,isPatient_id : true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        getHomeData()
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Serach"{
            let vc = segue.destination as! SerachDrAndNurseVC
            vc.isDoctor = (sender as! Bool)
        }
    }
    @IBAction func btnViewAllClicked(_ sender: UIButton) {
        if sender.tag == 1{
            performSegue(withIdentifier: "Service", sender: true)
        }
    }
    
    @IBAction func btnEmailOpenClicked(_ sender: Any) {
        sendEmail()
    }
    
    
    @IBAction func btnCallClikced(_ sender: Any) {
        if isWorkingCountryUK(){
            callNumber(phoneNumber: "+443455652081")
        }else{
            callNumber(phoneNumber: "+263715042307")
        }
        
    }
    
    func sendEmail() {
      if MFMailComposeViewController.canSendMail() {
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients(["customercare@checkuphealth.co.uk"])
        mail.setMessageBody("", isHTML: true)

        present(mail, animated: true)
      }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    @IBAction func btnViewMoreClicked(_ sender: UIButton) {
        if sender.tag == 3{
            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceProviderVC")  as! ServiceProviderVC
           // mapVc1.delegatesAddress1 = self
            navigationController?.pushViewController(mapVc1, animated: true)
        }else{
            if isWorkingCountryUK(){
                _appDelegator.tab.selectedIndex = 1
            }else{
                _appDelegator.tab.selectedIndex = 2
            }
           
        }
    }

    
    @IBAction func btnIDProofClicked(_ sender: Any) {
        
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "IdentityVerificationVC")  as! IdentityVerificationVC
       // mapVc1.delegatesAddress1 = self
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    
}
extension HomeVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 4{
            if indexPath.row != 0{
                if arryaAppointmenthistory.count > 0{
                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDetailsVC")  as! AppointmentDetailsVC
                    mapVc1.appoinmentID = arryaAppointmenthistory[indexPath.row - 1].appointment_id
                    mapVc1.isFor = arryaAppointmenthistory[indexPath.row - 1].appointment_for
                    navigationController?.pushViewController(mapVc1, animated: true)
                }
                
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = homeList{
            return 7
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if homeList.arrayBanner.count > 0{
                return 240 * _widthRatio
            }
            return 44 * _widthRatio
           
        }else if indexPath.section == 1{
             if indexPath.row == 0{
                return 44 * _widthRatio
            }else{
                return 128 * _widthRatio
            }
        }else if indexPath.section == 2{
             if indexPath.row == 0{
                return 44 * _widthRatio
            }else{
                return 128 * _widthRatio
            }
        }else if indexPath.section == 3{
            if indexPath.row == 0{
                return 44 * _widthRatio
            }else{
                return UITableView.automaticDimension
            }
            
        }else if indexPath.section == 4{
            if indexPath.row == 0{
                return 44 * _widthRatio
            }else{
                return 117 * _widthRatio
            }
        }else if indexPath.section == 5{
            if indexPath.row == 0{
                return 44 * _widthRatio
            }else{
                return UITableView.automaticDimension
            }
        }else{
            if indexPath.row == 0{
                return 44 * _widthRatio
            }else{
                return 300 * _widthRatio
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            if homeList.arrayLink.count > 0{
                return 2
            }
            return 0
        }else if section == 2{
            return 2
        }else if section == 3{
            
            if isWorkingCountryUK(){
                if _parameterGB.PARAMETER_SERVICE_PROVIDER_3 == "1"{
                    return 2
                }
                return 0
            }else{
                if _parameterZW.PARAMETER_SERVICE_PROVIDER_3 == "1"{
                    return 2
                }
                return 0
            }
            
        }else if section == 4{
            
            if arryaAppointmenthistory.count > 0{
                return arryaAppointmenthistory.count + 1
            }
            return 2
        }else if section == 5{
            return 2
        }else{
            if homeList.arryaInformation.count > 0{
                return 2
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
             
                let cell = tableView.dequeueReusableCell(withIdentifier: "ImageDocter") as! HomeCell
                cell.home = self
                cell.pageBanner.numberOfPages = homeList.arrayBanner.count
                cell.collectionBanner.reloadData()
                return cell
           
        }else if indexPath.section == 1{
             if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = "Complete Your Profile"

                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Link") as! HomeCell
                cell.home = self
                cell.collectionLike.reloadData()
                return cell
            }
        }else if indexPath.section == 2{
             if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = "Book Your Appointment"

                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Menu") as! HomeCell
                cell.home = self
                cell.collectionMenu.reloadData()
                return cell
            }
        }else if indexPath.section == 3{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "viewAll") as! HomeCell
                cell.lblInformation.text = "Service Providers"
                cell.btnViewAll.tag = indexPath.section
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "sevice") as! HomeCell
                cell.lblService.text = "If you are looking to connect with Hospitals, Labs, Ambulances, Pathologies, Pharmacies, etc. we have curated an exhaustive list of all the service providers. View our directory to contact them today. "
                return cell
            }
            
        }else if indexPath.section == 4{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "viewAll") as! HomeCell
                
                cell.lblInformation.text = "Upcoming"
                cell.btnViewAll.tag = indexPath.section
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "status") as! HomeCell
                if arryaAppointmenthistory.count > 0{
                    cell.lblPlaceHoder.text = ""
                    cell.lblDocterName.text = arryaAppointmenthistory[indexPath.row - 1].doctor_first_name + " " + arryaAppointmenthistory[indexPath.row - 1].doctor_last_name
                    
                    if isWorkingCountryUK() {
                        cell.lblInformation.text = arryaAppointmenthistory[indexPath.row - 1].doctor_specialisation_interest + "\n" + arryaAppointmenthistory[indexPath.row - 1].doctor_experience_clinic_name
                    } else {
                        cell.lblInformation.text = arryaAppointmenthistory[indexPath.row - 1].doctor_specialization + "\n" + arryaAppointmenthistory[indexPath.row - 1].doctor_experience_clinic_name
                    }
                    cell.imagePesant.kf.setImage(with: URL(string:arryaAppointmenthistory[indexPath.row - 1].doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                    }
                    cell.imagePesant.isHidden = false
                    cell.lblStatus.text = arryaAppointmenthistory[indexPath.row - 1].status
                }else{
                    cell.lblPlaceHoder.text = "No appointment found"
                    cell.lblInformation.text = ""
                    cell.lblDocterName.text = ""
                    cell.lblStatus.text = ""
                    cell.imagePesant.isHidden = true
                }
                return cell
            }
        }else if indexPath.section == 5{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = "Contact Clinic"

                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Contact") as! HomeCell
                if isWorkingCountryUK(){
                
                    
                    cell.lblDateTime.text = "Mon - Fri 9am - 5pm\nSat: 9am - 2pm\nSun: Closed"
                    let FormattedText = NSMutableAttributedString()
                    FormattedText
                        .Open("Monday – Thursday:\n9.00 – 14.30, 18.45 – 22.00;\nFriday:\n9.00 - 14.30;\nSaturday:\n9.30 – 11.30;\nSunday:\n9.45 - 14.15, 18.45 - 22.00")
                    cell.lblDateTime.attributedText = FormattedText
                    let FormattedText1 = NSMutableAttributedString()
                    FormattedText1
                        .viewAll("customercare@checkuphealth.co.uk")
                    cell.btnEmail.setAttributedTitle(FormattedText1, for: .normal)
                    let FormattedText2 = NSMutableAttributedString()
                    FormattedText2
                        .viewAll("+44 3455652081")
                    cell.btnCall.setAttributedTitle(FormattedText2, for: .normal)
                }else{
                    cell.lblDateTime.text = "Mon - Fri 9am - 5pm\nSat: 9am - 2pm\nSun: Closed"
                    let FormattedText = NSMutableAttributedString()
                    FormattedText
                        .Open("Mon - Fri:\n9am - 5pm;\nSat:\n9am - 2pm;\nSun:\nClosed")
                    cell.lblDateTime.attributedText = FormattedText
                    let FormattedText1 = NSMutableAttributedString()
                    FormattedText1
                        .viewAll("customercare@checkuphealth.co.uk")
                    cell.btnEmail.setAttributedTitle(FormattedText1, for: .normal)
                    let FormattedText2 = NSMutableAttributedString()
                    FormattedText2
                        .viewAll("+263 715042307")
                    cell.btnCall.setAttributedTitle(FormattedText2, for: .normal)
                }
                return cell
            }
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
                cell.lblInformation.text = "Information"

                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Inforationm") as! HomeCell
                cell.home = self
                return cell
            }
        }
        
    }
    
}
extension HomeCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionBanner{
            let visibleRect = CGRect(origin: self.collectionBanner.contentOffset, size: self.collectionBanner.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            if let visibleIndexPath = self.collectionBanner.indexPathForItem(at: visiblePoint) {
                self.pageBanner.currentPage = visibleIndexPath.row
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionMenu{
            if home.isWorkingCountryUK(){
                
                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BookInformationVC")  as! BookInformationVC
                home.navigationController?.pushViewController(mapVc1, animated: true)
                return
            }
            if indexPath.row == 0{
                home.performSegue(withIdentifier: "Serach", sender: true)
            }else if indexPath.row == 1{
                home.performSegue(withIdentifier: "Serach", sender: false)
            }else{
                home.performSegue(withIdentifier: "ChatUsWith", sender: nil)
            }
        }else if collectionView == collectionLike{
            
            
            if home.homeList.arrayLink[indexPath.row] == "Add registered address"{
                let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressAddVC")  as! AddressAddVC
                mapVc1.strHeader = "Add registered Address"
                home.navigationController?.pushViewController(mapVc1, animated: true)
            }
            
            if home.homeList.arrayLink[indexPath.row] == "Upload documents"{
                if home.homeList.isRegisterAddressPending == "1"{
                    home.showFailMessage(message: "Please verify your registered address")
                }else{
                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "IdentityVerificationVC")  as! IdentityVerificationVC
                   // mapVc1.delegatesAddress1 = self
                    home.navigationController?.pushViewController(mapVc1, animated: true)
                }
            }
            
            if home.homeList.arrayLink[indexPath.row] == "Link GP"{
                home.showAlert1(title: _appName, msgString: "Under developing", Ohk: "OK") {
                    
                }
            }
            
        }else if collectionView == collectionBanner{
            return
        }else{
            if let url = URL(string: home.homeList.arryaInformation[indexPath.row].vURL) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionMenu{
            if home.isWorkingCountryUK(){
                return 1
            }
            return 3
        }else if collectionView == collectionLike{
//            if home.isWorkingCountryUK(){
//                return 1
//            }
            
            return home.homeList.arrayLink.count
        }else if collectionView == collectionBanner{
        
            return home.homeList.arrayBanner.count
        }else{
            if home.homeList == nil{
                return 0
            }
            return home.homeList.arryaInformation.count
        }
        
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionMenu{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! HomeCollCell
            cell.ImageMenu.image = home.atyaMenuImage[indexPath.row]
            cell.lblInformation.text = home.arrMenu[indexPath.row]
            return cell
        }else if collectionView == collectionLike{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! HomeCollCell
            cell.ImageMenu.image = home.homeList.arrayLinkImage[indexPath.row]
            cell.lblInformation.text = home.homeList.arrayLink[indexPath.row]
            return cell
        }else if collectionView == collectionBanner{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Banner", for: indexPath) as! HomeCollCell
            cell.imageBanner.pin_updateWithProgress = true
            cell.imageBanner.pin_setImage(from: URL(string: home.homeList.arrayBanner[indexPath.row].vImagePath)!)
//            cell.imageBanner.kf.setImage(with: URL(string:home.homeList.arrayBanner[indexPath.row].vImagePath), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
//            }
            let width = collectionView.frame.width
            cell.widhtBannerImage.constant = width
            cell.hightBannerImage.constant = 220 * _widthRatio
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "INforamtion", for: indexPath) as! HomeCollCell
            cell.ImageMenu.kf.setImage(with: URL(string:home.homeList.arryaInformation[indexPath.row].vImagePath), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
            cell.lblTitleInforation.text = home.homeList.arryaInformation[indexPath.row].vTitle
            cell.lblDscrption.text = home.homeList.arryaInformation[indexPath.row].vDesc

            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionMenu{
            return CGSize(width: _screenSize.width / 3 , height: 114)
        }else if collectionView == collectionLike{
            return CGSize(width: _screenSize.width / 3 , height: 114)
        }else if collectionView == collectionBanner{
            let width = collectionView.frame.width

            return CGSize(width: width, height: 220 * _widthRatio)
        }else{
            return CGSize(width: 360 * _widthRatio, height: 320 * _widthRatio)
        }
    }
}
extension HomeVC{

    
    
    func getHomeData(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"patient_id\":\"\(_currentUser.patient_id!)\"}}"

        KPWebCall.call.postRequestApiClinet(relPath: "getHomeData_v4.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                if response.ReturnedData != nil{

                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                  self.homeList = nil
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        homeList = HomeList(dic: data)
                                    }
                                    if self.homeList.isDocumentVerificationPending == "0"{
                                        self.viewIDProof.isHidden = true
                                    }else{
                                        self.viewIDProof.isHidden = true
                                    }
                                    appointmenthistory()
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                }
            }
        }
    }
    
    func appointmenthistory(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"patient_id\":\"\(_currentUser.patient_id!)\",\"type\":\"upcoming\",\"dashboard\":\"\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "appointmenthistory.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                if response.ReturnedData != nil{

            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaAppointmenthistory.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaAppointmenthistory.append(Appointmenthistory(dic: obj as! NSDictionary))
                                }
                            }
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
                }
            }
        }
    }
}

@available(iOS 14.0, *)
public class LocalNetworkAuthorization: NSObject {
    private var browser: NWBrowser?
    private var netService: NetService?
    private var completion: ((Bool) -> Void)?
    
    public func requestAuthorization(completion: @escaping (Bool) -> Void) {
        self.completion = completion
        
        // Create parameters, and allow browsing over peer-to-peer link.
        let parameters = NWParameters()
        parameters.includePeerToPeer = true
        
        // Browse for a custom service type.
        let browser = NWBrowser(for: .bonjour(type: "_bonjour._tcp", domain: nil), using: parameters)
        self.browser = browser
        browser.stateUpdateHandler = { newState in
            switch newState {
            case .failed(let error):
                print(error.localizedDescription)
            case .ready, .cancelled:
                break
            case let .waiting(error):
                print("Local network permission has been denied: \(error)")
                self.reset()
                self.completion?(false)
            default:
                break
            }
        }
        
        self.netService = NetService(domain: "local.", type:"_lnp._tcp.", name: "LocalNetworkPrivacy", port: 1100)
        self.netService?.delegate = self
        
        self.browser?.start(queue: .main)
        self.netService?.publish()
    }
    
    private func reset() {
        self.browser?.cancel()
        self.browser = nil
        self.netService?.stop()
        self.netService = nil
    }
}

@available(iOS 14.0, *)
extension LocalNetworkAuthorization : NetServiceDelegate {
    public func netServiceDidPublish(_ sender: NetService) {
        self.reset()
        print("Local network permission has been granted")
        completion?(true)
    }
}
