//
//  AppointmentDetailsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 22/04/21.
//

import UIKit
protocol delegetReson {
    func cancelREson(strREscon: String)
    func getAddress(addressID: String)
}


class AppointmentDetailsCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var btnCancel: addButton!
    @IBOutlet weak var btnRescedule: addButton!
    @IBOutlet weak var btnDirection: submitButton!
    @IBOutlet weak var btnCall: submitButton!
    @IBOutlet weak var callWith: NSLayoutConstraint!
    
    @IBOutlet weak var btnJoinVideoCall: submitButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias: 5)
        
    }
}

class AppointmentDetailsVC: ParentViewController, delegetReson {
   
    
    
    
    @IBOutlet weak var lblDrName: LabelSemiBold!
    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet weak var viewTop: UIView!
    var arrData: [[Any]] = []
    var appointmenthistory : Appointmenthistory!
    
    var arryaTitle = ["ID:","Date & Time:","Address:","Patient Name:","Mobile No:","Payment Status:","Verification Code:","Appointment Status:","Referral Note:","Sick Note:"]
    var strCancelReson = ""
    var appoinmentID = ""
    var isFor = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.appointmentDetails), name: NSNotification.Name(rawValue: "appointmentDetails"), object: nil)
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

        imagePhoto.makeCornerRoundxs(redias : 5)
        if isFor == "DR" || isFor == "1"{
            appointmentDetailv(loader: true)
        }else{
            appointmentDetailNureser(loader: true)
        }
        
    }
    
    @objc func sortRefresh(){
        if isFor == "DR" || isFor == "1"{
            appointmentDetailv(loader: false)
        }else{
            appointmentDetailNureser(loader: false)
        }
    }
    
    @objc func appointmentDetails(_ notification: NSNotification){
        
        if isFor == "DR" || isFor == "1"{
            appointmentDetailv(loader: false)
        }else{
            appointmentDetailNureser(loader: false)
        }
    }
    
    
    
    @IBAction func btnAnotherClicked(_ sender: UIButton) {
        if "Referral Note:" == arrData[sender.tag][0] as! String{
            if let url = URL(string: appointmenthistory.referralfile) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if "Referral Note 2:" == arrData[sender.tag][0] as! String{
            if let url = URL(string: appointmenthistory.referralfile2) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if "Sick Note:" == arrData[sender.tag][0] as! String{
            if let url = URL(string: appointmenthistory.sicknotefile) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if "Sick Note 2:" == arrData[sender.tag][0] as! String{
            if let url = URL(string: appointmenthistory.sicknotefile2) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if "Verification Code:" == arrData[sender.tag][0] as! String{
            UIPasteboard.general.string = appointmenthistory.verification_code
            showSucessMessage(message : "copied to Verification Code")
        }
    }
    
    @IBAction func btnCallClicked(_ sender: Any) {
        callNumber(phoneNumber: appointmenthistory.doctor_phone)
    }
    
    @IBAction func btnDirectionClicked(_ sender: Any) {
        UserLocation.sharedInstance.fetchUserLocationForOnce(controller: self) { (location, error) in
            //   self.hideCentralSpinner()
            if let loc = location{
                let url = "http://maps.apple.com/maps?saddr=\(loc.coordinate.latitude),\(loc.coordinate.longitude)&daddr=\(self.appointmenthistory.doctor_latitude!),\(self.appointmenthistory.doctor_longitude!)"
                UIApplication.shared.openURL(URL(string:url)!)
                
            }
        }
        
    }
   
    @IBAction func btnAddressEditClicked(_ sender: Any) {

        let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressVC")  as! AddressVC
         mapVc1.delegatesAddress5 = self
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    func getAddress(addressID: String) {
        self.navigationController?.popViewController(animated: true)
                showAlert(title: "Update address", msgString: "Are you sure want to update address for this appointment?", Ohk: "YES", cancal: "NO") { [self] in
                    updateconsultationaddress(addressID : addressID)
                }
    }
    
    
    
    

    @IBAction func btnResuducal(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BookAppointmentVC")  as! BookAppointmentVC
        mapVc1.isResidule = true
        mapVc1.appointmentId = appointmenthistory.appointment_id
        mapVc1.doctor_id = appointmenthistory.doctor_id
       
        
        if appointmenthistory.appointment_for != "NURSE"{
            mapVc1.DoctoreName = appointmenthistory.doctor_first_name + " " + appointmenthistory.doctor_last_name
            mapVc1.DotoreSpectry = appointmenthistory.doctor_specialization
            mapVc1.DoctoreImage = appointmenthistory.doctor_profile_image
        }else{
            mapVc1.DoctoreName = appointmenthistory.doctor_first_name + " " + appointmenthistory.doctor_last_name
            mapVc1.DotoreSpectry = appointmenthistory.doctor_specialization
            mapVc1.DoctoreImage = appointmenthistory.doctor_profile_image
        }
        
        if appointmenthistory.appointment_for == "NURSE"{
            mapVc1.isDoctor = false
        }else{
            mapVc1.isDoctor = true
        }
        mapVc1.appointmentType = appointmenthistory.appointment_for
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    @IBAction func btnCalncelClicked(_ sender: Any) {
        performSegue(withIdentifier: "canxel", sender: true)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "canxel"{
            let vc = segue.destination as! ResonVC
            vc.dalegate = self
        }
    }
    
    func cancelREson(strREscon: String) {
        strCancelReson = strREscon
        self.dismiss(animated: true, completion: nil)
        cancelAppointment()
    }
    
    @IBAction func btnVideoCallClicked(_ sender: Any) {
        if #available(iOS 14, *) {
            let network = LocalNetworkAuthorization()

            network.requestAuthorization { done in
                
            }
        }
        
        if check_permistion_audio() == "1"
        {
            getAgoraAccessToken()
        }else  if  check_permistion_audio() == "2"{
            goto_seeting()
        }
    }
    
    
    
    
    
}
extension AppointmentDetailsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == arrData.count {
            
            if isWorkingCountryUK(){
                return 150 * _widthRatio
            }else{
                if appointmenthistory.appointment_availibility == "1" {
                    //both cancel & reschedule button will be hidden
                    return 180 * _widthRatio
                }else{
                    //both cancel & reschedule button will be visible
                    if appointmenthistory.appoint_type == "hospital"
                        || appointmenthistory.appoint_type == "meeting" {
                        return 180 * _widthRatio
                    }else{
                        return 180 * _widthRatio
                    }
                }
                
                
            }
        }
        
        if indexPath.row == 0 {
            return 30 * _widthRatio
        }
        return UITableView.automaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if arrData.count > 0{
            return arrData.count + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == arrData.count {
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == arrData.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Buttion") as! AppointmentDetailsCell
            cell.btnJoinVideoCall.isHidden = true
            if appointmenthistory.appointment_availibility == "1" {
                //both cancel & reschedule button will be hidden
                cell.btnCancel.isHidden = true
                cell.btnRescedule.isHidden = true
            }else{
                //both cancel & reschedule button will be visible
                cell.btnCancel.isHidden = false
                cell.btnRescedule.isHidden = false
            }
            
            if isWorkingCountryUK(){
                cell.btnJoinVideoCall.isHidden = true
                if appointmenthistory.triage_status == "1" || appointmenthistory.triage_status == "0"{
                    if appointmenthistory.appointment_status == "0"{
                        if appointmenthistory.triage_status == "1"{
                            cell.btnJoinVideoCall.isHidden = false
                        }
                    }
                }
                cell.btnDirection.isHidden = true
                cell.btnCall.isHidden = true
            }else{
                if appointmenthistory.appoint_type == "hospital"
                    || appointmenthistory.appoint_type == "meeting" {
                    cell.callWith.constant = 0
                    cell.btnDirection.isHidden = false
                    cell.btnCall.isHidden = false
                }else{
                    if appointmenthistory.appointment_status == "0"{
                        cell.btnJoinVideoCall.isHidden = false
                    }
                    cell.btnCall.isHidden = false
                    cell.callWith.constant = 90 * _widthRatio
                    cell.btnDirection.isHidden = true
                }
            }
            
            return cell
        }
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = arrData[indexPath.section][0] as! String
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            if indexPath.section == 0{
                cell.constraintTop.constant = 5
            }
            return cell
        }
        if "Consultation Address:" == arrData[indexPath.section][0] as! String{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc1") as! DrDetailsCell
            cell.lblDesc.textColor = .black
            let origImage = UIImage(named: "ic_edit")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            cell.btnDownload.setImage(tintedImage, for: .normal)
            cell.btnDownload.tintColor = .orange
            cell.btnDownload.isHidden = true
            if isWorkingCountryUK(){
                
                if appointmenthistory.triage_status == "1" || appointmenthistory.triage_status == "0"{
                    if appointmenthistory.appointment_status == "0"{
                        //Upcoming
                        cell.btnDownload.isHidden = false
                    }else if appointmenthistory.appointment_status == "1"{
                        //completed
                    }else if appointmenthistory.appointment_status == "2"{
                        //cancelled
                    }else if appointmenthistory.appointment_status == "3"{
                        //DNA
                    }else{
                        //pending
                        cell.btnDownload.isHidden = false
                    }
                }else if appointmenthistory.triage_status == "2"{
                    //Rejected
                }else{
                    //pending
                    cell.btnDownload.isHidden = false
                }
            }else{
                if appointmenthistory.appointment_status == "0"{
                    cell.btnDownload.isHidden = false
                }
            }
            cell.lblDesc.text = arrData[indexPath.section][1] as! String
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
        if "Appointment Status:" == arrData[indexPath.section][0] as! String{
            let str = arrData[indexPath.section][1] as! String
            if str.contains("cancelled".uppercased()){
                cell.lblDesc.textColor = .red
            }else if str.contains("Rejected".uppercased()){
                cell.lblDesc.textColor = .red
            }else if "DNA".uppercased() == arrData[indexPath.section][1] as! String{
                cell.lblDesc.textColor = .red
            }else if "completed".uppercased() == arrData[indexPath.section][1] as! String{
                cell.lblDesc.textColor = UIColor.colorWithRGB(r: 0, g: 148, b: 0)
            }else{
                cell.lblDesc.textColor = .black
            }
        }else{
            cell.lblDesc.textColor = .black
        }
        if "Referral Note:" == arrData[indexPath.section][0] as! String{
            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_download"), for: .normal)
        }else if "Referral Note 2:" == arrData[indexPath.section][0] as! String{
            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_download"), for: .normal)
        }else if "Sick Note:" == arrData[indexPath.section][0] as! String{
            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_download"), for: .normal)
        }else if "Sick Note 2:" == arrData[indexPath.section][0] as! String{
            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_download"), for: .normal)
        }else if "Verification Code:" == arrData[indexPath.section][0] as! String{
            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_copy"), for: .normal)
        }
        cell.btnDownload.tag = indexPath.section
        cell.lblDesc.text = arrData[indexPath.section][1] as! String
        cell.btnDownload.isHidden = arrData[indexPath.section][2] as! Bool
        cell.constraintTop.constant = -5
        cell.constraintBottom.constant = -5
        
        return cell
    }
    
    
    
    
}
extension AppointmentDetailsVC{
    func cancelAppointment(){
        let parameters = "{\"data\":{\"key\":\"\(appointmenthistory.appointment_for!)\",\"appointment_id\":\"\(appointmenthistory.appointment_id!)\",\"cancel_reason\":\"\(strCancelReson)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "cancelAppointment.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func appointmentDetailv(loader:Bool){
        
        var dic = [String:Any]()
        
        dic["appointment_id"] = appoinmentID
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        if loader{
            showCentralSpinner()
        }
        KPWebCall.call.postRequestApiClinet(relPath: "appointmentDetail.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.appointmenthistory = nil
                                self.tableView.backgroundView = nil
                                arrData.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        appointmenthistory = Appointmenthistory(dic: data)
                                    }
                                    imagePhoto.kf.setImage(with: URL(string:appointmenthistory.doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                                    }
                                    
                                    lblDrName.text = appointmenthistory.doctor_first_name + " " + appointmenthistory.doctor_last_name
                                    viewTop.frame.size.height = 197 * _widthRatio
                                    arrData.append(["ID:",appointmenthistory.appointment_order_id!,true])
                                    arrData.append(["Date & Time:",UTCToLocalSmart(date: appointmenthistory.booking_date! + " " + appointmenthistory.booking_time),true])
                                    
                                    
                                    let address = appointmenthistory.doctor_experience_clinic_name + appointmenthistory.doctor_experience_clinic_address
                                    if !String.validateStringValue(str: address){
                                        arrData.append(["Address:",appointmenthistory.doctor_experience_clinic_name + "\n" + appointmenthistory.doctor_experience_clinic_address!,true])
                                    }
                                    
                                    arrData.append(["Patient Name:",appointmenthistory.patient_first_name! + " " + appointmenthistory.patient_last_name!,true])
                                    arrData.append(["Mobile No:",appointmenthistory.patient_phone!,true])
                                    arrData.append(["Consultation Address:",appointmenthistory.patient_consultation_address!,true])
                                    if appointmenthistory.pharmacy_name != ""{
                                        arrData.append(["Preferred Pharmacy Name:",appointmenthistory.pharmacy_name!,true])
                                    }
                                    if appointmenthistory.delivery_name != ""{
                                        arrData.append(["Preferred Delivery Type:",appointmenthistory.delivery_name!,true])
                                    }
                                    if appointmenthistory.referralfile != ""{
                                        let refal = URL(string: appointmenthistory.referralfile!)
                                        arrData.append(["Referral Note:",refal?.lastPathComponent as Any,false])
                                    }
                                    if appointmenthistory.referralfile2 != ""{
                                        let refal = URL(string: appointmenthistory.referralfile2!)
                                        arrData.append(["Referral Note 2:",refal?.lastPathComponent as Any,false])
                                    }
                                    
                                    if appointmenthistory.sicknotefile != ""{
                                        let refalw = URL(string: appointmenthistory.sicknotefile!)
                                        arrData.append(["Sick Note:",refalw?.lastPathComponent as Any,false])
                                    }
                                    if appointmenthistory.sicknotefile2 != ""{
                                        let refalw = URL(string: appointmenthistory.sicknotefile2!)
                                        arrData.append(["Sick Note 2:",refalw?.lastPathComponent as Any,false])
                                    }
                                    arrData.append(["Payment Status:",appointmenthistory.payment_status!,true])
                                    if isWorkingCountryUK(){
                                        if appointmenthistory.triage_status == "1" || appointmenthistory.triage_status == "0"{
                                            if appointmenthistory.appointment_status == "0"{
                                                
                                                arrData.append(["Appointment Status:","Upcoming".uppercased(),true])
                                            }else if appointmenthistory.appointment_status == "1"{
                                                
                                                arrData.append(["Appointment Status:","completed".uppercased(),true])
                                            }else if appointmenthistory.appointment_status == "2"{
                                                
                                                arrData.append(["Appointment Status:","cancelled".uppercased() + "\nReason: \(appointmenthistory.cancel_reason!)" + "\nCancelled by: \(appointmenthistory.cancel_by!)",true])
                                            }else if appointmenthistory.appointment_status == "3"{
                                                
                                                arrData.append(["Appointment Status:","DNA".uppercased(),true])
                                            }else{
                                                arrData.append(["Appointment Status:","pending".uppercased(),true])
                                            }
                                        }else if appointmenthistory.triage_status == "2"{
                                            
                                            arrData.append(["Reason:","Rejected".uppercased(),true])
                                        }else{
                                            arrData.append(["Appointment Status:","pending".uppercased(),true])
                                        }
                                    }else{
                                        arrData.append(["Verification Code:",appointmenthistory.verification_code!,false])
                                        if appointmenthistory.appointment_status == "0"{
                                            
                                            arrData.append(["Appointment Status:","Upcoming".uppercased(),true])
                                        }else if appointmenthistory.appointment_status == "1"{
                                            
                                            arrData.append(["Appointment Status:","completed".uppercased(),true])
                                        }else if appointmenthistory.appointment_status == "2"{
                                            
                                            arrData.append(["Appointment Status:","cancelled".uppercased() + "\nReason: \(appointmenthistory.cancel_reason!)" + "\nCancelled by: \(appointmenthistory.cancel_by!)",true])
                                        }else if appointmenthistory.appointment_status == "3"{
                                            
                                            arrData.append(["Appointment Status:","DNA".uppercased(),true])
                                        }else{
                                            
                                            arrData.append(["Appointment Status:","pending".uppercased(),true])
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func appointmentDetailNureser(loader:Bool){
        
        var dic = [String:Any]()
        
        dic["nurse_appointment_id"] = appoinmentID
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        if loader{
            showCentralSpinner()
        }
        
        KPWebCall.call.postRequestApiClinet(relPath: "nurse_appointmentdetail.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.appointmenthistory = nil
                                self.tableView.backgroundView = nil
                                arrData.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        appointmenthistory = Appointmenthistory(dic: data)
                                    }
                                    imagePhoto.kf.setImage(with: URL(string:appointmenthistory.doctor_profile_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                                    }
                                    
                                    lblDrName.text = appointmenthistory.doctor_first_name + " " + appointmenthistory.doctor_last_name
                                    viewTop.frame.size.height = 197 * _widthRatio
                                    arrData.append(["ID:",appointmenthistory.appointment_order_id!,true])
                                    arrData.append(["Date & Time:",appointmenthistory.booking_date! + " " + appointmenthistory.booking_time,true])
                                    
                                    let address = appointmenthistory.doctor_experience_clinic_name + appointmenthistory.doctor_experience_clinic_address
                                    if !String.validateStringValue(str: address){
                                        arrData.append(["Address:",appointmenthistory.doctor_experience_clinic_name + "\n" + appointmenthistory.doctor_experience_clinic_address!,true])
                                    }
                                    
                                    arrData.append(["Patient Name:",appointmenthistory.patient_first_name! + " " + appointmenthistory.patient_last_name!,true])
                                    arrData.append(["Mobile No:",appointmenthistory.patient_phone!,true])
                                    arrData.append(["Consultation Address:",appointmenthistory.patient_consultation_address!,true])
                                    arrData.append(["Preferred Pharmacy Name:",appointmenthistory.pharmacy_name!,true])
                                    arrData.append(["Preferred Delivery Type:",appointmenthistory.delivery_name!,true])
                                    arrData.append(["Payment Status:",appointmenthistory.payment_status!,true])
                                    if isWorkingCountryUK(){
                                        
                                        
                                        
                                        
                                        if appointmenthistory.triage_status == "1"{
                                            
                                            if appointmenthistory.appointment_status == "0"{
                                                arrData.append(["Verification Code:",appointmenthistory.verification_code!,false])
                                            }
                                            arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                            
                                            if appointmenthistory.referralfile != ""{
                                                arrData.append(["Referral Note:",appointmenthistory.referralfile!,false])
                                            }
                                            if appointmenthistory.referralfile2 != ""{
                                                arrData.append(["Referral Note 2:",appointmenthistory.referralfile2!,false])
                                            }
                                            if appointmenthistory.sicknotefile != ""{
                                                arrData.append(["Sick Note:",appointmenthistory.sicknotefile!,false])
                                            }
                                            if appointmenthistory.sicknotefile2 != ""{
                                                arrData.append(["Sick Note 2:",appointmenthistory.sicknotefile2!,false])
                                            }
                                            
                                             if appointmenthistory.appointment_status == "2"{
                                                arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                                arrData.append(["Reason:",appointmenthistory.cancel_reason!,true])
                                                arrData.append(["Cancelled by:",appointmenthistory.cancel_by!,true])
                                            }else if appointmenthistory.appointment_status == "3"{
                                                arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                            }else{
                                                arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                            }
                                         }else if appointmenthistory.triage_status == "2"{
                                            arrData.append(["Reason:","Rejected",true])
                                        }else{
                                            arrData.append(["Appointment Status:","pending",true])
                                        }
                                    }else{
                                        arrData.append(["Verification Code:",appointmenthistory.verification_code!,false])
                                        if appointmenthistory.appointment_status == "0"{
                                            arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                        }else if appointmenthistory.appointment_status == "1"{
                                            arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                            if appointmenthistory.referralfile != ""{
                                                arrData.append(["Referral Note:",appointmenthistory.referralfile!,false])
                                            }
                                            if appointmenthistory.sicknotefile != ""{
                                                arrData.append(["Sick Note:",appointmenthistory.sicknotefile!,false])
                                            }
                                        }else if appointmenthistory.appointment_status == "2"{
                                            arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                            arrData.append(["Reason:",appointmenthistory.cancel_reason!,true])
                                            arrData.append(["Cancelled by:",appointmenthistory.cancel_by!,true])
                                        }else if appointmenthistory.appointment_status == "3"{
                                            arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                        }else{
                                            arrData.append(["Appointment Status:",appointmenthistory.status!,true])
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updateconsultationaddress(addressID : String){
        showCentralSpinner()
        let parameters = "{\"data\":{\"appointmentId\":\"\(appoinmentID)\",\"addressId\":\"\(addressID)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "updateconsultationaddress.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                if response.ReturnedData != nil{
                    do {
                        if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                            print(response)
                            DispatchQueue.main.async { [self] in
                                if let Response = response as? NSDictionary{
                                    if Response.getIntValue(key: "success") == 1{
                                      
                                        
                                            if isFor == "DR" || isFor == "1"{
                                                appointmentDetailv(loader: true)
                                            }else{
                                                appointmentDetailNureser(loader: true)
                                            }
                                        
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                    }
                    
                    
                }
            }
        }
    }
    
    
    func getAgoraAccessToken(){
        
       
        showCentralSpinner()
        let userrcoding = Int(appointmenthistory.doctor_id)! + Int(appointmenthistory.patient_id)!
        userrcodingAgora = "\(userrcoding)"
        let cehck = "CHECK" + appointmenthistory.appointment_order_id
        let parameters = "{\"data\":{\"vChannelName\":\"\(cehck)\",\"vUserAccount\":\"0\",\"nRole\":\"0\",\"nAgoraRecordingUserId\":\"\(userrcoding)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "getAgoraAccessToken.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                if response.ReturnedData != nil{
                    do {
                        if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                            print(response)
                            DispatchQueue.main.async { [self] in
                                if let Response = response as? NSDictionary{
                                    self.tableView.backgroundView = nil
                                    if Response.getIntValue(key: "success") == 1{
                                        if let data = Response.value(forKey: "data") as? NSDictionary{
                                            Token = data.getStringValue(key: "AccessToken")
                                            vAccessTokenForAgoraRecording = data.getStringValue(key: "AccessTokenForAgoraRecording")
                                            strAppointmentIdGlobal = appoinmentID
                                            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "VideoChatVC")  as! VideoChatVC
                                            mapVc1.strName = appointmenthistory.doctor_first_name + " " + appointmenthistory.doctor_last_name
                                            mapVc1.imageProfile = appointmenthistory.doctor_profile_image
                                            if  appointmenthistory.appoint_type == "audio"{
                                                mapVc1.isVideo = false
                                            }else{
                                                mapVc1.isVideo = true
                                            }
                                            mapVc1.chanal = appointmenthistory.appointment_order_id
                                            navigationController?.pushViewController(mapVc1, animated: true)
                                        }
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
            }
        }
    
        
        
    }
}
extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
    
    
    
}

