//
//  BookAppointmentVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 20/04/21.
//

import UIKit
import Foundation
var NextDate = Date()

class BookAppointmentCollCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var lblTime: LabelMedium!
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imageBg: UIImageView!
    @IBOutlet weak var viewDateBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let _ = viewDateBg{
            viewDateBg.maskCirclewe()
        }
        if let _ = imageBg{
            
        }
    }
}
class BookSlotCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var collectionTimeSlot: UICollectionView!
    weak var bookAppointment : BookTimeVC!
    
    
    
}


class BookAppointmentCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var hightColletion: NSLayoutConstraint!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnProview: UIButton!
    @IBOutlet weak var lblDate: LabelMedium!
    @IBOutlet weak var imageDay: UIImageView!
    @IBOutlet weak var imageSelect: UIImageView!
    @IBOutlet weak var lblDay: LabelMedium!
    @IBOutlet weak var lblTime: LabelMedium!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var collectionTime: UICollectionView!
    
    
    
    
    weak var bookAppointment : BookAppointmentVC!
    var isDateSlot = 0
    var bookTime : BookTimeVC!
    let currentDate1 = NSDate()
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "Date" || reuseIdentifier == "Day" || reuseIdentifier == "Time"{
            viewBg.makeCornerRound(redias: 5)
        }
    }
    
}
class BookAppointmentVC: ParentViewController {
    @IBOutlet weak var blheaderTitle: LabelMedium!
    @IBOutlet weak var btnUplaodDoumnet: submitButton!
    @IBOutlet weak var viewBottom: UIView!
    
    
    var strDate = ""
    var today = Date()
    var customday = Date()
    var allData = [[Any]]()
    var week = Date()
    var indext = 0
    var isDoctor = false
    var docterDetailes : DocterDetailes!
    var nurseDetailes : NurseDetailes!
    var doctor_id = ""
    var appointmentType = ""
    var isResidule = false
    var appointmentId = ""
    var rescheduleTime = ""
    var selectColor = false
    var selectIndex = -1
    var DoctoreName = ""
    var DotoreSpectry = ""
    var DoctoreImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnUplaodDoumnet.isHidden = true
        today = customday
        NextDate = customday
        let weekDate = Calendar.current.date(byAdding: .day, value: 13, to: customday)
        week = weekDate!
        allData.append([#imageLiteral(resourceName: "ic_morning_grey"),"Morning","8 am - 12 pm",true])
        allData.append([#imageLiteral(resourceName: "ic_afternoon_grey"),"Afternoon","12 pm - 6 pm",false])
        allData.append([#imageLiteral(resourceName: "ic_evening_grey"),"Evening","6 pm - 12 am",false])
        allData.append([#imageLiteral(resourceName: "ic_midnight_grey"),"Mid Night","12 am - 8 am",false])
        if isResidule{
            blheaderTitle.text = "Reschedule Appointment"
            viewBottom.frame.size.height = 155
            timeslot(booking_day: Day(str : today),booking_date: CalanderDate(str : today))
            
        }else{
            getFirebaseAnalytics(screenName : getEventName.SELECT_TIMESLOT,isPatient_id : true)
            viewBottom.frame.size.height = 0
            blheaderTitle.text = "Book An Appointment"
            if isDoctor{
                getdoctorlisttimeslotwise(booking_day: Day(str : today),booking_date: CalanderDate(str : today))
            }else{
                nursebookingdetail(booking_day: Day(str : today),booking_date: CalanderDate(str : today))
            }
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //   getuserprofile()
        
    }
    
    
    
    
    @IBAction func btnPreviewClicked(_ sender: Any) {
        let nextDate = Calendar.current.date(byAdding: .day, value: -1, to: customday)
        customday = nextDate!
        NextDate = customday
        if isResidule{
            timeslot(booking_day: Day(str : customday),booking_date: CalanderDate(str : customday))
        }else{
            if isDoctor{
                getdoctorlisttimeslotwise(booking_day: Day(str : customday),booking_date: CalanderDate(str : customday))
            }else{
                nursebookingdetail(booking_day: Day(str : customday),booking_date: CalanderDate(str : customday))
            }
        }
        
        indext = 0
        selectIndex = -1
        tableView.reloadData()
        
        
        
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: customday)
        customday = nextDate!
        NextDate = customday
        if isResidule{
            timeslot(booking_day: Day(str : customday),booking_date: CalanderDate(str : customday))
        }else{
            if isDoctor{
                getdoctorlisttimeslotwise(booking_day: Day(str : customday),booking_date: CalanderDate(str : customday))
            }else{
                nursebookingdetail(booking_day: Day(str : customday),booking_date: CalanderDate(str : customday))
            }
        }
        indext = 0
        selectIndex = -1
        tableView.reloadData()
    }
    
    func Day(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    func CalanderDate(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    func time(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Appointment"{
            let vc = segue.destination as! AppointmentConfirmationVC
            vc.isDoctor = isDoctor
            vc.strTime = sender as! String
            vc.strdate = CalanderDate(str : customday)
            vc.isDoctor = isDoctor
            vc.appointmentType = appointmentType
            if isDoctor{
                vc.docterDetailes = docterDetailes
            }else{
                vc.nurseDetailes = nurseDetailes
            }
        }
        
    }
    
    @IBAction func btnuploadClicked(_ sender: Any) {
        if String.validateStringValue(str: rescheduleTime){
            showFailMessage(message: "Please Select Time Slot")
        }else if selectIndex == -1{
            showFailMessage(message: "Please Select Time Slot")
        }else{
            rescheduleAppointment(time : rescheduleTime)
        }
        
    }
    
    
}
extension BookAppointmentVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            
        }else{
            if indexPath.row == 0{
                indext = indexPath.section - 1
                selectIndex = -1
                tableView.reloadData()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isDoctor{
            if let _ = docterDetailes{
                return allData.count + 1
            }
        }else{
            if let _ = nurseDetailes{
                return allData.count + 1
            }
        }
        
        return  1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 1{
                return 50 * _widthRatio
            }
            return 100 * _widthRatio
        }else if indexPath.section == 1{
            if allData[indexPath.section - 1][3] as! Bool{
                if indexPath.row == 0{
                    return 40 * _widthRatio
                }
                if let _ = docterDetailes{
                    if docterDetailes.time_slot.arryaMorning.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                if let _ = nurseDetailes{
                    if nurseDetailes.time_slot.arryaMorning.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                return 0
            }else{
                if indexPath.row == 0{
                    return 40 * _widthRatio
                }
                return 0
            }
            
        } else if indexPath.section == 2{
            if allData[indexPath.section - 1][3] as! Bool{
                if indexPath.row == 0{
                    return 40 * _widthRatio
                }
                if let _ = docterDetailes{
                    if docterDetailes.time_slot.arryaAfternoon.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                if let _ = nurseDetailes{
                    if nurseDetailes.time_slot.arryaAfternoon.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                return 0
            }else{
                if indexPath.row == 0{
                    return 40 * _widthRatio
                }
                return 0
            }
            
        }  else if indexPath.section == 3{
            if allData[indexPath.section - 1][3] as! Bool{
                if indexPath.row == 0{
                    return 40 * _widthRatio
                }
                if let _ = docterDetailes{
                    if docterDetailes.time_slot.arryaEvening.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                if let _ = nurseDetailes{
                    if nurseDetailes.time_slot.arryaEvening.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                return 0
            }else{
                if indexPath.row == 0{
                    return 40 * _widthRatio
                }
                return 0
            }
            
        }else {
            if allData[indexPath.section - 1][3] as! Bool{
                if indexPath.row == 0{
                    return 50 * _widthRatio
                }
                if let _ = docterDetailes{
                    if docterDetailes.time_slot.arryaNight.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                if let _ = nurseDetailes{
                    if nurseDetailes.time_slot.arryaNight.count > 0{
                        return UITableView.automaticDimension
                    }
                }
                return 0
            }else{
                if indexPath.row == 0{
                    return 50 * _widthRatio
                }
                return 0
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 2
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
                cell.imageDr.kf.setImage(with: URL(string:DoctoreImage), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
                }
                cell.lblExp.text = DotoreSpectry
                cell.lblDrName.text = DoctoreName
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Date") as! BookAppointmentCell
            if today == customday{
                cell.btnProview.isHidden = true
            }else{
                cell.btnProview.isHidden = false
            }
            if week == customday{
                cell.btnNext.isHidden = true
            }else{
                cell.btnNext.isHidden = false
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE, d MMMM"
            let todaysDate = dateFormatter.string(from: customday)
            cell.lblDate.text =   "\(todaysDate)"
            return cell
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Day") as! BookAppointmentCell
                cell.imageDay.image = allData[indexPath.section - 1][0] as! UIImage
                cell.lblDay.text =   allData[indexPath.section - 1][1] as! String
                cell.lblTime.text =   allData[indexPath.section - 1][2] as! String
                if indexPath.section == allData.count{
                    cell.constraintTop.constant = -5
                    cell.constraintBottom.constant = 5
                    if indext == indexPath.section - 1{
                        cell.constraintBottom.constant = -5
                    }
                }else{
                    cell.constraintBottom.constant = -5
                    cell.constraintTop.constant = -5
                }
                if indext == indexPath.section - 1{
                    cell.imageDay.setImageColor(color: .orange)
                    cell.imageSelect.image = #imageLiteral(resourceName: "ic_navigation_aro_up")
                    allData[indexPath.section - 1][3] = true
                }else{
                    cell.imageDay.setImageColor(color: .lightGray)
                    cell.imageSelect.image = #imageLiteral(resourceName: "ic_dropdown")
                    allData[indexPath.section - 1][3] = false
                }
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Time") as! BookAppointmentCell
            cell.bookAppointment = self
            cell.isDateSlot = indexPath.section - 1
            if indexPath.section == allData.count{
                cell.constraintBottom.constant = 5
            }else{
                cell.constraintBottom.constant = -5
            }
            cell.collectionTime.reloadData()
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.hightColletion.constant = cell.collectionTime.collectionViewLayout.collectionViewContentSize.height
            cell.collectionTime.reloadData()
            return cell
        }
        
    }
    
    
    
    
}
extension BookAppointmentCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = bookTime {
            var shouldEnable = false
            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DRListVC")  as! DRListVC
            mapVc1.strDay = bookTime.Day(str : bookTime.customday)
            mapVc1.strDate = bookTime.CalanderDate(str : bookTime.customday)
            if isDateSlot == 0{
                
                mapVc1.strTime = bookTime.arrayMonday[indexPath.row][0]

                if bookTime.arrayMonday[indexPath.row][1] == "1"{
                    shouldEnable = true
                }else{
                    shouldEnable = false
                }
                
             //    mapVc1.strTime = bookTime.arryaMonday[indexPath.row]
             //   shouldEnable = isTimeEnable(strTime: bookTime.arryaMonday[indexPath.row]) ? true : false
                
            }else if isDateSlot == 1{
                
                
                mapVc1.strTime = bookTime.arrayAfternoon[indexPath.row][0]

                if bookTime.arrayAfternoon[indexPath.row][1] == "1"{
                    shouldEnable = true
                }else{
                    shouldEnable = false
                }
                
                
                
//                mapVc1.strTime = bookTime.arryaAfternon[indexPath.row]
//                shouldEnable = isTimeEnable(strTime: bookTime.arryaAfternon[indexPath.row]) ? true : false
                
            }else if isDateSlot == 2{
                
                mapVc1.strTime = bookTime.arrayEvent[indexPath.row][0]

                if bookTime.arrayEvent[indexPath.row][1] == "1"{
                    shouldEnable = true
                }else{
                    shouldEnable = false
                }
                
                
//                mapVc1.strTime = bookTime.arryaEvent[indexPath.row]
//                shouldEnable = isTimeEnable(strTime: bookTime.arryaEvent[indexPath.row]) ? true : false
                
            }else{
                mapVc1.strTime = bookTime.arryaNigh[indexPath.row]
                shouldEnable = isTimeEnable(strTime: bookTime.arryaNigh[indexPath.row]) ? true : false
                
            }
            if shouldEnable{
                bookTime.navigationController?.pushViewController(mapVc1, animated: true)
            }
            return
        }
        
        if bookAppointment.isResidule{
            if bookAppointment.isDoctor{
                if isDateSlot == 0{
                    if bookAppointment.docterDetailes.time_slot.arryaMorning[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.docterDetailes.time_slot.arryaMorning[indexPath.row].time
                    }else{
                        return
                    }
                }else if isDateSlot == 1{
                    if bookAppointment.docterDetailes.time_slot.arryaAfternoon[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.docterDetailes.time_slot.arryaAfternoon[indexPath.row].time
                    }else{
                        return
                    }
                }else if isDateSlot == 2{
                    if bookAppointment.docterDetailes.time_slot.arryaEvening[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.docterDetailes.time_slot.arryaEvening[indexPath.row].time
                    }else{
                        return
                    }
                }else{
                    if bookAppointment.docterDetailes.time_slot.arryaNight[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.docterDetailes.time_slot.arryaNight[indexPath.row].time
                    }else{
                        return
                    }
                }
            }else{
                if isDateSlot == 0{
                    if bookAppointment.nurseDetailes.time_slot.arryaMorning[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.nurseDetailes.time_slot.arryaMorning[indexPath.row].time
                    }else{
                        return
                    }
                }else if isDateSlot == 1{
                    if bookAppointment.nurseDetailes.time_slot.arryaAfternoon[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.nurseDetailes.time_slot.arryaAfternoon[indexPath.row].time
                    }else{
                        return
                    }
                }else if isDateSlot == 2{
                    if bookAppointment.nurseDetailes.time_slot.arryaEvening[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.nurseDetailes.time_slot.arryaEvening[indexPath.row].time
                    }else{
                        return
                    }
                }else{
                    if bookAppointment.nurseDetailes.time_slot.arryaNight[indexPath.row].time_status == "1"{
                        bookAppointment.selectColor = true
                        bookAppointment.selectIndex = indexPath.row
                        bookAppointment.rescheduleTime = bookAppointment.nurseDetailes.time_slot.arryaNight[indexPath.row].time
                    }else{
                        return
                    }
                }
            }
            bookAppointment.tableView.reloadData()
        }else{
            if bookAppointment.isDoctor{
                if isDateSlot == 0{
                    if bookAppointment.docterDetailes.time_slot.arryaMorning[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.docterDetailes.time_slot.arryaMorning[indexPath.row].time)
                    }else{
                        return
                    }
                }else if isDateSlot == 1{
                    if bookAppointment.docterDetailes.time_slot.arryaAfternoon[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.docterDetailes.time_slot.arryaAfternoon[indexPath.row].time)
                    }else{
                        return
                    }
                }else if isDateSlot == 2{
                    if bookAppointment.docterDetailes.time_slot.arryaEvening[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.docterDetailes.time_slot.arryaEvening[indexPath.row].time)
                    }else{
                        return
                    }
                }else{
                    if bookAppointment.docterDetailes.time_slot.arryaNight[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.docterDetailes.time_slot.arryaNight[indexPath.row].time)
                    }else{
                        return
                    }
                }
            }else{
                if isDateSlot == 0{
                    if bookAppointment.nurseDetailes.time_slot.arryaMorning[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.nurseDetailes.time_slot.arryaMorning[indexPath.row].time)
                    }else{
                        return
                    }
                }else if isDateSlot == 1{
                    if bookAppointment.nurseDetailes.time_slot.arryaAfternoon[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.nurseDetailes.time_slot.arryaAfternoon[indexPath.row].time)
                    }else{
                        return
                    }
                }else if isDateSlot == 2{
                    if bookAppointment.nurseDetailes.time_slot.arryaEvening[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.nurseDetailes.time_slot.arryaEvening[indexPath.row].time)
                    }else{
                        return
                    }
                }else{
                    if bookAppointment.nurseDetailes.time_slot.arryaNight[indexPath.row].time_status == "1"{
                        bookAppointment.performSegue(withIdentifier: "Appointment", sender: bookAppointment.nurseDetailes.time_slot.arryaNight[indexPath.row].time)
                    }else{
                        return
                    }
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = bookTime {
            if isDateSlot == 0{
                return bookTime.arrayMonday.count
            }else if isDateSlot == 1{
                return bookTime.arrayAfternoon.count
            }else if isDateSlot == 2{
                return bookTime.arrayEvent.count
            }else{
                return bookTime.arryaNigh.count
            }
        }
        
        if bookAppointment.isDoctor{
            if isDateSlot == 0{
                if let _ = bookAppointment.docterDetailes.time_slot{
                    return bookAppointment.docterDetailes.time_slot.arryaMorning.count
                }
            }else if isDateSlot == 1{
                if let _ = bookAppointment.docterDetailes.time_slot{
                    return bookAppointment.docterDetailes.time_slot.arryaAfternoon.count
                }
            }else if isDateSlot == 2{
                if let _ = bookAppointment.docterDetailes.time_slot{
                    return bookAppointment.docterDetailes.time_slot.arryaEvening.count
                }
            }else{
                if let _ = bookAppointment.docterDetailes.time_slot{
                    return bookAppointment.docterDetailes.time_slot.arryaNight.count
                }
            }
        }else{
            if isDateSlot == 0{
                if let _ = bookAppointment.nurseDetailes.time_slot{
                    return bookAppointment.nurseDetailes.time_slot.arryaMorning.count
                }
            }else if isDateSlot == 1{
                if let _ = bookAppointment.nurseDetailes.time_slot{
                    return bookAppointment.nurseDetailes.time_slot.arryaAfternoon.count
                }
            }else if isDateSlot == 2{
                if let _ = bookAppointment.nurseDetailes.time_slot{
                    return bookAppointment.nurseDetailes.time_slot.arryaEvening.count
                }
            }else{
                if let _ = bookAppointment.nurseDetailes.time_slot{
                    return bookAppointment.nurseDetailes.time_slot.arryaNight.count
                }
            }
        }
        
        return 0
    }
    func isTimeEnable(strTime : String) -> Bool{
        
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale(identifier:"en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let time = formatter.string(from: Date().adding(minutes: 0))  // 2021-07-02 11:50 PM
        
        let dateCurrent = formatter.date(from: time)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strSelectedDate = dateFormatter.string(from: bookTime.customday)
        
        
        
        let strTemp = strSelectedDate + " " + strTime //  2021-07-02 11:25 PM
        
        let formatter4 = DateFormatter()
        formatter4.timeZone = TimeZone.current
        formatter4.locale = Locale(identifier:"en_US_POSIX")
        formatter4.dateFormat = "yyyy-MM-dd hh:mm a"
        let dateSelected = formatter4.date(from: strTemp)!
        
        if dateCurrent! > dateSelected {
            return false
        }else{
            return true
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Time", for: indexPath) as! BookAppointmentCollCell
        
        if let _ = bookTime {
            if isDateSlot == 0{
                //                if bookTime.checkTime(Start : bookTime.arryaMonday[indexPath.row], End: "7:40 AM"){
                //                     print(true)
                //               }else{
                //                print(false)
                //               }
            
                cell.lblTime.text = bookTime.arrayMonday[indexPath.row][0]
                
                if bookTime.arrayMonday[indexPath.row][1] == "1"{
                    cell.lblTime.textColor = UIColor.colorchaupbule()
                }else{
                    cell.lblTime.textColor = .lightGray
                }
                
               // cell.lblTime.textColor = isTimeEnable(strTime: bookTime.arrayMonday[indexPath.row][0]) ? UIColor.colorchaupbule() : UIColor.lightGray
                
            }else if isDateSlot == 1{
                cell.lblTime.text = bookTime.arrayAfternoon[indexPath.row][0]
                
                if bookTime.arrayAfternoon[indexPath.row][1] == "1"{
                    cell.lblTime.textColor = UIColor.colorchaupbule()
                }else{
                    cell.lblTime.textColor = .lightGray
                }
                
                
               // cell.lblTime.textColor = isTimeEnable(strTime: bookTime.arrayAfternoon[indexPath.row][0]) ? UIColor.colorchaupbule() : UIColor.lightGray
                
            }else if isDateSlot == 2{
                //                if bookTime.checkTime(Start : bookTime.arryaEvent[indexPath.row], End: "7:40 AM"){
                //                     print(true)
                //               }else{
                //                print(false)
                //               }
                cell.lblTime.text = bookTime.arrayEvent[indexPath.row][0]
                
                if bookTime.arrayEvent[indexPath.row][1] == "1"{
                    cell.lblTime.textColor = UIColor.colorchaupbule()
                }else{
                    cell.lblTime.textColor = .lightGray
                }
                
                
              //  cell.lblTime.textColor = isTimeEnable(strTime: bookTime.arrayEvent[indexPath.row][0]) ? UIColor.colorchaupbule() : UIColor.lightGray
                
            }else{
                cell.lblTime.text = bookTime.arryaNigh[indexPath.row]
                cell.lblTime.textColor = isTimeEnable(strTime: bookTime.arryaNigh[indexPath.row]) ? UIColor.colorchaupbule() : UIColor.lightGray
                
            }
            
        }
        if let _ = bookAppointment{
            if bookAppointment.isResidule{
                if bookAppointment.selectIndex == indexPath.row{
                    cell.backgroundColor = .orange
                }else{
                    cell.backgroundColor = .white
                }
            }
            if bookAppointment.isDoctor{
                if isDateSlot == 0{
                    cell.lblTime.text = bookAppointment.docterDetailes.time_slot.arryaMorning[indexPath.row].time
                    
                    if bookAppointment.docterDetailes.time_slot.arryaMorning[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }else if isDateSlot == 1{
                    cell.lblTime.text = bookAppointment.docterDetailes.time_slot.arryaAfternoon[indexPath.row].time
                    if bookAppointment.docterDetailes.time_slot.arryaAfternoon[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }else if isDateSlot == 2{
                    cell.lblTime.text = bookAppointment.docterDetailes.time_slot.arryaEvening[indexPath.row].time
                    if bookAppointment.docterDetailes.time_slot.arryaEvening[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }else{
                    cell.lblTime.text = bookAppointment.docterDetailes.time_slot.arryaNight[indexPath.row].time
                    if bookAppointment.docterDetailes.time_slot.arryaNight[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }
            }else{
                if isDateSlot == 0{
                    cell.lblTime.text = bookAppointment.nurseDetailes.time_slot.arryaMorning[indexPath.row].time
                    if bookAppointment.nurseDetailes.time_slot.arryaMorning[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }else if isDateSlot == 1{
                    cell.lblTime.text = bookAppointment.nurseDetailes.time_slot.arryaAfternoon[indexPath.row].time
                    if bookAppointment.nurseDetailes.time_slot.arryaAfternoon[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }else if isDateSlot == 2{
                    cell.lblTime.text = bookAppointment.nurseDetailes.time_slot.arryaEvening[indexPath.row].time
                    if bookAppointment.nurseDetailes.time_slot.arryaEvening[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }else{
                    cell.lblTime.text = bookAppointment.nurseDetailes.time_slot.arryaNight[indexPath.row].time
                    if bookAppointment.nurseDetailes.time_slot.arryaNight[indexPath.row].time_status == "1"{
                        cell.lblTime.textColor = UIColor.colorchaupbule()
                    }else{
                        cell.lblTime.textColor = .lightGray
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        return CGSize(width: 374 * _widthRatio / 3 , height: 35)
        return CGSize(width: (collectionView.frame.size.width / 3) - 15, height: 35)
        
        
    }
    
    
}
extension BookAppointmentVC{
    func getdoctorlisttimeslotwise(booking_day: String,booking_date: String){
        var sdtr = [String]()
        var dic = [String:Any]()
        // dic["memberId"] = _currentUser.patient_id!
        dic["booking_day"] = booking_day
        dic["booking_date"] = booking_date
        dic["doctor_id"] = doctor_id
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        var timeSlots: [Date] = []
        var DateFormeting = [String]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strSelectedDate = dateFormatter.string(from: today)
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "doctorbookingdetail_New.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.docterDetailes = nil
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        docterDetailes = DocterDetailes(dic: data)
                                        docterDetailes.time_slot.arryaMorning.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        docterDetailes.time_slot.arryaAfternoon.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        docterDetailes.time_slot.arryaEvening.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        docterDetailes.time_slot.arryaNight.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func nursebookingdetail(booking_day: String,booking_date: String){
        
        var dic = [String:Any]()
        dic["booking_day"] = booking_day
        dic["booking_date"] = booking_date
        dic["nurse_id"] = doctor_id
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "nursebookingdetail.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.nurseDetailes = nil
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        nurseDetailes = NurseDetailes(dic: data)
                                        
                                        nurseDetailes.time_slot.arryaMorning.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        nurseDetailes.time_slot.arryaAfternoon.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        nurseDetailes.time_slot.arryaEvening.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                        nurseDetailes.time_slot.arryaNight.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                    }
                                }else{
                                    
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func timeslot(booking_day: String,booking_date: String){
        var stringURL = ""
        var dic = [String:Any]()
        if appointmentType == "DR"{
            stringURL = "doctorbookingdetail_New.php"
            dic["doctor_id"] = doctor_id
        }else{
            stringURL = "timeslot.php"
            dic["id"] = doctor_id
        }
        dic["type"] = appointmentType // DR
        dic["booking_day"] = booking_day
        dic["booking_date"] = booking_date
        // dic["id"] = doctor_id //doctor_id
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: stringURL , param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.docterDetailes = nil
                                self.nurseDetailes = nil
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        if appointmentType == "DR"{
                                            docterDetailes = DocterDetailes(dic: data)
                                            docterDetailes.time_slot.arryaMorning.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                            docterDetailes.time_slot.arryaAfternoon.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                            docterDetailes.time_slot.arryaEvening.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                            docterDetailes.time_slot.arryaNight.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })

                                        }else{
                                            nurseDetailes = NurseDetailes(dic: data)
                                            nurseDetailes.time_slot.arryaMorning.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                            nurseDetailes.time_slot.arryaAfternoon.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                            nurseDetailes.time_slot.arryaEvening.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })
                                            nurseDetailes.time_slot.arryaNight.sort(by: { $0.custeDate.compare($1.custeDate) == .orderedAscending })

                                        }
                                    }
                                    btnUplaodDoumnet.isHidden = false
                                    
                                }else{
                                    btnUplaodDoumnet.isHidden = true
                                    
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func rescheduleAppointment(time : String){
        var dic = [String:Any]()
        dic["appointment_id"] = appointmentId
        dic["new_date"] = CalanderDate(str : customday)
        dic["new_time"] = time
        dic["key"] = appointmentType
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "rescheduleAppointment.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        strdate = data.getStringValue(key: "booking_date")
                                    }
                                    
                                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDoneVC")  as! AppointmentDoneVC
                                    mapVc1.docterDetailes = docterDetailes
                                    mapVc1.nurseDetailes = nurseDetailes
                                    mapVc1.isResuldue = isResidule
                                    if isDoctor{
                                        mapVc1.imageProfile = docterDetailes.doctor_profile_image
                                        mapVc1.FullName = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                                        if isWorkingCountryUK() {
                                            mapVc1.Specialist = docterDetailes.doctor_specialization
                                        } else {
                                            mapVc1.Specialist = docterDetailes.doctor_specialization
                                        }
                                    }else{
                                        mapVc1.imageProfile = nurseDetailes.nurse_profile_image
                                        mapVc1.FullName = nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
                                        mapVc1.Specialist = nurseDetailes.nurse_specialization
                                    }
                                    mapVc1.isDoctor = isDoctor
                                    mapVc1.strTime = time
                                    mapVc1.strdate = strdate
                                    navigationController?.pushViewController(mapVc1, animated: true)
                                }else{
                                    showResponseMessage(dict: Response)
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
}

extension BookAppointmentVC{
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        if dataname.getIntValue(key: "isVerified") == 2{
                                            btnUplaodDoumnet.isHidden = true
                                        }else{
                                            btnUplaodDoumnet.isHidden = false
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
