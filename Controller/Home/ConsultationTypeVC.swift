//
//  ConsultationTypeVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 19/04/21.
//

import UIKit
import FBSDKCoreKit
class ConsultationTypeCell: ConstrainedTableViewCell {
    
    
    @IBOutlet weak var btnTitleName: submitButton!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblDesc: LabelMedium!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias: 5)

    }
}
class ConsultationTypeVC: ParentViewController {
    var isDoctor = false
    var docterDetailes : DocterDetailes!
    var nurseDetailes : NurseDetailes!
    
    var appointmentType = ""
    var strDate = ""
    var strDay = ""
    var strTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFirebaseAnalytics(screenName : getEventName.SELECT_APPOINTMENT_TYPE,isPatient_id : true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Verification"{
            let vc = segue.destination as! IdentityVerificationVC
            vc.isDoctor = isDoctor
        }
        if segue.identifier == "TimeSlot"{
            let vc = segue.destination as! BookAppointmentVC
            if isDoctor{
                vc.doctor_id = docterDetailes.doctor_id
                vc.DoctoreImage =  docterDetailes.doctor_profile_image
                vc.DotoreSpectry = docterDetailes.doctor_specialization
                vc.DoctoreName =  docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
            }else{
                vc.doctor_id = nurseDetailes.nurse_id
                vc.DoctoreImage = nurseDetailes.nurse_profile_image
                vc.DotoreSpectry = nurseDetailes.nurse_specialization
                vc.DoctoreName =   nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
            }
            vc.isDoctor = isDoctor
            vc.appointmentType = appointmentType
        }
    }
    
    
    
    @IBAction func btnShowClicked(_ sender: UIButton) {
        var strType = ""
        var strNameType = ""
        if isDoctor{
            strType = "doctor"
            strNameType = docterDetailes.doctor_first_name + " " +  docterDetailes.doctor_last_name
        }else{
            strType = "nurse"
            strNameType = nurseDetailes.nurse_first_name + " " +  nurseDetailes.nurse_last_name
        }
        if sender.tag == 1{
            if isDoctor{
                appointmentType = "meeting"
            }else{
                appointmentType = "home"
            }
            let parameters = [
                AppEvents.ParameterName("Name").rawValue: "\(strNameType)",
                AppEvents.ParameterName("Type").rawValue: "\(strType)",
                AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                AppEvents.ParameterName("Timeslot").rawValue: "\(strDate)" + "\(strTime)",
                AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
              
            ] as [String : Any]
            AppEvents.logEvent(.init("Booking Initiated"), parameters: parameters)
            if isWorkingCountryUK(){
                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentConfirmationVC")  as! AppointmentConfirmationVC
                mapVc1.isDoctor = true
                mapVc1.docterDetailes = docterDetailes
                mapVc1.isDoctor = true
                mapVc1.strTime = strTime
                mapVc1.strdate = strDate
                mapVc1.appointmentType = appointmentType
                navigationController?.pushViewController(mapVc1, animated: true)
            }else{
                performSegue(withIdentifier: "TimeSlot", sender: nil)
            }
        }else if sender.tag == 2{
            if isDoctor{
                appointmentType = "video"
            }else{
                appointmentType = "hospital"
            }
            let parameters = [
                AppEvents.ParameterName("Name").rawValue: "\(strNameType)",
                AppEvents.ParameterName("Type").rawValue: "\(strType)",
                AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                AppEvents.ParameterName("Timeslot").rawValue: "\(strDate)" + "\(strTime)",
                AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
              
            ] as [String : Any]
            AppEvents.logEvent(.init("Booking Initiated"), parameters: parameters)
            if isDoctor{
                appointmentType = "video"
                CheckPatientVerification(appo: appointmentType)
            }else{
                performSegue(withIdentifier: "TimeSlot", sender: nil)
            }
            
           
        }else if sender.tag == 3{
            if isDoctor{
                appointmentType = "audio"
            }else{
                appointmentType = ""
            }
            let parameters = [
                AppEvents.ParameterName("Name").rawValue: "\(strNameType)",
                AppEvents.ParameterName("Type").rawValue: "\(strType)",
                AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                AppEvents.ParameterName("Timeslot").rawValue: "\(strDate)" + "\(strTime)",
                AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
              
            ] as [String : Any]
            AppEvents.logEvent(.init("Booking Initiated"), parameters: parameters)
            CheckPatientVerification(appo: appointmentType)
        }
    }
   
  
}
extension ConsultationTypeVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isDoctor{
            return 4
        }else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
                return 44 * _widthRatio
        }else if indexPath.section == 1{
            
            if isWorkingCountryUK(){
                if isDoctor{
                    return 0
                }else{
                    if indexPath.row == 0{
                        return 50 * _widthRatio
                    }else{
                        return 63 * _widthRatio
                    }
                }
                
              } else {
                if indexPath.row == 0{
                    return 50 * _widthRatio
                }else{
                    return 63 * _widthRatio
                }
            }
            
        }else if indexPath.section == 2{
            if isWorkingCountryUK(){
               if _parameterGB.PARAMETER_VIDEO_CONSULTATION_2 == "1"{
                if indexPath.row == 0{
                    return 50 * _widthRatio
                }else{
                    return 63 * _widthRatio
                }
                }else{
                    return 0
                }
            
            }else{
                if _parameterZW.PARAMETER_VIDEO_CONSULTATION_2 == "1"{
                 if indexPath.row == 0{
                     return 50 * _widthRatio
                 }else{
                     return 63 * _widthRatio
                 }
                 }else{
                     return 0
                 }
            }
        }else{

            if _userDefault.value(forKey: "nUserType") as! Int == 1{
                return 0
            }
            
            if isWorkingCountryUK(){
               if _parameterGB.PARAMETER_AUDIO_CONSULTATION_4 == "1"{
                if indexPath.row == 0{
                    return 50 * _widthRatio
                }else{
                    return 63 * _widthRatio
                }
                }else{
                    return 0
                }
            
            }else{
                if _parameterZW.PARAMETER_AUDIO_CONSULTATION_4 == "1"{
                 if indexPath.row == 0{
                     return 50 * _widthRatio
                 }else{
                     return 63 * _widthRatio
                 }
                 }else{
                     return 0
                 }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return 2
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = "How would you like your Consultation?"
            return cell
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Consulation") as! ConsultationTypeCell
                if isDoctor{
                    cell.lblDesc.text = "Visit your doctor at your schedule appointments."
                }else{
                    cell.lblDesc.text = "Your Nurse will visit you at your home."
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Buttom") as! ConsultationTypeCell
                if isDoctor{
                    cell.btnTitleName.setTitle("  Personal Meeting", for: .normal)
                    cell.btnTitleName.setImage(UIImage(named: "ic_personalmetting"), for: .normal)
                    
                }else{
                    cell.btnTitleName.setTitle("  Visit at Home", for: .normal)
                    cell.btnTitleName.setImage(UIImage(named: "ic_vistiathome"), for: .normal)
                    
                }
                cell.btnTitleName.tag = indexPath.section
                return cell
            }
        }else if indexPath.section == 2{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Consulation") as! ConsultationTypeCell
                if isDoctor{
                    cell.lblDesc.text = "Book Video call with Doctor."
                }else{
                    cell.lblDesc.text = "Your Nurse will visit you at your hospital."
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Buttom") as! ConsultationTypeCell
                if isDoctor{
                    cell.btnTitleName.setTitle("   Video Call", for: .normal)
                    cell.btnTitleName.setImage(UIImage(named: "ic_videocall"), for: .normal)
                }else{
                    cell.btnTitleName.setTitle("   Visit at Hospital", for: .normal)
                    cell.btnTitleName.setImage(UIImage(named: "ic_visitathospital"), for: .normal)
                }
                cell.btnTitleName.tag = indexPath.section
                return cell
            }
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Consulation") as! ConsultationTypeCell
                cell.lblDesc.text = "Book Audio call with Doctor"
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Buttom") as! ConsultationTypeCell
                cell.btnTitleName.setTitle("   Audio Call", for: .normal)
                cell.btnTitleName.setImage(UIImage(named: "ic_call_white"), for: .normal)
                cell.btnTitleName.tag = indexPath.section
                return cell
            }
        }
    }
    
    
    func CheckPatientVerification(appo : String){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "CheckPatientVerification.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        if data.getIntValue(key: "DocumentStatus") == 0{
                                            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "IdentityVerificationVC")  as! IdentityVerificationVC
                                           // mapVc1.delegatesAddress1 = self
                                            navigationController?.pushViewController(mapVc1, animated: true)
                                        }else if data.getIntValue(key: "DocumentStatus") == 1{
                                            let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "IdentityVerificationVC")  as! IdentityVerificationVC
                                           // mapVc1.delegatesAddress1 = self
                                            navigationController?.pushViewController(mapVc1, animated: true)
                                        }else{
                                            if isWorkingCountryUK(){
                                                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentConfirmationVC")  as! AppointmentConfirmationVC
                                                mapVc1.isDoctor = true
                                                mapVc1.docterDetailes = docterDetailes
                                                mapVc1.isDoctor = true
                                                mapVc1.strTime = strTime
                                                mapVc1.strdate = strDate
                                                mapVc1.appointmentType = appo
                                                navigationController?.pushViewController(mapVc1, animated: true)
                                            }else{
                                                performSegue(withIdentifier: "TimeSlot", sender: nil)
                                            }
                                        }
                                    
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}
