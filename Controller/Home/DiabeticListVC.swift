//
//  DiabeticListVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 13/07/21.
//

import UIKit

class DiabeticListVC: ParentViewController {
    @IBOutlet weak var viewBg: UIView!
    var isIsected = 0
    var arryaDiabeticList = [DiabeticList]()
    var deleget : MyHealthVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias : 6)
      //  isIsected = dalegate.IndexNumbrt
        self.tabBarController?.tabBar.isHidden = true
        getProblemList()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnApplyClicked(_ sender: Any) {
        getupdate(key : "p_type",value: "\(arryaDiabeticList[isIsected].patient_type_id!)")
    }
    

}
extension DiabeticListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isIsected = indexPath.row
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaDiabeticList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! UpcomingFilterCell
        
        if isIsected == indexPath.row{
            cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_fill")
        }else{
            cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_unfill")
        }
        
        cell.lblName.text = arryaDiabeticList[indexPath.row].patient_type
        return cell
    }
    
    
    
    
}


extension DiabeticListVC{
    func getProblemList(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "vital_patientTypeList.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaDiabeticList.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                            for str in data {
                                                arryaDiabeticList.append(DiabeticList(dic: str as! NSDictionary))
                                            }
                                    }
                               
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getupdate(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        _currentUser.patient_type = dataname.getStringValue(key: "patient_type")
                                        deleget.getFilter()
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
