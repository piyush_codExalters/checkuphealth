//
//  AppointmentConfirmationVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 20/04/21.
//

import UIKit
import FBSDKCoreKit
protocol getDelivery {
    func getDelivry(delivry : Deliverylist, pharmaci : Pharmacies)
}
protocol getReferll {
    func getFile(uploadFile : Data?, forment : String,referral : String, imageURL : UIImage?)
    func getFile2(forment : String,referral : String)
}
protocol getProblem {
    func getProbem(problemName : String, problemId: String, Quration: [ProblemQuestionList], problemNote : String)
    
}
class AppointmentList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t1 = SingIn()
        t1.placeholder = "What is your appointment for?"
        t1.keybord = .default
        t1.strValue = ""
        
        
        var t6 = SingIn()
        t6.placeholder = "Problem Type"
        t6.keybord = .default
        t6.strValue = ""
        
        var t2 = SingIn()
        t2.placeholder = "Location during appointment"
        t2.keybord = .default
        t2.strValue = ""
        
        
        var t4 = SingIn()
        t4.placeholder = "Select Pharmacy"
        t4.keybord = .default
        t4.strValue = ""
        
        var t5 = SingIn()
        t5.placeholder = "Select Referral"
        t5.keybord = .default
        t5.strValue = ""
        
        var t22 = SingIn()
        t22.placeholder = "I hereby give consent to CheckUp Health for video/ online consultation. I understand it will be recorded for quality assurance and training purposes."

        var t21 = SingIn()
        t21.placeholder = "I confirm that I do not believe this problem to be an emergency. If my symptoms worsen whilst awaiting the appointment I will contact my GP or 111 for advice, or 999 if they become life-threatening."
        
        var t23 = SingIn()
        t23.placeholder = "I confirm I understand that the Doctor will triage my appointment request and may need to contact me prior to the appointment if there are any concerns"
        
        allFields.append(t1)
        allFields.append(t6)
        allFields.append(t2)
        allFields.append(t4)
        allFields.append(t5)
        allFields.append(t22)
        allFields.append(t21)
        allFields.append(t23)
    }
    
    
}

class AppointmentConfirmationCell: ConstrainedTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
class AppointmentConfirmationVC: ParentViewController, UITextViewDelegate, addressDelegete, getDelivery, getReferll, getProblem {
    
    
    @IBOutlet weak var viewBottom: UIView!
    
    
    var fieldData = AppointmentList()
    var strAppointment = "Tell us about your symptoms, condition or reason for booking. Please add as much detail as possible."
    var strTime = ""
    var strdate = ""
    var isDoctor = false
    var docterDetailes : DocterDetailes!
    var nurseDetailes : NurseDetailes!
    var appointmentType = ""
    var selectAddres : AddressListName!
    var deliverylist : Deliverylist!
    var pharmacies : Pharmacies!
    var uploadfile : Data?
    var arryaProblemQuestionList = [ProblemQuestionList]()
    var problemName = ""
    var problemId = ""
    var isEmail = "0"
    var isPhone = "0"
    var referralList = ""
    var strProblemNote = ""
    var imgUploadfile : UIImage?
    var indexz = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        getuserprofile()
        viewBottom.frame.size.height = 95 * _widthRatio
    }
    
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if sender.tag == 10{
            if isEmail == "0"{
                isEmail = "1"
            }else{
                isEmail = "0"
            }
            
            tableView.reloadData()
            return
        }
        if sender.tag == 11{
            if isPhone == "0"{
                isPhone = "1"
            }else{
                isPhone = "0"
            }
            tableView.reloadData()
            return
        }
        if fieldData.allFields[sender.tag].strkey == "0"{
            fieldData.allFields[sender.tag].strkey = "1"
        }else{
            fieldData.allFields[sender.tag].strkey = "0"
        }
        tableView.reloadData()
    }
    
    @IBAction func btnContiuneClikced(_ sender: Any) {
        if validation().0{
            
            if let _ = docterDetailes{
                let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ConfirmPaymentVC")  as! ConfirmPaymentVC
                mapVc1.docterDetailes = docterDetailes
                mapVc1.strTime = strTime
                mapVc1.strdate = strdate
                mapVc1.appointmentType = appointmentType
                mapVc1.selectAddres = selectAddres
                mapVc1.deliverylist = deliverylist
                mapVc1.pharmacies = pharmacies
                mapVc1.isDoctor = isDoctor
                mapVc1.uploadfile = uploadfile
                mapVc1.problemId = problemId
                mapVc1.arryaProblemQuestionList = arryaProblemQuestionList
                mapVc1.isEmail = isEmail
                mapVc1.isPhone = isPhone
                mapVc1.nurseDetailes = nurseDetailes
                mapVc1.referralList = referralList
                mapVc1.strAppointment = strAppointment
                mapVc1.strProblemNote = strProblemNote
                mapVc1.imgUploadfile = imgUploadfile
                navigationController?.pushViewController(mapVc1, animated: true)
            }else{
                
                savenurseappointment()
            }
            
                    
        }else{
            showFailMessage(message : validation().1)
        }
        
    }
    func getProbem(problemName: String, problemId: String, Quration: [ProblemQuestionList], problemNote : String) {
        arryaProblemQuestionList = Quration
        self.problemName = problemName
        self.problemId = problemId
        fieldData.allFields[1].strValue = problemName
        strProblemNote = problemNote
        tableView.reloadData()
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Tell us about your symptoms, condition or reason for booking. Please add as much detail as possible."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 16)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Tell us about your symptoms, condition or reason for booking. Please add as much detail as possible."{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strAppointment
            }
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        strAppointment = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    func validation() -> (Bool,String){
        //  Appointment Desc Validation

        if String.validateStringValue(str: fieldData.allFields[0].strValue) || String.validateStringValue(str: strAppointment) || "Tell us about your symptoms, condition or reason for booking. Please add as much detail as possible." == strAppointment{
            return(false,"Please enter description")
        }
        
        // check uncheck phone number email Validation

        
        if isWorkingCountryUK(){
            if isEmail == "0" && isPhone == "0"{
                return(false,"Please select at-least one preferred contact method.")
            }
            // Problem Type Validation
            if String.validateStringValue(str: fieldData.allFields[1].strValue){
                return(false,"Please select Problem Type")
            }
            
        }
        //  Address Validation
        
        if String.validateStringValue(str: fieldData.allFields[2].strValue){
            if isWorkingCountryUK(){
                if _parameterGB.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
                    if String.validateStringValue(str: fieldData.allFields[2].strValue){
                        return(false,"Please select address")
                    }
                }
            }else{
                if String.validateStringValue(str: fieldData.allFields[2].strValue){
                    return(false,"Please select address")
                }
                
                if isDoctor{
                if _parameterZW.PARAMETER_PESCRIPTION_MODULE_1 == "1" {

                if String.validateStringValue(str: fieldData.allFields[2].strValue){
                    return(false,"Please select address")
                }
                }
                }else{
                    if appointmentType == "home"{
                        if String.validateStringValue(str: fieldData.allFields[2].strValue){
                            return(false,"Please select address")
                        }
                    }
                }
            }
        }
        
        //  Pharmacy Validation

        
        if isDoctor{
            
            /*
             COMMENTED BY YAMUNA. need to hide pharmacy field from this screen

                if isWorkingCountryUK(){
                    if _parameterGB.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
                        if isDoctor{
                            if String.validateStringValue(str: fieldData.allFields[3].strValue){
                                return(false,"Please select pharmacy")
                            }
                        }
                    }
                }else{
                    if _parameterZW.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
                        if isDoctor{
                            if String.validateStringValue(str: fieldData.allFields[3].strValue){
                                return(false,"Please select pharmacy")
                            }
                        }
                        
                    }
                }
                */
            
            
            //  Referral Validation
            
            if docterDetailes.nDoctorType == "2" {
                if isWorkingCountryUK(){
                        if isDoctor{
                            //1 = normal , 2 == Special - referral doctor
                            if String.validateStringValue(str: fieldData.allFields[4].strValue){
                                return(false,"Please select referral")
                            }
                        }
                    
                }else{
                        if isDoctor{
                            //1 = normal , 2 == Special - referral doctor
                            if String.validateStringValue(str: fieldData.allFields[4].strValue){
                                return(false,"Please select referral")
                            }
                        }
                }
            }
        }
        
        //  video and audio check Validation

        if isWorkingCountryUK(){
            if isDoctor{
                if appointmentType == "video" || appointmentType == "audio" {
                    if fieldData.allFields[5].strkey == "0"{
                        return(false,"Give consent to consultant/clinician for video/online consultation")
                    }else if fieldData.allFields[6].strkey == "0"{
                        return(false,"Please confirm emergency note.")
                    }
                }
            }
        }else{
            if isDoctor{
                if appointmentType == "video" || appointmentType == "audio" {
                    if fieldData.allFields[5].strkey == "0"{
                        return(false,"Give consent to consultant/clinician for video/online consultation")
                    }
                }
            }
        }
        
        
        return(true,"")
    }
    func getFile(uploadFile: Data?, forment: String, referral : String, imageURL : UIImage?) {
        fieldData.allFields[4].strValue = forment
        if forment == ""{
            fieldData.allFields[4].strValue = forment
        }
        referralList = referral
        uploadfile = uploadFile
        imgUploadfile = imageURL
        tableView.reloadData()
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    func getFile2( forment: String, referral : String) {
        fieldData.allFields[4].strValue = forment
        if forment == ""{
            fieldData.allFields[4].strValue = forment
        }
        uploadfile = nil
        referralList = referral
        tableView.reloadData()
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func getAddress(str: String, address: AddressListName,index: Int) {
        selectAddres = address
        fieldData.allFields[2].strValue = str
        indexz = index
        tableView.reloadData()
    }
    func getDelivry(delivry: Deliverylist, pharmaci: Pharmacies) {
        deliverylist = delivry
        pharmacies = pharmaci
        fieldData.allFields[3].strValue = pharmacies.name
        tableView.reloadData()
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Pharma"{
            let vc = segue.destination as! SelectPharmacyVC
            vc.selectAddres = selectAddres
            vc.deleget = self
        }
        if segue.identifier == "Book"{
            let vc = segue.destination as! AppointmentDoneVC
            vc.docterDetailes = docterDetailes
            vc.nurseDetailes = nurseDetailes
            vc.isDoctor = isDoctor
            vc.strTime = strTime
            vc.strdate = strdate
            if isDoctor{
                vc.imageProfile = docterDetailes.doctor_profile_image
                vc.FullName = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                if isWorkingCountryUK() {
                    vc.Specialist = docterDetailes.doctor_specialisation_interest
                } else {
                    vc.Specialist = docterDetailes.doctor_specialization
                }
            }else{
                vc.imageProfile = nurseDetailes.nurse_profile_image
                vc.FullName = nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
                vc.Specialist = nurseDetailes.nurse_specialization
            }
        }
        if segue.identifier == "Reffrer"{
            let vc = segue.destination as! SelectReferralVC
            vc.deleget = self
            vc.DoctorId = docterDetailes.doctor_id
        }
    }
}
extension AppointmentConfirmationVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else if section == 1{
            return 3
        }else if section == 2{
            return 4
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            // title appointment for
            
            return 130 * _widthRatio
        }else if indexPath.section == 1{
            // check uncheck phone number email
            
            if isWorkingCountryUK(){
                if indexPath.row == 1 || indexPath.row == 2{
                    return 40 * _widthRatio
                }
                return 30 * _widthRatio
                
            }else{
                return 0
            }
        }else if indexPath.section == 2{
            
            if indexPath.row == 0{
                // Problem Type
                if isWorkingCountryUK(){
                    return 90 * _widthRatio
                }else{
                    return 0
                }
            }
            if indexPath.row == 1{
               // Patient Consulation Address
                if isWorkingCountryUK(){
                    if _parameterGB.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
                        return 75 * _widthRatio
                    }
                    return 0
                }else{
//                    if isDoctor{
//                        if _parameterZW.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
//                            return 75 * _widthRatio
//                        }
//                    }else{
//                        if appointmentType == "home"{
//                            return 75 * _widthRatio
//                        }
//                    }
                    
                    return 75 * _widthRatio
                }
            }
            if indexPath.row == 2{                // Select Pharmacy

                
                return 0 // Added by yamuna. need to hide select pharmacy from this screen
                if isWorkingCountryUK(){
                    if _parameterGB.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
                        return 75 * _widthRatio
                    } else {
                        return 0
                    }
                }else{
                    if _parameterZW.PARAMETER_PESCRIPTION_MODULE_1 == "1" {
                        if isDoctor{
                            
                            if appointmentType == "video" || appointmentType == "audio" {
                                return 75 * _widthRatio
                            } else {
                                return 90 * _widthRatio
                            }
                        }else{
                            return 0
                        }
                    } else {
                        return 0
                    }
                }
            }
            if indexPath.row == 3{
               // Select Referral
                if isDoctor{
                    
                    if docterDetailes.nDoctorType == "2" {
                        if isWorkingCountryUK(){
                                if isDoctor{
                                    //1 = normal , 2 == Special - referral doctor
                                    return 90 * _widthRatio
                                }
                        }else{
                                if isDoctor{
                                    //1 = normal , 2 == Special - referral doctor
                                    return 90 * _widthRatio
                                }
                        }
                    }
                }
                return 0
            }
        }else{
            // video and audio check uncheck
            
            if indexPath.row == 0{
                if isDoctor{
                    if appointmentType == "video" || appointmentType == "audio" {
                        return UITableView.automaticDimension
                    } else {
                        return 0
                    }
                }else{
                    return 0
                }
            }
            
            if indexPath.row == 1{
                if isWorkingCountryUK(){
                    if isDoctor{
                        if appointmentType == "video" || appointmentType == "audio" {
                            return UITableView.automaticDimension
                        } else {
                            return 0
                        }
                    }else{
                        return 0
                    }
                }else{
                    return 0
                }
            }
            return 75 * _widthRatio
        }
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2{
            
            if indexPath.row == 0{
                if isWorkingCountryUK(){
                    let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ProblemTypeVC")  as! ProblemTypeVC
                    mapVc1.daleget = self
                    getFirebaseAnalytics(screenName : getEventName.SELECT_PROBLEM_TYPE,isPatient_id : true)
                    //  mapVc1.isPostCode = true
                    navigationController?.pushViewController(mapVc1, animated: true)
                }
            }
            if indexPath.row == 1{
                let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressVC")  as! AddressVC
                mapVc1.delegatesAddress3 = self
                getFirebaseAnalytics(screenName : getEventName.SELECT_CONSULTATION_ADDRESS,isPatient_id : true)
                mapVc1.isPostCode = true
                mapVc1.isSelectionIndex = indexz
                navigationController?.pushViewController(mapVc1, animated: true)
            }
            if indexPath.row == 2{
                if String.validateStringValue(str: fieldData.allFields[2].strValue){
                    return showFailMessage(message: "Please select address")
                }
                self.performSegue(withIdentifier: "Pharma", sender: nil)
            }
            if indexPath.row == 3{
                if docterDetailes.nDoctorType == "2" {
                    //1 = normal , 2 == Special - referral doctor
                    self.performSegue(withIdentifier: "Reffrer", sender: nil)
                }
            }
        }
    }
        
        
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
                cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
                cell.viewConstretTop.constant = 5
                cell.viewConstretBottom.constant = -5
                cell.textAreaNAme.tag = indexPath.row
                fieldData.allFields[indexPath.row].strValue = strAppointment
                cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
                return cell
            }else if indexPath.section == 1{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
                    cell.lblName.text = "Preferred Contact Method"
                    cell.viewText.isHidden = true
                    cell.viewConstretTop.constant = -5
                    cell.viewConstretBottom.constant = -5
                    return cell
                }else if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
                    cell.lblisAdvatigate.text = "Email" + " (\(_currentUser.patient_email_address!))"
                    cell.viewConstretTop.constant = -5
                    cell.viewConstretBottom.constant = -5
                    if isEmail == "0"{
                        cell.btnCheck.isSelected = false
                    }else{
                        cell.btnCheck.isSelected = true
                    }
                    cell.btnCheck.tag = 10
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
                    cell.lblisAdvatigate.text = "Mobile Phone" + " (\(_currentUser.patient_countrycode!)" + " \(_currentUser.patient_phone!))"
                    cell.viewConstretTop.constant = -5
                    cell.viewConstretBottom.constant = -5
                    if isPhone == "0"{
                        cell.btnCheck.isSelected = false
                    }else{
                        cell.btnCheck.isSelected = true
                    }
                    cell.btnCheck.tag = 11
                    return cell
                }
                
            }else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
                cell.viewText.isHidden = false
                cell.textEmail.tag = indexPath.row + 1
                cell.textEmail.keyboardType = fieldData.allFields[indexPath.row + 1].keybord
                cell.textEmail.text = fieldData.allFields[indexPath.row + 1].strValue
                cell.lblName.text = fieldData.allFields[indexPath.row + 1].placeholder
                cell.textEmail.isUserInteractionEnabled = false
                cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row + 1].isSecure
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
                if indexPath.row  == 2 {
                    if appointmentType == "video" || appointmentType == "audio" {
                        cell.viewConstretBottom.constant = -5
                    } else {
                        cell.viewConstretBottom.constant = 5
                    }
                }
                if indexPath.row == 3 {
                    if appointmentType == "video" || appointmentType == "audio" {
                        cell.viewConstretBottom.constant = -5
                    } else {
                        cell.viewConstretBottom.constant = 5
                    }
                }
                return cell
            }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
                    cell.lblisAdvatigate.text = fieldData.allFields[indexPath.row + 5].placeholder
                    cell.viewConstretTop.constant = -5
                    cell.viewConstretBottom.constant = -5
                    if fieldData.allFields[indexPath.row + 5].strkey == "0"{
                        cell.btnCheck.isSelected = false
                    }else{
                        cell.btnCheck.isSelected = true
                    }
                    if isWorkingCountryUK(){
                        if indexPath.row == 1{
                            cell.viewConstretTop.constant = -5
                            cell.viewConstretBottom.constant = 5
                        }
                    }else{
                        if indexPath.row == 0{
                            cell.viewConstretTop.constant = -5
                            cell.viewConstretBottom.constant = 5
                            
                        }
                    }
                    
                    cell.btnCheck.tag = indexPath.row + 5
                    return cell
                
            }
            
            
          

            
            
        }
        
        
        
        
    }
   
extension AppointmentConfirmationVC{
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        DispatchQueue.main.async { [self] in
                                          _currentUser.patient_type = dataname.getStringValue(key: "patient_email_address")
                                            _currentUser.patient_phone = dataname.getStringValue(key: "patient_phone")
                                            _currentUser.patient_countrycode = dataname.getStringValue(key: "patient_countrycode")
                                        }
                                        
                                    }
                                }
                                self.tableView.reloadData()

                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func savenurseappointment(){
        
        var dic = [String:Any]()
        dic["nurse_id"] = nurseDetailes.nurse_id
        dic["patient_id"] = _currentUser.patient_id
        dic["booking_date"] = strdate
        dic["booking_time"] = strTime
        dic["nurse_appointment_type"] = appointmentType
        if let _ = selectAddres{
            dic["addressId"] = selectAddres.address_id
        }else{
            dic["addressId"] = ""
        }
        dic["description"] = strAppointment
        dic["payment_method"] = " "
        dic["payment_orderCode"] = " "
        dic["payment_amount"] = " "
        dic["payment_currencyCode"] = " "
        dic["payment_expiryMonth"] = " "
        dic["payment_expiryYear"] = " "
        dic["payment_cardType"] = " "
        dic["payment_maskedCardNumber"] = " "
        dic["transaction_id"] = " "
        dic["currency_vSymbol"] = _currency.currency_vSymbol
        dic["currency_conversionRate"] = "1.0"
        dic["payment_charge"] = nurseDetailes.nurse_fees
//        if (cupanid != "") {
//            dic["dUseWalletAmount"] = usedCredits
//        }else{
//            if let _ = subcription{
//                if subcription.arryaActivesubscription.count > 0{
//                    dic["dUseWalletAmount"] = subcription.arryaActivesubscription[isSubcription].dAmount
//                }else{
//                    dic["dUseWalletAmount"] = subcription.arryaSubscription[isSubcription].dAmount
//                }
//            }
//        }
        dic["paymentStatus"] = " "
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "savenurseappointment.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    let parameters = [
                                        AppEvents.ParameterName("Name").rawValue: "\(nurseDetailes.nurse_first_name!)" + " " +  "\(nurseDetailes.nurse_last_name!)",
                                        AppEvents.ParameterName("Type").rawValue: "nurse",
                                        AppEvents.ParameterName("Appointment Type").rawValue: "\(appointmentType)",
                                        AppEvents.ParameterName("Timeslot").rawValue: "\(self.strdate)" + "\(self.strTime)",
                                        AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                                        AppEvents.ParameterName("Appointment ID").rawValue: "\(Response.getIntValue(key: "AppointmentId"))",
                                        AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                                        AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                                        AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
                                        
                                    ] as [String : Any]
                                    AppEvents.logEvent(.init("Appointment Booked"), parameters: parameters)
                                  //  self.performSegue(withIdentifier: "Book", sender: nil)
                                    
                                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDoneVC")  as! AppointmentDoneVC
                                    vc.docterDetailes = docterDetailes
                                    vc.isDoctor = isDoctor
                                    vc.strTime = strTime
                                    vc.strdate = strdate
                                    if isDoctor{
                                        vc.imageProfile = docterDetailes.doctor_profile_image
                                        vc.FullName = docterDetailes.doctor_first_name + " " + docterDetailes.doctor_last_name
                                        if isWorkingCountryUK() {
                                            vc.Specialist = docterDetailes.doctor_specialization
                                        } else {
                                            vc.Specialist = docterDetailes.doctor_specialization
                                        }
                                    }else{
                                        vc.imageProfile = nurseDetailes.nurse_profile_image
                                        vc.FullName = nurseDetailes.nurse_first_name + " " + nurseDetailes.nurse_last_name
                                        vc.Specialist = nurseDetailes.nurse_specialization
                                    }
                                    navigationController?.pushViewController(vc, animated: true)
                                    
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
