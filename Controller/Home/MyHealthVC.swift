//
//  MyHealthVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit
protocol dalegetPtionType {
    func getFilter()
}
class MyHealthVC: ParentViewController, dalegetPtionType {
    
    @IBOutlet weak var viewBG: UIView!
    var arryaMenuIcon = [#imageLiteral(resourceName: "ic_blood_pressure"),#imageLiteral(resourceName: "ic_blood_glucose"),#imageLiteral(resourceName: "ic_heartrate"),#imageLiteral(resourceName: "ic_bmi"),#imageLiteral(resourceName: "ic_weight"),#imageLiteral(resourceName: "ic_temprature")]
    var arryaMenuName = ["Blood Pressure", "Blood Glucose", "Heart Rate", "BMI", "Weight", "Temperature"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBG.makeCornerRound(redias : 6)
        self.tabBarController?.tabBar.isHidden = false


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getuserprofile()
    }
    
   

    func getFilter() {
        getuserprofile()
        self.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "Glucose", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Diabetic"{
            let vc = segue.destination as! DiabeticListVC
            vc.deleget = self
        }
        
    }
 

}
extension MyHealthVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
          
            if _currentUser.isBpOnboardingDone == "0"{
                let Question = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BPQuestionVC")  as! BPQuestionVC
                navigationController?.pushViewController(Question, animated: true)
            }else{
                performSegue(withIdentifier: "Blood", sender: nil)
            }
            
          

        }else if indexPath.row == 2{
            
            performSegue(withIdentifier: "Heart", sender: nil)

        }else if indexPath.row == 4{
            performSegue(withIdentifier: "Weight", sender: nil)

        }else if indexPath.row == 5{
            performSegue(withIdentifier: "Temperature", sender: nil)

        }else if indexPath.row == 3{
            performSegue(withIdentifier: "BMI", sender: nil)

        }else if indexPath.row == 1{
            if _currentUser.patient_type == "0" || _currentUser.patient_type == "" {
                performSegue(withIdentifier: "Diabetic", sender: nil)
            }else{
                performSegue(withIdentifier: "Glucose", sender: nil)
            }

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return arryaMenuName.count
      
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! MyPhpCollCell
            cell.imageMenuIcon.image = arryaMenuIcon[indexPath.row]
            cell.lblTitleName.text = arryaMenuName[indexPath.row]
            return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 413.5 * _widthRatio / 2 , height: 136)
       
    }
}
extension MyHealthVC{
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        DispatchQueue.main.async { [self] in
                                        _currentUser.patient_type = dataname.getStringValue(key: "patient_type")
                                        }
                                        
                                    }
                                }
                                self.myColView.reloadData()

                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
