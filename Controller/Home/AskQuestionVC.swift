//
//  AskQuestionVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 26/04/21.
//

import UIKit
import DropDown
protocol getAddition {
    func getShareAddition(information : AdditionalList)
}
class AskQuestionList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t1 = SingIn()
        t1.placeholder = "Treatment Preference/Problem Type"
        t1.keybord = .default
        
        var t2 = SingIn()
        t2.placeholder = "No Preference"
        t2.keybord = .default
        
        var t3 = SingIn()
        t3.placeholder = "Describe Your Issue In Detail"
        t3.keybord = .default
        t3.strValue = ""
        
        var t4 = SingIn()
        t4.placeholder = "Email"
        t4.isSecure = false
        t4.keybord = .default
        t4.strValue = ""
        
        var t5 = SingIn()
        t5.placeholder = "Share Basic Information"
        t5.keybord = .default
        
        var t6 = SingIn()
        t6.placeholder = "Sex"
        t6.keybord = .default
        t6.strkey = "1"
        
        var t7 = SingIn()
        t7.placeholder = "Date of Birth"
        t7.keybord = .default
        t7.image = #imageLiteral(resourceName: "ic_calender")
        t7.isSecure = true
        
        var t8 = SingIn()
        t8.placeholder = "City"
        t8.keybord = .default
        t8.isSecure = false
        
        
        var t9 = SingIn()
        t9.placeholder = "Area/Locality"
        t9.keybord = .default
        t9.isSecure = false
        
        var t10 = SingIn()
        t10.placeholder = "Height"
        t10.keybord = .default
        t10.image = #imageLiteral(resourceName: "ic_dropdown")
        t10.isSecure = false
        
        
        var t11 = SingIn()
        t11.placeholder = "Weight"
        t11.keybord = .default
        t11.image = #imageLiteral(resourceName: "ic_dropdown")
        t11.isSecure = false
        
        var t12 = SingIn()
        t12.placeholder = "Share Additional Information"
        t12.keybord = .default
        
        var t13 = SingIn()
        t13.placeholder = "You have to be above 16 to use this system"
        
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
        allFields.append(t4)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t11)
        allFields.append(t10)
        allFields.append(t12)
        allFields.append(t13)
        
    }
    
    
}
class AskQuestionVC: ParentViewController , UITextViewDelegate, getAddition {
    
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    var fieldData = AskQuestionList()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    let dropHight = DropDown()
    let dropWeight = DropDown()
    let dropPreference = DropDown()
    var addition = AdditionalList()
    var strAppointment = "For e.g. I Lost somebody close last year and since then i feel very depressed and prefer keeping to myself all the time. please help me."
    var ayyaHight = [String]()
    var ayyaWeight = [String]()
    var Preference = ["No Preference", "Men's Health", "Women's Health","Family's Health"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getHight()
        getWeight()
        viewTop.frame.size.height = 44 * _widthRatio
        viewBottom.frame.size.height = 197 * _widthRatio
        
        datefrmenter.dateFormat = "yyy-MM-dd"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = NSDate()
        var components = DateComponents()
        components.calendar = calendar
        components.year = -100
        let minDate = calendar.date(byAdding: components, to: currentDate as Date)!
        
        components.year = -10
        let maxDate = calendar.date(byAdding: components, to: currentDate as Date)!
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[1].placeholder = Preference[0]
        fieldData.allFields[1].placeholder1 = "0"
        fieldData.allFields[10].strValue = ayyaHight[0]
        fieldData.allFields[9].strValue = ayyaWeight[0]
    }
    
    func getShareAddition(information: AdditionalList) {
        addition = information
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[6].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 6, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[6].strValue
    }
    @IBAction func btnMaleClicked(_ sender: Any) {
        fieldData.allFields[5].strkey = "1"
        tableView.reloadData()
    }
    @IBAction func btnFemaleClicekd(_ sender: Any) {
        fieldData.allFields[5].strkey = "2"
        tableView.reloadData()
    }
    
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if fieldData.allFields[sender.tag].strkey == "0"{
            fieldData.allFields[sender.tag].strkey = "1"
        }else{
            fieldData.allFields[sender.tag].strkey = "0"
        }
        tableView.reloadData()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "For e.g. I Lost somebody close last year and since then i feel very depressed and prefer keeping to myself all the time. please help me."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "For e.g. I Lost somebody close last year and since then i feel very depressed and prefer keeping to myself all the time. please help me."{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strAppointment
            }
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        strAppointment = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    
    @IBAction func btnAskNowClicked(_ sender: Any) {
        if validation().0{
            if fieldData.allFields[12].strkey == "0"{
                return showFailMessage(message : "You have to be above 16 to use this system")
            }
            addquestion()
        }else{
            showFailMessage(message : validation().1)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Add"{
            let vc = segue.destination as! ShareAdditionalInformationVC
            vc.deleget = self
        }
        
    }
    
    
}
extension AskQuestionVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 11{
            performSegue(withIdentifier: "Add", sender: nil)
            
        }
        if indexPath.row == 1{
            getdropPreference()
            dropPreference.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.row].placeholder = Preference[index]
                fieldData.allFields[indexPath.row].placeholder1 = "\([index])"
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropPreference.show()
        }
        if indexPath.row == 10{
            getdropHight()
            dropHight.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.row].strValue = ayyaHight[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropHight.show()
        }
        if indexPath.row == 9{
            getdropWeight()
            dropWeight.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.row].strValue = ayyaWeight[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropWeight.show()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 10{
            return 90 * _widthRatio
        }
        if indexPath.row == 0 ||  indexPath.row == 2 || indexPath.row == 4{
            return 40 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 11{
            return 60 * _widthRatio
        }else if indexPath.row == 3{
            return 100 * _widthRatio
        }else if  indexPath.row == 5{
            return 50 * _widthRatio
        }else if  indexPath.row == 12{
            return 40 * _widthRatio
        }else{
            return 75 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = fieldData.allFields[indexPath.row].placeholder
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! SettingCell
            cell.lblNAme.text = fieldData.allFields[indexPath.row].placeholder
            cell.imageIcon.image = #imageLiteral(resourceName: "ic_dropdown")
            dropPreference.anchorView = cell.lblNAme
            
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
            cell.lblName.text = ""
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant  = -5
            cell.textAreaNAme.tag = indexPath.row
            fieldData.allFields[indexPath.row].strValue = strAppointment
            cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Gender") as! ProfileCell
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = 5
            cell.viewConstretBottom.constant = -5
            cell.btnMale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
            cell.btnFemale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
            if fieldData.allFields[indexPath.row].strkey == "1"{
                cell.btnMale.isSelected = true
                cell.btnFemale.isSelected = false
            }else{
                cell.btnMale.isSelected = false
                cell.btnFemale.isSelected = true
            }
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = fieldData.allFields[indexPath.row].isSecure
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if #available(iOS 13.4, *) {
                datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                datePickerView.preferredDatePickerStyle = .wheels
                datePickerView.preferredDatePickerStyle = .wheels
                cell.textEmail.inputView = datePickerView
            }else{
                cell.textEmail.inputView = datePickerView
            }
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            return cell
        }else if indexPath.row == 7 || indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            return cell
        }else if indexPath.row == 9 || indexPath.row == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = fieldData.allFields[indexPath.row].isSecure
            cell.imageCalander.image = #imageLiteral(resourceName: "ic_dropdown")
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 9{
                dropWeight.anchorView = cell.textEmail
            }
            if indexPath.row == 10{
                cell.viewConstretBottom.constant = 5
                dropHight.anchorView = cell.textEmail
            }
            
            return cell
        }else if indexPath.row == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! SettingCell
            cell.lblNAme.text = fieldData.allFields[indexPath.row].placeholder
            cell.imageIcon.image = #imageLiteral(resourceName: "ic_navigation_aro")
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
            cell.lblisAdvatigate.text = fieldData.allFields[indexPath.row].placeholder
            cell.btnCheck.tag = indexPath.row
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if fieldData.allFields[indexPath.row].strkey == "0"{
                cell.btnCheck.isSelected = false
            }else{
                cell.btnCheck.isSelected = true
            }
            return cell
        }
        
    }
    
    
    func getHight(){
        var i = 1
        while (i < 8) {
            ayyaHight.append("\(i)'")
            for j in 1...11 {
                ayyaHight.append("\(i)'\(j)''")
            }
            i += 1
        }
        i = 1
    }
    func getWeight(){
        var i = 1
        while (i <= 150) {
            let lbs = Float(i) * 2.2
            if (i == 1) {
                ayyaWeight.append("\(i)" + " kg (" + "\(lbs)" + "lbs)")
            } else {
                ayyaWeight.append("\(i)" + " kgs (" + "\(lbs)" + "lbs)")
            }
            i += 1
        }
    }
    func getdropHight(){
        dropHight.dataSource = ayyaHight
        dropHight.direction = .bottom
        dropHight.bottomOffset = CGPoint(x: 0, y:(dropHight.anchorView?.plainView.bounds.height)!)
        dropHight.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropWeight(){
        dropWeight.dataSource = ayyaWeight
        dropWeight.direction = .bottom
        dropWeight.bottomOffset = CGPoint(x: 0, y:(dropWeight.anchorView?.plainView.bounds.height)!)
        dropWeight.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    func getdropPreference(){
        dropPreference.dataSource = Preference
        dropPreference.direction = .bottom
        dropPreference.bottomOffset = CGPoint(x: 0, y:(dropPreference.anchorView?.plainView.bounds.height)!)
        dropPreference.width = 263 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
}
extension AskQuestionVC{
    func addquestion(){
        var dic = [String:Any]()
        dic["patientId"] = _currentUser.patient_id
        dic["diagnosed"] = fieldData.allFields[0].strkey
        dic["diagnosedName"] = fieldData.allFields[0].strcode
        dic["diagnosedHave"] = fieldData.allFields[0].strkey1
        dic["symptom"] = fieldData.allFields[0].strValue
        dic["medication"] = fieldData.allFields[1].strkey
        dic["medicationName"] = fieldData.allFields[1].strValue
        dic["allergie"] = fieldData.allFields[2].strkey
        dic["allergieName"] = fieldData.allFields[2].strValue
        dic["askFor"] = "0"
        dic["someoneId"] = ""
        if strAppointment == "For e.g. I Lost somebody close last year and since then i feel very depressed and prefer keeping to myself all the time. please help me." || String.validateStringValue(str: strAppointment) {
            dic["question"] = ""
        }else{
            dic["question"] = fieldData.allFields[3].strValue
        }
        
        dic["city"] = fieldData.allFields[7].strValue
        dic["area"] = fieldData.allFields[8].strValue
        dic["dob"] = fieldData.allFields[6].strValue
        dic["height"] = fieldData.allFields[10].strValue
        dic["weight"] = fieldData.allFields[9].strValue
        dic["preferredType"] = fieldData.allFields[1].placeholder1
        if fieldData.allFields[5].strkey == "1"{
            dic["gender"] = "Male"
        }else{
            dic["gender"] = "Female"
        }
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addquestion.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[3].strValue) || fieldData.allFields[3].strValue == "For e.g. I Lost somebody close last year and since then i feel very depressed and prefer keeping to myself all the time. please help me."{
            return(false,"Question should not be empty")
        }else if String.validateStringValue(str: fieldData.allFields[6].strValue){
            return(false,"Birth day should not be empty")
        }else if String.validateStringValue(str: fieldData.allFields[7].strValue){
            return(false,"Please enter city")
        }else if String.validateStringValue(str: fieldData.allFields[8].strValue){
            return(false,"Please enter area/locality")
        }
        return(true,"")
    }
}

