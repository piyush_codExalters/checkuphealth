//
//  SelectPharmacyVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 20/04/21.
//

import UIKit
import DropDown
class SelectPharmacyCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblPharmacyName: LabelSemiBold!
    @IBOutlet weak var lblTime: LabelSemiBold!
    @IBOutlet weak var btnRedio: UIImageView!
    @IBOutlet weak var lblMiles: LabelMedium!
    
    @IBOutlet weak var btnAction: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "Time"{
            viewBg.makeCornerRound(redias : 6)
        }
    }
}
class SelectPharmacyVC: ParentViewController {

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var textDrop: JPWidthTextField!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    var Delivry = [String]()
    var DelivryID = [String]()
    let dropCity = DropDown()
    var arryaDeliverylist = [Deliverylist]()
    var selectAddres : AddressListName!
    var deleget : AppointmentConfirmationVC!
    var arryaPharmacies = [Pharmacies]()
    var isIsected = -1
    var indext = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias : 6)
        viewBottom.makeCornerRound(redias : 6)

        viewText.makeCustomRound(radius:6,bc:.black)
        viewTop.frame.size.height = 146 * _widthRatio
        dropCity.anchorView = textDrop
        getDeliverylist()
        
    }
    
    @IBAction func btnDropDownClicked(_ sender: Any) {
        dropCity.selectionAction = { [unowned self] (index: Int, item: String) in
            self.textDrop.text! = Delivry[index]
            indext = index
            if index != 0{
                getCloserPharmacies(deliveryid: DelivryID[index-1],postal_code: selectAddres.postcode)
            }else{
                self.arryaPharmacies.removeAll()
                self.tableView.backgroundView = nil
                tableView.reloadData()
            }
        }
        self.view.endEditing(true)
        self.dropCity.show()
    }
    
   
    func getdropCity(){
        //Delivry.removeAll()
        Delivry.append("Select Delivery Type")
        for obj in arryaDeliverylist {
            Delivry.append(obj.name)
            DelivryID.append(obj.id)
        }
        dropCity.dataSource = Delivry
        dropCity.direction = .bottom
        dropCity.bottomOffset = CGPoint(x: 0, y:(dropCity.anchorView?.plainView.bounds.height)!)
        dropCity.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    @IBAction func btnACtionClicekd(_ sender: UIButton) {
        
      
        isIsected = sender.tag
        tableView.reloadData()
        
        deleget.getDelivry(delivry: arryaDeliverylist[indext], pharmaci: arryaPharmacies[sender.tag])
        
    }
    
}
extension SelectPharmacyVC: UITableViewDelegate, UITableViewDataSource{
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isIsected = indexPath.row
        tableView.reloadData()
        
        deleget.getDelivry(delivry: arryaDeliverylist[indext], pharmaci: arryaPharmacies[indexPath.row])
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaPharmacies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Time") as! SelectPharmacyCell
        cell.lblPharmacyName.text = arryaPharmacies[indexPath.row].name
        cell.lblMiles.text = "Miles: \(arryaPharmacies[indexPath.row].miles!)"
        let FormattedText = NSMutableAttributedString()
        FormattedText
            .Open("Mon - Fri \(arryaPharmacies[indexPath.row].mon_fri!)\nSat: \(arryaPharmacies[indexPath.row].saturday!)\n")
            .Cloase("Sun: \(arryaPharmacies[indexPath.row].sunday!)")
        cell.lblTime.attributedText = FormattedText
        if isIsected == indexPath.row{
            cell.btnRedio.image  = #imageLiteral(resourceName: "ic_radio_fill")
        }else{
            cell.btnRedio.image  = #imageLiteral(resourceName: "ic_radio_unfill")
        }
        cell.btnAction.tag = indexPath.row
      


        return cell
        
    }
    
   
    
    
    
}
extension SelectPharmacyVC{
    func getDeliverylist(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "getDeliverylist.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaDeliverylist.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        if let obj = data.value(forKey: "deliveries") as? NSArray{
                                            for str in obj {
                                                let checkName = str as! NSDictionary
                                                if checkName.getStringValue(key: "id") != "7" {
                                                    arryaDeliverylist.append(Deliverylist(dic: checkName))

                                                }

                                            }
                                        }
                                    }
                                    
                                    getdropCity()
                                    
                                    self.textDrop.text! = Delivry[0]
                                    indext = 0
                                 //   getCloserPharmacies(deliveryid: DelivryID[0],postal_code: selectAddres.postcode)
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getCloserPharmacies(deliveryid: String,postal_code: String){
        
        var dic = [String:Any]()
        // dic["memberId"] = _currentUser.patient_id!
        dic["patientid"] = _currentUser.patient_id
        dic["deliveryid"] = deliveryid
        dic["postal_code"] = postal_code
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getCloserPharmacies.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaPharmacies.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        if let obj = data.value(forKey: "pharmacies") as? NSArray{
                                            for str in obj {
                                                arryaPharmacies.append(Pharmacies(dic: str as! NSDictionary))

                                            }
                                        }
                                    }
                                    isIsected = -1
                                    if arryaPharmacies.count > 0{
                                        self.tableView.backgroundView = nil
                                    }else{
                                        self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "No Pharmacies available at selected Address", image : nil)
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
