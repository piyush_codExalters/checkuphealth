//
//  BookTimeVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 17/05/21.
//

import UIKit

class timeSlotList {
    var Date : String!
    var day : String!
    var strDate : String!
    var DoctorAvailableStatus : Int!
    init(dic: NSDictionary){
        
        Date = dic.getStringValue(key: "Date")
        DoctorAvailableStatus = dic.getIntValue(key: "DoctorAvailableStatus")
        day =  getDay(strTime : Date)
        strDate = getDate(strTime : Date)
    }
    
    
    
    func getDay(strTime : String) -> String{
        let formatter4 = DateFormatter()
        formatter4.timeZone = TimeZone.current
        formatter4.locale = Locale(identifier:"en_US_POSIX")
        formatter4.dateFormat = "yyyy-MM-dd"
        let dateSelected = formatter4.date(from: strTime)!
        formatter4.dateFormat = "E"
        let myStringDate = formatter4.string(from: dateSelected)
        return myStringDate
    }
    func getDate(strTime : String) -> String{
        let formatter4 = DateFormatter()
        formatter4.timeZone = TimeZone.current
        formatter4.locale = Locale(identifier:"en_US_POSIX")
        formatter4.dateFormat = "yyyy-MM-dd"
        let dateSelected = formatter4.date(from: strTime)!
        formatter4.dateFormat = "d"
        let myStringDate = formatter4.string(from: dateSelected)
        return myStringDate
    }
}

class BookTimeVC: ParentViewController {
    @IBOutlet weak var blheaderTitle: LabelMedium!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnUplaodDoumnet: submitButton!
    @IBOutlet weak var viewBottom: UIView!
   
    var strDate = ""
    var today = Date()
    var customday = Date()
    var allData = [[Any]]()
    var week = Date()
    var indext = 0
    var arraySlot = [timeSlotList]()
    
    var selectDay = 0

    
    var arryaNigh = ["12:00 AM","12:20 AM","12:40 AM","01:00 AM","01:20 AM","01:40 AM","02:00 AM","02:20 AM","02:40 AM","03:00 AM","03:20 AM","03:40 AM","04:00 AM","04:20 AM","04:40 AM","05:00 AM","05:20 AM","05:40 AM","06:00 AM","06:20 AM","06:40 AM","07:00 AM","07:20 AM","07:40 AM"]
    
    
    var arryaMonday = ["10:00 AM","10:20 AM","10:40 AM","11:00 AM","11:20 AM","11:40 AM"]
    var arryaAfternon = ["12:00 PM","12:20 PM","12:40 PM","01:00 PM","01:20 PM","01:40 PM"]
    var arryaEvent = ["07:00 PM","07:20 PM","07:40 PM","08:00 PM","08:20 PM","08:40 PM","09:00 PM","09:20 PM","09:40 PM"]

    var arrayMonday = [[String]]()
    var arrayAfternoon = [[String]]()
    var arrayEvent = [[String]]()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        today = customday
        viewBG.makeCornerRound(redias: 5)
        btnUplaodDoumnet.isHidden = true
        viewBG.isHidden = true
        viewBottom.frame.size.height = 250 * _widthRatio
        let weekDate = Calendar.current.date(byAdding: .day, value: 13, to: customday)
        week = weekDate!
        allData.append([#imageLiteral(resourceName: "ic_morning_grey"),"Morning","10 am - 12 pm",true])
        allData.append([#imageLiteral(resourceName: "ic_afternoon_grey"),"Afternoon","12 pm - 2 pm",false])
        allData.append([#imageLiteral(resourceName: "ic_evening_grey"),"Evening","7 pm - 10 pm",false])
        getFirebaseAnalytics(screenName : getEventName.SELECT_TIMESLOT,isPatient_id : true)
        
        
        
       // allData.append([#imageLiteral(resourceName: "ic_midnight_grey"),"Mid Night","12 pm - 8 am",false])
       // getdoctorlisttimeslotwise(booking_day: Day(str : today),booking_date: CalanderDate(str : today),timecurent: time(str : today))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getuserprofile()
        checkAvailabilitylist()
    }
    
    @IBAction func btnPreviewClicked(_ sender: Any) {
        let nextDate = Calendar.current.date(byAdding: .day, value: -1, to: customday)
        customday = nextDate!
        indext = 0
        checkAvailabilitylist()
        tableView.reloadData()
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: customday)
        customday = nextDate!
        indext = 0
        checkAvailabilitylist()
        tableView.reloadData()
    }
    
    
    func Day(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    func CalanderDate(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    func time(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    
    func checkTime(Start : String, End: String) -> Bool {

    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "PKT")
    dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"

    let startingSlot = "2021-05-31 06:00 PM" //UTC
    let endingSlot = "2021-05-31 08:30 AM" //UTC

    let date = Date()

    let date1: Date = dateFormatter.date(from: startingSlot)!
    let date2: Date = dateFormatter.date(from: endingSlot)!

    let currentTime = 60*Calendar.current.component(.hour, from: date) + Calendar.current.component(.minute, from: date) + (Calendar.current.component(.second, from: date)/60) // in minutes
    let time1 = 60*Calendar.current.component(.hour, from: date1) + Calendar.current.component(.minute, from: date1) + (Calendar.current.component(.second, from: date1)/60) // in minutes
    let time2 =  60*Calendar.current.component(.hour, from: date2) + Calendar.current.component(.minute, from: date2) + (Calendar.current.component(.second, from: date1)/60) // in minutes

    print(currentTime)
    print(time1)
    print(time2)

    if(currentTime >= time1 && currentTime <= time2) {
        return true
    } else {
        return false
    }
    }
    
    @IBAction func btnuploadClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "IdentityVerificationVC")  as! IdentityVerificationVC
       // mapVc1.delegatesAddress1 = self
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
}
extension BookTimeVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            
        }else{
            if indexPath.row == 0{
                indext = indexPath.section - 1
                
                tableView.reloadData()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allData.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
                return 100 * _widthRatio
           
        }else{
            if indexPath.row == 0{
                return 50 * _widthRatio
            }
            if indexPath.section == 1{
                if arrayMonday.count == 0{
                    return 0
                }
            }else if indexPath.section == 2{
                if arrayAfternoon.count == 0{
                    return 0
                }
            }else if indexPath.section == 3{
                if arrayEvent.count == 0{
                    return 0
                }
            }
            return UITableView.automaticDimension
        }
        
//        else if indexPath.section - 1 == allData.count{
//            if allData[indexPath.section - 1][3] as! Bool{
//                if indexPath.row == 0{
//                    return 50 * _widthRatio
//                }
//                return UITableView.automaticDimension
//            }else{
//                if indexPath.row == 0{
//                    return 50 * _widthRatio
//                }
//                return 0
//            }
//
//        }else{
//            if allData[indexPath.section - 1][3] as! Bool{
//                if indexPath.row == 0{
//                    return 40 * _widthRatio
//                }
//                return UITableView.automaticDimension
//            }else{
//                if indexPath.row == 0{
//                    return 40 * _widthRatio
//                }
//                return 0
//            }
//
//        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeSlot") as! BookSlotCell
            cell.bookAppointment = self
            cell.collectionTimeSlot.reloadData()
            return cell
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Day") as! BookAppointmentCell
                cell.imageDay.image = allData[indexPath.section - 1][0] as! UIImage
                cell.lblDay.text =   allData[indexPath.section - 1][1] as! String
                cell.lblTime.text =   allData[indexPath.section - 1][2] as! String
//                if indexPath.section == allData.count{
//                    cell.constraintTop.constant = -5
//                    cell.constraintBottom.constant = 5
//                    if indext == indexPath.section - 1{
//                        cell.constraintBottom.constant = 5
//                    }
//                }else{
                    cell.constraintBottom.constant = -5
                    cell.constraintTop.constant = -5
                if indexPath.section - 1 == 0{
                    cell.constraintTop.constant = 5

                }
              //  }
                //if indext == indexPath.section - 1{
                    cell.imageDay.setImageColor(color: .orange)
                    cell.imageSelect.image = #imageLiteral(resourceName: "ic_navigation_aro_up")
                    cell.imageSelect.isHidden = true
                    allData[indexPath.section - 1][3] = true
               // }else{
             //       cell.imageDay.setImageColor(color: .lightGray)
              //      cell.imageSelect.image = #imageLiteral(resourceName: "ic_dropdown")
             //       allData[indexPath.section - 1][3] = false
             //   }
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Time") as! BookAppointmentCell
            cell.bookTime = self
            cell.isDateSlot = indexPath.section - 1
            if indexPath.section == allData.count{
                cell.constraintBottom.constant = 5
            }else{
                cell.constraintBottom.constant = -5
            }
            cell.collectionTime.reloadData()
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.hightColletion.constant = cell.collectionTime.collectionViewLayout.collectionViewContentSize.height
            cell.collectionTime.reloadData()
            return cell
        }
        
    }

    
}

extension BookSlotCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if bookAppointment.arraySlot[indexPath.row].DoctorAvailableStatus == 1{
            bookAppointment.selectDay = indexPath.row
            bookAppointment.checkAvailabilitylist()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bookAppointment.arraySlot.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSlot", for: indexPath) as! BookAppointmentCollCell
        cell.lblDay.textColor = .lightGray
        cell.lblDate.textColor = .black
        cell.imageBg.image = nil
        cell.lblDay.text = bookAppointment.arraySlot[indexPath.row].day.maxLength(length: 2)
        cell.lblDate.text = bookAppointment.arraySlot[indexPath.row].strDate
        
        if bookAppointment.selectDay == indexPath.row{
            cell.lblDate.textColor = .lightGray
            cell.lblDay.textColor = .lightGray
            if bookAppointment.arraySlot[indexPath.row].DoctorAvailableStatus == 1{
                cell.imageBg.image = UIImage(named: "ic_date_bg")
                cell.lblDay.textColor = .white
                cell.lblDate.textColor = .black
            }
        }else if bookAppointment.arraySlot[indexPath.row].DoctorAvailableStatus == 0{
            cell.lblDate.textColor = .lightGray
            cell.lblDay.textColor = .lightGray
        }else{
            cell.lblDate.textColor = .black
        }
         return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        return CGSize(width: 374 * _widthRatio / 3 , height: 35)
        return CGSize(width: 63, height: 92)
        
        
    }
}


extension BookTimeVC{
        func getuserprofile(){
            let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
            showCentralSpinner()
            KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
                self.hideCentralSpinner()
                if let response = response {
                    do {
                        if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                            print(response)
                            DispatchQueue.main.async { [self] in
                                if let Response = response as? NSDictionary{
                                    if Response.getIntValue(key: "success") == 1{
                                        if let dataname = Response["data"] as? NSDictionary{
                                            if dataname.getIntValue(key: "isVerified") == 2{
                                                btnUplaodDoumnet.isHidden = true
                                                viewBG.isHidden = true
                                            }else{
                                                btnUplaodDoumnet.isHidden = false
                                                viewBG.isHidden = false
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
            }
        }
    
    
    func checkAvailabilitylist(){
        var param = [String:Any]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let todaysDate = dateFormatter.string(from: customday)

        param["start_date"] = todaysDate
        if selectDay == 0{
            param["booking_date"] = todaysDate
        }else{
            param["booking_date"] = arraySlot[selectDay].Date
        }
        

        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        let parameters = "{\"data\":\(jsonString)}"
        print(parameters)

        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "checkAvailabilitylist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        self.arrayMonday.removeAll()
                        self.arrayAfternoon.removeAll()
                        self.arrayEvent.removeAll()
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        arraySlot.removeAll()
                                        if let time = dataname["DoctorAvailability"] as? NSArray{
                                          //  var i = 0
                                         //   var isCheck = false
                                            for obj in time {
                                              //  let srt = obj as! NSDictionary
//                                                if srt.getStringValue(key: "DoctorAvailableStatus") == "1"{
//                                                    if !isCheck{
//                                                        selectDay = i
//                                                        isCheck = true
//                                                    }
//                                                }
                                                arraySlot.append(timeSlotList(dic: obj as! NSDictionary))
                                           //     i += 1
                                            }
                                            
                                        }
                                       if let timeSlot = dataname["TimeSlot"] as? NSArray{
                                           for obj in timeSlot {
                                               
                                               if let time = obj as? NSDictionary{

                                                   for morning in arryaMonday {
                                                       
                                                       if time.getStringValue(key: "time") == morning{
                                                           
                                                           var status = time.getStringValue(key: "time_status")
                                                           
                                                           if !isTimeEnable(strTime: time.getStringValue(key: "time")){
                                                               status = "0"
                                                           }
                                                           if status == "1"{
                                                               arrayMonday.append([time.getStringValue(key: "time"),status])
                                                           }
                                                           
                                                           
                                                       }
                                                       
                                                   }
                                                   
                                                   for morning in arryaAfternon {
                                                       
                                                       if time.getStringValue(key: "time") == morning{
                                                           
                                                           
                                                           var status = time.getStringValue(key: "time_status")
                                                           
                                                           if !isTimeEnable(strTime: time.getStringValue(key: "time")){
                                                               status = "0"
                                                           }
                                                           if status == "1"{
                                                               arrayAfternoon.append([time.getStringValue(key: "time"),status])
                                                           }
                                                          
                                                       }
                                                       
                                                   }
                                                   
                                                   for morning in arryaEvent {
                                                       
                                                       if time.getStringValue(key: "time") == morning{
                                                           
                                                           var status = time.getStringValue(key: "time_status")
                                                           
                                                           if !isTimeEnable(strTime: time.getStringValue(key: "time")){
                                                               status = "0"
                                                           }
                                                           if status == "1"{
                                                               arrayEvent.append([time.getStringValue(key: "time"),status])
                                                           }
                                                            
                                                           
                                                       }
                                                       
                                                   }
                                                   
                                                   
                                               }
                                               
                                           }
                                        }

                                        
                                        
                                    }
                                    tableView.reloadData()
                                    
                                }

                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }

    
    func isTimeEnable(strTime : String) -> Bool{
        
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current

        formatter.locale = Locale(identifier:"en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let time = formatter.string(from: Date().adding(minutes: 0))  // 2021-07-02 11:50 PM

        let dateCurrent = formatter.date(from: time)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strSelectedDate = dateFormatter.string(from: customday)
        

        
        
        let strTemp = arraySlot[selectDay].Date + " " + strTime //  2021-07-02 11:25 PM
        
        let formatter4 = DateFormatter()
        formatter4.timeZone = TimeZone.current
        formatter4.locale = Locale(identifier:"en_US_POSIX")
        formatter4.dateFormat = "yyyy-MM-dd hh:mm a"
        let dateSelected = formatter4.date(from: strTemp)!
        
        if dateCurrent! > dateSelected {
            return false
        }else{
            return true
        }
        
        
    }
    
    
    }
extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
extension String {
   func maxLength(length: Int) -> String {
       var str = self
       let nsString = str as NSString
       if nsString.length >= length {
           str = nsString.substring(with:
               NSRange(
                location: 0,
                length: nsString.length > length ? length : nsString.length)
           )
       }
       return  str
   }
}
