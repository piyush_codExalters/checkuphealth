//
//  AppointmentDoneVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 21/04/21.
//

import UIKit
import EventKit
import EventKitUI

class AppointmentDoneCell: ConstrainedTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class AppointmentDoneVC: ParentViewController {
    @IBOutlet weak var lblTitlHeadeNAme: LabelMedium!
    @IBOutlet weak var lblDesc: LabelSemiBold!
    @IBOutlet weak var btnGotoDescbrd: addButton!
    
    var strTime = ""
    var strdate = ""
    var isDoctor = false
    var docterDetailes : DocterDetailes!
    var nurseDetailes : NurseDetailes!
    let eventStore : EKEventStore = EKEventStore()
    var imageProfile = ""
    var FullName = ""
    var Specialist = ""
    var isResuldue = false
    var referralList : ReferralList!

    override func viewDidLoad() {
        super.viewDidLoad()
        if isResuldue{
            lblTitlHeadeNAme.text = "Reschedule Appointment Confirmation"
            btnGotoDescbrd.setTitle("Go To Home", for: .normal)
            lblDesc.text =  "Your appointment has been rescheduled at\n \(strdate), \(strTime)."
        }else{
            lblTitlHeadeNAme.text = "Appointment Confirmation"
            if isWorkingCountryUK(){
                
                lblDesc.text = "Your appointment request for \(convertDateFormater2(strdate)), \(strTime) has been submitted. Please be advised that the Doctor will triage your appointment request and may need to contact you prior to the appointment if there are any concerns."
            }else{
                lblDesc.text = "Your appointment has been confirmed at\n \(strdate), \(strTime)."
            }
            btnGotoDescbrd.setTitle("Go To Dashboard", for: .normal)
        }
    }
    
    
    
    
    
    @IBAction func btnGotoDesbordCliceld(_ sender: Any) {
        
        var ischeck  = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeVC.self) {
                ischeck = true
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        if !ischeck{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AppointmentsVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    return
                }
            }
        }
        if !ischeck{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: QuestionsVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    return
                }
            }
        }
    }
    @IBAction func btnCalanderClicked(_ sender: Any) {
        showCentralSpinner()
        eventStore.requestAccess(to: .event) { [self] (granted, error) in
            
            if (granted) && (error == nil) {
               
                print("granted \(granted)")
                print("error \(error)")
                let event:EKEvent = EKEvent(eventStore: self.eventStore)
                
                event.title = "Checkup Health"
                event.startDate = self.getDate()
                let dateEnd = Calendar.current.date(byAdding: .day, value: 1, to: self.getDate()!)

                event.endDate = dateEnd
                    if isResuldue{
                        event.notes = "Your appointment has been rescheduled \(FullName)"
                    }else{
                        event.notes = "Your appointment has been confirmed \(FullName)"
                    }
                event.calendar = self.eventStore.defaultCalendarForNewEvents
                
                do {
                    try self.eventStore.save(event, span: .thisEvent)
                } catch let error as NSError {
                    DispatchQueue.main.async {
                    self.hideCentralSpinner()
                    self.showFailMessage(message: "failed to save event with error : \(error)")
                    }
                }
                DispatchQueue.main.async {
                self.hideCentralSpinner()
                self.showSucessMessage(message: "Event add Successfully!")
                }
            }
            else{
                DispatchQueue.main.async {
                self.hideCentralSpinner()
                    let alertController = UIAlertController (title: _appName, message: "Go to Settings?", preferredStyle: .alert)

                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }

                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    UIApplication.shared.windows.last?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    func getDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        return dateFormatter.date(from: strdate + " " + strTime) // replace Date String
    }
    
}
extension AppointmentDoneVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100 * _widthRatio
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
        if isDoctor{
            cell.lblDrName.text = FullName
            cell.imageDr.kf.setImage(with: URL(string:imageProfile), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
            cell.lblExp.text = Specialist
        }else{
            cell.lblDrName.text = FullName
            cell.imageDr.kf.setImage(with: URL(string:imageProfile), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
            }
            cell.lblExp.text = Specialist
        }
        return cell
        
        
    }
    
    
    
    
}
