//
//  ServiceProviderVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 21/04/21.
//

import UIKit

class ServiceProviderVC: ParentViewController , UITextFieldDelegate{
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var textSearch: JPWidthTextField!
    var arryaServiceProvideList = [ServiceProvideList]()
    var search = [ServiceProvideList]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias: 5)
        getServiceProviderCategory()
        
        
    }
    
    @IBAction func textEditSearch(_ sender: UITextField) {
        arryaServiceProvideList = search.filter { $0.category_name.localizedCaseInsensitiveContains(sender.text!) }
        if sender.text == ""{
            arryaServiceProvideList = search
        }
        self.myColView.reloadData()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        arryaServiceProvideList = search
        self.myColView.reloadData()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ServiceList"{
            let vc = segue.destination as! ServiceProviderListVC
            vc.serviceProvideList = (sender as! ServiceProvideList)
        }
        if segue.identifier == "List"{
            let vc = segue.destination as! ServiceListVC
            vc.serviceProvideList = (sender as! ServiceProvideList)
        }
    }
}
extension ServiceProviderVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arryaServiceProvideList[indexPath.row].category_ishasspeciality == "1"{
            performSegue(withIdentifier: "ServiceList", sender: arryaServiceProvideList[indexPath.row])
        }else{
            performSegue(withIdentifier: "List", sender: arryaServiceProvideList[indexPath.row])
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryaServiceProvideList.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! MyPhpCollCell
        cell.lblTitleName.text = arryaServiceProvideList[indexPath.row].category_name
        cell.imageMenuIcon.kf.setImage(with: URL(string:arryaServiceProvideList[indexPath.row].category_image), placeholder: #imageLiteral(resourceName: "ic_placeholder_logo"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 413.5 * _widthRatio / 2 , height: 170)
        
    }
}
extension ServiceProviderVC{
    func getServiceProviderCategory(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "getServiceProviderCategory.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaServiceProvideList.removeAll()
                                self.myColView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for str in data {
                                            arryaServiceProvideList.append(ServiceProvideList(dic: str as! NSDictionary))
                                            //
                                        }
                                        search = arryaServiceProvideList
                                    }
                                }else{
                                    self.myColView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                self.myColView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
