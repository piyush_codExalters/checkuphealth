//
//  IdentityVerificationVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 19/04/21.
//

import UIKit
import CropViewController
import Photos

class DocumentData : NSObject{
    
    var nIdProofStatus : Int!
    var nIdProofType : Int!
    var nResidenceProofStatus : Int!
    var nResidenceProofType : Int!
    
    init(dict : NSDictionary){
        nIdProofType = dict.getIntValue(key: "nIdProofType")
        nIdProofStatus = dict.getIntValue(key: "nIdProofStatus")
        nResidenceProofStatus = dict.getIntValue(key: "nResidenceProofStatus")
        nResidenceProofType = dict.getIntValue(key: "nResidenceProofType")
    }
}

class IdentityVerificationCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var lblTilteName: LabelSemiBold!
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "TitleName"{
            viewBg.makeCornerRound(redias: 5)
        }
    }
}
class IdentityVerificationVC: ParentViewController {
    
    @IBOutlet weak var customCameraView: UIView!

    @IBOutlet weak var btnpickPhoto: UIButton!
    
    @IBOutlet weak var btnCancelPhoto: UIButton!
    
    var isDoctor = false
    var imagePicker = UIImagePickerController()
    var imgUser : UIImage?
    var vIdProofImagePath : UIImage?
    var vSelfieImagePath : UIImage?
    var vResidenceProofImagePath : UIImage?
    var IdProofType = 0
    var ResidenceProofType = 0
    var comeraType = 0
    var isDocumentType = 0
    var documentData : DocumentData!
    var nid = ""
    var viewCamera = UIView()
    

   



    override func viewDidLoad() {
        super.viewDidLoad()
        getFirebaseAnalytics(screenName : getEventName.UPLOAD_VERIFICATION_DOCUMENTS,isPatient_id : true)
        self.tableView.contentInset.bottom = 100 * _widthRatio
        customCameraView.isHidden = true
        btnpickPhoto.isHidden = true
        btnCancelPhoto.isHidden = true
        btnpickPhoto.makeRound()

        CheckPatientVerification()
    }
    
    @IBAction func btnUploadNowClicked(_ sender: Any) {
        if isDocumentType == 0 || isDocumentType == 1{
            if vIdProofImagePath == nil || vSelfieImagePath == nil{
                return showFailMessage(message: "Please choose any one identify proof!")
            }
        }
        if isDocumentType == 0 || isDocumentType == 2{
            if vResidenceProofImagePath == nil{
                return showFailMessage(message: "Please choose any one place of residence! proof!")
            }
        }
        uploadPatientVerificationDocument1()
    }
    
    
    @IBAction func btnCancelPhoto(_ sender: Any) {
        customCameraView.isHidden = true
        btnpickPhoto.isHidden = true
        btnCancelPhoto.isHidden = true
        
    }
    
    
    
}

extension IdentityVerificationVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1{
            
                IdProofType = indexPath.row
                showAlert1(title: _appName, msgString: "Take a Selfie First!", Ohk: "OK") {
                    

                self.openCamera(type: 1)
                    
//                    let mapVc1 = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "PhotoCameraVC")  as! PhotoCameraVC
//                    self.navigationController?.pushViewController(mapVc1, animated: true)

                   
                }
            
            
        }
        if indexPath.section == 2{
                ResidenceProofType = indexPath.row
                showAlert1(title: _appName, msgString: "Take a picture of document", Ohk: "OK") {
                    self.openDocumet(type: 3)
                }
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 44 * _widthRatio
            }
            return 30 * _widthRatio
        }else if indexPath.section == 1{
            if isDocumentType == 0 || isDocumentType == 1{
                if indexPath.row == 0{
                    return 44 * _widthRatio
                }else{
                    return 60 * _widthRatio
                }
            }
            return 0
        }else{
            if isDocumentType == 0 || isDocumentType == 2{
                if indexPath.row == 0{
                    return 44 * _widthRatio
                }else if indexPath.row == 7 {
                    return UITableView.automaticDimension
                }else{
                    return 60 * _widthRatio
                }
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 4
        }else if section == 1{
            return 4
        }else{
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = "Select Document"
                cell.lblDesc.font = cell.lblDesc.font.withSize(15 * _widthRatio)
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "You will take a picture of it in the next step."
                cell.lblTitles.font = cell.lblTitles.font.withSize(15 * _widthRatio)
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = "Note:"
                cell.lblDesc.font = cell.lblDesc.font.withSize(12 * _widthRatio)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Driving licence can not be used twice."
                cell.lblTitles.font = cell.lblTitles.font.withSize(12 * _widthRatio)

                return cell

            }
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! IdentityVerificationCell
                cell.lblTilteName.text = "Proof of ID : (any of 3)"
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "TitleName") as! IdentityVerificationCell
                cell.isUserInteractionEnabled = true
                cell.viewBg.backgroundColor = UIColor.white
                if indexPath.row == 1{
                   cell.lblTilteName.text = "Passport"
                }else if indexPath.row == 2{
                   cell.lblTilteName.text = "Driving Licence"
                    if ResidenceProofType == 4{
                        cell.isUserInteractionEnabled = false
                        cell.viewBg.backgroundColor = UIColor.mlLightGrayColor()
                    }
                }else {
                   cell.lblTilteName.text = "Citizen Card"
                }
                if IdProofType == indexPath.row{
                    cell.imageIcon.image = #imageLiteral(resourceName: "ic_mobilenumber_verified")
                }else{
                    cell.imageIcon.image = #imageLiteral(resourceName: "ic_navigation_aro")
                }
                return cell
            }
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Details") as! IdentityVerificationCell
                cell.lblTilteName.text = "Proof of Place of residence : (any of 7)"
                return cell
            }else if indexPath.row == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "TitleName") as! IdentityVerificationCell
                cell.lblTilteName.text = "Correspondence between you and a government authority regarding the receipt of benefits such as a pension, unemployment benefits, housing benefits etc."
                if ResidenceProofType == indexPath.row{
                    cell.imageIcon.image = #imageLiteral(resourceName: "ic_mobilenumber_verified")
                }else{
                    cell.imageIcon.image = #imageLiteral(resourceName: "ic_navigation_aro")
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "TitleName") as! IdentityVerificationCell
                cell.lblTilteName.text = "Certificate of voter registration"
                cell.isUserInteractionEnabled = true
                cell.viewBg.backgroundColor = UIColor.white
                if indexPath.row == 1{
                    cell.lblTilteName.text = "Utility Bill"
                }else if indexPath.row == 2{
                    cell.lblTilteName.text = "Bank statement"
                }else if indexPath.row == 3{
                    cell.lblTilteName.text = "Tax assessment"
                }else if indexPath.row == 4{
                    cell.lblTilteName.text = "Driving Licence"
                     if IdProofType == 2{
                        cell.isUserInteractionEnabled = false
                        cell.viewBg.backgroundColor = UIColor.mlLightGrayColor()
                    }
                }else if indexPath.row == 5{
                    cell.lblTilteName.text = "Mortgage statement"
                }else if indexPath.row == 6{
                    cell.lblTilteName.text = "Certificate of voter registration"
                }
                if ResidenceProofType == indexPath.row{
                    cell.imageIcon.image = #imageLiteral(resourceName: "ic_mobilenumber_verified")
                }else{
                    cell.imageIcon.image = #imageLiteral(resourceName: "ic_navigation_aro")
                }
                return cell
            }
        }
    }
}

extension IdentityVerificationVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate{
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        
       
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        
        customCameraView.isHidden = true
        btnpickPhoto.isHidden = true
        btnCancelPhoto.isHidden = true
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropViewController, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropViewController, animated: true)
        }
    }
    
    
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            // 'image' is the newly cropped version of the original image
        if comeraType == 1{
            vSelfieImagePath = image
        }
        if comeraType == 2{
            vIdProofImagePath = image
            
        }
        if comeraType == 3{
            vResidenceProofImagePath = image
            
        }
        dismiss(animated:true, completion: nil)
        if comeraType == 1{
            showAlert1(title: _appName, msgString: "Now Take a picture of document!", Ohk: "OK") {
                self.openDocumet(type: 2)
            }
        }
        tableView.reloadData()

    }
  
    
    
   
    
    @IBAction func cameraButtonPressed(_ sender: Any) {

        imagePicker.takePicture()

    }
    
    
   
    
    
    func openCamera(type : Int){
        customCameraView.isHidden = false
        btnpickPhoto.isHidden = false
        btnCancelPhoto.isHidden = true
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            imagePicker.cameraDevice = .front
            imagePicker.showsCameraControls = false
            addChild(imagePicker)
            // Add the child's View as a subview
            self.customCameraView.addSubview((imagePicker.view)!)
            imagePicker.view.frame = customCameraView.bounds
            comeraType = type
          //  self.present(imagePicker, animated: true, completion: nil)
        }
    }
   
    
    func openDocumet(type : Int){
        customCameraView.isHidden = false
        btnpickPhoto.isHidden = false
        btnCancelPhoto.isHidden = true
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            imagePicker.cameraDevice = .rear
            imagePicker.showsCameraControls = false
            addChild(imagePicker)
            // Add the child's View as a subview
            self.customCameraView.addSubview((imagePicker.view)!)
            imagePicker.view.frame = customCameraView.bounds
            comeraType = type
          //  self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
     
    
}

 

extension IdentityVerificationVC{

    
   
    func uploadPatientVerificationDocument1(){
        
        showCentralSpinner()
        var dic = [String:Any]()
        dic["nPatientId"] = "\(_currentUser.patient_id!)"
        dic["id"] = nid
        if vIdProofImagePath != nil{
            dic["nIdProofType"] = "\(IdProofType)"
            let imageResized = vIdProofImagePath!.resizeWith(percentage: 0.25)
            let base64 = imageResized?.toBase64()
            dic["vIdProofImagePath"] = base64
        }
        if vSelfieImagePath != nil{
            let imageResized1 = vSelfieImagePath!.resizeWith(percentage: 0.25)
            let base641 = imageResized1?.toBase64()
            dic["vSelfieImagePath"] = base641
        }
        if vResidenceProofImagePath != nil{
            dic["nResidenceProofType"] = "\(ResidenceProofType)"
            let imageResized2 = vResidenceProofImagePath!.resizeWith(percentage: 0.25)
            let base642 = imageResized2?.toBase64()
            dic["vResidenceProofImagePath"] = base642
        }
       
        self.showCentralSpinner()
        KPWebCall.call.worldPayPayment(param: dic) { (json, status) in
            self.hideCentralSpinner()
            if let jsonData = json as? NSDictionary{
                if jsonData.getIntValue(key: "success") == 1{
                    self.showAlert1(title: "Thank You", msgString: "We have received your ID and Address document, post verification you will be able to book a consultation.", Ohk: "OK") {
                        var ischeck  = false
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: HomeVC.self) {
                                ischeck = true
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        if !ischeck{
                            _ = self.navigationController?.popViewController(animated: true)

                        }
                    }
                }else{
                    self.showResponseMessage(dict: jsonData)
                }
            }else{
                self.showSomethingWrong()
            }
        }
        
    }
    
    
    
    func CheckPatientVerification(){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "CheckPatientVerification.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        
                                        if data.getIntValue(key: "DocumentStatus") == 0{
                                           
                                        }else if data.getIntValue(key: "DocumentStatus") == 1{
                                            
                                            if let document = data.value(forKey: "DocumentData") as? NSDictionary{
                                                self.documentData = DocumentData(dict: document)
                                                self.nid = document.getStringValue(key: "id")
                                                if document.getIntValue(key: "nIdProofStatus") == 0 ||  document.getIntValue(key: "nResidenceProofStatus") == 0{
                                                    
                                                    self.showAlert1(title: "Identification under process", msgString: "You will be notified once admin approves your identification and then you can proceed with booking video consultation appointment.", Ohk: "OK") {
                                                        
                                                        var ischeck  = false
                                                        for controller in self.navigationController!.viewControllers as Array {
                                                            if controller.isKind(of: HomeVC.self) {
                                                                ischeck = true
                                                                self.navigationController!.popToViewController(controller, animated: true)
                                                                break
                                                            }
                                                        }
                                                        if !ischeck{
                                                            _ = self.navigationController?.popViewController(animated: true)

                                                        }
                                                        
                                                    }
                                                    
                                                }else if document.getIntValue(key: "nIdProofStatus") == 2 &&  document.getIntValue(key: "nResidenceProofStatus") == 2{
                                                    self.isDocumentType = 0
                                                    self.showAlert1(title: "Document verification failed!", msgString: "Your document was rejected!", Ohk: "Upload again") {
                                                    }
                                                    
                                                } else if document.getIntValue(key: "nIdProofStatus") == 2 {
                                                    self.isDocumentType = 1
                                                    self.showAlert1(title: "Document verification failed!", msgString: "Your proof of ID was rejected!", Ohk: "Upload again") {
                                                    }
                                                    
                                                }else if document.getIntValue(key: "nResidenceProofStatus") == 2 {
                                                    self.isDocumentType = 2
                                                    self.showAlert1(title: "Document verification failed!", msgString: "Your proof of place of residence was rejected!", Ohk: "Upload again") {
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                          
                                            
                                        }
                                    
                                    }
                                    tableView.reloadData()
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
