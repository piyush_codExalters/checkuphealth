//
//  FilterDoctorVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 21/05/21.
//

import UIKit

class FilterDoctorVC: ParentViewController {
    
    @IBOutlet weak var segmentItem: UISegmentedControl!
    var isSpecialInterest = 0
    var daleget : DRListVC!
    var arryaGender = ["Any","Male", "Female"]
    var arryaspectization = [String]()
    var isSpecialInterestIsected = 0
    var isGenderIsected = 0
    var sregende = ""
    var sreSpec = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentItem.setupSegment()
    }
    
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentItem.changeUnderlinePosition()
        switch segmentItem.selectedSegmentIndex {
        case 0:
            isSpecialInterest = 0
            
        default:
            isSpecialInterest = 1
        }
        tableView.reloadData()
    }
    
    @IBAction func btnClear(_ sender: Any) {

        daleget.getFilterName(strSpecial: "", strGende: "", indSpecial: 0, indGende: 0)
    }
    
    @IBAction func btnApply(_ sender: Any) {
        
        if isSpecialInterestIsected == 0 && isGenderIsected == 0{
            daleget.getFilterName(strSpecial: "", strGende: "", indSpecial: 0, indGende: 0)
        }else if isSpecialInterestIsected == 0 && isGenderIsected != 0{
            daleget.getFilterName(strSpecial: "", strGende: sregende, indSpecial: isSpecialInterestIsected, indGende: isGenderIsected)
        }else if isSpecialInterestIsected != 0 && isGenderIsected == 0{
            daleget.getFilterName(strSpecial: sreSpec, strGende: "", indSpecial: isSpecialInterestIsected, indGende: isGenderIsected)
        }else{
            daleget.getFilterName(strSpecial: sreSpec, strGende: sregende, indSpecial: isSpecialInterestIsected, indGende: isGenderIsected)

        }
    }
    
    
}

extension FilterDoctorVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        if isSpecialInterest == 1{
            sregende = arryaGender[indexPath.row]
            isGenderIsected = indexPath.row
        }else{
            sreSpec = arryaspectization[indexPath.row]
            isSpecialInterestIsected = indexPath.row
        }

        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSpecialInterest == 1{
            return arryaGender.count
        }else{
            return arryaspectization.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSpecialInterest == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! UpcomingFilterCell
            cell.viewBg.makeCornerRound(redias: 5)
            if isGenderIsected == indexPath.row{
                cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_fill")
            }else{
                cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_unfill")
            }
            cell.hightTop.constant = -5
            cell.hightBottom.constant = -5
            if indexPath.row == 0{
                cell.hightTop.constant = 5
                cell.hightBottom.constant = -5
            }
            if indexPath.row + 1 == arryaGender.count{
                cell.hightTop.constant = -5
                cell.hightBottom.constant = 5
            }
            cell.lblName.text = arryaGender[indexPath.row]
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! UpcomingFilterCell
            cell.viewBg.makeCornerRound(redias: 5)
            if isSpecialInterestIsected == indexPath.row{
                cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_fill")
            }else{
                cell.imageRadio.image  = #imageLiteral(resourceName: "ic_radio_unfill")
            }
            cell.hightTop.constant = -5
            cell.hightBottom.constant = -5
            if indexPath.row == 0{
                cell.hightTop.constant = 5
                cell.hightBottom.constant = -5
            }
            if indexPath.row + 1 == arryaspectization.count{
                cell.hightTop.constant = -5
                cell.hightBottom.constant = 5
            }
            cell.lblName.text = arryaspectization[indexPath.row]
            return cell
        }
        
        
        
    }
    
    
    
    
}
