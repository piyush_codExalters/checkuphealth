//
//  ResonVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 13/05/21.
//

import UIKit

class ResonVC: ParentViewController, UITextViewDelegate {
    @IBOutlet weak var lblCont: LabelSemiBold!
    
    @IBOutlet weak var viewBg: UIView!
    var dalegate : AppointmentDetailsVC!
    var str = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias : 6)
        lblCont.text = "0" + "/160"
    }
    func textView(_ textView: UITextView,
                  shouldChangeTextIn range: NSRange,
                  replacementText text: String) -> Bool {
        return self.textLimit(existingText: textView.text,
                              newText: text,
                              limit: 160)
    }
    
    func textLimit(existingText: String?,
                          newText: String,
                          limit: Int) -> Bool {
       let text = existingText ?? ""
       let isAtLimit = text.count + newText.count <= limit
       return isAtLimit
   }
    
    func textViewDidChange(_ textView: UITextView) {
        lblCont.text = "\(str.count + 1)" + "/160"
        str = textView.text!
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func btnApplyClicked(_ sender: Any) {
        dalegate.cancelREson(strREscon: str)
    }
    
}
