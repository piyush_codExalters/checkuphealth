//
//  RequestPopVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit

class RequestPopVC: ParentViewController {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblName: LabelRegular!
    
    var moduleName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias : 6)
        let str = moduleName.replacingOccurrences(of: ",", with: ",\n" )
        lblName.text = str
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   

}
