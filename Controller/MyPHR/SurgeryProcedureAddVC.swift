//
//  SurgeryProcedureAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
class SurgeryProcedureList: NSObject {
    
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Name *"
        t3.keybord = .default
        t3.placeholder1 = "e.g. Fitting pacemaker, etc."

        
        var t8 = SingIn()
        t8.placeholder = "Implant/ Support Devices *"
        t8.keybord = .default
        t8.placeholder1 = "e.g. Pacemaker, Stent, etc."


        
        var t9 = SingIn()
        t9.placeholder = "Date of Surgery/Procedure *"
        t9.keybord = .default
        t9.image = #imageLiteral(resourceName: "ic_calender")
        t9.placeholder1 = "Date"


        
        var t5 = SingIn()
        t5.placeholder = "Treated By *"
        t5.keybord = .default
        t5.placeholder1 = "e.g. Dr. Gopal at Ortho Hospital, etc."

        
      
        
        var t0 = SingIn()
        t0.placeholder = "Additional Notes"
        t0.keybord = .default
        t0.strValue = "Add Notes"

        
        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t0)
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter name")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter implant/ support devices")
        }else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please enter date")
        }else if String.validateStringValue(str: allFields[3].strValue){
            return(false,"Please enter treated by")
        }
        return(true,"")
    }
}
class SurgeryProcedureAddVC: ParentViewController , UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!

    var strHeader = "Add Surgery/Procedure"
    var strPlacholder = "Add Notes"
    
    var strDate = ""

    var fieldData = SurgeryProcedureList()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var surgerylist : Surgerylist!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        datefrmenter.dateFormat = "dd-MM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        strDate = datefrmenter.string(from: currentDate as Date)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        if "Add Surgery/Procedure" == strHeader{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("Update", for: .normal)
            fieldData.allFields[0].strValue = surgerylist.surgery_name
            fieldData.allFields[1].strValue = surgerylist.surgery_implants
            fieldData.allFields[2].strValue = surgerylist.surgery_diagnosed_date
            fieldData.allFields[3].strValue = surgerylist.surgery_by
            fieldData.allFields[4].strValue = surgerylist.surgery_note
            strDate = surgerylist.surgery_diagnosed_date
            if surgerylist.surgery_note == ""{
                fieldData.allFields[4].strValue = "Add Notes"
            }else{
                strPlacholder = surgerylist.surgery_note
            }
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[2].strValue = datefrmenter.string(from: sender.date)
        strDate = fieldData.allFields[2].strValue
        let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[2].strValue
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2{
            textField.text! = strDate
            fieldData.allFields[2].strValue = textField.text!
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Notes"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Notes"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[4].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if fieldData.validation().0{
            if "Add Surgery/Procedure" == strHeader{
                addsurgery()
            }else{
                updatesurgery()
            }
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }

}
extension SurgeryProcedureAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 {
            return 75 * _widthRatio
        }
        
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0 || indexPath.row == 1 ||  indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            return cell
        }
        
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if indexPath.row == 2{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        return cell
    }
}
extension SurgeryProcedureAddVC{
    func addsurgery(){
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["surgeryName"] = fieldData.allFields[0].strValue
        dic["surgeryBy"] = fieldData.allFields[3].strValue
        dic["surgeryImplants"] = fieldData.allFields[1].strValue
        if fieldData.allFields[4].strValue == "Add Notes"{
            fieldData.allFields[4].strValue = ""
        }
        dic["surgeryNote"] = fieldData.allFields[4].strValue
        let dateString = fieldData.allFields[2].strValue
        let strDay = fieldData.allFields[2].strValue.index(fieldData.allFields[2].strValue.startIndex, offsetBy: 2)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let date = formatter.date(from: dateString) else {
            return
        }
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: date)
        formatter.dateFormat = "MM"
        let month = formatter.string(from: date)
        
        dic["surgeryDay"] = String(fieldData.allFields[2].strValue[..<strDay])
        dic["surgeryMonth"] = month
        dic["surgeryYear"] = year
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addsurgery.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updatesurgery(){
        var dic = [String:Any]()
        dic["surgeryId"] = surgerylist.surgery_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["surgeryName"] = fieldData.allFields[0].strValue
        dic["surgeryBy"] = fieldData.allFields[3].strValue
        dic["surgeryImplants"] = fieldData.allFields[1].strValue
        if fieldData.allFields[4].strValue == "Add Notes"{
            fieldData.allFields[4].strValue = ""
        }
        dic["surgeryNote"] = fieldData.allFields[4].strValue
        let dateString = fieldData.allFields[2].strValue
        let strDay = fieldData.allFields[2].strValue.index(fieldData.allFields[2].strValue.startIndex, offsetBy: 2)
        let formatter = DateFormatter()
        formatter.locale = NSLocale.current
        formatter.dateFormat = "dd-MM-yyyy"
        guard let date = formatter.date(from: dateString) else {
            return
        }
        
        
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: date)
        formatter.dateFormat = "MM"
        let month = formatter.string(from: date)
        
        dic["surgeryDay"] = String(fieldData.allFields[2].strValue[..<strDay])
        dic["surgeryMonth"] = month
        dic["surgeryYear"] = year
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatesurgery.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
