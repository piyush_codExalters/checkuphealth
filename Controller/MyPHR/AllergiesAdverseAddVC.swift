//
//  AllergiesAdverseAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
class AllergiesAdverseList: NSObject {
    
    var allFields: [SingIn] = []
    var fieldData = medicationNAme()
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Name *"
        t3.keybord = .default
        t3.placeholder1 = "e.g. Penicillin"

        
        var t8 = SingIn()
        t8.placeholder = "Triggered By *"
        t8.keybord = .default
        t8.placeholder1 = "e.g. Antibiotic use"

        
        var t9 = SingIn()
        t9.placeholder = "Reaction *"
        t9.keybord = .default
        t9.placeholder1 = "Reddish skin"

        
        
        var t5 = SingIn()
        t5.placeholder = "How Often Does It Occur *"
        t5.keybord = .default
        t5.placeholder1 = "All the time, etc."

        
        var t6 = SingIn()
        t6.placeholder = "First Diagnosed On *"
        t6.keybord = .numberPad
        t6.image = #imageLiteral(resourceName: "ic_calender")
        t6.placeholder1 = "Date"


        
        var t10 = SingIn()
        t10.placeholder = "First Diagnosed On"
        t10.keybord = .numberPad
        
        var t11 = SingIn()
        t11.placeholder = "First Diagnosed On"
        t11.keybord = .numberPad
        
        
        var t0 = SingIn()
        t0.placeholder = "Additional Notes"
        t0.keybord = .default
        t0.strValue = "Add Notes"

        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t10)
        allFields.append(t11)
        allFields.append(t0)
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter name")
        }
//        else if String.validateStringValue(str: allFields[1].strValue){
//            return(false,"Please enter triggered by")
//        }
        else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please enter reaction")
        }else if String.validateStringValue(str: allFields[3].strValue){
            return(false,"Please enter occurred by.")
        }else if String.validateStringValue(str: allFields[4].strValue){
            return(false,"Please enter first diagnosed on")
        }
        return(true,"")
    }
}
class AllergiesAdverseAddVC: ParentViewController , UITextViewDelegate , UITextFieldDelegate{
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!
    
    
    var strHeader = "Add Allergies/Adverse Reaction"
    var strPlacholder = "Add Notes"
    var fieldData = AllergiesAdverseList()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var allergylist : Allergylist!
    var arryaMedicationlist = [Medicationlist]()
    var strDate = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        datefrmenter.dateFormat = "dd-MMM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        strDate = datefrmenter.string(from: currentDate as Date)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getmedicationlist()
        if strHeader == "Add Allergies/Adverse Reaction"{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("update", for: .normal)
            fieldData.allFields[0].strValue = allergylist.allergy_name
            fieldData.allFields[1].strValue = allergylist.allergy_trigger_by
            fieldData.allFields[2].strValue = allergylist.allergy_reaction
            fieldData.allFields[3].strValue = allergylist.allergy_occur
            fieldData.allFields[4].strValue = allergylist.allergy_diagnosed_date
            fieldData.allFields[7].strValue = allergylist.allergy_note
            strDate = allergylist.allergy_diagnosed_date

                if allergylist.allergy_note == ""{
                    fieldData.allFields[7].strValue = "Add Notes"
                }else{
                    strPlacholder = allergylist.allergy_note
                }
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[4].strValue = datefrmenter.string(from: sender.date)
        strDate = fieldData.allFields[4].strValue
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 4))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[4].strValue
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 4{
            textField.text! = strDate
            fieldData.allFields[4].strValue = textField.text!
        }
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Notes"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Notes"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        strPlacholder = textView.text!
        fieldData.allFields[7].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnAddMedicalClicekd(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AddMedicationVC")  as! AddMedicationVC
            mapVc1.strHeader = "Add Medication"
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if fieldData.fieldData.allFields[sender.tag].strkey == "0"{
            fieldData.fieldData.allFields[sender.tag].strkey = "1"
        }else{
            fieldData.fieldData.allFields[sender.tag].strkey = "0"
        }
        tableView.reloadData()
    }
    @IBAction func btnAddClicked(_ sender: Any) {
        if fieldData.validation().0{
            if "Add Allergies/Adverse Reaction" == strHeader{
                addallergy()
            }else{
                updateallergy()
            }
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }
}
extension AllergiesAdverseAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1{
            return 0
        }
        if indexPath.section == 0{
            return 90 * _widthRatio
        }else if indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 {
            return 75 * _widthRatio
        }
        if indexPath.section == 5 || indexPath.section == 6{
            return 33 * _widthRatio
        }
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 6{
            return fieldData.fieldData.allFields.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 0 || indexPath.section == 1 ||  indexPath.section == 2 || indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.section == 0{
                cell.viewConstretTop.constant = 5
            }
            return cell
        }
        
        if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1

            if indexPath.section == 4{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            return cell
        }
        if  indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = "Medication"
            cell.btnViewAll.isHidden = false
            let FormattedText1 = NSMutableAttributedString()
            FormattedText1
                .unlink("Add Medicine")
            cell.btnViewAll.setAttributedTitle(FormattedText1, for: .normal)
            cell.viewBG.makeCornerRound(redias : 6)
            
            return cell
        }
        if indexPath.section == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
            cell.lblisAdvatigate.text = fieldData.fieldData.allFields[indexPath.row].placeholder
            cell.btnCheck.tag = indexPath.row
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            
            if fieldData.fieldData.allFields[indexPath.row].strkey == "0"{
                cell.btnCheck.isSelected = false
            }else{
                cell.btnCheck.isSelected = true
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.section
        
        cell.textAreaNAme.text = fieldData.allFields[indexPath.section].strValue
        return cell
    }
}
extension AllergiesAdverseAddVC{
        func getmedicationlist(){
            showCentralSpinner()
            let parameters = "{\"data\":{\"memberId\":\"\(_currentUser.patient_id!)\",\"memberType\":\"patient\"}}"
            KPWebCall.call.postRequestApiClinet(relPath: "medicationlist.php", param: parameters) { (response) in
                self.hideCentralSpinner()
                if let response = response {
                    do {
                        if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                            print(response)
                            DispatchQueue.main.async { [self] in
                                if let Response = response as? NSDictionary{
                                    self.arryaMedicationlist.removeAll()
                                   
                                    if Response.getIntValue(key: "success") == 1{
                                        self.fieldData.fieldData.allFields.removeAll()
                                        self.tableView.backgroundView = nil
                                        if let data = Response.value(forKey: "data") as? NSArray{
                                            for obj in data{
                                                let str =  obj as? NSDictionary
                                                arryaMedicationlist.append(Medicationlist(dic: obj as! NSDictionary))
                                                var t7 = SingIn()
                                                t7.placeholder = str!.getStringValue(key: "medication_name")
                                                t7.strValue = str!.getStringValue(key: "medication_id")
                                                
                                                if strHeader == "Add Allergies/Adverse Reaction"{
                                                    t7.strkey = "0"
                                                }else{
                                                    
                                                    for obj in allergylist.medication_name{
                                                        if obj == t7.strValue{
                                                            print("\(obj)----------\(t7.strValue)")
                                                            t7.strkey = "1"
                                                            break
                                                        }else{
                                                            print("\(obj)----------\(t7.strValue)")
                                                            t7.strkey = "0"
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                                fieldData.fieldData.allFields.append(t7)
                                                
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
            }
        }
    
    func addallergy(){
        var arryaTime = [String]()
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["allergyName"] = fieldData.allFields[0].strValue
      //  dic["allergyTriggerBy"] = fieldData.allFields[1].strValue
        dic["allergyReaction"] = fieldData.allFields[2].strValue
        dic["allergyOccur"] = fieldData.allFields[3].strValue
        if fieldData.allFields[7].strValue == "Add Notes"{
            fieldData.allFields[7].strValue = ""
        }
        dic["allergyNote"] = fieldData.allFields[7].strValue
        let dateString = fieldData.allFields[4].strValue
        let strDay = fieldData.allFields[4].strValue.index(fieldData.allFields[4].strValue.startIndex, offsetBy: 2)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let date = formatter.date(from: dateString) else {
            return
        }
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: date)
        formatter.dateFormat = "MM"
        let month = formatter.string(from: date)
        
        dic["allergyDiagnosedDay"] = String(fieldData.allFields[4].strValue[..<strDay])
        dic["allergyDiagnosedMonth"] = month
        dic["allergyDiagnosedYear"] = year
        for obj in fieldData.fieldData.allFields{
            if obj.strkey == "1"{
                arryaTime.append(obj.strValue)
            }
        }
        dic["allergyMedication"] =  arryaTime
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addallergy.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updateallergy(){
        var arryaTime = [String]()
        var dic = [String:Any]()
        dic["allergyId"] = allergylist.allergy_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["allergyName"] = fieldData.allFields[0].strValue
        dic["allergyTriggerBy"] = fieldData.allFields[1].strValue
        dic["allergyReaction"] = fieldData.allFields[2].strValue
        dic["allergyOccur"] = fieldData.allFields[3].strValue
        if fieldData.allFields[7].strValue == "Add Notes"{
            fieldData.allFields[7].strValue = ""
        }
        dic["allergyNote"] = fieldData.allFields[7].strValue
        let dateString = fieldData.allFields[4].strValue
        let strDay = fieldData.allFields[4].strValue.index(fieldData.allFields[4].strValue.startIndex, offsetBy: 2)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let date = formatter.date(from: dateString) else {
            return
        }
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: date)
        formatter.dateFormat = "MM"
        let month = formatter.string(from: date)
        
        dic["allergyDiagnosedDay"] = String(fieldData.allFields[4].strValue[..<strDay])
        dic["allergyDiagnosedMonth"] = month
        dic["allergyDiagnosedYear"] = year
        for obj in fieldData.fieldData.allFields{
            if obj.strkey == "1"{
                arryaTime.append(obj.strValue)
            }
        }
        dic["allergyMedication"] =  arryaTime
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateallergy.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
