//
//  Add Medication.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
import DropDown
class AddItemList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Standardized Dosing Times"
        t3.keybord = .numberPad
        t3.image = #imageLiteral(resourceName: "ic_dropdown")
        
        allFields.append(t3)
    }
}
class MedicationList: NSObject {
    
    var allFields: [SingIn] = []
    var fieldData = AddItemList()
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Medicine *"
        t3.keybord = .default
        t3.placeholder1 = "Medication Name"

        
        var t8 = SingIn()
        t8.placeholder = "For *"
        t8.keybord = .default
        t8.placeholder1 = "e.g. Diabetes"

        
        var t9 = SingIn()
        t9.placeholder = "Dosage Form *"
        t9.keybord = .default
        t9.image = #imageLiteral(resourceName: "ic_dropdown")
        
        
        var t5 = SingIn()
        t5.placeholder = "Strength *"
        t5.keybord = .default
        t5.placeholder1 = "100"

        
        var t6 = SingIn()
        t6.placeholder = "Standardized Dosing Times"
        t6.keybord = .default
        t6.image = #imageLiteral(resourceName: "ic_dropdown")

        
        var t0 = SingIn()
        t0.placeholder = "Add Another"
        t0.keybord = .default
        
        var t10 = SingIn()
        t10.placeholder = "Other Instructions"
        t10.keybord = .default
        t10.strValue = "Note to patient e.g. Medicine to be taken 30 min before meal."

        var t11 = SingIn()
        t11.placeholder = "Start On"
        t11.keybord = .numberPad
        t11.image = #imageLiteral(resourceName: "ic_calender")
        t11.placeholder1 = "Date"

        
        var t12 = SingIn()
        t12.placeholder = "Until"
        t12.keybord = .numberPad
        t12.image = #imageLiteral(resourceName: "ic_calender")
        t12.placeholder1 = "Date"

        
        var t16 = SingIn()
        t16.placeholder = "Long Term"
        t16.strkey = "0"
        t16.strValue = "0"
        
        var t14 = SingIn()
        t14.placeholder = "Prescribed By"
        t14.keybord = .default
        t14.placeholder1 = "e.g. Dr. Ram Malhotra"

        
        var t15 = SingIn()
        t15.placeholder = "Side effects to expect"
        t15.keybord = .default
        t15.placeholder1 = "e.g. Redness, swelling, etc."

        
        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t0)
        
        allFields.append(t10)
        allFields.append(t11)
        allFields.append(t12)
        allFields.append(t16)
        allFields.append(t14)
        allFields.append(t15)
        
        
    }
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter medication name")
        }else  if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter medication for")
        }else  if String.validateStringValue(str: allFields[3].strValue){
            return(false,"Please enter strength")
        }
        return(true,"")
    }
}
class AddMedicationVC: ParentViewController, UITextViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!
    var strHeader = "Add Medication"
    var strPlacholder = "Note to patient e.g. Medicine to be taken 30 min before meal."
    var fieldData = MedicationList()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    let datePickerView1: UIDatePicker = UIDatePicker()
    let datefrmenter1 = DateFormatter()
    var medicationlist : Medicationlist!
    var dosageData : Dosage!
    let dropDown = DropDown()
    let dropDown1 = DropDown()
    var arryaDesage = [String]()
    var arryaTimeDay = [String]()
    var arryaDesageKey = [String]()
    var arryaTimeDayKey = [String]()
    var strDate = ""
    var strDate1 = ""

    
    override func viewDidLoad() {
        //fatalError()
        super.viewDidLoad()
        getdosage_new()
   
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        
        datefrmenter.dateFormat = "dd-MM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.minimumDate = currentDate as Date
        strDate = datefrmenter.string(from: currentDate as Date)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        datefrmenter1.dateFormat = "dd-MM-yyyy"
        datePickerView1.datePickerMode = UIDatePicker.Mode.date
        datePickerView1.minimumDate = currentDate as Date
        strDate1 = datefrmenter.string(from: currentDate as Date)
        datePickerView1.addTarget(self, action: #selector(self.datePickerValueChanged1), for: UIControl.Event.valueChanged)
        if "Add Medication" == strHeader{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("Update", for: .normal)
            fieldData.allFields[0].strValue = medicationlist.medication_name
            fieldData.allFields[1].strValue = medicationlist.medication_for
            fieldData.allFields[3].strValue = medicationlist.medication_strength
            fieldData.fieldData.allFields.removeAll()
            for obj in medicationlist.arryaDosageList {
                var t3 = SingIn()
                t3.placeholder = "Standardized Dosing Times"
                t3.keybord = .numberPad
                t3.image = #imageLiteral(resourceName: "ic_dropdown")
                t3.strValue = obj.dosage_time_title
                t3.strkey = obj.dosage_time_id
                fieldData.fieldData.allFields.append(t3)
                tableView.reloadData()
            }
            tableView.reloadData()
            fieldData.allFields[6].strValue = medicationlist.medication_instructions
            fieldData.allFields[7].strValue = medicationlist.medication_start_date
            fieldData.allFields[8].strValue = medicationlist.medication_end_date
            fieldData.allFields[9].strValue = medicationlist.medication_long_term
            fieldData.allFields[10].strValue = medicationlist.medication_by
            fieldData.allFields[11].strValue = medicationlist.medication_side_effect
            strDate = medicationlist.medication_start_date
            strDate1 = medicationlist.medication_end_date
            if medicationlist.medication_instructions == ""{
                fieldData.allFields[6].strValue = "Note to patient e.g. Medicine to be taken 30 min before meal."
            }else{
                strPlacholder = medicationlist.medication_instructions
            }
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[7].strValue = datefrmenter.string(from: sender.date)
        strDate = fieldData.allFields[7].strValue
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 7))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[7].strValue
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker) {
        fieldData.allFields[8].strValue = datefrmenter.string(from: sender.date)
        strDate1 = fieldData.allFields[8].strValue
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 8))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[8].strValue
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 7{
            textField.text! = strDate
            fieldData.allFields[7].strValue = textField.text!
        }else if textField.tag == 8{
            textField.text! = strDate1
            fieldData.allFields[8].strValue = textField.text!
        }
        
    }
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if fieldData.allFields[9].strValue == "0"{
            fieldData.allFields[9].strValue = "1"
        }else{
            fieldData.allFields[9].strValue = "0"
        }
        tableView.reloadData()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Note to patient e.g. Medicine to be taken 30 min before meal."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Note to patient e.g. Medicine to be taken 30 min before meal."{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        strPlacholder = textView.text!
        fieldData.allFields[6].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    @IBAction func btnAnotherClicked(_ sender: Any) {
        
        var t3 = SingIn()
        t3.placeholder = "Standardized Dosing Times"
        t3.keybord = .numberPad
        t3.image = #imageLiteral(resourceName: "ic_dropdown")
        t3.strValue = dosageData.arryaStandardizedDosingTime[0].name
        t3.strkey = dosageData.arryaStandardizedDosingTime[0].id
        
        fieldData.fieldData.allFields.append(t3)
        tableView.reloadData()
    }
    @IBAction func btnAddClcikcedData(_ sender: Any) {
        
        if fieldData.validation().0{
            if "Add Medication" == strHeader{
                addmedication()
            }else{
                updatemedication()
            }
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }
    
    
}
extension AddMedicationVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2{
            getdropDesage()
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.section].strValue = arryaDesage[index]
                fieldData.allFields[indexPath.section].strkey = arryaDesageKey[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropDown.show()
            return
        }
        if  indexPath.section == 4 {
            getdropTimeDay()
            dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.fieldData.allFields[indexPath.row].strValue = arryaTimeDay[index]
                fieldData.fieldData.allFields[indexPath.row].strkey = arryaTimeDayKey[index]
                
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropDown1.show()
            return
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldData.allFields.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 11{
            return 90 * _widthRatio
        }else if indexPath.section == 1 || indexPath.section == 3 || indexPath.section == 10 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 7 || indexPath.section == 8{
            return 75 * _widthRatio
        }
        if indexPath.section == 9 {
            return 45 * _widthRatio
        }
        if indexPath.section == 5 {
            return 33 * _widthRatio
        }
        return 150 * _widthRatio
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4{
            return fieldData.fieldData.allFields.count
        }
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 3 || indexPath.section == 10 || indexPath.section == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.textEmail.inputView = nil
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1

            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.section == 0{
                cell.viewConstretTop.constant = 5
            }
            if indexPath.section == 11{
                cell.viewConstretBottom.constant = 5
            }
            cell.textEmail.isUserInteractionEnabled = true
            return cell
        }
        if indexPath.section == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
            cell.lblisAdvatigate.text = fieldData.allFields[indexPath.section].placeholder
            cell.btnCheck.tag = indexPath.section
            
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            
            if fieldData.allFields[indexPath.section].strValue == "0"{
                cell.btnCheck.isSelected = false
            }else{
                cell.btnCheck.isSelected = true
            }
            return cell
        }
        if  indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            cell.textEmail.inputView = nil
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1

            cell.textEmail.isUserInteractionEnabled = false
            dropDown.anchorView = cell.textEmail
            return cell
        }
        if  indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            cell.imageCalander.image = fieldData.fieldData.allFields[indexPath.row].image
            cell.textEmail.inputView = nil
            cell.textEmail.isUserInteractionEnabled = false
            dropDown1.anchorView = cell.textEmail
            return cell
        }
        if  indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = ""
            cell.btnViewAll.isHidden = false
            let FormattedText1 = NSMutableAttributedString()
            FormattedText1
                .unlink("Add Another")
            cell.btnViewAll.setAttributedTitle(FormattedText1, for: .normal)
            cell.viewBG.makeCornerRound(redias : 6)
            
            return cell
        }
        if indexPath.section == 7 || indexPath.section == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.isUserInteractionEnabled = true
            cell.viewConstretTop.constant = -5
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1

            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            if indexPath.section == 7{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            if indexPath.section == 8{
                if #available(iOS 13.4, *) {
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.preferredDatePickerStyle = .wheels
                    datePickerView1.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView1
                }else{
                    cell.textEmail.inputView = datePickerView1
                }
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = -5
        cell.textAreaNAme.tag = indexPath.section
        cell.textAreaNAme.text = fieldData.allFields[indexPath.section].strValue
        return cell
    }
    
    
}
extension AddMedicationVC{
    func getdosage_new(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "dosage_new.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                //   self.arryaHealthlist.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        dosageData = Dosage(dic: data)
                                    }
                                    fieldData.allFields[2].strValue = dosageData.arryaDregesNameList[0].name
                                    fieldData.allFields[2].strkey = dosageData.arryaDregesNameList[0].id
                                    if "Add Medication" == strHeader{
                                        fieldData.fieldData.allFields[0].strValue = dosageData.arryaStandardizedDosingTime[0].name
                                        fieldData.fieldData.allFields[0].strkey = dosageData.arryaStandardizedDosingTime[0].id

                                    }else{
                                        
                                        for obj in dosageData.arryaDregesNameList {
                                            if medicationlist.dosage_type_id == obj.id{
                                                fieldData.allFields[2].strkey = obj.id
                                                fieldData.allFields[2].strkey = obj.id
                                                fieldData.allFields[2].strValue = obj.name
                                            }
                                        }
                                        
                                    }
                                    self.tableView.reloadData()
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func getdropDesage(){
        arryaDesage.removeAll()
        arryaDesageKey.removeAll()
        for obj in dosageData.arryaDregesNameList {
            arryaDesage.append(obj.name)
            arryaDesageKey.append(obj.id)
        }
        dropDown.dataSource = arryaDesage
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropTimeDay(){
        arryaTimeDay.removeAll()
        arryaTimeDayKey.removeAll()
        for obj in dosageData.arryaStandardizedDosingTime {
            arryaTimeDay.append(obj.name)
            arryaTimeDayKey.append(obj.id)
        }
        dropDown1.dataSource = arryaTimeDay
        dropDown1.direction = .bottom
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
        dropDown1.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
}
extension AddMedicationVC{
    func addmedication(){
        var arryaTime = [String]()
        var arryaQty = [String]()
        var arryaWen = [String]()

        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["medicationName"] = fieldData.allFields[0].strValue
        dic["medicationFor"] = fieldData.allFields[1].strValue
        dic["dosageHow"] = fieldData.allFields[2].strkey
        dic["dosageType"] = fieldData.allFields[2].strkey
        dic["dosageStrength"] = fieldData.allFields[3].strValue
        if fieldData.allFields[6].strValue == "Note to patient e.g. Medicine to be taken 30 min before meal."{
            fieldData.allFields[6].strValue = ""
        }
        dic["medicationInstruction"] = fieldData.allFields[6].strValue
        dic["medicationStartDate"] = fieldData.allFields[7].strValue
        if fieldData.allFields[9].strValue == "0"{
            dic["medicationEndDate"] = fieldData.allFields[8].strValue
        }else{
            dic["medicationEndDate"] = ""
        }
        dic["longTerm"] = fieldData.allFields[9].strValue
        dic["prescribedBy"] = fieldData.allFields[10].strValue
        dic["sideEffect"] = fieldData.allFields[11].strValue
        for obj in fieldData.fieldData.allFields{
            arryaTime.append(obj.strkey)
            arryaQty.append("1")
            arryaWen.append("2")
        }
        dic["time"] = arryaTime
        dic["quantity"] = arryaQty
        dic["when"] = arryaWen
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addmedication.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    func updatemedication(){
        var arryaTime = [String]()
        var arryaQty = [String]()
        var arryaWen = [String]()

        var dic = [String:Any]()
        dic["medicationId"] = medicationlist.medication_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["medicationName"] = fieldData.allFields[0].strValue
        dic["medicationFor"] = fieldData.allFields[1].strValue
        dic["dosageHow"] = fieldData.allFields[2].strkey
        dic["dosageType"] = fieldData.allFields[2].strkey
        dic["dosageStrength"] = fieldData.allFields[3].strValue
        if fieldData.allFields[6].strValue == "Note to patient e.g. Medicine to be taken 30 min before meal."{
            fieldData.allFields[6].strValue = ""
        }
        dic["medicationInstruction"] = fieldData.allFields[6].strValue
        dic["medicationStartDate"] = fieldData.allFields[7].strValue
        if fieldData.allFields[9].strValue == "0"{
            dic["medicationEndDate"] = fieldData.allFields[8].strValue
        }else{
            dic["medicationEndDate"] = ""
        }
        dic["longTerm"] = fieldData.allFields[9].strValue
        dic["prescribedBy"] = fieldData.allFields[10].strValue
        dic["sideEffect"] = fieldData.allFields[11].strValue
        for obj in fieldData.fieldData.allFields{
            arryaTime.append(obj.strkey)
            arryaQty.append("1")
            arryaWen.append("2")
        }
        dic["time"] = arryaTime
        dic["quantity"] = arryaQty
        dic["when"] = arryaWen

        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatemedication.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
}

