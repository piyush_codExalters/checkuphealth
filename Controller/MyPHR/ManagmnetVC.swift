//
//  ManagmnetVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
class CustAdd: UIButton {
    var sectiontag : Int!
}
class ManagmnetVC: ParentViewController {
    
    var arryaManagementList = [ManagementList]()
    var isGP = "0"
    var sectionInRow = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        reviewwithdiagnosis()
    }
    @IBAction func btnGpSwiftClcike(_ sender: UISwitch) {
        if sender.isOn{
            getupdate(key : "isSharewithGP",value: "1")
        }else{
            getupdate(key : "isSharewithGP",value: "0")
        }
        
    }
    
   
    
    @IBAction func btnDownloadClcked(_ sender: CustAdd) {
        if sender.sectiontag == 21{
            if let url = URL(string: arryaManagementList[sender.tag].review_referralpath) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        if sender.sectiontag == 23{
            if let url = URL(string: arryaManagementList[sender.tag].review_referralpath2) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        if sender.sectiontag == 25{
            if let url = URL(string: arryaManagementList[sender.tag].review_sicknotepath) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        if sender.sectiontag == 27{
            if let url = URL(string: arryaManagementList[sender.tag].review_sicknotepath2) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    
}
extension ManagmnetVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaManagementList.count + 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 0
        }else{
            if indexPath.row == 0{
                return UITableView.automaticDimension
            }else if indexPath.row == 1 || indexPath.row == 2{
                if arryaManagementList[indexPath.section - 1].review_problemtype == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 3 || indexPath.row == 4{
                if arryaManagementList[indexPath.section - 1].review_complaint == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 5 || indexPath.row == 6{
                if arryaManagementList[indexPath.section - 1].review_historycomplaint == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 7 || indexPath.row == 8{
                if arryaManagementList[indexPath.section - 1].review_pastmedicalhistory == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 9 || indexPath.row == 10{
                if arryaManagementList[indexPath.section - 1].review_medicalhistory == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 11 || indexPath.row == 12{
                if arryaManagementList[indexPath.section - 1].review_allergies == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 13 || indexPath.row == 14{
                if arryaManagementList[indexPath.section - 1].review_socialhistory == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 15 || indexPath.row == 16{
                if arryaManagementList[indexPath.section - 1].review_examination == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 17 || indexPath.row == 18{
                if arryaManagementList[indexPath.section - 1].review_impression == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 19 || indexPath.row == 20{
                if arryaManagementList[indexPath.section - 1].review_plan == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }else if indexPath.row == 21 || indexPath.row == 22{
                if arryaManagementList[indexPath.section - 1].review_referralpath == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }else if indexPath.row == 23 || indexPath.row == 24{
                if arryaManagementList[indexPath.section - 1].review_referralpath2 == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }else if indexPath.row == 25 || indexPath.row == 26{
                if arryaManagementList[indexPath.section - 1].review_sicknotepath == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }else if indexPath.row == 27 || indexPath.row == 28{
                if arryaManagementList[indexPath.section - 1].review_sicknotepath2 == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }else if indexPath.row == 29 || indexPath.row == 30{
                if arryaManagementList[indexPath.section - 1].review_isfollowupadvised == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 31 || indexPath.row == 32{
                if arryaManagementList[indexPath.section - 1].review_date == ""{
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
                
            }else if indexPath.row == 33 || indexPath.row == 34{
//                if arryaManagementList[indexPath.section - 1].review_date == ""{
//                    return 0
//                }else{
//                    return UITableView.automaticDimension
//                }
                return UITableView.automaticDimension
            }else{
                return UITableView.automaticDimension
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        if arryaManagementList[section - 1].arraybloodDetails.count == 0{
            return 33
        }
        return 36
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title2") as! DrDetailsCell
            cell.viewBg.makeCornerRound(redias: 5)
            if isGP == "0"{
                cell.btnGP.isOn = false
            }else{
                cell.btnGP.isOn = true
            }
            return cell
        }
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NAmeDemo") as! DrDetailsCell
            cell.viewBg.makeCornerRound(redias: 5)
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_by
            cell.lblDay.text = arryaManagementList[indexPath.section - 1].review_from
            
            cell.constraintTop.constant = 5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Problem Type:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_problemtype
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Presenting Complaint:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_complaint
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "History of Presenting Complaint:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_historycomplaint
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Past Medical History:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_pastmedicalhistory
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Medication History:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_medicalhistory
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Allergies:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 12{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_allergies
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 13{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Social History:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 14{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_socialhistory
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 15{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Examination Findings:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 16{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_examination
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 17{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Impression:"
            cell.btnDownload.isHidden = true
            cell.constraintTop.constant = -5
            cell.btnDownload.tag = indexPath.section - 1
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 18{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_impression
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 19{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Plan:"
            cell.btnDownload.isHidden = true
            cell.btnDownload.tag = indexPath.section - 1
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 20{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_plan
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 21{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Referral Note:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.sectiontag = indexPath.row
            cell.btnDownload.isHidden = false
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 22{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            let refal = URL(string: arryaManagementList[indexPath.section - 1].review_referralpath!)
            cell.lblDesc.text = refal?.lastPathComponent
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        } else if indexPath.row == 23{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Referral Note 2:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.sectiontag = indexPath.row
            cell.btnDownload.isHidden = false
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 24{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            let refal = URL(string: arryaManagementList[indexPath.section - 1].review_referralpath2!)
            cell.lblDesc.text = refal?.lastPathComponent
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 25{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Sick Note:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.sectiontag = indexPath.row
            cell.btnDownload.isHidden = false
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 26{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            let refal = URL(string: arryaManagementList[indexPath.section - 1].review_sicknotepath!)
            cell.lblDesc.text = refal?.lastPathComponent
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        } else if indexPath.row == 27{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Sick Note 2:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = false
            cell.btnDownload.sectiontag = indexPath.row
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 28{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            let refal = URL(string: arryaManagementList[indexPath.section - 1].review_sicknotepath2!)
            cell.lblDesc.text = refal?.lastPathComponent
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 29{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Follow Up Advised:"
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.isHidden = true
            cell.btnDownload.sectiontag = indexPath.row
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 30{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_isfollowupadvised
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 31{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Diagnosed on:"
            cell.btnDownload.isHidden = true
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.sectiontag = indexPath.row
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 32{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaManagementList[indexPath.section - 1].review_date
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            if arryaManagementList[indexPath.section - 1].arraybloodDetails.count == 0{
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
            }
            return cell
        }else if indexPath.row == 33{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Lab Request List:"
            cell.btnDownload.isHidden = true
            cell.btnDownload.tag = indexPath.section - 1
            cell.btnDownload.sectiontag = indexPath.row
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 34{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = "Nationwide pathology"//arryaManagementList[indexPath.section - 1].review_date
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LabeTitle") as! DrDetailsCell
            
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = 5
            cell.managmnet = self
            cell.frame = tableView.bounds
            sectionInRow = indexPath.section - 1
            cell.layoutIfNeeded()
            cell.HightLabel.constant = CGFloat(44 * (arryaManagementList[indexPath.section - 1].arraybloodDetails.count + 1))
            DispatchQueue.main.async {
                cell.labTableView.reloadData()
            }
            
            return cell
            
        }
        
    }
    
    
    
    
}

extension DrDetailsCell : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return managmnet.arryaManagementList[managmnet.sectionInRow].arraybloodDetails.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Title1") as! DrDetailsCell
        var strSymblo = ""
        if managmnet.isWorkingCountryUK(){
            strSymblo = "£"
        }else{
            strSymblo = "$"
        }
        cell.testName.text =  managmnet.arryaManagementList[managmnet.sectionInRow].arraybloodDetails[indexPath.row - 1].bloodName
        
        cell.lblPrice.text = strSymblo + managmnet.arryaManagementList[managmnet.sectionInRow].arraybloodDetails[indexPath.row - 1].dSellingPrice
        cell.lblTat.text = managmnet.arryaManagementList[managmnet.sectionInRow].arraybloodDetails[indexPath.row - 1].Tatname
        return cell
    }
    
}
extension ManagmnetVC{
    func reviewwithdiagnosis(){
        showCentralSpinner()
        
        var isParent = ""
        if  _userDefault.value(forKey: "nUserType") as! Int == 1 ||  _userDefault.value(forKey: "nUserType") as! Int == 2{
            isParent = "0"
        }else{
            isParent = "1"
        }
        
        let parameters = "{\"data\":{\"userId\":\"\(_currentUser.patient_id!)\",\"isParent\":\"\(isParent)\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "reviewwithdiagnosis_New.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaManagementList.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    sectionIndexGlobel = 0
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            arryaManagementList.append(ManagementList(dic: obj as! NSDictionary))
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                getuserprofile()
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getuserprofile(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "getuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        isGP =  dataname.getStringValue(key: "isSharewithGP")
                                        tableView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getupdate(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if value == "1"{
                                        showSucessMessage(message: "Your data is now being shared with GP")
                                    }else{
                                        showFailMessage(message: "Your data will not be shared with GP now & onwards.")
                                    }
                                    getuserprofile()
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
