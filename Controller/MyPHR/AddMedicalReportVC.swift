//
//  AddMedicalReportVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
import MobileCoreServices
import CropViewController
import IQKeyboardManagerSwift

class AddMedicalReportList: NSObject {
    
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Name *"
        t3.keybord = .default
        t3.placeholder1 = "e.g. Chest X-ray, Blood test etc."

        
        var t8 = SingIn()
        t8.placeholder = "Date *"
        t8.keybord = .default
        t8.image = #imageLiteral(resourceName: "ic_calender")
        t8.placeholder1 = "Date"

        
        var t9 = SingIn()
        t9.placeholder = "Findings"
        t9.keybord = .default
        t9.strValue = "TSH - e.g High, T4 - Normal, T3 - Normal, Interpreation - Mild (subclinical) Hypothyroidism"
        
        
        var t5 = SingIn()
        t5.placeholder = "Attach Document"
        t5.keybord = .default
        t5.strValue = "Choose File"
        
        var t6 = SingIn()
        t6.placeholder = "Notes"
        t6.keybord = .default
        t6.placeholder1 = "Notes"

        
        var t0 = SingIn()
        t0.placeholder = "Prescribed By"
        t0.keybord = .default
        t0.placeholder1 = "e.g. Dr. john at Physician Center"
        
        var t10 = SingIn()
        t10.placeholder = "Treated By"
        t10.keybord = .default
        t10.placeholder1 = "e.g. Med Diagnostic Lab"
        
        
        
        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t0)
        allFields.append(t10)

    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter name")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter date")
        }
        return(true,"")
    }
}
class AddMedicalReportVC: ParentViewController, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!

    var strHeader = "Add Medical Report"
    var strPlacholder = "Add Notes"
    var fieldData = AddMedicalReportList()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var imagePicker = UIImagePickerController()
    var imgUser : UIImage?
    var uploadFile : Data?
    var strDate = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        let currentDate = NSDate()
        datefrmenter.dateFormat = "dd-MM-yyyy"
        datefrmenter.locale = Locale(identifier:"en_US_POSIX")
        strDate = datefrmenter.string(from: currentDate as Date)
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[1].strValue = datefrmenter.string(from: sender.date)
        strDate = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[1].strValue
    }
   
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "TSH - e.g High, T4 - Normal, T3 - Normal, Interpreation - Mild (subclinical) Hypothyroidism"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "TSH - e.g High, T4 - Normal, T3 - Normal, Interpreation - Mild (subclinical) Hypothyroidism"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[textView.tag].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1{
            textField.text! = strDate
            fieldData.allFields[1].strValue = textField.text!
        }
        
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if fieldData.validation().0{
            addmedical_report()
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
    }
    
  

}
extension AddMedicalReportVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3{
            openPickerCamera()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isWorkingCountryUK() {
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6{
                return 0
            }
            if indexPath.row == 3{
                return 90 * _widthRatio
            }
         }else{
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6{
                return 75 * _widthRatio
            }
        }
        
        if indexPath.row == 0 || indexPath.row == 6{
            return 90 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 3  || indexPath.row == 5{
            return 75 * _widthRatio
        }
        
        return 130 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 3  || indexPath.row == 5 || indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            if indexPath.row == 6{
                cell.viewConstretBottom.constant = 5
            }
            if indexPath.row == 3{
                cell.textEmail.isUserInteractionEnabled = false
                cell.viewText.backgroundColor = .lightGray
                cell.viewText.makeCustomRound(radius:6,bc:.lightGray)
                if isWorkingCountryUK() {
                    cell.viewConstretBottom.constant = 5
                 }
            }else{
                cell.textEmail.isUserInteractionEnabled = true
                cell.viewText.backgroundColor = .white
                cell.viewText.makeCustomRound(radius:6,bc:.black)

            }
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if indexPath.row == 1{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = -5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        return cell
    }
}
extension AddMedicalReportVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropViewController, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropViewController, animated: true)
        }
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            // 'image' is the newly cropped version of the original image
        uploadFile = nil
        fieldData.allFields[3].strValue = "jpg://checkupfile.jpg"
        imgUser = image
        tableView.reloadData()

        dismiss(animated:true, completion: nil)

    }
  
    
    func openPickerCamera(){
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        let action2 = UIAlertAction(title: "Open Gallery", style: .default) { (action) in
            self.openLibrary()
        }
        let action4 = UIAlertAction(title: "iCloud", style: .default) { (action) in
            self.openIcould()
        }
        let action3 = UIAlertAction(title: "Close", style: .cancel) { (action) in
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action4)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openIcould(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypePNG),String(kUTTypeJPEG),"com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document","public.text"], in: .import)
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}
extension AddMedicalReportVC: UIDocumentMenuDelegate,UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        do {
            imgUser = nil
            let DOc = try Data(contentsOf: myURL)
            print(DOc.count.byteSize)
            let fileSize = Double(DOc.count / 1048576) //Convert in to MB
            print("File size in MB: ", fileSize)
            if fileSize <= 2.0{
                uploadFile = DOc
                fieldData.allFields[3].strValue = "\(myURL.lastPathComponent)"
                fieldData.allFields[3].strValue1 = "\(myURL)".getPathExtension()
                tableView.reloadData()
            }else{
                showFailMessage(message: "Max file size should be 2 mb only.")
            }
        } catch {
            print(error)
            return
        }
        
        
        print("import result : \(myURL)")
    }
    
    
    func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension AddMedicalReportVC{
    func addmedical_report(){
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["medicalReportName"] = fieldData.allFields[0].strValue
        dic["medicalReportDate"] = fieldData.allFields[1].strValue
        if "TSH - e.g High, T4 - Normal, T3 - Normal, Interpreation - Mild (subclinical) Hypothyroidism" == fieldData.allFields[2].strValue{
            fieldData.allFields[2].strValue = ""
        }
        dic["medicalReportResult"] = fieldData.allFields[2].strValue
        dic["medicalReportNote"] = fieldData.allFields[4].strValue
        dic["medicalReportBy"] = fieldData.allFields[5].strValue
        dic["medicalReportCentre"] = fieldData.allFields[6].strValue
        if imgUser == nil{
            if uploadFile == nil{
                dic["medicalReportAttachment"] = ""
                dic["medicalReportFormat"] = ""
            }else{
                dic["medicalReportAttachment"] = "\(uploadFile!.base64EncodedString())"
                dic["medicalReportFormat"] = fieldData.allFields[3].strValue1
            }
        }else{
            let imageResized = imgUser!.resizeWith(percentage: 0.25)
            let base64 = imageResized?.toBase64()
            dic["medicalReportAttachment"] = "\(base64!)"
            dic["medicalReportFormat"] = "jpg"
        }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addmedical_report.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }

}
