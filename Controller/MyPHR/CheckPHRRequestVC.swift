//
//  CheckPHRRequestVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit

class CheckPHRRequestVC: ParentViewController {

    var arryaPhrrequestList = [PhrrequestList]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getphrrequest()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Request"{
            let vc = segue.destination as! RequestPopVC
            vc.moduleName = sender as! String
        }
    }
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        phrrequestoperation(status: "2", request_id: arryaPhrrequestList[sender.tag].request_id)
    }
    
    @IBAction func btnAcpptClicked(_ sender: UIButton) {
        phrrequestoperation(status: "1", request_id: arryaPhrrequestList[sender.tag].request_id)
    }
    

}
extension CheckPHRRequestVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
            performSegue(withIdentifier: "Request", sender: arryaPhrrequestList[indexPath.section].request_module)

        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaPhrrequestList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio

        }else{
            return 72 * _widthRatio
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arryaPhrrequestList[section].request_status == ""{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arryaPhrrequestList[indexPath.section].request_status == ""{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Book") as! DrAndNuresListCell
                cell.btnCall.tag = indexPath.section
                cell.btnBook.tag = indexPath.section
                return cell
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrDetails") as! DrAndNuresListCell
        cell.imageDr.kf.setImage(with: URL(string:arryaPhrrequestList[indexPath.section].request_doctor_image), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        cell.lblActive.text = arryaPhrrequestList[indexPath.section].request_status
        cell.lblActive.textColor = arryaPhrrequestList[indexPath.section].colorCode
        cell.lblDrName.text = arryaPhrrequestList[indexPath.section].request_doctor_name
        cell.lblDateTime.text = arryaPhrrequestList[indexPath.section].request_time
        cell.lblRequestModule.text =  "Requested Module: " + arryaPhrrequestList[indexPath.section].request_module
        cell.viewConstretBottom.constant = 5
        if arryaPhrrequestList[indexPath.section].request_status == ""{
            cell.viewConstretBottom.constant = -5
        }
        return cell
    }
    
    
    
    
}
extension CheckPHRRequestVC{
    func getphrrequest(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"patient_id\":\"\(_currentUser.patient_id!)\",\"key\":\"all\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "phrrequest.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaPhrrequestList.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaPhrrequestList.append(PhrrequestList(dic: obj as! NSDictionary))
                                }

                            }
                            self.tableView.reloadData()
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func phrrequestoperation(status : String,request_id : String){
        let parameters = "{\"data\":{\"patient_id\":\"\(_currentUser.patient_id!)\",\"request_id\":\"\(request_id)\"\"key\":\"all\"\"status\":\"\(status)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "phrrequestoperation.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getphrrequest()
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
