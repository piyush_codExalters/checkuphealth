//
//  FamilyHistoryAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
import DropDown
class FamilyHistoryList: NSObject {
    
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Family History For"
        t3.keybord = .default
        t3.image = #imageLiteral(resourceName: "ic_dropdown")
        
        
        var t0 = SingIn()
        t0.placeholder = "Description *"
        t0.keybord = .default
        t0.strValue = "eg. has a history of high blood pressure."

        
        allFields.append(t3)
        allFields.append(t0)
    }
    
    func validation() -> (Bool,String){
         if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter desc.")
        }
        return(true,"")
    }
}
class FamilyHistoryAddVC: ParentViewController , UITextViewDelegate {
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!

    var strHeader = "Add Family History"
    var strPlacholder = "eg. has a history of high blood pressure."
    var fieldData = FamilyHistoryList()
    var arryaFamilytypelist = [Familytypelist]()
    var aryaFamily = [String]()
    var aryaFamilyKey = [String]()
    let dropDown = DropDown()
    var familylist : Familylist!
    var strDate = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if "Add Family History" == strHeader{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("Update", for: .normal)
        }
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getfamilytypelist()
        if strHeader == "Add Family History"{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("update", for: .normal)
            fieldData.allFields[1].strValue = familylist.family_desc
            if familylist.family_desc == ""{
                fieldData.allFields[1].strValue = "eg. has a history of high blood pressure."
            }else{
                strPlacholder = familylist.family_desc
            }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "eg. has a history of high blood pressure."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "eg. has a history of high blood pressure."{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[1].strValue = textView.text!

    }
    func getdrop(){
        aryaFamily.removeAll()
        aryaFamilyKey.removeAll()
        for obj in arryaFamilytypelist {
            aryaFamily.append(obj.family_type_title)
            aryaFamilyKey.append(obj.family_id)
        }
        dropDown.dataSource = aryaFamily
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if fieldData.validation().0{
            if "Add Family History" == strHeader{
                addfamily()
            }else{
                updatefamily()
            }
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }
    
}
extension FamilyHistoryAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            getdrop()
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.row].strValue = aryaFamily[index]
                fieldData.allFields[indexPath.row].strkey = aryaFamilyKey[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropDown.show()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = 5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            dropDown.anchorView = cell.textEmail
            cell.textEmail.isUserInteractionEnabled = false
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        return cell
    }
}
extension FamilyHistoryAddVC{
    func getfamilytypelist(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "familytypelist.php", param: "") { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaFamilytypelist.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaFamilytypelist.append(Familytypelist(dic: obj as! NSDictionary))
                                }
                                if "Add Family History" == strHeader{
                                    fieldData.allFields[0].strValue = arryaFamilytypelist[0].family_type_title
                                    fieldData.allFields[0].strkey = arryaFamilytypelist[0].family_id

                                }else{
                                    fieldData.allFields[0].strValue = familylist.family_type_title
                                    for obj in arryaFamilytypelist {
                                        if fieldData.allFields[0].strValue == obj.family_type_title{
                                            fieldData.allFields[0].strkey = obj.family_id
                                        }
                                    }
                                }
                            }
                           
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    
    func addfamily(){
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        if String.validateStringValue(str: fieldData.allFields[1].strValue) || fieldData.allFields[1].strValue == "eg. has a history of high blood pressure."{
            return showFailMessage(message: "Please enter desc.")
        }
        dic["familyDesc"] = fieldData.allFields[1].strValue
        dic["familyTypeId"] = fieldData.allFields[0].strkey
   
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addfamily.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updatefamily(){
        var dic = [String:Any]()
        dic["familyId"] = familylist.family_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        if String.validateStringValue(str: fieldData.allFields[1].strValue) || fieldData.allFields[1].strValue == "eg. has a history of high blood pressure."{
            return showFailMessage(message: "Please enter desc.")
        }
        dic["familyDesc"] = fieldData.allFields[1].strValue
        dic["familyTypeId"] = fieldData.allFields[0].strkey
   
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatefamily.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
