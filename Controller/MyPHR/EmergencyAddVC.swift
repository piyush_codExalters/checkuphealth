//
//  EmergencyAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
import SKCountryPicker
class EmergencyList: NSObject {
    
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Name"
        t0.keybord = .default
        t0.placeholder1 = "e.g. Dr. john Doe"

        
        var t1 = SingIn()
        t1.placeholder = "Relation"
        t1.keybord = .default
        t1.placeholder1 = "e.g. Doctor, Spouse."
        
        let country = CountryManager.shared.currentCountry
        
        var t2 = SingIn()
        t2.placeholder = "Mobile No"
        t2.strkey = country!.countryCode
        t2.strcode = (country?.dialingCode)!
        t2.image1 = country!.flag
        t2.keybord = .numberPad
        
        var t3 = SingIn()
        t3.placeholder = "Home Phone"
        t3.strkey = country!.countryCode
        t3.strcode = (country?.dialingCode)!
        t3.image1 = country!.flag
        t3.keybord = .numberPad
        
        var t4 = SingIn()
        t4.placeholder = "Office Phone"
        t4.strkey = country!.countryCode
        t4.strcode = (country?.dialingCode)!
        t4.image1 = country!.flag
        t4.keybord = .numberPad
        
        var t5 = SingIn()
        t5.placeholder = "Address"
        t5.keybord = .default
        t5.image = #imageLiteral(resourceName: "ic_navigation_aro")
        
        var t6 = SingIn()
        t6.placeholder = "Address Line 1"
        t6.keybord = .default
        
        var t7 = SingIn()
        t7.placeholder = "Address Line 2(Optional)"
        t7.keybord = .default
        
        var t9 = SingIn()
        t9.placeholder = "Area(Optional)"
        t9.keybord = .default
        
        var t10 = SingIn()
        t10.placeholder = "City"
        t10.keybord = .default
        
        var t11 = SingIn()
        t11.placeholder = "District(Optional)"
        t11.keybord = .default
        
        var t12 = SingIn()
        t12.placeholder = "State/Province/Region(Optional)"
        t12.keybord = .default
        
        var t13 = SingIn()
        t13.placeholder = "Country"
        t13.keybord = .default
        
        var t16 = SingIn()
        t16.placeholder = "Postcode"
        t16.keybord = .default
        
        
        var t14 = SingIn()
        t14.placeholder = "Remarks"
        t14.keybord = .default
        
        
        allFields.append(t0)
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
        allFields.append(t4)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
        allFields.append(t9)
        allFields.append(t10)
        allFields.append(t11)
        allFields.append(t12)
        allFields.append(t13)
        allFields.append(t16)
        allFields.append(t14)
    }
    
    
}


class EmergencyAddVC: ParentViewController, addressDelegete {
    
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!
    
    var strHeader = "Add Emergency Numbers"
    var fieldData = EmergencyList()
    var emergencyListName : EmergencyListName!
    var indexs = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        if "Add Emergency Numbers" == strHeader{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("Update", for: .normal)
            fieldData.allFields[0].strValue = emergencyListName.emergency_name
            fieldData.allFields[1].strValue = emergencyListName.emergency_relation
            fieldData.allFields[2].strValue = String(emergencyListName.emergency_mobile.dropFirst(emergencyListName.emergency_countrycode.count))
            fieldData.allFields[2].strcode = emergencyListName.emergency_countrycode
            fieldData.allFields[3].strValue = String(emergencyListName.emergency_hphone.dropFirst(emergencyListName.emergency_hphone_countrycode.count))
            fieldData.allFields[3].strcode = emergencyListName.emergency_hphone_countrycode
            fieldData.allFields[4].strValue = String(emergencyListName.emergency_ophone.dropFirst(emergencyListName.emergency_ophone_countrycode.count))
            fieldData.allFields[4].strcode = emergencyListName.emergency_ophone_countrycode
            fieldData.allFields[5].strValue = emergencyListName.emergency_address
            fieldData.allFields[6].strValue = emergencyListName.emergencyAddress
            fieldData.allFields[7].strValue = emergencyListName.emergencyAddress1
            fieldData.allFields[8].strValue = emergencyListName.emergency_street
            fieldData.allFields[9].strValue = emergencyListName.emergency_city
            fieldData.allFields[10].strValue = emergencyListName.emergency_district
            fieldData.allFields[11].strValue = emergencyListName.emergency_state
            fieldData.allFields[12].strValue = emergencyListName.emergency_country
            fieldData.allFields[13].strValue = emergencyListName.emergency_pin
            fieldData.allFields[14].strValue = emergencyListName.emergency_remarks
        }
    }
    
    func getAddress(str: String,address: AddressListName, index: Int) {
        fieldData.allFields[5].strValue = str
        fieldData.allFields[6].strValue = address.addressLine1
        fieldData.allFields[7].strValue = address.addressLine2
        fieldData.allFields[8].strValue = address.area
        fieldData.allFields[9].strValue = address.city
        fieldData.allFields[10].strValue = address.district
        fieldData.allFields[11].strValue = address.state
        fieldData.allFields[12].strValue = address.country
        fieldData.allFields[13].strValue = address.postcode
        indexs = index
        tableView.reloadData()
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    
    @IBAction func btnContyCodeClikced(_ sender: UIButton) {
        _ = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.fieldData.allFields[sender.tag].strkey = country.countryCode // IN
            self.fieldData.allFields[sender.tag].strcode = country.dialingCode! // +91
            self.tableView.reloadData()
        }
        
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if validation().0{
            if "Add Emergency Numbers" == strHeader{
                addemergency()
            }else{
                updateemergency()
            }
        }else{
            showFailMessage(message : validation().1)
        }
        
    }
   
    
}
extension EmergencyAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressVC")  as! AddressVC
            mapVc1.delegatesAddress2 = self
            mapVc1.isSelectionIndex = indexs
            navigationController?.pushViewController(mapVc1, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 14{
            return 90 * _widthRatio
        }
        if isWorkingCountryUK(){
            if indexPath.row == 13{
                return 75 * _widthRatio
            }
        }else{
            if indexPath.row == 13{
                return 0
            }
        }
        return 75 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custTextConact") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.lblCountryCode.text = "\(fieldData.allFields[indexPath.row].strcode)"
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.viewBg.makeCornerRound(redias : 6)
            cell.btnContory.tag = indexPath.row
            return cell
        }
        if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            cell.textEmail.isUserInteractionEnabled = false
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.inputView = nil
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = -5
        if indexPath.row == 0{
            cell.viewConstretTop.constant = 5
        }
        if indexPath.row == 14{
            cell.viewConstretBottom.constant = 5
        }
        return cell
    }
}

extension EmergencyAddVC{
    
    func addemergency(){
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["emergencyName"] = fieldData.allFields[0].strValue
        dic["emergencyRelation"] = fieldData.allFields[1].strValue
        dic["emergencyMobile"] = fieldData.allFields[2].strValue
        dic["emergencyHphone"] = fieldData.allFields[3].strValue
        dic["emergencyOphone"] = fieldData.allFields[4].strValue
        dic["emergency_address"] = fieldData.allFields[5].strValue
        dic["emergencyAddress"] = fieldData.allFields[6].strValue
        dic["emergencyAddress1"] = fieldData.allFields[7].strValue
        dic["emergencyStreet"] = fieldData.allFields[8].strValue
        dic["emergencyCity"] = fieldData.allFields[9].strValue
        dic["emergencyDistrict"] = fieldData.allFields[10].strValue
        dic["emergencyState"] = fieldData.allFields[11].strValue
        dic["emergencyCountry"] = fieldData.allFields[12].strValue
        dic["emergencyPin"] =  fieldData.allFields[13].strValue
        dic["emergencyRemarks"] = fieldData.allFields[14].strValue
        dic["emergencycountrycode"] = fieldData.allFields[2].strcode
        dic["emergencyHphonecountrycode"] = fieldData.allFields[3].strcode
        dic["emergencyOphonecountrycode"] = fieldData.allFields[4].strcode
       
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addemergency.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updateemergency(){
        var dic = [String:Any]()
        dic["emergencyId"] = emergencyListName.emergency_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["memberType"] = "patient"
        dic["emergencyName"] = fieldData.allFields[0].strValue
        dic["emergencyRelation"] = fieldData.allFields[1].strValue
        dic["emergencyMobile"] = fieldData.allFields[2].strValue
        dic["emergencyHphone"] = fieldData.allFields[3].strValue
        dic["emergencyOphone"] = fieldData.allFields[4].strValue
        dic["emergency_address"] = fieldData.allFields[5].strValue
        dic["emergencyAddress"] = fieldData.allFields[6].strValue
        dic["emergencyAddress1"] = fieldData.allFields[7].strValue
        dic["emergencyStreet"] = fieldData.allFields[8].strValue
        dic["emergencyCity"] = fieldData.allFields[9].strValue
        dic["emergencyDistrict"] = fieldData.allFields[10].strValue
        dic["emergencyState"] = fieldData.allFields[11].strValue
        dic["emergencyCountry"] = fieldData.allFields[12].strValue
        dic["emergencyPin"] =  fieldData.allFields[13].strValue
        dic["emergencyRemarks"] = fieldData.allFields[14].strValue
        dic["emergencycountrycode"] = fieldData.allFields[2].strcode
        dic["emergencyHphonecountrycode"] = fieldData.allFields[3].strcode
        dic["emergencyOphonecountrycode"] = fieldData.allFields[4].strcode
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateemergency.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Please enter name")
        }else if String.validateStringValue(str: fieldData.allFields[1].strValue){
            return(false,"Please enter relation")
        }else if String.validateStringValue(str: fieldData.allFields[2].strValue){
            return(false,"Please enter mobile number")
        }
//        else if fieldData.allFields[2].strValue.count <= 9 {
//            return(false,"Please enter valid mobile number")
//        }
        else if !String.validateStringValue(str: fieldData.allFields[3].strValue){
//            if fieldData.allFields[3].strValue.count <= 9 {
//                return(false,"Please enter valid home contact")
//            }else
            if !String.validateStringValue(str: fieldData.allFields[4].strValue){
//                if fieldData.allFields[4].strValue.count <= 9 {
//                    return(false,"Please enter valid office contact")
//                }
            }else if String.validateStringValue(str: fieldData.allFields[13].strValue){
                if isWorkingCountryUK(){
                    return(false,"Please enter postcode")
                }
                
            }
        }else if !String.validateStringValue(str: fieldData.allFields[4].strValue){
//            if fieldData.allFields[4].strValue.count <= 9 {
//                return(false,"Please enter valid office contact")
//            }else
            if String.validateStringValue(str: fieldData.allFields[13].strValue){
                if isWorkingCountryUK(){
                    return(false,"Please enter postcode")
                }
                
            }
        }else if String.validateStringValue(str: fieldData.allFields[13].strValue){
            if isWorkingCountryUK(){
                return(false,"Please enter postcode")
            }
            
        }
        return(true,"")
    }
}


