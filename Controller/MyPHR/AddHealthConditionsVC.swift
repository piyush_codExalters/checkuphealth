//
//  AddHealthConditionsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
import DropDown
class medicationNAme: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t7 = SingIn()
        t7.placeholder = "Medication"
        t7.strkey = "0"
        // allFields.append(t7)
        
    }
}
class AddHealthConditionsList: NSObject {
    
    var allFields: [SingIn] = []
    var fieldData = medicationNAme()
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Health Conditions *"
        t3.keybord = .default
        t3.placeholder1 = "e.g. Diabetes, Glaucoma etc"
        
        var t8 = SingIn()
        t8.placeholder = "Diagnosed On"
        t8.keybord = .default
        t8.image = #imageLiteral(resourceName: "ic_calender")
        t8.placeholder1 = "Date"

        var t9 = SingIn()
        t9.placeholder = "Status *"
        t9.keybord = .default
        t9.image = #imageLiteral(resourceName: "ic_dropdown")
        
        
        var t5 = SingIn()
        t5.placeholder = "Treated By *"
        t5.keybord = .default
        t5.image = #imageLiteral(resourceName: "ic_dropdown")
        t5.placeholder1 = "Write a name of a doctor"

        var t6 = SingIn()
        t6.placeholder = "Add Medication"
        t6.keybord = .numberPad
        
        var t7 = SingIn()
        t7.placeholder = "Medication"
        t7.strkey = "0"
        
        var t0 = SingIn()
        t0.placeholder = "Additional Notes"
        t0.keybord = .default
        t0.strValue = "Add Notes"
        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
        allFields.append(t0)
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter health conditions")
        }else  if String.validateStringValue(str: allFields[3].strValue){
            return(false,"Please enter treated by")
        }
        return(true,"")
    }
}
class AddHealthConditionsVC: ParentViewController, UITextViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    var strHeader = "Add Health Conditions"
    var strPlacholder = "Add Notes"
    @IBOutlet weak var btnAdd: submitButton!
    
    
    var fieldData = AddHealthConditionsList()
    var arryaStatus = ["Ongoing","Past"]
    var arryaTrdy = ["GP", "Hospital", "Privately"]

    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    let dropDown = DropDown()
    let dropDownq = DropDown()
    var healthlist : Healthlist!
    var arryaMedicationlist = [Medicationlist]()
    var strDate = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBottom.frame.size.height = 100 * _widthRatio
        lblHeaderTitle.text = strHeader
        datefrmenter.dateFormat = "dd-MMM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        strDate = datefrmenter.string(from: currentDate as Date)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getmedicationlist()
        if strHeader == "Add Health Conditions"{
            btnAdd.setTitle("Save", for: .normal)
            fieldData.allFields[2].strValue = arryaStatus[0]
            if isWorkingCountryUK(){
                fieldData.allFields[3].strValue = arryaTrdy[0]
            }
        }else{
            btnAdd.setTitle("update", for: .normal)
            fieldData.allFields[0].strValue = healthlist.health_name
            fieldData.allFields[1].strValue = healthlist.health_diagnosed_date
            fieldData.allFields[2].strValue = healthlist.health_status
            fieldData.allFields[3].strValue = healthlist.health_treated_by
            fieldData.allFields[6].strValue = healthlist.health_note
            strDate = fieldData.allFields[1].strValue
            if healthlist.health_note == ""{
                fieldData.allFields[6].strValue = "Add Notes"
            }else{
                strPlacholder = healthlist.health_note
            }
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[1].strValue = datefrmenter.string(from: sender.date)
        strDate = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[1].strValue
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1{
            textField.text! = strDate
            fieldData.allFields[1].strValue = textField.text!
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Notes"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Notes"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        strPlacholder = textView.text!
        fieldData.allFields[6].strValue  = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if fieldData.fieldData.allFields[sender.tag].strkey == "0"{
            fieldData.fieldData.allFields[sender.tag].strkey = "1"
        }else{
            fieldData.fieldData.allFields[sender.tag].strkey = "0"
        }
        tableView.reloadData()
    }
    
    
    
    
    
    func getdrop(){
        dropDown.dataSource = arryaStatus
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdrop1(){
        dropDownq.dataSource = arryaTrdy
        dropDownq.direction = .bottom
        dropDownq.bottomOffset = CGPoint(x: 0, y:(dropDownq.anchorView?.plainView.bounds.height)!)
        dropDownq.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    @IBAction func btnAddMedicalClicekd(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AddMedicationVC")  as! AddMedicationVC
        mapVc1.strHeader = "Add Medication"
        navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if fieldData.validation().0{
            if "Add Health Conditions" == strHeader{
                addhealth()
            }else{
                updatehealth()
            }
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }
    
    
}
extension AddHealthConditionsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3{
            if isWorkingCountryUK() {
                getdrop1()
                dropDownq.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.allFields[indexPath.section].strValue = arryaTrdy[index]
                    self.tableView.reloadData()
                }
                self.view.endEditing(true)
                self.dropDownq.show()
           }
        }
        if indexPath.section == 2{
            getdrop()
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.section].strValue = arryaStatus[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropDown.show()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 90 * _widthRatio
        }else if indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 {
            return 75 * _widthRatio
        }
        if indexPath.section == 4 {
            return 33 * _widthRatio
        }
        if indexPath.section == 5 {
            return 45 * _widthRatio
        }
        return 150 * _widthRatio
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 5{
            return fieldData.fieldData.allFields.count
        }
        return 1
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldData.allFields.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.section == 0{
                cell.viewConstretTop.constant = 5
            }
            return cell
        }
        if  indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            dropDown.anchorView = cell.textEmail
            cell.textEmail.isUserInteractionEnabled = false
            return cell
        }
        if  indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
             if isWorkingCountryUK() {
                cell.textEmail.isUserInteractionEnabled = false
                dropDownq.anchorView = cell.textEmail
                cell.imageCalander.image = fieldData.allFields[indexPath.section].image
             }else{
                cell.imageCalander.image = nil
             }
            return cell
        }
        if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
            cell.lblisAdvatigate.text = fieldData.fieldData.allFields[indexPath.row].placeholder
            cell.btnCheck.tag = indexPath.row
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            
            if fieldData.fieldData.allFields[indexPath.row].strkey == "0"{
                cell.btnCheck.isSelected = false
            }else{
                cell.btnCheck.isSelected = true
            }
            return cell
        }
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.section].placeholder1
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            cell.textEmail.isUserInteractionEnabled = true
            if indexPath.section == 1{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            return cell
        }
        if  indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = "Medication"
            cell.btnViewAll.isHidden = false
            let FormattedText1 = NSMutableAttributedString()
            FormattedText1
                .unlink("Add medication")
            cell.btnViewAll.setAttributedTitle(FormattedText1, for: .normal)
            cell.viewBG.makeCornerRound(redias : 6)
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.section
        // fieldData.allFields[indexPath.section].strValue = strPlacholder
        cell.textAreaNAme.text = fieldData.allFields[indexPath.section].strValue
        return cell
    }
}
extension AddHealthConditionsVC{
    func getmedicationlist(){
        showCentralSpinner()
        let parameters = "{\"data\":{\"memberId\":\"\(_currentUser.patient_id!)\",\"memberType\":\"patient\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "medicationlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaMedicationlist.removeAll()
                                
                                if Response.getIntValue(key: "success") == 1{
                                    self.fieldData.fieldData.allFields.removeAll()
                                    self.tableView.backgroundView = nil
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            let str =  obj as? NSDictionary
                                            arryaMedicationlist.append(Medicationlist(dic: obj as! NSDictionary))
                                            var t7 = SingIn()
                                            t7.placeholder = str!.getStringValue(key: "medication_name")
                                            t7.strValue = str!.getStringValue(key: "medication_id")
                                            
                                            if strHeader == "Add Health Conditions"{
                                                t7.strkey = "0"
                                            }else{
                                                
                                                for obj in healthlist.medication_name{
                                                    if obj == t7.strValue{
                                                        print("\(obj)----------\(t7.strValue)")
                                                        t7.strkey = "1"
                                                        break
                                                    }else{
                                                        print("\(obj)----------\(t7.strValue)")
                                                        t7.strkey = "0"
                                                    }
                                                    
                                                }
                                                
                                            }
                                            fieldData.fieldData.allFields.append(t7)
                                            
                                        }
                                        
                                    }
                                }
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func addhealth(){
        var arryaTime = [String]()
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["healthName"] = fieldData.allFields[0].strValue
        dic["healthTreatedBy"] = fieldData.allFields[3].strValue
        dic["healthStatus"] = fieldData.allFields[2].strValue
        if fieldData.allFields[6].strValue == "Add Notes"{
            fieldData.allFields[6].strValue = ""
        }
        dic["healthNote"] = fieldData.allFields[6].strValue
        if fieldData.allFields[1].strValue == ""{
            dic["healthDiagnosedDay"] = ""
            dic["healthDiagnosedMonth"] = ""
            dic["healthDiagnosedYear"] = ""
        }else{
            let dateString = fieldData.allFields[1].strValue
            let strDay = fieldData.allFields[1].strValue.index(fieldData.allFields[1].strValue.startIndex, offsetBy: 2)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            guard let date = formatter.date(from: dateString) else {
                return
            }
            formatter.dateFormat = "yyyy"
            let year = formatter.string(from: date)
            formatter.dateFormat = "MM"
            let month = formatter.string(from: date)
            
            dic["healthDiagnosedDay"] = String(fieldData.allFields[1].strValue[..<strDay])
            dic["healthDiagnosedMonth"] = month
            dic["healthDiagnosedYear"] = year
        }
     
        for obj in fieldData.fieldData.allFields{
            if obj.strkey == "1"{
                arryaTime.append(obj.strValue)
            }
        }
        dic["healthMedication"] =  arryaTime
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addhealth.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updatehealth(){
        var arryaTime = [String]()
        var dic = [String:Any]()
        dic["healthId"] = healthlist.health_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["healthName"] = fieldData.allFields[0].strValue
        dic["healthTreatedBy"] = fieldData.allFields[3].strValue
        dic["healthStatus"] = fieldData.allFields[2].strValue
        if fieldData.allFields[6].strValue == "Add Notes"{
            fieldData.allFields[6].strValue = ""
        }
        dic["healthNote"] = fieldData.allFields[6].strValue
        if fieldData.allFields[1].strValue == "" || fieldData.allFields[1].strValue == "--"{
            dic["healthDiagnosedDay"] = ""
            dic["healthDiagnosedMonth"] = ""
            dic["healthDiagnosedYear"] = ""
        }else{
            let dateString = fieldData.allFields[1].strValue
            let strDay = fieldData.allFields[1].strValue.index(fieldData.allFields[1].strValue.startIndex, offsetBy: 2)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            guard let date = formatter.date(from: dateString) else {
                return
            }
            formatter.dateFormat = "yyyy"
            let year = formatter.string(from: date)
            formatter.dateFormat = "MM"
            let month = formatter.string(from: date)
            
            dic["healthDiagnosedDay"] = String(fieldData.allFields[1].strValue[..<strDay])
            dic["healthDiagnosedMonth"] = month
            dic["healthDiagnosedYear"] = year
        }
       
        for obj in fieldData.fieldData.allFields{
            if obj.strkey == "1"{
                arryaTime.append(obj.strValue)
            }
        }
        dic["healthMedication"] =  arryaTime
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatehealth.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
