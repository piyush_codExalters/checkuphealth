//
//  AllergiesAdverseReactionVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit

class AllergiesAdverseReactionVC: ParentViewController {
    
    var indextSet = -1
    var arryaAllergylist = [Allergylist]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset.bottom = 150 * _widthRatio
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getallergylist(loader: true)
        
    }
   
    @IBAction func btnAddNewClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AllergiesAdverseAddVC")  as! AllergiesAdverseAddVC
        mapVc1.strHeader = "Add Allergies/Adverse Reaction"
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    @IBAction func btnEditClicked(_ sender: UIButton) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AllergiesAdverseAddVC")  as! AllergiesAdverseAddVC
        mapVc1.strHeader = "Edit Allergies/Adverse Reaction"
        mapVc1.allergylist = arryaAllergylist[sender.tag]
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    @IBAction func btnDeletedClicked(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            deleteuserphr(request_id : arryaAllergylist[sender.tag].allergy_id)
        }
    }
}
extension AllergiesAdverseReactionVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            if indextSet == indexPath.section{
                indextSet = -1
            }else{
                indextSet = indexPath.section
            }
            tableView.reloadData()

        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaAllergylist.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 7 || indexPath.row == 8{
            return 0
        }
        
        if indexPath.row == 0{
            return 75 * _widthRatio
        }else{
            return UITableView.automaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if indextSet == section{
            return 11
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! HealthConditionsCell
            cell.btnEdti.tag = indexPath.section
            cell.btnDeleted.tag = indexPath.section
            cell.lblDate.text = arryaAllergylist[indexPath.section].allergy_diagnosed_date
            cell.lblTitle.text = arryaAllergylist[indexPath.section].allergy_name
            if indextSet == indexPath.section{
                cell.viewConstretBottom.constant = -5
                cell.imageDrop.image = #imageLiteral(resourceName: "ic_navigation_aro_up")
            }else{
                cell.imageDrop.image = #imageLiteral(resourceName: "ic_dropdown")
                cell.viewConstretBottom.constant = 5
            }
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Name:"
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaAllergylist[indexPath.section].allergy_name
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Diagnosed On:"
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaAllergylist[indexPath.section].allergy_diagnosed_date
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Reaction:"
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaAllergylist[indexPath.section].allergy_reaction
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Triggered By:"
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaAllergylist[indexPath.section].allergy_trigger_by
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
            cell.lblTitles.text = "Additional Notes:"
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = -5
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaAllergylist[indexPath.section].allergy_note
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = 5
            return cell
        }
        
    }
    
    
    
    
}
extension AllergiesAdverseReactionVC{
    @objc func sortRefresh(){
        getallergylist(loader: false)
    }
    func getallergylist(loader : Bool){
        if loader{
            showCentralSpinner()
        }
        let parameters = "{\"data\":{\"memberId\":\"\(_currentUser.patient_id!)\",\"memberType\":\"patient\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "allergylist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        self.arryaAllergylist.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaAllergylist.append(Allergylist(dic: obj as! NSDictionary))
                                }
                            }
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func deleteuserphr(request_id : String){
        let parameters = "{\"data\":{\"id\":\"\(request_id)\",\"type\":\"allergies\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "deleteuserphr.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getallergylist(loader: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
