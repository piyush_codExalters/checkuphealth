//
//  VaccinationsAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
class VaccinationsList: NSObject {
    
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Vaccination For *"
        t3.keybord = .default
        t3.placeholder1 = "e.g. MMR"

        
        var t8 = SingIn()
        t8.placeholder = "Taken On *"
        t8.keybord = .default
        t8.image = #imageLiteral(resourceName: "ic_calender")
        t8.placeholder1 = "Date"


        
        var t9 = SingIn()
        t9.placeholder = "Vaccine Name *"
        t9.keybord = .default
        t9.placeholder1 = "e.g. IPV, etc"

        
        
        var t5 = SingIn()
        t5.placeholder = "Vaccine Details *"
        t5.keybord = .default
        t5.placeholder1 = "vaccine Details"

        
        var t6 = SingIn()
        t6.placeholder = "Lot Number *"
        t6.keybord = .default
        t6.placeholder1 = "e.g. A80123"


        
        var t0 = SingIn()
        t0.placeholder = "Additional Notes"
        t0.keybord = .default
        t0.strValue = "Add Notes"
        
        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t0)
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter vaccination for")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please select date")
        }else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please enter vaccine name")
        }else if String.validateStringValue(str: allFields[3].strValue){
            return(false,"Please enter vaccine details")
        }else if String.validateStringValue(str: allFields[4].strValue){
            return(false,"Please enter lot number")
        }
        return(true,"")
    }
}
class VaccinationsAddVC: ParentViewController, UITextViewDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!

    var fieldData = VaccinationsList()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var vaccinationlist : Vaccinationlist!
    var strHeader = "Add Vaccination"
    var strPlacholder = "Add Notes"
    var strDate = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        datefrmenter.dateFormat = "dd-MM-yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        strDate = datefrmenter.string(from: currentDate as Date)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        if "Add Vaccination" == strHeader{
            btnAdd.setTitle("Save", for: .normal)
        }else{
            btnAdd.setTitle("Update", for: .normal)
            fieldData.allFields[0].strValue = vaccinationlist.vaccination_for
            fieldData.allFields[1].strValue = vaccinationlist.vaccination_taken_on
            fieldData.allFields[2].strValue = vaccinationlist.vaccination_name
            fieldData.allFields[3].strValue = vaccinationlist.vaccination_details
            fieldData.allFields[4].strValue = vaccinationlist.vaccination_lot_number
            fieldData.allFields[5].strValue = vaccinationlist.vaccination_note
            strDate = vaccinationlist.vaccination_taken_on
            if vaccinationlist.vaccination_note == ""{
                fieldData.allFields[5].strValue = "Add Notes"
            }else{
                strPlacholder = vaccinationlist.vaccination_note
            }
            tableView.reloadData()
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[1].strValue = datefrmenter.string(from: sender.date)
        strDate = fieldData.allFields[1].strValue
        let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[1].strValue
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1{
            textField.text! = strDate
            fieldData.allFields[1].strValue = textField.text!
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Notes"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Notes"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        strPlacholder = textView.text!
        fieldData.allFields[5].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if fieldData.validation().0{
            if "Add Vaccination" == strHeader{
                addvaccination()
            }else{
                updatevaccination()
            }
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
        
    }

}
extension VaccinationsAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 {
            return 75 * _widthRatio
        }
        
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0 || indexPath.row == 2 ||  indexPath.row == 3 || indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1

            if indexPath.row == 1{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        return cell
    }
}
extension VaccinationsAddVC{
    func addvaccination(){
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["vaccinationName"] = fieldData.allFields[2].strValue
        dic["vaccinationFor"] = fieldData.allFields[0].strValue
        dic["vaccinationDetails"] = fieldData.allFields[3].strValue
        dic["vaccinationTakenOn"] = fieldData.allFields[1].strValue
        dic["vaccinationLotNumber"] = fieldData.allFields[4].strValue
        if fieldData.allFields[5].strValue == "Add Notes"{
            fieldData.allFields[5].strValue = ""
        }
        dic["vaccinationNote"] = fieldData.allFields[5].strValue
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addvaccination.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updatevaccination(){
        var dic = [String:Any]()
        dic["vaccinationId"] = vaccinationlist.vaccination_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        dic["vaccinationName"] = fieldData.allFields[2].strValue
        dic["vaccinationFor"] = fieldData.allFields[0].strValue
        dic["vaccinationDetails"] = fieldData.allFields[3].strValue
        dic["vaccinationTakenOn"] = fieldData.allFields[1].strValue
        dic["vaccinationLotNumber"] = fieldData.allFields[4].strValue
        if fieldData.allFields[5].strValue == "Add Notes"{
            fieldData.allFields[5].strValue = ""
        }
        dic["vaccinationNote"] = fieldData.allFields[5].strValue
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatevaccination.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
