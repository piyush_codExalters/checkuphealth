//
//  LifestyleHabitsAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
import DropDown
class alcoholList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Alcohol *"
        t0.keybord = .default
        
        var t1 = SingIn()
        t1.placeholder = "Consumption *"
        t1.keybord = .numberPad
        
        allFields.append(t0)
        allFields.append(t1)
    }
    
}
class smokerList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Smoking *"
        t0.keybord = .default
        
        var t1 = SingIn()
        t1.placeholder = "Number per day *"
        t1.keybord = .numberPad
        t1.placeholder1 = "2"

        
        allFields.append(t0)
        allFields.append(t1)
    }
    
}
class alternativeList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Alternative Therapy *"
        t0.keybord = .default
        t0.placeholder1 = "Alternative Therapy"

        
        allFields.append(t0)
        
    }
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter alternative therapy")
        }
        return(true,"")
    }
    
}
class ExerciseList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Exercise *"
        t0.keybord = .default
        
        
        allFields.append(t0)
        
    }
    
}
class ReligionList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Religion *"
        t0.keybord = .default
        t0.placeholder1 = "Religion"

        
        var t1 = SingIn()
        t1.placeholder = "Church *"
        t1.keybord = .default
        t1.placeholder1 = "Church"

        
        var t2 = SingIn()
        t2.placeholder = "Minister/Pastor *"
        t2.keybord = .default
        t2.placeholder1 = "Minister/Pastor"

        
        var t3 = SingIn()
        t3.placeholder = "Other"
        t3.keybord = .default
        t3.placeholder1 = "Other"

        
        allFields.append(t0)
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
        
    }
    
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter religion")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter church")
        }else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please enter minister/pastor")
        }
        return(true,"")
    }
    
}
class SanitationList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Water Supply *"
        t0.keybord = .default
        t0.placeholder1 = "e.g. Bottled, Tap Water, Borehole"

        
        var t1 = SingIn()
        t1.placeholder = "Toilets *"
        t1.keybord = .default
        t1.placeholder1 = "e.g Blair, Flush, Other"

        
        
        allFields.append(t0)
        allFields.append(t1)
        
    }
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter water supply")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter toilets")
        }
        return(true,"")
    }
    
}
class HabitsList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = "Habit *"
        t0.keybord = .default
        t0.placeholder1 = "Habit"

        
        var t1 = SingIn()
        t1.placeholder = "Frequency *"
        t1.keybord = .default
        t1.placeholder1 = "Frequency"

        
        var t2 = SingIn()
        t2.placeholder = "Since? *"
        t2.keybord = .default
        t2.placeholder1 = "Since"

        
        allFields.append(t0)
        allFields.append(t1)
        allFields.append(t2)
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter habit")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter frequency")
        }else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please enter since")
        }
        return(true,"")
    }
    
}

class LifestyleHabitsList: NSObject {
    var allFields: [SingIn] = []
    var fieldDataHabits = HabitsList()
    var fieldDataSanitation = SanitationList()
    var fieldDataReligion = ReligionList()
    var fieldDataExercise = ExerciseList()
    var fieldDataAlternative = alternativeList()
    var fieldDataalcohol = alcoholList()
    var fieldDatasmoker = smokerList()
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t0 = SingIn()
        t0.placeholder = ""
        t0.keybord = .default
        allFields.append(t0)
    }
    
}
class LifestyleHabitsAddVC: ParentViewController {
    
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!
    var lifestylelist : Lifestylelist!
    var strHeader = "Add Lifestyle Habits"
    var fieldData = LifestyleHabitsList()
    
    var aryaHobite = ["Habits","Basic Sanitation","Religion","Exercise","Alternative Therapies","Alcohol","Smoking"]
    var arySmoker = ["Never Smoked","Ex smoker","Current smoker"]
    var aryAlcohol = ["Doesn’t drink alcohol","Ex-drinker of alcohol","Current drinker"]
    var aryExercise = ["No","Light","Moderate","Heavy"]
    var aryDrink = ["Drinks 1-2 times per week","Drinks 2-6 times per week","Drinks Daily"]
    
    var numberOfHobites = 0
    var numberOfSmoker = 0
    var numberOfAlochol = 0
    let dropDown = DropDown()
    let dropDown1 = DropDown()
    let dropDown2 = DropDown()
    let dropDown3 = DropDown()
    let dropDown4 = DropDown()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if isWorkingCountryUK(){
            let Hobite = ["Religion","Exercise","Alternative Therapies","Alcohol","Smoking"]
            aryaHobite = Hobite
            numberOfHobites = 2
           
        }
        if "Add Lifestyle Habits" == strHeader{
            btnAdd.setTitle("Save", for: .normal)
            fieldData.allFields[0].strValue = aryaHobite[0]
            fieldData.fieldDataExercise.allFields[0].strValue = aryExercise[0]
            fieldData.fieldDatasmoker.allFields[0].strValue = arySmoker[0]
            fieldData.fieldDataalcohol.allFields[0].strValue = aryAlcohol[0]
            fieldData.fieldDataalcohol.allFields[1].strValue = aryDrink[0]
        }else{
            btnAdd.setTitle("Update", for: .normal)
            for obj in aryaHobite.enumerated(){
                if lifestylelist.type == obj.element{
                    if isWorkingCountryUK(){
                        numberOfHobites = obj.offset + 2
                    }else{
                        numberOfHobites = obj.offset
                    }
                }
            }
            for obj in aryAlcohol.enumerated(){
                if lifestylelist.alcohol_type == obj.element{
                    numberOfAlochol = obj.offset
                }
            }
            for obj in arySmoker.enumerated(){
                if lifestylelist.smoking_type == obj.element{
                    numberOfSmoker = obj.offset
                }
            }
            fieldData.allFields[0].strValue = lifestylelist.type
            if numberOfHobites == 2{
                fieldData.fieldDataReligion.allFields[0].strValue = lifestylelist.religion_name
                fieldData.fieldDataReligion.allFields[1].strValue = lifestylelist.religion_church
                fieldData.fieldDataReligion.allFields[2].strValue = lifestylelist.religion_pastor
                fieldData.fieldDataReligion.allFields[3].strValue = lifestylelist.religion_other
            }
            if numberOfHobites == 3{
                fieldData.fieldDataExercise.allFields[0].strValue = lifestylelist.exercise
            }
            if numberOfHobites == 4{
                fieldData.fieldDataAlternative.allFields[0].strValue = lifestylelist.alternative_therapy
            }
            if numberOfHobites == 5{
                fieldData.fieldDataalcohol.allFields[0].strValue = lifestylelist.alcohol_type
                fieldData.fieldDataalcohol.allFields[1].strValue = lifestylelist.alcohol_drinkertype
            }
            if numberOfHobites == 6{
                fieldData.fieldDatasmoker.allFields[0].strValue = lifestylelist.smoking_type
                if lifestylelist.smoking_smokernumber == "0"{
                    lifestylelist.smoking_smokernumber = ""
                }
                fieldData.fieldDatasmoker.allFields[1].strValue = lifestylelist.smoking_smokernumber
            }
            if numberOfHobites == 0{
                fieldData.fieldDataHabits.allFields[0].strValue = lifestylelist.habit
                fieldData.fieldDataHabits.allFields[1].strValue = lifestylelist.habit_feq
                fieldData.fieldDataHabits.allFields[2].strValue = lifestylelist.habit_since
            }
            if numberOfHobites == 1{
                fieldData.fieldDataSanitation.allFields[0].strValue = lifestylelist.basic_water
                fieldData.fieldDataSanitation.allFields[1].strValue = lifestylelist.basic_toilet
            }
        }
        lblHeaderTitle.text = strHeader
        viewBottom.frame.size.height = 100 * _widthRatio
        
        
    }
    
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        if numberOfHobites == 2{
            fieldData.fieldDataReligion.allFields[sender.tag].strValue = sender.text!
        }
        if numberOfHobites == 3{
            fieldData.fieldDataExercise.allFields[sender.tag].strValue = sender.text!
        }
        if numberOfHobites == 4{
            fieldData.fieldDataAlternative.allFields[sender.tag].strValue = sender.text!
        }
        if numberOfHobites == 5{
            fieldData.fieldDataalcohol.allFields[sender.tag].strValue = sender.text!
        }
        if numberOfHobites == 6{
            fieldData.fieldDatasmoker.allFields[sender.tag].strValue = sender.text!
        }
        if numberOfHobites == 0{
            fieldData.fieldDataHabits.allFields[sender.tag].strValue = sender.text!
        }
        if numberOfHobites == 1{
            fieldData.fieldDataSanitation.allFields[sender.tag].strValue = sender.text!
        }
    }
    
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if "Add Lifestyle Habits" == strHeader{
            if numberOfHobites == 2{
                if fieldData.fieldDataReligion.validation().0{
                    addlifestyle()
                }else{
                    showFailMessage(message : fieldData.fieldDataReligion.validation().1)
                }
            }
            if numberOfHobites == 3{
                addlifestyle()
            }
            if numberOfHobites == 4{
                if fieldData.fieldDataAlternative.validation().0{
                    addlifestyle()
                }else{
                    showFailMessage(message : fieldData.fieldDataAlternative.validation().1)
                }
            }
            if numberOfHobites == 5{
                addlifestyle()
            }
            if numberOfHobites == 6{
                if numberOfSmoker == 2{
                    if String.validateStringValue(str: fieldData.fieldDatasmoker.allFields[1].strValue){
                        return showFailMessage(message: "Please enter number per day")
                    }else{
                        addlifestyle()
                    }
                }else{
                    addlifestyle()
                }
            }
            if numberOfHobites == 0{
                if fieldData.fieldDataHabits.validation().0{
                    addlifestyle()
                }else{
                    showFailMessage(message : fieldData.fieldDataHabits.validation().1)
                }
            }
            if numberOfHobites == 1{
                if fieldData.fieldDataSanitation.validation().0{
                    addlifestyle()
                }else{
                    showFailMessage(message : fieldData.fieldDataSanitation.validation().1)
                }
            }
            
        }else{
            updatelifestyle()
        }
    }
    
    
    
    
    
    func getdropHobite(){
        if isWorkingCountryUK(){
            let Hobite = ["Religion","Exercise","Alternative Therapies","Alcohol","Smoking"]
            aryaHobite = Hobite
        }
        dropDown.dataSource = aryaHobite
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropSmoker(){
        dropDown1.dataSource = arySmoker
        dropDown1.direction = .bottom
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
        dropDown1.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropAlcohol(){
        dropDown2.dataSource = aryAlcohol
        dropDown2.direction = .bottom
        dropDown2.bottomOffset = CGPoint(x: 0, y:(dropDown2.anchorView?.plainView.bounds.height)!)
        dropDown2.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropExercise(){
        dropDown3.dataSource = aryExercise
        dropDown3.direction = .bottom
        dropDown3.bottomOffset = CGPoint(x: 0, y:(dropDown3.anchorView?.plainView.bounds.height)!)
        dropDown3.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getdropDrink(){
        dropDown4.dataSource = aryDrink
        dropDown4.direction = .bottom
        dropDown4.bottomOffset = CGPoint(x: 0, y:(dropDown4.anchorView?.plainView.bounds.height)!)
        dropDown4.width = 300 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
}
extension LifestyleHabitsAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldData.allFields.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if indexPath.row == 0{
                getdropHobite()
                dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                    if isWorkingCountryUK(){
                        numberOfHobites = index + 2
                    }else{
                        numberOfHobites = index
                    }
                    fieldData.allFields[indexPath.section].strValue = aryaHobite[index]
                    self.tableView.reloadData()
                }
                self.view.endEditing(true)
                self.dropDown.show()
                return
            }
            
        }
        
        if numberOfHobites == 3{
            getdropExercise()
            dropDown3.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.fieldDataExercise.allFields[indexPath.row].strValue = aryExercise[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropDown3.show()
        }
        if numberOfHobites == 5{
            if indexPath.row == 0{
                getdropAlcohol()
                dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
                    numberOfAlochol = index
                    fieldData.fieldDataalcohol.allFields[indexPath.row].strValue = aryAlcohol[index]
                    self.tableView.reloadData()
                }
                self.view.endEditing(true)
                self.dropDown2.show()
            }
            if indexPath.row == 1{
                getdropDrink()
                dropDown4.selectionAction = { [unowned self] (index: Int, item: String) in
                    fieldData.fieldDataalcohol.allFields[indexPath.row].strValue = aryDrink[index]
                    self.tableView.reloadData()
                }
                self.view.endEditing(true)
                self.dropDown4.show()
            }
            
        }
        if numberOfHobites == 6{
            if indexPath.row == 0{
                getdropSmoker()
                dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
                    numberOfSmoker = index
                    fieldData.fieldDatasmoker.allFields[indexPath.row].strValue = arySmoker[index]
                    self.tableView.reloadData()
                }
                self.view.endEditing(true)
                self.dropDown1.show()
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 80 * _widthRatio
        }
        if numberOfHobites == 0{
            if indexPath.row == 2{
                return 90 * _widthRatio
            }
            return 75 * _widthRatio
        }
        if numberOfHobites == 1{
            if indexPath.row == 1{
                return 90 * _widthRatio
            }
            return 75 * _widthRatio
        }
        
        if numberOfHobites == 2{
            if indexPath.row == 3{
                return 90 * _widthRatio
            }
            return 75 * _widthRatio
        }
        if numberOfHobites == 3{
            return 90 * _widthRatio
        }
        if numberOfHobites == 4{
            return 90 * _widthRatio
        }
        if numberOfHobites == 5{
            return 90 * _widthRatio
        }
        return 90 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        if numberOfHobites == 0{
            return fieldData.fieldDataHabits.allFields.count
        }
        if numberOfHobites == 1{
            return fieldData.fieldDataSanitation.allFields.count
        }
        
        if numberOfHobites == 2{
            return fieldData.fieldDataReligion.allFields.count
        }
        if numberOfHobites == 3{
            return fieldData.fieldDataExercise.allFields.count
        }
        if numberOfHobites == 4{
            return fieldData.fieldDataAlternative.allFields.count
        }
        if numberOfHobites == 5{
            if numberOfAlochol == 2{
                return fieldData.fieldDataalcohol.allFields.count
            }
            return 1
        }
        if numberOfSmoker == 2{
            return fieldData.fieldDatasmoker.allFields.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = false
            cell.viewConstretTop.constant = 5
            
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = #imageLiteral(resourceName: "ic_dropdown")
            dropDown.anchorView = cell.textEmail
            return cell
        }
        if numberOfHobites == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDataHabits.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDataHabits.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.fieldDataHabits.allFields[indexPath.row].placeholder1

            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.fieldDataHabits.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 2{
                cell.viewConstretBottom.constant = 5
            }
            return cell
            
        }
        if numberOfHobites == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDataSanitation.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDataSanitation.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.fieldDataSanitation.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.fieldDataSanitation.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 1{
                cell.viewConstretBottom.constant = 5
            }
            return cell
            
        }
        
        if numberOfHobites == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDataReligion.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDataReligion.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.textEmail.placeholder = fieldData.fieldDataReligion.allFields[indexPath.row].placeholder1

            cell.lblName.text = fieldData.fieldDataReligion.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 3{
                cell.viewConstretBottom.constant = 5
            }
            return cell
            
        }
        if numberOfHobites == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDataExercise.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDataExercise.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.fieldDataExercise.allFields[indexPath.row].placeholder
            cell.textEmail.isUserInteractionEnabled = false
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = 5
            cell.imageCalander.image = #imageLiteral(resourceName: "ic_dropdown")
            dropDown3.anchorView = cell.textEmail
            return cell
            
        }
        if numberOfHobites == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDataAlternative.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDataAlternative.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.fieldDataAlternative.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.fieldDataAlternative.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretBottom.constant = 5
            }
            return cell
            
        }
        if numberOfHobites == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDataalcohol.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDataalcohol.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.fieldDataalcohol.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.fieldDataalcohol.allFields[indexPath.row].placeholder1

            cell.textEmail.isUserInteractionEnabled = false
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = 5
            if numberOfAlochol == 2{
                cell.viewConstretBottom.constant = -5
            }
            if indexPath.row == 1{
                cell.viewConstretBottom.constant = 5
                dropDown4.anchorView = cell.textEmail
                
            }else{
                dropDown2.anchorView = cell.textEmail
                
            }
            cell.imageCalander.image = #imageLiteral(resourceName: "ic_dropdown")
            return cell
            
        }
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldDatasmoker.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldDatasmoker.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.fieldDatasmoker.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.fieldDatasmoker.allFields[indexPath.row].placeholder1

            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 1{
                cell.viewConstretBottom.constant = 5
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.fieldDatasmoker.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.fieldDatasmoker.allFields[indexPath.row].strValue
        cell.lblName.text = fieldData.fieldDatasmoker.allFields[indexPath.row].placeholder
        cell.textEmail.placeholder = fieldData.fieldDatasmoker.allFields[indexPath.row].placeholder1

        cell.textEmail.isUserInteractionEnabled = false
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        if numberOfSmoker == 2{
            cell.viewConstretBottom.constant = -5
        }
        cell.imageCalander.image = #imageLiteral(resourceName: "ic_dropdown")
        dropDown1.anchorView = cell.textEmail
        return cell
    }
}
extension LifestyleHabitsAddVC{
    func addlifestyle(){
        var dic = [String:Any]()
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        if numberOfHobites == 2{
            
            dic["type"] = "Religion"
            dic["religion_name"] = fieldData.fieldDataReligion.allFields[0].strValue
            dic["religion_church"] = fieldData.fieldDataReligion.allFields[1].strValue
            dic["religion_pastor"] = fieldData.fieldDataReligion.allFields[2].strValue
            dic["religion_other"] = fieldData.fieldDataReligion.allFields[3].strValue
        }
        if numberOfHobites == 3{
            dic["type"] = "Exercise"
            dic["exercise"] = fieldData.fieldDataExercise.allFields[0].strValue
        }
        if numberOfHobites == 4{
            dic["type"] = "Alternative Therapies"
            dic["alternative_therapy"] = fieldData.fieldDataAlternative.allFields[0].strValue
        }
        if numberOfHobites == 5{
            dic["type"] = "Alcohol"
            dic["alcohol_type"] = fieldData.fieldDataalcohol.allFields[0].strValue
            if numberOfAlochol == 2{
                if String.validateStringValue(str: fieldData.fieldDataalcohol.allFields[1].strValue){
                    return showFailMessage(message: "Please select consumption")
                }
                dic["alcohol_drinkertype"] = fieldData.fieldDataalcohol.allFields[1].strValue

            }else{
                fieldData.fieldDataalcohol.allFields[1].strValue = ""
                dic["alcohol_drinkertype"] = fieldData.fieldDataalcohol.allFields[1].strValue

            }
        }
        if numberOfHobites == 6{
            dic["type"] = "Smoking"
            dic["smoking_type"] = fieldData.fieldDatasmoker.allFields[0].strValue
           
            if numberOfSmoker == 2{
                if String.validateStringValue(str: fieldData.fieldDatasmoker.allFields[1].strValue){
                    return showFailMessage(message: "Please enter number par day")
                }
                dic["smoking_smokernumber"] = fieldData.fieldDatasmoker.allFields[1].strValue

            }else{
                fieldData.fieldDatasmoker.allFields[1].strValue = ""
                dic["smoking_smokernumber"] = fieldData.fieldDatasmoker.allFields[1].strValue

            }
            
           
        }
        
        if numberOfHobites == 0{
            dic["type"] = "Habits"
            dic["habit"] = fieldData.fieldDataHabits.allFields[0].strValue
            dic["habit_feq"] = fieldData.fieldDataHabits.allFields[1].strValue
            dic["habit_since"] = fieldData.fieldDataHabits.allFields[2].strValue
        }
        
        if numberOfHobites == 1{
            dic["type"] = "Basic Sanitation"
            dic["basic_water"] = fieldData.fieldDataSanitation.allFields[0].strValue
            dic["basic_toilet"] = fieldData.fieldDataSanitation.allFields[1].strValue
        }
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addlifestyle.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func updatelifestyle(){
        var dic = [String:Any]()
        dic["lifestyleId"] = lifestylelist.lifestyle_id!
        dic["memberId"] = _currentUser.patient_id!
        dic["memberType"] = "patient"
        if numberOfHobites == 2{
            dic["type"] = "Religion"
            dic["religion_name"] = fieldData.fieldDataReligion.allFields[0].strValue
            dic["religion_church"] = fieldData.fieldDataReligion.allFields[1].strValue
            dic["religion_pastor"] = fieldData.fieldDataReligion.allFields[2].strValue
            dic["religion_other"] = fieldData.fieldDataReligion.allFields[3].strValue
        }
        if numberOfHobites == 3{
            dic["type"] = "Exercise"
            dic["exercise"] = fieldData.fieldDataExercise.allFields[0].strValue
        }
        if numberOfHobites == 4{
            dic["type"] = "Alternative Therapies"
            dic["alternative_therapy"] = fieldData.fieldDataAlternative.allFields[0].strValue
        }
        if numberOfHobites == 5{
            dic["type"] = "Alcohol"
            dic["alcohol_type"] = fieldData.fieldDataalcohol.allFields[0].strValue
            if numberOfAlochol == 2{
                if String.validateStringValue(str: fieldData.fieldDataalcohol.allFields[1].strValue){
                    return showFailMessage(message: "Please select consumption")
                }
                dic["alcohol_drinkertype"] = fieldData.fieldDataalcohol.allFields[1].strValue

            }else{
                fieldData.fieldDataalcohol.allFields[1].strValue = ""
                dic["alcohol_drinkertype"] = fieldData.fieldDataalcohol.allFields[1].strValue

            }
        }
        if numberOfHobites == 6{
            dic["type"] = "Smoking"
            dic["smoking_type"] = fieldData.fieldDatasmoker.allFields[0].strValue
            if numberOfSmoker == 2{
                if String.validateStringValue(str: fieldData.fieldDatasmoker.allFields[1].strValue){
                    return showFailMessage(message: "Please enter number par day")
                }
                dic["smoking_smokernumber"] = fieldData.fieldDatasmoker.allFields[1].strValue

            }else{
                fieldData.fieldDatasmoker.allFields[1].strValue = ""
                dic["smoking_smokernumber"] = fieldData.fieldDatasmoker.allFields[1].strValue

            }
        }
        if numberOfHobites == 0{
            dic["type"] = "Habits"
            dic["habit"] = fieldData.fieldDataHabits.allFields[0].strValue
            dic["habit_feq"] = fieldData.fieldDataHabits.allFields[1].strValue
            dic["habit_since"] = fieldData.fieldDataHabits.allFields[2].strValue
        }
        
        if numberOfHobites == 1{
            dic["type"] = "Basic Sanitation"
            dic["basic_water"] = fieldData.fieldDataSanitation.allFields[0].strValue
            dic["basic_toilet"] = fieldData.fieldDataSanitation.allFields[1].strValue
        }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatelifestyle.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
