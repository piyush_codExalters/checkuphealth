//
//  FamilyHistoryVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit

class FamilyHistoryVC: ParentViewController {
    
    var indextSet = -1
    var arryaFamilylist = [Familylist]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset.bottom = 150 * _widthRatio
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getfamilylist(loader: true)
        
    }
    
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "FamilyHistoryAddVC")  as! FamilyHistoryAddVC
        mapVc1.strHeader = "Add Family History"
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    @IBAction func btnEditClicked(_ sender: UIButton) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "FamilyHistoryAddVC")  as! FamilyHistoryAddVC
        mapVc1.strHeader = "Edit Family History"
        mapVc1.familylist = arryaFamilylist[sender.tag]

        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    
    @IBAction func btnDeletedClicked(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            deleteuserphr(request_id : arryaFamilylist[sender.tag].family_id)
        }
    }
}
extension FamilyHistoryVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaFamilylist.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 60 * _widthRatio
        }else{
            return UITableView.automaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! HealthConditionsCell
            cell.btnEdti.tag = indexPath.section
            cell.btnDeleted.tag = indexPath.section
            cell.lblName.text = arryaFamilylist[indexPath.section].family_type_title
            cell.viewConstretBottom.constant = -5
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
            cell.lblDesc.text = arryaFamilylist[indexPath.section].family_desc
            cell.constraintTop.constant = -5
            cell.constraintBottom.constant = 5
            return cell
        }
        
    }
    
    
    
    
}
extension FamilyHistoryVC{
    @objc func sortRefresh(){
        getfamilylist(loader: false)
    }
    func getfamilylist(loader: Bool){
        if loader{
            showCentralSpinner()
        }
        let parameters = "{\"data\":{\"memberId\":\"\(_currentUser.patient_id!)\",\"memberType\":\"patient\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "familylist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                       self.arryaFamilylist.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaFamilylist.append(Familylist(dic: obj as! NSDictionary))
                                }
                            }
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func deleteuserphr(request_id : String){
        let parameters = "{\"data\":{\"id\":\"\(request_id)\",\"type\":\"familyhistory\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "deleteuserphr.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getfamilylist(loader: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
