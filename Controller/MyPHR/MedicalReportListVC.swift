//
//  MedicalReportListVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit
class MedicalReportListCell: ConstrainedTableViewCell {
    @IBOutlet weak var lblTitleName: LabelMedium!
    @IBOutlet weak var lblDate: LabelMedium!
    
    
    @IBOutlet weak var hightCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionPhoto: UICollectionView!
    var sectioNumber = 0
    weak var medicalRepor : MedicalReportListVC!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class MedicalReportListVC: ParentViewController {
    
    var arryaReportlistDate = [ReportlistDate]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset.bottom = 150 * _widthRatio
        
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        medical_reportlist(loader: true)
    }
    
    
    
    
    
    @IBAction func btnAddNow(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AddMedicalReportVC")  as! AddMedicalReportVC
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    
}
extension MedicalReportListVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaReportlistDate.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            
            return 35 * _widthRatio
            
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! MedicalReportListCell
            cell.lblDate.text = ""
            cell.lblTitleName.text = arryaReportlistDate[indexPath.section].medical_report_upload_by
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Photo") as! MedicalReportListCell
        cell.collectionPhoto.reloadData()
        cell.medicalRepor = self
        cell.sectioNumber = indexPath.section
        cell.frame = tableView.bounds
        cell.layoutIfNeeded()
        cell.hightCollection.constant = cell.collectionPhoto.collectionViewLayout.collectionViewContentSize.height
        cell.collectionPhoto.reloadData()
        return cell
    }
    
    
    
    
}
extension MedicalReportListCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension() == "jpg" || medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension() == "png"{
            
            let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "OpenImageVC")  as! OpenImageVC
            mapVc1.str = medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment
            mapVc1.name = medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_name
            medicalRepor.navigationController?.pushViewController(mapVc1, animated: true)
        }else if medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment == ""{
            medicalRepor.showFailMessage(message: "No attachment found")
        }else{
            if let url = URL(string: "\(medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment!)") {
                UIApplication.shared.open(url)
            }
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return medicalRepor.arryaReportlistDate[sectioNumber].arryaReport.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Menu", for: indexPath) as! MyPhpCollCell
        cell.imageMenuIcon.kf.setImage(with: URL(string:medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment), placeholder: UIImage(named: "ic_placeholder_logo1"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        if "pdf" == medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension(){
            cell.imageMenuIcon.image = #imageLiteral(resourceName: "pdf_icon")
        }else if "txt" == medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension(){
            cell.imageMenuIcon.image = #imageLiteral(resourceName: "txt_icon")
        }else if "doc" == medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension() || "docx" == medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension() ||  "document" == medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension(){
            cell.imageMenuIcon.image = #imageLiteral(resourceName: "docx_icon")
        }else if "excel" == medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_attachment.getPathExtension(){
            cell.imageMenuIcon.image = #imageLiteral(resourceName: "excel_icon")
        }
        cell.lblTitleName.text = medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_name
        cell.lblDate.text = medicalRepor.arryaReportlistDate[sectioNumber].arryaReport[indexPath.row].medical_report_date
        cell.imageMenuIcon.makeCornerRoundxs(redias : 5)
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 413.5 * _widthRatio / 2 , height: 238 * _widthRatio)
        
    }
}
extension MedicalReportListVC{
    @objc func sortRefresh(){
        medical_reportlist(loader: false)
    }
    func medical_reportlist(loader: Bool){
        if loader{
            showCentralSpinner()
        }
        let parameters = "{\"data\":{\"memberId\":\"\(_currentUser.patient_id!)\",\"memberType\":\"patient\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "medical_reportlist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()
            
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.arryaReportlistDate.removeAll()
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        
                                        for obj in data{
                                            arryaReportlistDate.append(ReportlistDate(dic: obj as! NSDictionary, arryaDic: data))
                                        }
                                    }
                                    
                                    let uniqueMessages = arryaReportlistDate.unique{$0.keyz ?? ""}
                                    arryaReportlistDate = uniqueMessages
                                    
                                    for obj in arryaReportlistDate.enumerated() {
                                        
                                        
                                        arryaReportlistDate[obj.offset].arryaReport.sorted(by: { $0.rDate.compare($1.rDate) == .orderedAscending }) // sorting order date array
                                        
                                        
                                        
                                    }
                                    
                                    
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                strdate = ""
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
class OpenImageVC: ParentViewController{
    
    
    @IBOutlet weak var lblTitleNmae: UILabel!
    @IBOutlet weak var imageOpen: UIImageView!
    var str = ""
    var name = ""
    var imageOpenRepoert : UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: str)!
        lblTitleNmae.text = name
        DispatchQueue.global().async {
            // Fetch Image Data
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    // Create Image and Update Image View
                    self.imageOpen.image = UIImage(data: data)
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.isUserInteractionEnabled = true
    }
    
    @IBAction func btnDownloadClikced(_ sender: Any) {
        showCentralSpinner()
        DispatchQueue.global(qos: .background).async { [self] in
            do
            {
                let data = try Data.init(contentsOf: URL.init(string:str)!)
                DispatchQueue.main.async {
                    self.hideCentralSpinner()
                    let image: UIImage = UIImage(data: data)!
                    UIImageWriteToSavedPhotosAlbum( image , self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                }
            }
            catch {
                self.hideCentralSpinner()
            }
        }
        
        
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
}
extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}
