//
//  LifestyleHabitsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 28/04/21.
//

import UIKit

class LifestyleHabitsVC: ParentViewController {
    var aryaHobite = ["Religion","Exercise","Alternative Therapies","Alcohol","Smoking","Habits","Basic Sanitation"]
    var indextSet = -1
    var arryaLifestylelist = [Lifestylelist]()
    var numberOfHobites = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset.bottom = 150 * _widthRatio
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getlifestylelist(loader: true)
        
    }
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "LifestyleHabitsAddVC")  as! LifestyleHabitsAddVC
        mapVc1.strHeader = "Add Lifestyle Habits"
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    @IBAction func btnEditClicked(_ sender: UIButton) {
        let mapVc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "LifestyleHabitsAddVC")  as! LifestyleHabitsAddVC
        mapVc1.strHeader = "Edit Lifestyle Habits"
        mapVc1.lifestylelist = arryaLifestylelist[sender.tag]
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    @IBAction func btnDeletedClicked(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            deleteuserphr(request_id : arryaLifestylelist[sender.tag].lifestyle_id)
        }
    }
    
    
}
extension LifestyleHabitsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            if indextSet == indexPath.section{
                indextSet = -1
            }else{
                indextSet = indexPath.section
            }
            tableView.reloadData()

        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arryaLifestylelist.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 75 * _widthRatio
        }else{
            return UITableView.automaticDimension

        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if indextSet == section{
            for obj in aryaHobite.enumerated(){
                if arryaLifestylelist[section].type == obj.element{
                    numberOfHobites = obj.offset
                }
            }
                if numberOfHobites == 0{
                    return 9
                }
                if numberOfHobites == 1{
                    return 3
                }
                if numberOfHobites == 2{
                    return 3
                }
                if numberOfHobites == 3{
                    if arryaLifestylelist[section].alcohol_drinkertype == ""{
                        return 3
                    }else{
                        return 5
                    }
                   
                }
                if numberOfHobites == 4{
                    if arryaLifestylelist[section].smoking_smokernumber == "" || arryaLifestylelist[section].smoking_smokernumber == "0"{
                        return  3
                    }else{
                        return  5
                    }
                    
                }
                if numberOfHobites == 5{
                    return  7
                }
                if numberOfHobites == 6{
                    return  5
                }
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! HealthConditionsCell
            cell.btnEdti.tag = indexPath.section
            cell.btnDeleted.tag = indexPath.section
            cell.lblName.text = arryaLifestylelist[indexPath.section].type

            if indextSet == indexPath.section{
                cell.viewConstretBottom.constant = -5
                cell.imageDrop.image = #imageLiteral(resourceName: "ic_navigation_aro_up")
            }else{
                cell.imageDrop.image = #imageLiteral(resourceName: "ic_dropdown")
                cell.viewConstretBottom.constant = 5
            }
            return cell
        }
        
        if numberOfHobites == 0{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = arryaLifestylelist[indexPath.section].type + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].religion_name
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Church:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].religion_church
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Minister/Pastor:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 6{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].religion_pastor
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Other:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].religion_other
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
        }
        if numberOfHobites == 1{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = arryaLifestylelist[indexPath.section].type + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].exercise
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
        }
        if numberOfHobites == 2{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Alternative Therapy" + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].alternative_therapy
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
        }
        if numberOfHobites == 3{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = arryaLifestylelist[indexPath.section].type + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].alcohol_type
                if arryaLifestylelist[indexPath.section].alcohol_drinkertype == ""{
                    cell.constraintTop.constant = -5
                    cell.constraintBottom.constant = 5
                }else{
                    cell.constraintTop.constant = -5
                    cell.constraintBottom.constant = -5
                }
                
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Consumption:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].alcohol_drinkertype
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
        }
        if numberOfHobites == 4{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = arryaLifestylelist[indexPath.section].type + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].smoking_type
                if arryaLifestylelist[indexPath.section].smoking_smokernumber == "" || arryaLifestylelist[indexPath.section].smoking_smokernumber == "0"{
                    cell.constraintTop.constant = -5
                    cell.constraintBottom.constant = 5
                }else{
                    cell.constraintTop.constant = -5
                    cell.constraintBottom.constant = -5
                }
                
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Number per day:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].smoking_smokernumber
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
        }
        if numberOfHobites == 5{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = arryaLifestylelist[indexPath.section].type + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].habit
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Frequency:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].habit_feq
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Since?:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].habit_since
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
         
        }
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = arryaLifestylelist[indexPath.section].type + ":"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].basic_water
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! DrDetailsCell
                cell.lblTitles.text = "Toilet:"
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = -5
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Desc") as! DrDetailsCell
                cell.lblDesc.text = arryaLifestylelist[indexPath.section].basic_toilet
                cell.constraintTop.constant = -5
                cell.constraintBottom.constant = 5
                return cell
            }
    }
    
    
    
    
}
extension LifestyleHabitsVC{
    @objc func sortRefresh(){
        getlifestylelist(loader: false)
    }
    func getlifestylelist(loader: Bool){
        if loader{
            showCentralSpinner()
        }
        let parameters = "{\"data\":{\"memberId\":\"\(_currentUser.patient_id!)\",\"memberType\":\"patient\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "lifestylelist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                       self.arryaLifestylelist.removeAll()
                        self.tableView.backgroundView = nil
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    arryaLifestylelist.append(Lifestylelist(dic: obj as! NSDictionary))
                                }
                                
                            }
                        }else{
                            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                        }
                        self.tableView.reloadData()
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func deleteuserphr(request_id : String){
        let parameters = "{\"data\":{\"id\":\"\(request_id)\",\"type\":\"lifestyle\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "deleteuserphr.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    getlifestylelist(loader: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
