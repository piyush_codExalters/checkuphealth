//
//  MeasurementsAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit
import Charts
import TinyConstraints
class BloodList: NSObject {
    
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        var t1 = SingIn()
        t1.placeholder = "Systolic *"
        t1.keybord = .numberPad
        
        
        var t2 = SingIn()
        t2.placeholder = "Diastolic *"
        t2.keybord = .numberPad
        
        
        var t3 = SingIn()
        t3.placeholder = "Heart Rate"
        t3.keybord = .numberPad
        
        var t4 = SingIn()
        t4.placeholder = "Mood"
        
        var t8 = SingIn()
        t8.placeholder = "Heart Rate"
        t8.keybord = .numberPad
        
        var t5 = SingIn()
        t5.placeholder = "Date"
        
        t5.keybord = .numberPad
        t5.image = #imageLiteral(resourceName: "ic_calender")
        
        var t6 = SingIn()
        t6.placeholder = "Time"
        t6.keybord = .numberPad
        t6.image = #imageLiteral(resourceName: "ic_time")
        
        var t7 = SingIn()
        t7.placeholder = "Note"
        t7.keybord = .numberPad
        t7.strValue = "Add Note"
        
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
        allFields.append(t4)
        allFields.append(t8)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
    }
}



class MeasurementsAddCell: ConstrainedTableViewCell {
    
    @IBOutlet weak var collectionModd: UICollectionView!
    
    @IBOutlet weak var viewBg: UIView!
    weak var measurementsAdd : AddBloodPressureVC!
    weak var hearAdd : HearAddVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias: 5)
        
    }
}
class MeasurementsCollCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var lblNAme: LabelMedium!
    @IBOutlet weak var imageIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}


class AddBloodPressureVC: ParentViewController, UITextViewDelegate, ChartViewDelegate {
    
    @IBOutlet weak var sider0: UISlider!
    @IBOutlet weak var sider110: UISlider!
    @IBOutlet weak var sider120: UISlider!
    @IBOutlet weak var sider140: UISlider!
    @IBOutlet weak var sider135: UISlider!
    @IBOutlet weak var sider130: UISlider!
    @IBOutlet weak var sider125: UISlider!
    @IBOutlet weak var sider190: UISlider!
    @IBOutlet weak var sider200: UISlider!
    @IBOutlet weak var sider180: UISlider!
    @IBOutlet weak var sider170: UISlider!
    @IBOutlet weak var sider160: UISlider!
    @IBOutlet weak var sider150: UISlider!
    
    
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeader: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!
    var fieldData = BloodList()
    var strPlacholder = "Add Note"
    var arryaMood = ["Great","Happy","Moderate","Angry","Stressed"]
    var arryMoodImage = [#imageLiteral(resourceName: "ic_great_grey"),#imageLiteral(resourceName: "ic_happy_grey"),#imageLiteral(resourceName: "ic_moderate_grey"),#imageLiteral(resourceName: "ic_gift_grey"),#imageLiteral(resourceName: "ic_stressed_grey")]
    var arryisMoodImage = [#imageLiteral(resourceName: "ic_great_orange"),#imageLiteral(resourceName: "ic_happy_orange"),#imageLiteral(resourceName: "ic_moderate_orange"),#imageLiteral(resourceName: "ic_gift_orange"),#imageLiteral(resourceName: "ic_stressed_orange")]
    var indextNumber = -1
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var header = "Add Blood Pressure"
    let datePickerView1: UIDatePicker = UIDatePicker()
    let datefrmenter1 = DateFormatter()
    var trendingChart : TrendingChart!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        siderHiden()
        lblHeader.text = header
        
        viewBottom.frame.size.height = 421 * _widthRatio
        datefrmenter.dateFormat = "yyyy-MM-dd"
        datefrmenter.locale = Locale(identifier:"en_US_POSIX")

        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[5].strValue = datefrmenter.string(from: currentDate as Date)
        
        datefrmenter1.dateFormat = "hh:mm a"
        datefrmenter1.locale = Locale(identifier:"en_US_POSIX")

        datePickerView1.datePickerMode = UIDatePicker.Mode.time
        let currentDate1 = NSDate()
        datePickerView1.maximumDate = currentDate1 as Date
        fieldData.allFields[6].strValue = datefrmenter1.string(from: currentDate1 as Date)
        datePickerView1.addTarget(self, action: #selector(self.datePickerValueChanged1), for: UIControl.Event.valueChanged)
        
        if header == "Add Blood Pressure"{
            btnAdd.isHidden = false
        }else{
            btnAdd.isHidden = true
            fieldData.allFields[0].strValue = trendingChart.systolic
            fieldData.allFields[1].strValue = trendingChart.dystolic
            fieldData.allFields[2].strValue = trendingChart.heart_rate
            fieldData.allFields[7].strValue = trendingChart.note
            fieldData.allFields[5].strValue = trendingChart.date
            fieldData.allFields[6].strValue = trendingChart.time
            showChart(Systic: Float(trendingChart.systolic)!, Disagtic: Float(trendingChart.dystolic)!)
            for obj in arryaMood.enumerated(){
                if obj.element == trendingChart.mood{
                    indextNumber = obj.offset
                }
            }
            tableView.reloadData()
        }
    }
    func siderHiden() {
        sider0.isHidden = true
        sider110.isHidden = true
        sider120.isHidden = true
        sider140.isHidden = true
        sider135.isHidden = true
        sider130.isHidden = true
        sider125.isHidden = true
        sider190.isHidden = true
        sider200.isHidden = true
        sider180.isHidden = true
        sider170.isHidden = true
        sider160.isHidden = true
        sider150.isHidden = true
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[5].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 5, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[5].strValue
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker) {
        fieldData.allFields[6].strValue = datefrmenter1.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 6, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[6].strValue
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Note"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[7].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
        if sender.tag == 0{
            
            if String.validateStringValue(str: fieldData.allFields[1].strValue){
                return
            }
            if String.validateStringValue(str: fieldData.allFields[0].strValue){
                return
            }
            showChart(Systic: Float(fieldData.allFields[0].strValue)!, Disagtic: Float(fieldData.allFields[1].strValue)!)
        }else if sender.tag == 1{
            
            if String.validateStringValue(str: fieldData.allFields[0].strValue){
                return
            }
            if String.validateStringValue(str: fieldData.allFields[1].strValue){
                return
            }
            showChart(Systic: Float(fieldData.allFields[0].strValue)!, Disagtic: Float(fieldData.allFields[1].strValue)!)
        }
    }
    
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        if validation().0{
            vital_addbpdata()
        }else{
            showFailMessage(message : validation().1)
        }
    }
    
    func setData(){
        sider0.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider110.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider120.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider140.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider135.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider130.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider125.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider190.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider200.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider180.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider170.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider160.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        sider150.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        
    }
    
    
}
extension AddBloodPressureVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6{
            return 75 * _widthRatio
        }
        if indexPath.row == 3{
            return 33 * _widthRatio
        }
        if indexPath.row == 7{
            return 150 * _widthRatio
        }
        return 108 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            if header == "Add Blood Pressure"{
                cell.textEmail.isUserInteractionEnabled = true
            }else{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        if indexPath.row == 5 || indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if header == "Add Blood Pressure"{
                cell.textEmail.isUserInteractionEnabled = true
            }else{
                cell.textEmail.isUserInteractionEnabled = false
            }
            if indexPath.row == 5{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            if indexPath.row == 6{
                if #available(iOS 13.4, *) {
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.preferredDatePickerStyle = .wheels
                    datePickerView1.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView1
                }else{
                    cell.textEmail.inputView = datePickerView1
                }
            }
            return cell
        }
        if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewBG.makeCornerRound(redias: 5)
            return cell
        }
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Rating") as! MeasurementsAddCell
            cell.measurementsAdd = self
            cell.collectionModd.reloadData()
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        if header == "Add Blood Pressure"{
            cell.textAreaNAme.isUserInteractionEnabled = true
        }else{
            cell.textAreaNAme.isUserInteractionEnabled = false
        }
        return cell
    }
}
extension MeasurementsAddCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = hearAdd{
            if hearAdd.strHeader == "Add Heart Rate"{
                hearAdd.fieldData.allFields[2].strValue = hearAdd.arryaMood[indexPath.row]
                hearAdd.indextNumber = indexPath.row
                hearAdd.tableView.reloadData()
            }
        }else{
            if measurementsAdd.header == "Add Blood Pressure"{
                measurementsAdd.fieldData.allFields[4].strValue = measurementsAdd.arryaMood[indexPath.row]
                measurementsAdd.indextNumber = indexPath.row
                measurementsAdd.tableView.reloadData()
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = hearAdd{
            return hearAdd.arryaMood.count
            
        }else{
            return measurementsAdd.arryaMood.count
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Mood", for: indexPath) as! MeasurementsCollCell
        if let _ = hearAdd{
            cell.lblNAme.text = hearAdd.arryaMood[indexPath.row]
            if hearAdd.indextNumber == indexPath.row{
                cell.imageIcon.image = hearAdd.arryisMoodImage[indexPath.row]
            }else{
                cell.imageIcon.image = hearAdd.arryMoodImage[indexPath.row]
            }
            
        }else{
            
            cell.lblNAme.text = measurementsAdd.arryaMood[indexPath.row]
            if measurementsAdd.indextNumber == indexPath.row{
                cell.imageIcon.image = measurementsAdd.arryisMoodImage[indexPath.row]
            }else{
                cell.imageIcon.image = measurementsAdd.arryMoodImage[indexPath.row]
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: viewBg.frame.width / 5 , height: 89)
    }
}





extension AddBloodPressureVC{
    func vital_addbpdata(){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["systolic"] = fieldData.allFields[0].strValue
        dic["diastolic"] = fieldData.allFields[1].strValue
        dic["hrt_rate"] = fieldData.allFields[2].strValue
        dic["mood"] = fieldData.allFields[4].strValue
        if fieldData.allFields[7].strValue == "Add Note"{
            fieldData.allFields[7].strValue = ""
        }
        dic["note"] = fieldData.allFields[7].strValue
        
        dic["date"] = fieldData.allFields[5].strValue
        dic["time"] = fieldData.allFields[6].strValue
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_addbpdata.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        
                                        if data.getStringValue(key: "showWarning") == "1"{
                                            
                                            showAlert1(title: data.getStringValue(key: "warningTitle"), msgString: data.getStringValue(key: "warningDescription"), Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                        }else{
                                            _defaultCenter.post(name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    
                                /*    var vtitle = ""
                                    var vstrMsg = ""
                                    var isPop = false
                                    
                                    let diastolicType = _currentUser.patient_type // Diabetic Type
                                    
                                    if _currentUser.patient_type == "1" { // Non-Diabetic Patient
                                        //  systolic = fieldData.allFields[0].strValue
                                        //  diastolic = fieldData.allFields[1].strValue
                                        
                                        if (Float(fieldData.allFields[0].strValue)! < 90 || Float(fieldData.allFields[1].strValue)! < 60){
                                            vtitle = ""
                                            vstrMsg = "Possible low blood pressure; please repeat and seek prompt medical attention if feeling lightheaded eg by calling 111"
                                            isPop = true
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 90 && Float(fieldData.allFields[0].strValue)! < 135 || Float(fieldData.allFields[1].strValue)! >= 60 && Float(fieldData.allFields[1].strValue)! < 85){
                                            vtitle = ""
                                            vstrMsg = "Satisfactory (nondiabetic)"
                                            isPop = true
                                            
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 135 && Float(fieldData.allFields[0].strValue)! < 150 || Float(fieldData.allFields[1].strValue)! >= 85 && Float(fieldData.allFields[1].strValue)! < 95){
                                            vtitle = ""
                                            vstrMsg = "Stage 1 Hypertension (nondiabetic)"
                                            isPop = true
                                            
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 150 && Float(fieldData.allFields[0].strValue)! < 180 || Float(fieldData.allFields[1].strValue)! >= 95 && Float(fieldData.allFields[1].strValue)! < 120){
                                            vtitle = ""
                                            vstrMsg = "Stage 2 Hypertension (nondiabetic); please book routine GP appointment"
                                            isPop = true
                                            
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 180 || Float(fieldData.allFields[1].strValue)! >= 120){
                                            vtitle = "WARNING"
                                            vstrMsg = "Dangerously high blood pressure. Please repeat and seek prompt medical attention if you have any of the following: visual changes, confusion, chest pain, shortness of breath, headache or reduced urine output (eg ring 111 or 999 depending on symptom severity). Otherwise please book a GP appointment."
                                            isPop = true
                                            
                                        }
                                        
                                    }else if _currentUser.patient_type == "2" ||  _currentUser.patient_type == "3"{
                                        
                                        
                                        if (Float(fieldData.allFields[0].strValue)! < 90 || Float(fieldData.allFields[1].strValue)! < 60){
                                            
                                            vtitle = ""
                                            vstrMsg = "Possible low blood pressure; please repeat and seek prompt medical attention if feeling lightheaded eg by calling 111"
                                            isPop = true
                                            
                                           
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 90 && Float(fieldData.allFields[0].strValue)! < 134) || (Float(fieldData.allFields[1].strValue)! >= 60 && Float(fieldData.allFields[1].strValue)! < 84){
                                            
                                            vtitle = ""
                                            vstrMsg = "Satisfactory range"
                                            isPop = true
                                            
                                           
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 135 && Float(fieldData.allFields[0].strValue)! < 149) || (Float(fieldData.allFields[1].strValue)! >= 85 && Float(fieldData.allFields[1].strValue)! < 94){
                                            
                                            vtitle = ""
                                            vstrMsg = "Outside optimal range - please book a routine GP Appt."
                                            isPop = true
                                            
                                          
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 150 && Float(fieldData.allFields[0].strValue)! < 179) || (Float(fieldData.allFields[1].strValue)! >= 95 && Float(fieldData.allFields[1].strValue)! < 119){
                                            
                                            vtitle = ""
                                            vstrMsg = "Outside optimal range - please book a routine GP Appt."
                                            isPop = true
                                            
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 180 || Float(fieldData.allFields[1].strValue)! >= 120){
                                            
                                            vtitle = "WARNING"
                                            vstrMsg = "Dangerously high blood pressure. Please repeat and seek prompt medical attention if you have any of the following: visual changes, confusion, chest pain, shortness of breath, headache or reduced urine output (eg ring 111 or 999 depending on symptom severity). Otherwise please book a GP appointment."
                                            isPop = true
                                            
                                        }
                                        
                                        
                                        
                                    }else{
                                        
                                        
                                        if (Float(fieldData.allFields[0].strValue)! < 90 || Float(fieldData.allFields[1].strValue)! < 60){
                                            
                                            vtitle = ""
                                            vstrMsg = "Possible low blood pressure; please repeat and seek prompt medical attention if feeling lightheaded eg by calling 111"
                                            isPop = true
                                            
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 90 && Float(fieldData.allFields[0].strValue)! < 130) || (Float(fieldData.allFields[1].strValue)! >= 60 && Float(fieldData.allFields[1].strValue)! < 80){
                                            
                                            vtitle = ""
                                            vstrMsg = "Satisfactory range"
                                            isPop = true
                                            
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 130 && Float(fieldData.allFields[0].strValue)! < 149) || (Float(fieldData.allFields[1].strValue)! >= 80 && Float(fieldData.allFields[1].strValue)! < 94){
                                            
                                            vtitle = ""
                                            vstrMsg = "Outside optimal range - please book a routine GP Appt."
                                            isPop = true
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 150 && Float(fieldData.allFields[0].strValue)! < 179) || (Float(fieldData.allFields[1].strValue)! >= 95 && Float(fieldData.allFields[1].strValue)! < 119){
                                            
                                            vtitle = ""
                                            vstrMsg = "Outside optimal range - please book a routine GP Appt."
                                            isPop = true
                                            
                                        }
                                        if (Float(fieldData.allFields[0].strValue)! >= 180 || Float(fieldData.allFields[1].strValue)! >= 120){
                                            
                                            vtitle = "WARNING"
                                            vstrMsg = "Dangerously high blood pressure. Please repeat and seek prompt medical attention if you have any of the following: visual changes, confusion, chest pain, shortness of breath, headache or reduced urine output (eg ring 111 or 999 depending on symptom severity). Otherwise please book a GP appointment."
                                            isPop = true
                                            
                                            
                                        }
                                    }
                                    if isPop{
                                        showAlert1(title: vtitle, msgString: vstrMsg, Ohk: "OK") {
                                            _defaultCenter.post(name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                    }else{
                                        _defaultCenter.post(name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)
                                        _ = self.navigationController?.popViewController(animated: true)
                                    }
                                 
                                 */
                                    
                                  
                                    
                                }else{
                                    showResponseMessage(dict: response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
        
    }
//    else if Float(fieldData.allFields[0].strValue)! < 40{
//      return(false,"Systolic should be >40")
//    }else if Float(fieldData.allFields[0].strValue)! > 300{
//      return(false,"Systolic should be <300")
//    }else if Float(fieldData.allFields[1].strValue)! < 30{
//    return(false,"Diastolic should be >30")
//    }else if Float(fieldData.allFields[1].strValue)! > 130{
//    return(false,"Diastolic should be <130")
//}
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Enter systolic")
        }else if String.validateStringValue(str: fieldData.allFields[1].strValue){
            return(false,"Enter diastolic")
        }
        return(true,"")
    }
    func showChart(Systic: Float,Disagtic: Float){
        var max = 0.0
        if Systic >= 200{
            sider0.isHidden   = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = false
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 190{
            sider0.isHidden   = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = false
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 180{
            sider0.isHidden   = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = false
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 170{
            sider0.isHidden   = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = false
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 160{
            sider0.isHidden   = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = false
            sider150.isHidden = true
        }else if Systic >= 150{
            sider0.isHidden = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = false
        }else if Systic >= 140{
            sider0.isHidden = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = false
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 130{
            sider0.isHidden = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = false
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 120{
            sider0.isHidden = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = false
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 110{
            sider0.isHidden = true
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = false
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 90{
            sider0.isHidden = true
            sider110.isHidden = true
            sider120.isHidden = false
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else if Systic >= 50{
            sider0.isHidden = true
            sider110.isHidden = false
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }else{
            sider0.isHidden = false
            sider110.isHidden = true
            sider120.isHidden = true
            sider140.isHidden = true
            sider135.isHidden = true
            sider130.isHidden = true
            sider125.isHidden = true
            sider190.isHidden = true
            sider200.isHidden = true
            sider180.isHidden = true
            sider170.isHidden = true
            sider160.isHidden = true
            sider150.isHidden = true
        }
        if 60 >= Disagtic{
            max = 200
        }else if 80 >= Disagtic{
            max = 160
        }else if 90 >= Disagtic{
            max = 135
        }else if 100 >= Disagtic{
            max = 122
        }else if 110 >= Disagtic{
            max = 120
        }else{
            max = Double(Disagtic)
        }
        if Systic >= 200{
            sider200.maximumValue = Float(max)
            sider200.value = Disagtic
        }else if Systic >= 190{
            sider190.maximumValue = Float(max)
            sider190.value = Disagtic
        }else if Systic >= 180{
            sider180.maximumValue = Float(max)
            sider180.value = Disagtic
        }else if Systic >= 170{
            sider170.maximumValue = Float(max)
            sider170.value = Disagtic
        }else if Systic >= 160{
            sider160.maximumValue = Float(max)
            sider160.value = Disagtic
        }else if Systic >= 150{
            sider150.maximumValue = Float(max)
            sider150.value = Disagtic
        }else if Systic >= 140{
            sider140.maximumValue = Float(max)
            sider140.value = Disagtic
        }else if Systic >= 130{
            sider135.maximumValue = Float(max)
            sider135.value = Disagtic
        }else if Systic >= 120{
            sider130.maximumValue = Float(max)
            sider130.value = Disagtic
        }else if Systic >= 110{
            sider125.maximumValue = Float(max)
            sider125.value = Disagtic
        }else if Systic >= 90{
            sider120.maximumValue = Float(max)
            sider120.value = Disagtic
        }else if Systic >= 50{
            sider110.maximumValue = Float(max)
            sider110.value = Disagtic
        }else{
            sider0.maximumValue = Float(max)
            sider0.value = Disagtic
        }
    }
    
}
