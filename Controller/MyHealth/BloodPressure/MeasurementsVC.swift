//
//  MeasurementsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit
class MeasurementsCell: ConstrainedTableViewCell {
    @IBOutlet weak var viBg: UIView!
    @IBOutlet weak var viewColorStatus: UIView!
    @IBOutlet weak var lblDate: LabelSemiBold!
    @IBOutlet weak var lblSystolic: LabelSemiBold!
    @IBOutlet weak var lblDiastic: LabelSemiBold!
    @IBOutlet weak var lblHeart: LabelSemiBold!
    @IBOutlet weak var imageMood: UIImageView!
    @IBOutlet weak var btnDelted: UIButton!
    @IBOutlet weak var lblML: LabelSemiBold!
    @IBOutlet weak var lblGr: LabelSemiBold!
    @IBOutlet weak var lblcal: LabelSemiBold!
    
    @IBOutlet weak var lblstrWeight: LabelSemiBold!
    @IBOutlet weak var lblStrHight: LabelSemiBold!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viBg.makeCornerRound(redias: 5)
        
        
    }
}
class MeasurementsVC: ParentViewController {
    var arryaTrendingChart = [TrendingChart]()

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

        NotificationCenter.default.addObserver(self, selector: #selector(self.getReloadDate), name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)

    }
    func setDate() {
        tableView.backgroundView = nil
        if arryaTrendingChart.count == 0{
            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "Data Not Found.", image : nil)
        }
        tableView.reloadData()
    }
    @objc func getReloadDate(){
        vital_bpdatalist(page:"", loader: true)
    }

    
    @IBAction func btnDeltedClkced(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            vital_bp_del_record(page:arryaTrendingChart[sender.tag].bp_id)
        }
        
    }
    
    
    
}
extension MeasurementsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AddBloodPressureVC")  as! AddBloodPressureVC
        mapVc1.header = "Blood Pressure"
        mapVc1.trendingChart = arryaTrendingChart[indexPath.row]
        self.navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 143 * _widthRatio
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaTrendingChart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! MeasurementsCell
        cell.viewColorStatus.makeCornecscrRoun1(redias: 2)
        cell.lblDate.text = arryaTrendingChart[indexPath.row].date + ", " + arryaTrendingChart[indexPath.row].time
        cell.lblSystolic.text = arryaTrendingChart[indexPath.row].systolic
        cell.lblDiastic.text = arryaTrendingChart[indexPath.row].dystolic
        cell.lblHeart.text = (arryaTrendingChart[indexPath.row].heart_rate == "" ? "-" : arryaTrendingChart[indexPath.row].heart_rate)
        cell.viewColorStatus.backgroundColor = arryaTrendingChart[indexPath.row].color
        cell.imageMood.image = arryaTrendingChart[indexPath.row].imageMood
        cell.btnDelted.tag = indexPath.row
        return cell
        
        
    }
    
    
    
    
}
extension MeasurementsVC{
    @objc func sortRefresh(){
        vital_bpdatalist(page: "", loader: false)
    }
    func vital_bpdatalist(page:String, loader: Bool){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        if loader{
            showCentralSpinner()
        }
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bpdatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                arryaTrendingChart.removeAll()
                                tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            arryaTrendingChart.append(TrendingChart(dic: obj as! NSDictionary))
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func vital_bp_del_record(page:String){
        var dic = [String:Any]()
        dic["bp_id"] = page
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bp_del_record.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)

                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}

