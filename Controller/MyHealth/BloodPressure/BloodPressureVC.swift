//
//  BloodPressureVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 26/04/21.
//

import UIKit

class BloodPressureVC: ParentViewController {
    
    @IBOutlet weak var viewHeartRate: UIView!
    @IBOutlet weak var viewMeasurements: UIView!
    @IBOutlet weak var viewTrends: UIView!
    @IBOutlet weak var segmentItem: UISegmentedControl!
    var arryaTrendingChart = [TrendingChart]()
    var arryaTemperChart = [TemperChart]()

    var dataChart = [DateAvgChart]()
    var monthlydataChart = [MonthlyAvgChart]()
    var yeardataChart = [YearAvgChart]()
    var weekdataChart = [WeekAvgChart]()
    var isBPQuestion = false
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentItem.setupSegment()
        viewHeartRate.isHidden = true
        viewMeasurements.isHidden = true
        viewTrends.isHidden = false
        vital_bpdatalist(page:"1", Index: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getReloadDate), name: NSNotification.Name(rawValue: "BloodRelooadData"), object: nil)

    }
    
    
    @IBAction func btnBackClickedNavigation(_ sender: Any) {
        
        if isBPQuestion{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MyHealthVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    
    @objc func getReloadDate(){
        if segmentItem.selectedSegmentIndex == 0{
            vital_bpdatalist(page:"1", Index: 0)
        }else if segmentItem.selectedSegmentIndex == 1{
            vital_bpdatalist(page:"1", Index: 1)
        }else{
            vital_hrdatalist()
        }
    }
    
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentItem.changeUnderlinePosition()
        switch segmentItem.selectedSegmentIndex {
        case 0:
            vital_bpdatalist(page:"1", Index: 0)
            viewHeartRate.isHidden = true
            viewMeasurements.isHidden = true
            viewTrends.isHidden = false
        case 1:
            vital_bpdatalist(page:"1", Index: 1)
            viewHeartRate.isHidden = true
            viewMeasurements.isHidden = false
            viewTrends.isHidden = true
        default:
            vital_hrdatalist()
            viewHeartRate.isHidden = false
            viewMeasurements.isHidden = true
            viewTrends.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Trender"{
            let vc = segue.destination as! TrendsVC
            vc.arryaTrendingChart = self.arryaTrendingChart
            vc.dataChart = dataChart
            vc.monthlydataChart = monthlydataChart
            vc.yeardataChart = yeardataChart
            vc.weekdataChart = weekdataChart
        }
        if segue.identifier == "Heart"{
            let vc = segue.destination as! HeartRateVC
            vc.arryaTemperChart = self.arryaTemperChart
            vc.dataChart = dataChart
            vc.monthlydataChart = monthlydataChart
            vc.yeardataChart = yeardataChart
            vc.weekdataChart = weekdataChart
        
        }
        if segue.identifier == "List"{
            let vc = segue.destination as! MeasurementsVC
            vc.arryaTrendingChart = self.arryaTrendingChart
        }
    }
    
    @IBAction func btnAddClicekd(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AddBloodPressureVC")  as! AddBloodPressureVC
        self.navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    @IBAction func btnUpdateBPQuestionsClicked(_ sender: Any) {
        
        let Question = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BPQuestionVC")  as! BPQuestionVC
        Question.isUpdateQuestion = true
        navigationController?.pushViewController(Question, animated: true)
    }
    
   
}
extension BloodPressureVC{
    func vital_bpdatalist(page:String, Index : Int){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bpdatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                arryaTrendingChart.removeAll()
                                dataChart.removeAll()
                                weekdataChart.removeAll()
                                monthlydataChart.removeAll()
                                yeardataChart.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                    
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            dataChart.append(DateAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            weekdataChart.append(WeekAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            monthlydataChart.append(MonthlyAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            yeardataChart.append(YearAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            arryaTrendingChart.append(TrendingChart(dic: obj as! NSDictionary))
                                        }
                                    }
                                    let uniqueMessages = dataChart.unique{$0.date ?? ""}
                                    dataChart = uniqueMessages
                                    let uniqueMessages1 = monthlydataChart.unique{$0.date ?? ""}
                                    monthlydataChart = uniqueMessages1
                                    let uniqueMessages2 = weekdataChart.unique{$0.week ?? 0}
                                    weekdataChart = uniqueMessages2
                                    let uniqueMessages3 = yeardataChart.unique{$0.date ?? ""}
                                    yeardataChart = uniqueMessages3
                                }
                                if Index == 0{
                                    for vc in self.children{
                                        if vc is TrendsVC{
                                            let vc2 = vc as! TrendsVC
                                            vc2.arryaTrendingChart = self.arryaTrendingChart
                                            vc2.dataChart = dataChart
                                            vc2.monthlydataChart = monthlydataChart
                                            vc2.yeardataChart = yeardataChart
                                            vc2.weekdataChart = weekdataChart
                                            vc2.showChart()
                                        }
                                    }
                                }else if Index == 1{
                                    for vc in self.children{
                                        if vc is MeasurementsVC{
                                            let vc2 = vc as! MeasurementsVC
                                            vc2.arryaTrendingChart = self.arryaTrendingChart
                                            vc2.setDate()
                                        }
                                    }
                                }else{
                                    for vc in self.children{
                                        if vc is HeartRateVC{
                                            let vc2 = vc as! HeartRateVC
                                            vc2.arryaTemperChart = self.arryaTemperChart
                                            vc2.dataChart = dataChart
                                            vc2.monthlydataChart = monthlydataChart
                                            vc2.yeardataChart = yeardataChart
                                            vc2.weekdataChart = weekdataChart
                                            vc2.showChart()
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func vital_hrdatalist(){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_hrdatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                arryaTemperChart.removeAll()
                                dataChart.removeAll()
                                weekdataChart.removeAll()
                                monthlydataChart.removeAll()
                                yeardataChart.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                 
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            dataChart.append(DateAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            weekdataChart.append(WeekAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            monthlydataChart.append(MonthlyAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            yeardataChart.append(YearAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            arryaTemperChart.append(TemperChart(dic: obj as! NSDictionary))
                                        }
                                        
                                        
                                    }
                                    let uniqueMessages = dataChart.unique{$0.date ?? ""}
                                    dataChart = uniqueMessages
                                    let uniqueMessages1 = monthlydataChart.unique{$0.date ?? ""}
                                    monthlydataChart = uniqueMessages1
                                    let uniqueMessages2 = weekdataChart.unique{$0.week ?? 0}
                                    weekdataChart = uniqueMessages2
                                    let uniqueMessages3 = yeardataChart.unique{$0.date ?? ""}
                                    yeardataChart = uniqueMessages3
                                    for vc in self.children{
                                        if vc is HeartRateVC{
                                            let vc2 = vc as! HeartRateVC
                                            vc2.arryaTemperChart = self.arryaTemperChart
                                            vc2.dataChart = dataChart
                                            vc2.monthlydataChart = monthlydataChart
                                            vc2.yeardataChart = yeardataChart
                                            vc2.weekdataChart = weekdataChart
                                            vc2.showChart()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    
    
    
}
