//
//  TrendsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit
import Charts
import TinyConstraints
class TrendsVC: ParentViewController, ChartViewDelegate {
    @IBOutlet weak var viewStatistic: LineChartView!

    
    var arryaName = ["Daily","Weekly","Monthly","Yearly"]
    var indext = 0
    var arryaTrendingChart = [TrendingChart]()
    
    var dataEntries: [ChartDataEntry] = []
    var dataEntries1: [ChartDataEntry] = []
    var arryaDystolic = [Double]()
    var arryaSystolic = [Double]()
    var arryaDate = [String]()
    var dataChart = [DateAvgChart]()
    var monthlydataChart = [MonthlyAvgChart]()
    var yeardataChart = [YearAvgChart]()
    var weekdataChart = [WeekAvgChart]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showChart(){
        dataEntries.removeAll()
        dataEntries1.removeAll()
        arryaDystolic.removeAll()
        arryaSystolic.removeAll()
        arryaDate.removeAll()
        if indext == 1{
                    for obj in weekdataChart {
                        arryaDystolic.append(obj.dystolic)
                        arryaSystolic.append(obj.systolic)
                        arryaDate.append(obj.date)
                    }
                    
                }else  if indext == 2{
                    for obj in monthlydataChart {
                        arryaDystolic.append(obj.dystolic)
                        arryaSystolic.append(obj.systolic)
                        arryaDate.append(obj.date)
                    }
                    
                }else if indext == 3{
                    for obj in yeardataChart {
                        arryaDystolic.append(obj.dystolic)
                        arryaSystolic.append(obj.systolic)
                        arryaDate.append(obj.date)
                    }
                   
                }else{
                    for obj in dataChart {
                        arryaDystolic.append(obj.dystolic)
                        arryaSystolic.append(obj.systolic)
                        arryaDate.append(obj.date)
                    }
                    
                }
        
        arryaDystolic.reverse()
        arryaSystolic.reverse()
        arryaDate.reverse()
        viewStatistic.gridBackgroundColor = .lightGray
        viewStatistic.delegate = self
        let xaxis = viewStatistic.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bothSided
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.arryaDate)
        xaxis.granularity = 1
        xaxis.setLabelCount(4, force: false)

        
        let yaxis = viewStatistic.leftAxis

        yaxis.valueFormatter = YAxisValueFormatter()
        yaxis.drawGridLinesEnabled = true
        
        viewStatistic.rightAxis.enabled = true
        for i in 0..<arryaDate.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: arryaDystolic[i], data: i)
            dataEntries1.append(dataEntry)
            let dataEntry1 = ChartDataEntry(x: Double(i), y: arryaSystolic[i], data: i)
            dataEntries.append(dataEntry1)
        }
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "Systolic")
        lineChartDataSet.colors = [UIColor.colorchaupbule()]
        lineChartDataSet.circleColors = [UIColor.colorchaupbule()]
        lineChartDataSet.circleHoleRadius = 3
        lineChartDataSet.circleRadius = 5.5
        lineChartDataSet.drawCirclesEnabled = true
        lineChartDataSet.lineWidth = 3
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.axisDependency = .left

        
        let lineChartDataSet1 = LineChartDataSet(entries: dataEntries1, label: "Diastolic")
        lineChartDataSet1.colors = [UIColor.colorchaupYellow()]
        lineChartDataSet1.circleColors = [UIColor.colorchaupYellow()]
        lineChartDataSet1.circleHoleRadius = 3
        lineChartDataSet1.circleRadius = 5.5
        lineChartDataSet1.drawCirclesEnabled = true
        lineChartDataSet1.lineWidth = 3
        lineChartDataSet1.mode = .cubicBezier
        
        let dataSets: [LineChartDataSet] = [lineChartDataSet,lineChartDataSet1]
        let chartData = LineChartData(dataSets: dataSets)
        viewStatistic.data = chartData
        viewStatistic.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)
    }
    

}
extension TrendsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indext = indexPath.row
        showChart()
        myColView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arryaName.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Tilte", for: indexPath) as! HeartCollCell
        cell.lblHeaderTitle.text = arryaName[indexPath.row]
        if indext == indexPath.row{
            cell.backgroundColor = UIColor.colorchaupOrange()
            cell.makeCornerRoundClear(redias : 6)
            cell.lblHeaderTitle.textColor = .white
        }else{
            cell.backgroundColor = nil
            cell.lblHeaderTitle.textColor = UIColor.colorchaupOrange()
        }
        return cell
        
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 4 , height: 41 * _widthRatio)
        
    }
}
class YAxisValueFormatter: IAxisValueFormatter {
   
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
       
        if value >= 1{
            return String(value.rounded(toPlaces: 1))
        }else{
            return String(value.rounded(toPlaces: 1))

        }
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
