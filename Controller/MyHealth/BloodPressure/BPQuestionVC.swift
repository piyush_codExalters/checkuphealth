//
//  BPQuestionVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 10/05/22.
//

import UIKit


class BPQuestionCell: ConstrainedTableViewCell {
    
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblTitle: LabelMedium!
    
    @IBOutlet weak var lblQuestion: LabelRegular!
    
    @IBOutlet weak var btnCheckbox: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBackground.makeCornerRound(redias: 5)
    }
}

class BPQuestionVC: ParentViewController {
    
    
    var answerOfQuestion = "Please answer a few questions about your health so that we may optimise the feedback you receive."
    
    var firstQuestion = "Do you have diabetes mellitus?"
    
    
    var secondQuestion = [[Any]]()
    
    
    var threadQuestion = ["Are you known to have albuminuria (increased protein in the urine) or 2 or more of the following?",". On blood pressure (BP) medication or BP often >135/85.",". On medication for triglycerides, or raised triglyceride levels (> 1.7 mmol/L)",". On cholesterol medication, or lowered HDL (< 1.03 mmol/L men; <1.3 mmol/L women)",". Waistline ≥ 40 inches (men). ≥ 35 inches (women)"]
    
    var fourQuestion = "Do you have kidney/renal impairment (CKD or chronic kidney disease)?"
    
    var fiveQuestion = [[Any]]()
    
    var sixQuestion = [[Any]]()
    
    var sevenQuestion = "Do you have kidney/renal impairment (CKD or chronic kidney disease)?"
    
    
    var isHaveDiabeticMellitus = ""
    var nDiabeticMellitusType = ""
    var nAgeBracket = ""
    var isHaveKidneyImpairment = ""
    var nAlbuminCreatinineRatio = ""
    var isHaveAlbuminuria = ""
    var isUpdateQuestion = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        secondQuestion.append(["Which type?",false,0])
        secondQuestion.append(["Type 1 DM: Also known as insulin dependent",false,0])
        secondQuestion.append(["Type 2 DM: Also known as non-insulin dependent",false,0])

      
        
        fiveQuestion.append(["if known, what is your urinary albumin: creatinine ratio?",false,0])
        fiveQuestion.append(["<70mg/mmol or not known",false,0])
        fiveQuestion.append(["≥70mg/mmol",false,0])
        
        sixQuestion.append(["Kindly confirm your age bracket.",false,0])
        sixQuestion.append(["Adult < 80 years",false,0])
        sixQuestion.append(["≥ 80 years",false,0])
        
        
        //        isHaveDiabeticMellitus (0-no /1-yes)
        //        nDiabeticMellitusType (1- Type 1 DM / 2 -type 2 DM)
        //        nAgeBracket (0- less than 80 / 1 - greater than 80)
        //        isHaveKidneyImpairment  (0-no /1-yes)
        //        nAlbuminCreatinineRatio ( 0- less than 70 / 1 - greater than 70 )
        //        isHaveAlbuminuria  (0-no/1-yes)
        
    }
    
    
    
    @IBAction func btnYesClicked(_ sender: Any) {
        
        
        
        if String.validateStringValue(str: isHaveDiabeticMellitus){
            isHaveDiabeticMellitus = "1"
            tableView.reloadData()
            return
            
        }
        if isHaveDiabeticMellitus == "0"{
            if String.validateStringValue(str: isHaveKidneyImpairment){
                isHaveKidneyImpairment = "1"
                tableView.reloadData()
                return
                
            }
            
        }
        
        if isHaveDiabeticMellitus == "1"{
            
            if nDiabeticMellitusType == "1"{
                
                if String.validateStringValue(str: isHaveAlbuminuria){
                    isHaveAlbuminuria = "1"
                    
                    bpOnboarding()
                    
                    return
                    
                }
            }
            
            if nDiabeticMellitusType == "2"{
                
                if nAgeBracket == "0"{
                    
                    if String.validateStringValue(str: isHaveKidneyImpairment){
                        isHaveKidneyImpairment = "1"
                        bpOnboarding()
                        return
                        
                    }
                    
                }
                
            }
            
            
            
        }
       
    }
    
    
    @IBAction func btnNoClicked(_ sender: Any) {
        
        if String.validateStringValue(str: isHaveDiabeticMellitus){
            isHaveDiabeticMellitus = "0"
            tableView.reloadData()
            return
           
        }
        
        if isHaveDiabeticMellitus == "0"{
            if String.validateStringValue(str: isHaveKidneyImpairment){
                isHaveKidneyImpairment = "0"
                
                
                bpOnboarding()
                
                
                return
                
            }
            
        }
        
        if isHaveDiabeticMellitus == "1"{
            
            if nDiabeticMellitusType == "1"{
                
                if String.validateStringValue(str: isHaveAlbuminuria){
                    isHaveAlbuminuria = "0"
                    bpOnboarding()
                    
                    return
                    
                }
            }
            
            if nDiabeticMellitusType == "2"{
                
                if nAgeBracket == "0"{
                    
                    if String.validateStringValue(str: isHaveKidneyImpairment){
                        isHaveKidneyImpairment = "0"
                        bpOnboarding()
                        return
                        
                    }
                    
                }
                
            }
            
            
            
        }
        
    }
    
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        
        if isHaveDiabeticMellitus == "1"{
            
            if nDiabeticMellitusType == "2"{
                if sixQuestion[1][1] as! Bool == false && sixQuestion[2][1] as! Bool == false{
                  return  showFailMessage(message: "Please select age bracket")
                }else{
                    if sixQuestion[1][1] as! Bool{
                        nAgeBracket = "0"
                        tableView.reloadData()
                        return
                    }else if sixQuestion[2][1] as! Bool{
                        nAgeBracket = "1"
                        bpOnboarding()
                        return
                    }
                }
            }
            
            
            if secondQuestion[1][1] as! Bool == false && secondQuestion[2][1] as! Bool == false{
              return  showFailMessage(message: "Please select diabetic mellitus type")
            }else{
                if secondQuestion[1][1] as! Bool{
                    nDiabeticMellitusType = "1"
                    tableView.reloadData()
                }else if secondQuestion[2][1] as! Bool{
                    nDiabeticMellitusType = "2"
                    tableView.reloadData()
                }
            }
        }else if isHaveDiabeticMellitus == "0"{
            
            if isHaveKidneyImpairment == "1"{
                if fiveQuestion[1][1] as! Bool == false && fiveQuestion[2][1] as! Bool == false{
                  return  showFailMessage(message: "Please select albumin:creatinine ratio")
                }else{
                    if fiveQuestion[1][1] as! Bool{
                        nAlbuminCreatinineRatio = "0"
                        bpOnboarding()
                        return
                    }else if fiveQuestion[2][1] as! Bool{
                        nAlbuminCreatinineRatio = "1"
                        bpOnboarding()
                        return
                    }
                }
            }
            
        }
    }
   
    
    
}
extension BPQuestionVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        //        isHaveDiabeticMellitus (0-no /1-yes)
        if isHaveDiabeticMellitus == "1"{
            
            //        nDiabeticMellitusType (1- Type 1 DM / 2 -type 2 DM)
            if nDiabeticMellitusType == "1"{
                
                if indexPath.row == 5{
                    return 80
                }
                return UITableView.automaticDimension
                
            }else if nDiabeticMellitusType == "2"{
                
                //        nAgeBracket (0- less than 80 / 1 - greater than 80)
                
                if nAgeBracket == "0"{
                    
                    if indexPath.row == 1{
                        return 80
                    }
                    return UITableView.automaticDimension
                    
                }else{
                    if indexPath.row == 3{
                        return 80
                    }
                    return UITableView.automaticDimension
                }
                
            }else{
                if indexPath.row == 3{
                    return 80
                }
                return UITableView.automaticDimension
            }
            
            
            
        }else if isHaveDiabeticMellitus == "0"{
            
            //        isHaveKidneyImpairment  (0-no /1-yes)
            
           if isHaveKidneyImpairment == "1"{
                if indexPath.row == 3{
                    return 80
                }
                return UITableView.automaticDimension
            }else{
                if indexPath.row == 1{
                    return 80
                }
                return UITableView.automaticDimension
                
            }
            
            
           
        }else{
            if indexPath.row == 2{
                return 80
            }
            return UITableView.automaticDimension
            
        }
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        isHaveDiabeticMellitus (0-no /1-yes)
        if isHaveDiabeticMellitus == "1"{
            //        nDiabeticMellitusType (1- Type 1 DM / 2 -type 2 DM)
            if nDiabeticMellitusType == "1"{
                
                return threadQuestion.count + 1
                
            }else if nDiabeticMellitusType == "2"{
                
                //        nAgeBracket (0- less than 80 / 1 - greater than 80)
                
                if nAgeBracket == "0"{
                    return 2
                }else{
                    return sixQuestion.count + 1
                }
                
            }else{
                return secondQuestion.count + 1
            }
            
        }else if isHaveDiabeticMellitus == "0"{
            
            //        isHaveKidneyImpairment  (0-no /1-yes)
            
           if isHaveKidneyImpairment == "1"{
               return fiveQuestion.count + 1
            }else{
                return 2
                
            }
           
            
        }else{
            return 3
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isHaveDiabeticMellitus == "1"{
            if nDiabeticMellitusType == "2"{
                if indexPath.row == 1  || indexPath.row == 2{
                    sixQuestion[1][1] = false
                    sixQuestion[2][1] = false
                    sixQuestion[indexPath.row][1] = true
                    tableView.reloadData()
                    return
                }
            }
            
            if indexPath.row == 1  || indexPath.row == 2{
                secondQuestion[1][1] = false
                secondQuestion[2][1] = false
                secondQuestion[indexPath.row][1] = true
                tableView.reloadData()
                return
            }
            
        }
        if isHaveDiabeticMellitus == "0"{
            if isHaveKidneyImpairment == "1"{
                if indexPath.row == 1  || indexPath.row == 2{
                    fiveQuestion[1][1] = false
                    fiveQuestion[2][1] = false
                    fiveQuestion[indexPath.row][1] = true
                    tableView.reloadData()
                    return
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
//        isHaveDiabeticMellitus (0-no /1-yes)
        if isHaveDiabeticMellitus == "1"{
            
            
            
            //        nDiabeticMellitusType (1- Type 1 DM / 2 -type 2 DM)
            if nDiabeticMellitusType == "1"{
                
                if indexPath.row == 5{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Answer") as! BPQuestionCell
                    
                    return cell
                }
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Question") as! BPQuestionCell
                    cell.lblQuestion.text = threadQuestion[indexPath.row]
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "Question3") as! BPQuestionCell
                cell.lblQuestion.text = threadQuestion[indexPath.row]
                return cell
                
            }else if nDiabeticMellitusType == "2"{
                
                
                //        nAgeBracket (0- less than 80 / 1 - greater than 80)
                
                if nAgeBracket == "0"{
                    
                    if indexPath.row == 1{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "Answer") as! BPQuestionCell
                        
                        return cell
                    }
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Question") as! BPQuestionCell
                    cell.lblQuestion.text = sevenQuestion
                    return cell
                    
                }else{
                    if indexPath.row == sixQuestion.count{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "Answer1") as! BPQuestionCell
                        return cell
                    }
                    if indexPath.row == 0{
                        
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Question1") as! BPQuestionCell
                        cell.lblQuestion.text = sixQuestion[indexPath.row][0] as! String
                    return cell
                        
                    }
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Question4") as! BPQuestionCell
                    cell.lblQuestion.text = sixQuestion[indexPath.row][0] as! String
                    if sixQuestion[indexPath.row][1] as! Bool {
                        cell.btnCheckbox.image = UIImage(named: "ic_radio_fill")
                    }else {
                        cell.btnCheckbox.image = UIImage(named: "ic_radio_unfill")
                    }
                    
                    return cell
                }
                
                
                
                
                
            }else{
                if indexPath.row == secondQuestion.count{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Answer1") as! BPQuestionCell
                    return cell
                }
                if indexPath.row == 0{
                    
                let cell = tableView.dequeueReusableCell(withIdentifier: "Question1") as! BPQuestionCell
                    cell.lblQuestion.text = secondQuestion[indexPath.row][0] as! String
                return cell
                    
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Question4") as! BPQuestionCell
                cell.lblQuestion.text = secondQuestion[indexPath.row][0] as! String
                if secondQuestion[indexPath.row][1] as! Bool {
                    cell.btnCheckbox.image = UIImage(named: "ic_radio_fill")
                }else {
                    cell.btnCheckbox.image = UIImage(named: "ic_radio_unfill")
                }
                
                return cell
            }
            
            
            
            
        }else if isHaveDiabeticMellitus == "0"{
            
            
            //        isHaveKidneyImpairment  (0-no /1-yes)
            
           if isHaveKidneyImpairment == "1"{
               
               
               
               if indexPath.row == fiveQuestion.count{
                   let cell = tableView.dequeueReusableCell(withIdentifier: "Answer1") as! BPQuestionCell
                   return cell
               }
               if indexPath.row == 0{
                   
               let cell = tableView.dequeueReusableCell(withIdentifier: "Question1") as! BPQuestionCell
                   cell.lblQuestion.text = fiveQuestion[indexPath.row][0] as! String
               return cell
                   
               }
               
               let cell = tableView.dequeueReusableCell(withIdentifier: "Question4") as! BPQuestionCell
               cell.lblQuestion.text = fiveQuestion[indexPath.row][0] as! String
               if fiveQuestion[indexPath.row][1] as! Bool {
                   cell.btnCheckbox.image = UIImage(named: "ic_radio_fill")
               }else {
                   cell.btnCheckbox.image = UIImage(named: "ic_radio_unfill")
               }
               
               return cell
               
            }else{
                if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Answer") as! BPQuestionCell
                    
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "Question") as! BPQuestionCell
                cell.lblQuestion.text = fourQuestion
                return cell
                
            }
           
        
            
        }else{
            if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Question") as! BPQuestionCell
                cell.lblQuestion.text = firstQuestion
                return cell
            }
            if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Answer") as! BPQuestionCell
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Title") as! BPQuestionCell
            cell.lblTitle.text = answerOfQuestion
            return cell
        }
        
        
        
        
    }
    
    
    
}
extension BPQuestionVC{
    
    func bpOnboarding(){
        var dic = [String:Any]()
        if String.validateStringValue(str: isHaveDiabeticMellitus){
            isHaveDiabeticMellitus = "0"
            
        }
        if String.validateStringValue(str: nDiabeticMellitusType){
            nDiabeticMellitusType = "0"
            
        }
        if String.validateStringValue(str: nAgeBracket){
           
            nAgeBracket = "0"
        }
        if String.validateStringValue(str: isHaveKidneyImpairment){
            isHaveKidneyImpairment = "0"
            
        }
        if String.validateStringValue(str: nAlbuminCreatinineRatio){
            nAlbuminCreatinineRatio = "0"
            
        }
        if String.validateStringValue(str: isHaveAlbuminuria){
            isHaveAlbuminuria = "0"
            
        }
        //        isHaveDiabeticMellitus (0-no /1-yes)
        //        nDiabeticMellitusType (1- Type 1 DM / 2 -type 2 DM)
        //        nAgeBracket (0- less than 80 / 1 - greater than 80)
        //        isHaveKidneyImpairment  (0-no /1-yes)
        //        nAlbuminCreatinineRatio ( 0- less than 70 / 1 - greater than 70 )
        //        isHaveAlbuminuria  (0-no/1-yes)
        
        
        dic["isHaveDiabeticMellitus"] = isHaveDiabeticMellitus
        dic["nDiabeticMellitusType"] = nDiabeticMellitusType
        dic["nAgeBracket"] = nAgeBracket
        dic["isHaveKidneyImpairment"] = isHaveKidneyImpairment
        dic["nAlbuminCreatinineRatio"] = nAlbuminCreatinineRatio
        dic["isHaveAlbuminuria"] = isHaveAlbuminuria
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "bpOnboarding.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response["data"] as? NSDictionary{
                                    _currentUser = UserDetail(dic: data)
                                    _userDefault.set(self.getSaveData(dataname : data), forKey: "userInfo")
                                    _userDefault.synchronize()
                                        if isUpdateQuestion{
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }else{
                                            let BP = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BloodPressureVC")  as! BloodPressureVC
                                            BP.isBPQuestion = true
                                            navigationController?.pushViewController(BP, animated: true)
                                        }
                                   
                                    }
                                }else{
                                    showResponseMessage(dict: response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
}
