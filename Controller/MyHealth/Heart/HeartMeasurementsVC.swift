//
//  HeartMeasurementsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit

class HeartMeasurementsVC: ParentViewController {
    var arryaTemperChart = [TemperChart]()

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
        NotificationCenter.default.addObserver(self, selector: #selector(self.getReloadDate), name: NSNotification.Name(rawValue: "HearRelooadData"), object: nil)
    }
    func setDate() {
        tableView.backgroundView = nil
        if arryaTemperChart.count == 0{
            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "Data Not Found.", image : nil)
        }
        tableView.reloadData()
    }
    
    @IBAction func btnDeltedClkced(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            vital_hr_del_record(page:arryaTemperChart[sender.tag].hr_id)
        }
        
    }
    @objc func getReloadDate(){
        vital_hrdatalist(page:"", loader: true)
    }
}
extension HeartMeasurementsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HearAddVC")  as! HearAddVC
        mapVc1.strHeader = "Heart Rate"
        mapVc1.temperChart = arryaTemperChart[indexPath.row]
        self.navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 143 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaTemperChart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! MeasurementsCell
        cell.lblDate.text = arryaTemperChart[indexPath.row].date + ", " + arryaTemperChart[indexPath.row].time
        cell.lblSystolic.text = arryaTemperChart[indexPath.row].heart_rate
        cell.btnDelted.tag = indexPath.row
        cell.imageMood.image = arryaTemperChart[indexPath.row].imageMood
        if arryaTemperChart[indexPath.row].hr_id == ""{
            cell.btnDelted.isHidden = true
        }else{
            cell.btnDelted.isHidden = false
        }
        return cell
        
        
    }
    
    
    
    
}
extension HeartMeasurementsVC{
    @objc func sortRefresh(){
        vital_hrdatalist(page: "", loader: false)
    }
    func vital_hrdatalist(page:String,loader:Bool){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        if loader{
            showCentralSpinner()
        }
        KPWebCall.call.postRequestApiClinet(relPath: "vital_hrdatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                self.tableView.backgroundView = nil
                                arryaTemperChart.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            arryaTemperChart.append(TemperChart(dic: obj as! NSDictionary))
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func vital_hr_del_record(page:String){
        var dic = [String:Any]()
        dic["hr_id"] = page
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_hr_del_record.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "HearRelooadData"), object: nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}

