//
//  HearAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit
class HeartList: NSObject {
    
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Heart Rate"
        t3.keybord = .numberPad
        
        var t4 = SingIn()
        t4.placeholder = "Mood"
        
        var t8 = SingIn()
        t8.placeholder = "Heart Rate"
        t8.keybord = .numberPad
        
        var t5 = SingIn()
        t5.placeholder = "Date"
        t5.keybord = .numberPad
        t5.image = #imageLiteral(resourceName: "ic_calender")
        
        var t6 = SingIn()
        t6.placeholder = "Time"
        t6.keybord = .numberPad
        t6.image = #imageLiteral(resourceName: "ic_time")
        
        var t7 = SingIn()
        t7.placeholder = "Note"
        t7.keybord = .numberPad
        t7.strValue = "Add Note"

        
        allFields.append(t3)
        allFields.append(t4)
        allFields.append(t8)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
    }
}
class HearAddVC: ParentViewController , UITextViewDelegate{
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    var temperChart : TemperChart!

    
    var strHeader = ""
    var fieldData = HeartList()
    var strPlacholder = "Add Note"
    var arryaMood = ["Great","Happy","Moderate","Angry","Stressed"]
    var arryMoodImage = [#imageLiteral(resourceName: "ic_great_grey"),#imageLiteral(resourceName: "ic_happy_grey"),#imageLiteral(resourceName: "ic_moderate_grey"),#imageLiteral(resourceName: "ic_gift_grey"),#imageLiteral(resourceName: "ic_stressed_grey")]
    var arryisMoodImage = [#imageLiteral(resourceName: "ic_great_orange"),#imageLiteral(resourceName: "ic_happy_orange"),#imageLiteral(resourceName: "ic_moderate_orange"),#imageLiteral(resourceName: "ic_gift_orange"),#imageLiteral(resourceName: "ic_stressed_orange")]
    var indextNumber = -1
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()

    let datePickerView1: UIDatePicker = UIDatePicker()
    let datefrmenter1 = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTitle.text = strHeader
        datefrmenter.dateFormat = "yyyy-MM-dd"
        datefrmenter.locale = Locale(identifier:"en_US_POSIX")

        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[3].strValue = datefrmenter.string(from: currentDate as Date)
        
        datefrmenter1.dateFormat = "hh:mm a"
        datefrmenter1.locale = Locale(identifier:"en_US_POSIX")

        datePickerView1.datePickerMode = UIDatePicker.Mode.time
        let currentDate1 = NSDate()
        datePickerView1.maximumDate = currentDate1 as Date
        fieldData.allFields[4].strValue = datefrmenter1.string(from: currentDate1 as Date)
        datePickerView1.addTarget(self, action: #selector(self.datePickerValueChanged1), for: UIControl.Event.valueChanged)
        
        if strHeader == "Add Heart Rate"{
            viewBottom.frame.size.height = 100 * _widthRatio
        }else{
            viewBottom.frame.size.height = 0 * _widthRatio
             fieldData.allFields[0].strValue = temperChart.heart_rate
             fieldData.allFields[5].strValue = temperChart.note
             fieldData.allFields[3].strValue = temperChart.date
            fieldData.allFields[4].strValue = temperChart.time
            for obj in arryaMood.enumerated(){
                if obj.element == temperChart.mood{
                    indextNumber = obj.offset
                }
            }
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[3].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 3, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[3].strValue
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker) {
        fieldData.allFields[4].strValue = datefrmenter1.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 4, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[4].strValue
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Note"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[5].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        if validation().0{
            vital_addhrdata()
        }else{
            showFailMessage(message : validation().1)
        }
    }
}
extension HearAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else if indexPath.row == 3 || indexPath.row == 4 {
            return 75 * _widthRatio
        }
        if indexPath.row == 1{
            return 33 * _widthRatio
        }
        if indexPath.row == 5{
            return 150 * _widthRatio
        }
        return 108 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            if strHeader != "Add Heart Rate"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        if indexPath.row == 3 || indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if strHeader != "Add Heart Rate"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            if indexPath.row == 3{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            if indexPath.row == 4{
                if #available(iOS 13.4, *) {
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.preferredDatePickerStyle = .wheels
                    datePickerView1.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView1
                }else{
                    cell.textEmail.inputView = datePickerView1
                }
            }
            return cell
        }
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewBG.makeCornerRound(redias: 5)
            return cell
        }
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Rating") as! MeasurementsAddCell
            cell.hearAdd = self
            cell.collectionModd.reloadData()

            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        if strHeader != "Add Heart Rate"{
            cell.textAreaNAme.isUserInteractionEnabled = false
        }
        return cell
    }
}

extension HearAddVC{
    func vital_addhrdata(){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["hrt_rate"] = fieldData.allFields[0].strValue
        dic["mood"] = fieldData.allFields[2].strValue
        if fieldData.allFields[5].strValue == "Add Note"{
            fieldData.allFields[5].strValue = ""
        }
        dic["note"] = fieldData.allFields[5].strValue
        dic["date"] = fieldData.allFields[3].strValue
        dic["time"] = fieldData.allFields[4].strValue
      
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_addhrdata.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "HearRelooadData"), object: nil)
                                    _ = self.navigationController?.popViewController(animated: true)
                                }else{
                                    showResponseMessage(dict: response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Enter heart rate.")
        }else if (Float(fieldData.allFields[0].strValue)! < 30) {
            return(false,"Heart rate should be >30")
        } else if (Float(fieldData.allFields[0].strValue)! > 220) {
            return(false, "Heart rate should be <220")
        }
        return(true,"")
    }
}
