//
//  BMIMeasurmentsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit

class BMIMeasurmentsVC: ParentViewController {
    
    
    @IBOutlet weak var btnMetric: JPWidthButton!
    @IBOutlet weak var btnImperical: JPWidthButton!
    
    @IBOutlet weak var lblunit: LabelRegular!
    var arryaBMList = [BMList]()
    
    var isMetric = "1"

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
        NotificationCenter.default.addObserver(self, selector: #selector(self.getReloadDate), name: NSNotification.Name(rawValue: "BMIRelooadData"), object: nil)
        btnMetric.isHidden = true
        btnImperical.isHidden = true
        lblunit.isHidden = true
        if isMetric == "1"{
        btnMetric.isSelected = true
        btnImperical.isSelected = false
        }else{
         btnMetric.isSelected = false
        btnImperical.isSelected = true
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func getReloadDate(){
        vital_bmidatalist(page:"", loader: true)
    }
    
    func setDate() {
        tableView.backgroundView = nil
        btnMetric.isHidden = false
        btnImperical.isHidden = false
        lblunit.isHidden = false
        if arryaBMList.count == 0{
            btnMetric.isHidden = true
            btnImperical.isHidden = true
            lblunit.isHidden = true
            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "Data Not Found.", image : nil)
        }
        tableView.reloadData()
    }
    
    @IBAction func btnDeltedClkced(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            vital_bmi_del_record(page:arryaBMList[sender.tag].bmi_id)
        }
        
    }
    
    
    
    @IBAction func btnMetricClicked(_ sender: UIButton) {
        isMetric = "1"
        if isMetric == "1"{
        btnMetric.isSelected = true
        btnImperical.isSelected = false
        }else{
         btnMetric.isSelected = false
        btnImperical.isSelected = true
        }
       
        tableView.reloadData()
    }
    
    
    @IBAction func btnImperialClicked(_ sender: UIButton) {
        isMetric = "2"
        if isMetric == "1"{
        btnMetric.isSelected = true
        btnImperical.isSelected = false
        }else{
        btnMetric.isSelected = false
        btnImperical.isSelected = true
        }
       
        tableView.reloadData()
    }
    

    
}
extension BMIMeasurmentsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BMIAddVC")  as! BMIAddVC
        mapVc1.strHeader = "BMI"
        mapVc1.bMList = arryaBMList[indexPath.row]
        mapVc1.fieldData.allFields[1].strValue = isMetric
        self.navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 143 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaBMList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! MeasurementsCell
        cell.viewColorStatus.makeCornecscrRoun1(redias: 2)
        cell.lblDate.text = arryaBMList[indexPath.row].date + ", " + arryaBMList[indexPath.row].time
        cell.lblSystolic.text = "\(Float(arryaBMList[indexPath.row].height)!.roundedFlaot(toPlaces: 2))"
        cell.lblDiastic.text = "\(Float(arryaBMList[indexPath.row].weight)!.roundedFlaot(toPlaces: 2))"
        cell.lblHeart.text = "\(arryaBMList[indexPath.row].bmi!)"
        cell.viewColorStatus.backgroundColor = arryaBMList[indexPath.row].color
        if isMetric == "1"{
            cell.lblstrWeight.text = "Weight (kg)"
            cell.lblStrHight.text = "Height (cm)"
            cell.lblSystolic.text = "\(Float(arryaBMList[indexPath.row].height)!.roundedFlaot(toPlaces: 2))"
            cell.lblDiastic.text = "\(Float(arryaBMList[indexPath.row].weight)!.roundedFlaot(toPlaces: 2))"
            
        }else{
            cell.lblstrWeight.text = "Weight (lb)"
            cell.lblStrHight.text = "Height (inch)"
            
           let strHight = Float(arryaBMList[indexPath.row].height)! / 2.54   // inch to cm
           let strWidht = Float(arryaBMList[indexPath.row].weight)! * 2.205  // lb to kg

            
            cell.lblSystolic.text = "\(strHight.roundedFlaot(toPlaces: 2))"
            cell.lblDiastic.text = "\(strWidht.roundedFlaot(toPlaces: 2))"
        }
        
        cell.btnDelted.tag = indexPath.row
        return cell
        
        
    }
    
    
    
    
}
extension BMIMeasurmentsVC{
    @objc func sortRefresh(){
        vital_bmidatalist(page: "", loader: false)
    }
    func vital_bmidatalist(page:String,loader:Bool){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        if loader{
            showCentralSpinner()
        }
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bmidatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                arryaBMList.removeAll()
                                btnMetric.isHidden = false
                                btnImperical.isHidden = false
                                lblunit.isHidden = false
                                self.tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            arryaBMList.append(BMList(dic: obj as! NSDictionary))
                                        }
                                    }
                                }else{
                                    btnMetric.isHidden = true
                                    btnImperical.isHidden = true
                                    lblunit.isHidden = true
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func vital_bmi_del_record(page:String){
        var dic = [String:Any]()
        dic["bmi_id"] = page
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bmi_del_record.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "BMIRelooadData"), object: nil)

                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}
