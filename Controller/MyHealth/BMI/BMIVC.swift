//
//  BMIVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit

class BMIVC: ParentViewController {
    
    @IBOutlet weak var viewTrends: UIView!
    @IBOutlet weak var viewMeasurements: UIView!
    @IBOutlet weak var segmentItem: UISegmentedControl!
    var arryaBMList = [BMList]()
    var dataChart = [DateAvgChart]()
    var monthlydataChart = [MonthlyAvgChart]()
    var yeardataChart = [YearAvgChart]()
    var weekdataChart = [WeekAvgChart]()
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentItem.setupSegment()
        viewTrends.isHidden = false
        viewMeasurements.isHidden = true
        vital_bmidatalist(page:"1", Index : 0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getReloadDate), name: NSNotification.Name(rawValue: "BMIRelooadData"), object: nil)
        
    }
    
    @objc func getReloadDate(){
        if segmentItem.selectedSegmentIndex == 0{
            vital_bmidatalist(page:"1", Index : 0)
        }else{
            vital_bmidatalist(page:"1", Index : 1)
        }
    }
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl){
        segmentItem.changeUnderlinePosition()
        switch segmentItem.selectedSegmentIndex {
        case 0:
            vital_bmidatalist(page:"1", Index : 0)
            viewMeasurements.isHidden = true
            viewTrends.isHidden = false
        default:
            vital_bmidatalist(page:"1", Index : 1)
            viewMeasurements.isHidden = false
            viewTrends.isHidden = true
        }
    }
    
    @IBAction func btnAddClicekd(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BMIAddVC")  as! BMIAddVC
        mapVc1.strHeader = "Add BMI"
        
        self.navigationController?.pushViewController(mapVc1, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Trender"{
            let vc = segue.destination as! TrendBMIVC
            vc.arryaBMList = self.arryaBMList
            vc.dataChart = dataChart
            vc.monthlydataChart = monthlydataChart
            vc.yeardataChart = yeardataChart
            vc.weekdataChart = weekdataChart
            
        }
        
        if segue.identifier == "List"{
            let vc = segue.destination as! BMIMeasurmentsVC
            vc.arryaBMList = self.arryaBMList
        }
    }
    
}
extension BMIVC{
    func vital_bmidatalist(page:String, Index : Int){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bmidatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                arryaBMList.removeAll()
                                dataChart.removeAll()
                                weekdataChart.removeAll()
                                monthlydataChart.removeAll()
                                yeardataChart.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                    
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            dataChart.append(DateAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            weekdataChart.append(WeekAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            monthlydataChart.append(MonthlyAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            yeardataChart.append(YearAvgChart(dic: obj as! NSDictionary, arryaChart: data))
                                            arryaBMList.append(BMList(dic: obj as! NSDictionary))
                                        }
                                    }
                                    let uniqueMessages = dataChart.unique{$0.date ?? ""}
                                    dataChart = uniqueMessages
                                    let uniqueMessages1 = monthlydataChart.unique{$0.date ?? ""}
                                    monthlydataChart = uniqueMessages1
                                    let uniqueMessages2 = weekdataChart.unique{$0.week ?? 0}
                                    weekdataChart = uniqueMessages2
                                    let uniqueMessages3 = yeardataChart.unique{$0.date ?? ""}
                                    yeardataChart = uniqueMessages3
                                    
                                }
                                if Index == 0{
                                    for vc in self.children{
                                        if vc is TrendBMIVC{
                                            let vc2 = vc as! TrendBMIVC
                                            vc2.arryaBMList = self.arryaBMList
                                            vc2.dataChart = dataChart
                                            vc2.monthlydataChart = monthlydataChart
                                            vc2.yeardataChart = yeardataChart
                                            vc2.weekdataChart = weekdataChart
                                            vc2.showChart()
                                        }
                                    }
                                }else{
                                    for vc in self.children{
                                        if vc is BMIMeasurmentsVC{
                                            let vc2 = vc as! BMIMeasurmentsVC
                                            vc2.arryaBMList = self.arryaBMList
                                            vc2.setDate()
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
}
