//
//  BMIAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//



import UIKit
class BMIList: NSObject {
    
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Age *"
        t3.keybord = .numberPad
        
        var t2 = SingIn()
        t2.strValue = "1"
        t2.keybord = .numberPad
        
        var t8 = SingIn()
        t8.placeholder = "Height(cm) *"
        t8.keybord = .decimalPad
        t8.strValue = ""

        
        var t9 = SingIn()
        t9.placeholder = "Weight(kg) *"
        t9.keybord = .decimalPad
        t9.strValue = ""

        
        var t5 = SingIn()
        t5.placeholder = "Date"
        t5.keybord = .numberPad
        t5.image = #imageLiteral(resourceName: "ic_calender")
        
        var t6 = SingIn()
        t6.placeholder = "Time"
        t6.keybord = .numberPad
        t6.image = #imageLiteral(resourceName: "ic_time")
        
        var t7 = SingIn()
        t7.placeholder = "Note"
        t7.keybord = .default
        t7.strValue = "Add Note"
        
        
        allFields.append(t3)
        allFields.append(t2)
        allFields.append(t8)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
    }
}
class BMIAddVC:ParentViewController, UITextViewDelegate {
    
    
    @IBOutlet weak var sliderAnimation: UISlider!
    
    @IBOutlet weak var btnadd: submitButton!
    @IBOutlet weak var lblCalculater: LabelSemiBold!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var viewBg: UIView!
    var strHeader = ""
    var fieldData = BMIList()
    var strPlacholder = "Add Note"
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    let datePickerView1: UIDatePicker = UIDatePicker()
    let datefrmenter1 = DateFormatter()
    var strBMI = ""
    var bMList : BMList!
        
    var hightString = ""
    var wightString = ""
    var isImperial = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRoundClear(redias: 5)
        sliderAnimation.setThumbImage(UIImage(named: "ic_pin_forgraph"), for: .normal)
        
        
        lblHeaderTitle.text = strHeader
        datefrmenter.dateFormat = "yyyy-MM-dd"
        datefrmenter.locale = Locale(identifier:"en_US_POSIX")

        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[4].strValue = datefrmenter.string(from: currentDate as Date)
        
        datefrmenter1.dateFormat = "hh:mm a"
        datefrmenter1.locale = Locale(identifier:"en_US_POSIX")

        datePickerView1.datePickerMode = UIDatePicker.Mode.time
        let currentDate1 = NSDate()
        datePickerView1.maximumDate = currentDate1 as Date
        fieldData.allFields[5].strValue = datefrmenter1.string(from: currentDate1 as Date)
        datePickerView1.addTarget(self, action: #selector(self.datePickerValueChanged1), for: UIControl.Event.valueChanged)
        if strHeader == "Add BMI"{
            btnadd.isHidden = false
            viewBottom.frame.size.height = 412 * _widthRatio
        }else{
            btnadd.isHidden = true
            viewBottom.frame.size.height = 412 * _widthRatio
            fieldData.allFields[3].strValue = bMList.weight
            fieldData.allFields[2].strValue = bMList.height
            fieldData.allFields[6].strValue = bMList.note
            fieldData.allFields[4].strValue = bMList.date
            fieldData.allFields[5].strValue = bMList.time
            lblCalculater.text = "\(bMList.bmi.roundedFlaot(toPlaces: 2))"
            if String.validateStringValue(str: fieldData.allFields[3].strValue){
                fieldData.allFields[3].strValue = "0.0"
            }
            if String.validateStringValue(str: fieldData.allFields[2].strValue){
                fieldData.allFields[2].strValue = "0.0"
            }
            if fieldData.allFields[1].strValue != "1"{
                let strHight = Float(fieldData.allFields[2].strValue)! / 2.54   // inch to cm
                let strWidht = Float(fieldData.allFields[3].strValue)! * 2.205  // lb to kg
                 fieldData.allFields[2].strValue = "\(strHight.roundedFlaot(toPlaces: 2))"
                 fieldData.allFields[3].strValue = "\(strWidht.roundedFlaot(toPlaces: 2))"
            }else{
                let strHight = Float(fieldData.allFields[2].strValue)!  // inch to cm
                 let strWidht = Float(fieldData.allFields[3].strValue)! // lb to kg
                fieldData.allFields[2].strValue = "\(strHight.roundedFlaot(toPlaces: 2))"
                fieldData.allFields[3].strValue = "\(strWidht.roundedFlaot(toPlaces: 2))"
            }
           // getMetricToImperial(chight:hightString,cwidht:wightString)
            getBMICalculation(chight: fieldData.allFields[2].strValue, cwidht: fieldData.allFields[3].strValue)

            fieldData.allFields[0].strValue = bMList.age
        }
    }
    
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[4].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 4, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[4].strValue
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker) {
        fieldData.allFields[5].strValue = datefrmenter1.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 5, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[5].strValue
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Note"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[6].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
        hightString = fieldData.allFields[2].strValue
        wightString = fieldData.allFields[3].strValue
        isImperial  = fieldData.allFields[1].strValue
        getBMICalculation(chight: fieldData.allFields[2].strValue, cwidht: fieldData.allFields[3].strValue)
        
    }
    func getBMICalculation(chight:String,cwidht:String) {
        
        var strHight = chight
        var strwidht = cwidht
        
        if String.validateStringValue(str: strHight){
            strHight = "0.0"
        }
        if String.validateStringValue(str: strwidht){
            strwidht = "0.0"
        }
        
        var hight = Float(strHight)
        var widht = Float(strwidht)
            if fieldData.allFields[1].strValue != "1"{
                         hight! = hight! * 2.54  // inch to cm
                         widht! = widht! / 2.205 // lb to kg
            }

        
        var isfalse = true
        if (hight! < 40) {
            isfalse = false
        } else if (hight! > 200) {
            isfalse = false
        }
        if (widht! < 20) {
            isfalse = false
        } else if (widht! > 200) {
            isfalse = false
        }
        
        
        let cmTomm = hight! / 100
        let avrageHight = cmTomm * cmTomm
        if isfalse{
            let totalBMI = widht! / avrageHight
            strBMI = "\(totalBMI.roundedFlaot(toPlaces: 2))"
            lblCalculater.text = "\(totalBMI.roundedFlaot(toPlaces: 2))"
            if totalBMI.roundedFlaot(toPlaces: 2) >= 35.5{
                sliderAnimation.value = 9
            }else if totalBMI.roundedFlaot(toPlaces: 2) >= 30{
                sliderAnimation.value = 7
            }else if totalBMI.roundedFlaot(toPlaces: 2) >= 25{
                sliderAnimation.value = 5
            }else if totalBMI.roundedFlaot(toPlaces: 2) >= 18.5{
                sliderAnimation.value = 3
            }else{
                sliderAnimation.value = 1
            }
        }else{
            let totalBMI : Float = 00
            strBMI = "\(totalBMI.roundedFlaot(toPlaces: 2))"
            lblCalculater.text = "\(totalBMI.roundedFlaot(toPlaces: 2))"
            if totalBMI.roundedFlaot(toPlaces: 2) >= 35.5{
                sliderAnimation.value = 9
            }else if totalBMI.roundedFlaot(toPlaces: 2) >= 30{
                sliderAnimation.value = 7
            }else if totalBMI.roundedFlaot(toPlaces: 2) >= 25{
                sliderAnimation.value = 5
            }else if totalBMI.roundedFlaot(toPlaces: 2) >= 18.5{
                sliderAnimation.value = 3
            }else if totalBMI.roundedFlaot(toPlaces: 2) == 00{
                sliderAnimation.value = 0
            }else{
                sliderAnimation.value = 1
                
            }
        }
       
        
    }
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        if validation().0{
            vital_addbmidata()
        }else{
            showFailMessage(message : validation().1)
        }
    }
    
    
    @IBAction func btnMetricClicked(_ sender: UIButton) {
        
        fieldData.allFields[1].strValue = "1"
        fieldData.allFields[2].placeholder = "Height(cm) *"
        fieldData.allFields[3].placeholder = "Weight(kg) *"
        getMetricToImperial(chight:hightString,cwidht:wightString)
        getBMICalculation(chight: fieldData.allFields[2].strValue, cwidht: fieldData.allFields[3].strValue)


        tableView.reloadData()
    }
    
    
    @IBAction func btnImperialClicked(_ sender: UIButton) {
        
        
        fieldData.allFields[1].strValue = "2"
        fieldData.allFields[2].placeholder = "Height(inch) *"
        fieldData.allFields[3].placeholder = "Weight(lb) *"
        getMetricToImperial(chight:hightString,cwidht:wightString)
        getBMICalculation(chight: fieldData.allFields[2].strValue, cwidht: fieldData.allFields[3].strValue)

        
        tableView.reloadData()
    }
    
    
    
    func getMetricToImperial(chight:String,cwidht:String) {
        var strHight = chight
        var strwidht = cwidht
        
        if String.validateStringValue(str: strHight){
            strHight = "0.0"
        }
        if String.validateStringValue(str: strwidht){
            strwidht = "0.0"
        }
        
        var hight = Float(strHight)
        var widht = Float(strwidht)
        
        if isImperial == "1"{
            if fieldData.allFields[1].strValue == "2"{
                 hight! = hight! / 2.54  // inch to cm
                 widht! = widht! * 2.205 // lb to kg
            }
        }
        
        if isImperial == "2"{
            if fieldData.allFields[1].strValue == "1"{
                 hight! = hight! * 2.54  // cm to inch
                 widht! = widht! / 2.205 // kg to lb
            }
        }
        
        
        fieldData.allFields[2].strValue = "\(hight!.roundedFlaot(toPlaces: 2))"
        fieldData.allFields[3].strValue = "\(widht!.roundedFlaot(toPlaces: 2))"
        
        if fieldData.allFields[2].strValue == "0" || fieldData.allFields[2].strValue == "0.0" || fieldData.allFields[2].strValue == "0.00"{
            fieldData.allFields[2].strValue = ""
        }
        if fieldData.allFields[3].strValue == "0" || fieldData.allFields[3].strValue == "0.0" || fieldData.allFields[3].strValue == "0.00"{
            fieldData.allFields[3].strValue = ""
        }
    }
    
    
    
}
extension BMIAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5{
            return 75 * _widthRatio
        }
        
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Gender") as! ProfileCell
            cell.lblName.text = "Unit"
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.btnMale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
            cell.btnFemale.titleLabel?.font =  UIFont.Maven_pro_Medium(size: 15)
            if fieldData.allFields[indexPath.row].strValue == "1"{
                cell.btnMale.isSelected = true
                cell.btnFemale.isSelected = false
            }else{
                cell.btnMale.isSelected = false
                cell.btnFemale.isSelected = true
            }
            
            if strHeader == "Add BMI"{
                cell.btnMale.isUserInteractionEnabled = true
                cell.btnFemale.isUserInteractionEnabled = true
            }else{
                cell.btnMale.isUserInteractionEnabled = false
                cell.btnFemale.isUserInteractionEnabled = false
                
            }
            return cell
        }
        if  indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            if strHeader != "Add BMI"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        
        if  indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            if strHeader != "Add BMI"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        if indexPath.row == 4 || indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if strHeader != "Add BMI"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            if indexPath.row == 4{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            if indexPath.row == 5{
                if #available(iOS 13.4, *) {
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.preferredDatePickerStyle = .wheels
                    datePickerView1.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView1
                }else{
                    cell.textEmail.inputView = datePickerView1
                }
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.textAreaNAme.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        if strHeader != "Add BMI"{
            cell.textAreaNAme.isUserInteractionEnabled = false
        }
        return cell
    }
    
}

extension BMIAddVC{
    func vital_addbmidata(){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        
        if fieldData.allFields[1].strValue == "1"{
            dic["weight"] = fieldData.allFields[3].strValue
            dic["height"] = fieldData.allFields[2].strValue
            
        }else{
            let inch = "\(Float(fieldData.allFields[2].strValue)! * 2.54)"
            let lb = "\(Float(fieldData.allFields[3].strValue)! / 2.205)"
            dic["height"] =  "\(Float(inch)!.roundedFlaot(toPlaces: 2))" // inch to cm
            dic["weight"] =  "\(Float(lb)!.roundedFlaot(toPlaces: 2))" // lb to kg
            
        }
        
        
        
        if fieldData.allFields[6].strValue == "Add Note"{
            fieldData.allFields[6].strValue = ""
        }
        dic["note"] = fieldData.allFields[6].strValue
        dic["date"] = fieldData.allFields[4].strValue
        dic["time"] = fieldData.allFields[5].strValue
        dic["bmi"] = strBMI
        dic["age"] = fieldData.allFields[0].strValue
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_addbmidata.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "BMIRelooadData"), object: nil)
                                    
                                    _ = self.navigationController?.popViewController(animated: true)
                                }else{
                                    showResponseMessage(dict: response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Enter age")
        }else if fieldData.allFields[1].strValue == "1"{
            
            
            if String.validateStringValue(str: fieldData.allFields[2].strValue){
                return(false,"Enter height")
            }else if (Float(fieldData.allFields[2].strValue)! < 40) {
                return(false,"height should be >40cm")
            } else if (Float(fieldData.allFields[2].strValue)! > 200) {
                return(false,"height should be <200cm")
            }else  if String.validateStringValue(str: fieldData.allFields[3].strValue){
                return(false,"Enter weight")
            }else if (Float(fieldData.allFields[3].strValue)! < 20) {
                return(false,"weight should be >20kg")
            } else if (Float(fieldData.allFields[3].strValue)! > 200) {
                return(false,"weight should be <200kg")
            }
            
            
        }else{
            
            
            if String.validateStringValue(str: fieldData.allFields[2].strValue){
                return(false,"Enter height")
            }else if (Float(fieldData.allFields[2].strValue)! < 15) {
                return(false,"height should be >15inch")
            } else if (Float(fieldData.allFields[2].strValue)! > 78) {
                return(false,"height should be <78inch")
            }else  if String.validateStringValue(str: fieldData.allFields[3].strValue){
                return(false,"Enter weight")
            }else if (Float(fieldData.allFields[3].strValue)! < 44) {
                return(false,"weight should be >44lb")
            } else if (Float(fieldData.allFields[3].strValue)! > 440) {
                return(false,"weight should be <440lb")
            }
            
        }
        
        
        return(true,"")
    }
    
}


extension Float {
    /// Rounds the double to decimal places value
    func roundedFlaot(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
