//
//  TrendWeightVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/05/21.
//

import UIKit
import Charts
import TinyConstraints

class TrendWeightVC: ParentViewController, ChartViewDelegate {
    @IBOutlet weak var viewStatistic: LineChartView!
    
    
    var arryaName = ["Daily","Weekly","Monthly","Yearly"]
    var indext = 0
    var arryaTemperChart = [TemperChart]()
    var dataEntries: [ChartDataEntry] = []
    var arryaTemper = [Double]()
    var arryaDate = [String]()
    var dataChart = [DateAvgChart]()
    var monthlydataChart = [MonthlyAvgChart]()
    var yeardataChart = [YearAvgChart]()
    var weekdataChart = [WeekAvgChart]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showChart(){
        dataEntries.removeAll()
        arryaTemper.removeAll()
        arryaDate.removeAll()
        if indext == 1{
            for obj in weekdataChart {
                arryaTemper.append(obj.weight)
                arryaDate.append(obj.date)
            }
            
        }else  if indext == 2{
            for obj in monthlydataChart {
                arryaTemper.append(obj.weight)
                arryaDate.append(obj.date)
            }
            
        }else if indext == 3{
            for obj in yeardataChart {
                arryaTemper.append(obj.weight)
                arryaDate.append(obj.date)
            }
            
        }else{
            for obj in dataChart {
                arryaTemper.append(obj.weight)
                arryaDate.append(obj.date)
            }
            
        }
        
        arryaTemper.reverse()
        arryaDate.reverse()
        viewStatistic.gridBackgroundColor = .lightGray
        viewStatistic.delegate = self
        let xaxis = viewStatistic.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bothSided
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.arryaDate)
        xaxis.granularity = 1
        xaxis.setLabelCount(4, force: false)
        let yaxis = viewStatistic.leftAxis
        yaxis.valueFormatter = YAxisValueFormatter()
        yaxis.drawGridLinesEnabled = true
        viewStatistic.rightAxis.enabled = true
        for i in 0..<arryaDate.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: arryaTemper[i], data: i)
            dataEntries.append(dataEntry)
            
        }
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "Weight")
        lineChartDataSet.colors = [UIColor.colorchaupbule()]
        lineChartDataSet.circleColors = [UIColor.colorchaupbule()]
        lineChartDataSet.circleHoleRadius = 3
        lineChartDataSet.circleRadius = 5.5
        lineChartDataSet.drawCirclesEnabled = true
        lineChartDataSet.lineWidth = 3
        lineChartDataSet.mode = .cubicBezier
        
        
        
        let dataSets: [LineChartDataSet] = [lineChartDataSet]
        let chartData = LineChartData(dataSets: dataSets)
        viewStatistic.data = chartData
        viewStatistic.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)
    }
    
    
}
extension TrendWeightVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indext = indexPath.row
        showChart()
        myColView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arryaName.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Tilte", for: indexPath) as! HeartCollCell
        cell.lblHeaderTitle.text = arryaName[indexPath.row]
        if indext == indexPath.row{
            cell.backgroundColor = UIColor.colorchaupOrange()
            cell.makeCornerRoundClear(redias : 6)
            cell.lblHeaderTitle.textColor = .white
        }else{
            cell.backgroundColor = nil
            cell.lblHeaderTitle.textColor = UIColor.colorchaupOrange()
        }
        return cell
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 4 , height: 41 * _widthRatio)
        
    }
}
