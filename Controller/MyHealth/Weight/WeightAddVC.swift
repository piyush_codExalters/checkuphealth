//
//  WeightAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit
class WeightList: NSObject {
    
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Weight(kg) *"
        t3.keybord = .numberPad
        t3.placeholder1 = "Diabets, Glaucoma etc..."

        
        var t5 = SingIn()
        t5.placeholder = "Date"
        t5.keybord = .numberPad
        t5.image = #imageLiteral(resourceName: "ic_calender")
        
        var t6 = SingIn()
        t6.placeholder = "Time"
        t6.keybord = .numberPad
        t6.image = #imageLiteral(resourceName: "ic_time")
        
        var t7 = SingIn()
        t7.placeholder = "Note"
        t7.keybord = .numberPad
        t7.strValue = "Add Note"
        
        allFields.append(t3)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
    }
}
class WeightAddVC: ParentViewController, UITextViewDelegate {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    var temperChart : TemperChart!
    var strHeader = ""
    var fieldData = WeightList()
    var strPlacholder = "Add Note"
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()

    let datePickerView1: UIDatePicker = UIDatePicker()
    let datefrmenter1 = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTitle.text = strHeader
        datefrmenter.dateFormat = "yyyy-MM-dd"
        datefrmenter.locale = Locale(identifier:"en_US_POSIX")

        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[1].strValue = datefrmenter.string(from: currentDate as Date)
        
        datefrmenter1.dateFormat = "hh:mm a"
        
        datefrmenter1.locale = Locale(identifier:"en_US_POSIX")

        datePickerView1.datePickerMode = UIDatePicker.Mode.time
        let currentDate1 = NSDate()
        datePickerView1.maximumDate = currentDate1 as Date
        fieldData.allFields[2].strValue = datefrmenter1.string(from: currentDate1 as Date)
        datePickerView1.addTarget(self, action: #selector(self.datePickerValueChanged1), for: UIControl.Event.valueChanged)
        
        if strHeader == "Add Weight"{
            viewBottom.frame.size.height = 100 * _widthRatio
        }else{
            viewBottom.frame.size.height = 0 * _widthRatio
            fieldData.allFields[0].strValue = temperChart.weight
            fieldData.allFields[3].strValue = temperChart.note
            fieldData.allFields[1].strValue = temperChart.date
            fieldData.allFields[2].strValue = temperChart.time
            tableView.reloadData()
        }
    }
    

    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[1].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[1].strValue
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker) {
        fieldData.allFields[2].strValue = datefrmenter1.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[2].strValue
    }

    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Note"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                //textView.text = textView.text
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[3].strValue = textView.text!
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        if validation().0{
            vital_addweightdata()
        }else{
            showFailMessage(message : validation().1)
        }
    }
}
extension WeightAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }else if indexPath.row == 1 || indexPath.row == 2 {
            return 75 * _widthRatio
        }
        
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            if indexPath.row == 0{
                cell.viewConstretTop.constant = 5
            }
            if strHeader != "Add Weight"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        if indexPath.row == 1 || indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if strHeader != "Add Weight"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            if indexPath.row == 1{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            if indexPath.row == 2{
                if #available(iOS 13.4, *) {
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.preferredDatePickerStyle = .wheels
                    datePickerView1.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView1
                }else{
                    cell.textEmail.inputView = datePickerView1
                }
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.row
        cell.textAreaNAme.text = fieldData.allFields[indexPath.row].strValue
        if strHeader != "Add Weight"{
            cell.textAreaNAme.isUserInteractionEnabled = false
        }
        return cell
    }
}

extension WeightAddVC{
    func vital_addweightdata(){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["weight"] = fieldData.allFields[0].strValue
        if fieldData.allFields[3].strValue == "Add Note"{
            fieldData.allFields[3].strValue = ""
        }
        dic["note"] = fieldData.allFields[3].strValue
        dic["date"] = fieldData.allFields[1].strValue
        dic["time"] = fieldData.allFields[2].strValue
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_addweightdata.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "WeightRelooadData"), object: nil)

                                    _ = self.navigationController?.popViewController(animated: true)
                                }else{
                                    showResponseMessage(dict: response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Enter weight")
        } else if (Float(fieldData.allFields[0].strValue)! < 20) {
            return(false, "weight should be > 20")
        }else if (Float(fieldData.allFields[0].strValue)! > 200) {
            return(false, "weight should be < 200")
        }
        return(true,"")
    }
}
