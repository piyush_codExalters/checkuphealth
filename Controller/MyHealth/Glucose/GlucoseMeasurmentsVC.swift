//
//  GlucoseMeasurmentsVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit

class GlucoseMeasurmentsVC: ParentViewController {
    var arryaBloodGlucose = [BloodGlucose]()
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action:  #selector(sortRefresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl

        NotificationCenter.default.addObserver(self, selector: #selector(self.getReloadDate), name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
       
        // Do any additional setup after loading the view.
    }
    @objc func getReloadDate(){
        vital_bgdatalist(page:"", loader: true)
    }
    
    func setDate() {
        tableView.backgroundView = nil
        if arryaBloodGlucose.count == 0{
            self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "Data Not Found.", image : nil)
        }
        tableView.reloadData()
    }
    @IBAction func btnDeltedClkced(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete?", Ohk: "YES", cancal: "NO") { [self] in
            vital_bg_del_record(page:arryaBloodGlucose[sender.tag].bloodGlucose_id)
        }
        
    }

    
    
}
extension GlucoseMeasurmentsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapVc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "GlucoseAddVC")  as! GlucoseAddVC
        mapVc1.strHeader = "Blood Glucose"
        mapVc1.bloodGlucose = arryaBloodGlucose[indexPath.row]
        self.navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 143 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryaBloodGlucose.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! MeasurementsCell
        cell.lblDate.text = arryaBloodGlucose[indexPath.row].date + ", " + arryaBloodGlucose[indexPath.row].time
        cell.lblSystolic.text = arryaBloodGlucose[indexPath.row].blood_glucose
        cell.btnDelted.tag = indexPath.row
        cell.viewColorStatus.backgroundColor = arryaBloodGlucose[indexPath.row].color

        cell.lblML.text = arryaBloodGlucose[indexPath.row].blood_glucose_unit
        if arryaBloodGlucose[indexPath.row].total_carbs == ""{
            cell.lblDiastic.text = ""
            cell.lblGr.text = ""
        }else{
            cell.lblDiastic.text = arryaBloodGlucose[indexPath.row].total_carbs
            cell.lblGr.text = arryaBloodGlucose[indexPath.row].total_carbs_unit
        }
        if arryaBloodGlucose[indexPath.row].total_energy == ""{
            cell.lblcal.text = ""
            cell.lblHeart.text = ""
        }else{
            cell.lblcal.text = arryaBloodGlucose[indexPath.row].total_energy_unit
            cell.lblHeart.text = arryaBloodGlucose[indexPath.row].total_energy
        }
        return cell
        
        
    }
    
    
    
    
}
extension GlucoseMeasurmentsVC{
    @objc func sortRefresh(){
        vital_bgdatalist(page: "", loader: false)
    }
    func vital_bgdatalist(page:String, loader:Bool){
        var dic = [String:Any]()
        dic["patient_id"] = _currentUser.patient_id
        dic["page"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        if loader{
            showCentralSpinner()
        }
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bgdatalist.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            self.refreshControl.endRefreshing()

            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                arryaBloodGlucose.removeAll()
                                tableView.backgroundView = nil
                                if Response.getIntValue(key: "success") == 1{
                                   
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            arryaBloodGlucose.append(BloodGlucose(dic: obj as! NSDictionary))
                                        }
                                    }
                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func vital_bg_del_record(page:String){
        var dic = [String:Any]()
        dic["bloodGlucose_id"] = page
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_bg_del_record.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)

                                }else{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : Response.getStringValue(key: "message"), image : nil)
                                }
                                tableView.reloadData()
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
}
