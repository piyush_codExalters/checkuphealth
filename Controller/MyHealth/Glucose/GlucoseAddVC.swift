//
//  GlucoseAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit
import DropDown
class GlucoseItemList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Medicine"
        t3.keybord = .numberPad
        
        allFields.append(t3)
    }
}
class GlucoseList: NSObject {
    
    var allFields: [SingIn] = []
    var fieldData = GlucoseItemList()
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t3 = SingIn()
        t3.placeholder = "Blood Glucose *"
        t3.keybord = .decimalPad
        
        var t8 = SingIn()
        t8.placeholder = "Meal *"
        t8.keybord = .numberPad
        t8.image = #imageLiteral(resourceName: "ic_dropdown")
        
        var t10 = SingIn()
        t10.arryPlaceholder = [String]()
        t10.keybord = .numberPad
        
        
        var t9 = SingIn()
        t9.placeholder = "Medicine *"
        t9.keybord = .numberPad
        
        
        var t5 = SingIn()
        t5.placeholder = "Other Medicine"
        t5.keybord = .default
        
        var t6 = SingIn()
        t6.placeholder = "Carbs(gr)"
        t6.keybord = .numberPad
        t6.image = #imageLiteral(resourceName: "ic_navigation_aro")
        
        var t7 = SingIn()
        t7.placeholder = "Physical Activity(kcal)"
        t7.keybord = .numberPad
        t7.image = #imageLiteral(resourceName: "ic_navigation_aro")
        
        var t2 = SingIn()
        t2.placeholder = "Date"
        t2.keybord = .numberPad
        t2.image = #imageLiteral(resourceName: "ic_calender")
        
        var t1 = SingIn()
        t1.placeholder = "Time"
        t1.keybord = .numberPad
        t1.image = #imageLiteral(resourceName: "ic_time")
        
        var t0 = SingIn()
        t0.placeholder = "Note"
        t0.keybord = .default
        t0.strValue = "Add Note"

        
        allFields.append(t3)
        allFields.append(t8)
        allFields.append(t10)
        allFields.append(t9)
        allFields.append(t5)
        allFields.append(t6)
        allFields.append(t7)
        allFields.append(t2)
        allFields.append(t1)
        allFields.append(t0)
    }
}
class GlucoseCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblName: LabelRegular!
    @IBOutlet weak var textEmail: JPWidthTextField!
    @IBOutlet weak var viewConstretBottom: NSLayoutConstraint!
    @IBOutlet weak var viewConstretTop: NSLayoutConstraint!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var imageCalander: UIImageView!
    @IBOutlet weak var viewText2: UIView!
    @IBOutlet weak var textDrop: JPWidthTextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnMedicationType: UIButton!
    @IBOutlet weak var btnMedication: UIButton!
    @IBOutlet weak var btndrop: UIButton!
    @IBOutlet weak var addWidhtRemove: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBg.makeCornerRound(redias : 6)
        viewText.makeCustomRound(radius:6,bc:.black)
        viewText2.makeCustomRound(radius:6,bc:.black)
        
    }
}


class GlucoseAddVC: ParentViewController, UITextViewDelegate {
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    var bloodGlucose : BloodGlucose!
    
    var strHeader = ""
    var fieldData = GlucoseList()
    
    var strPlacholder = "Add Note"
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    
    let datePickerView1: UIDatePicker = UIDatePicker()
    let datefrmenter1 = DateFormatter()
    
    var arryBloodGlucose = ["mmol/L","mg/dL"]
    var arryMeal = ["Before-Meal","After-Meal","Fasting","90 min after the meal"]
    var arrymedicineType = ["Oral","Insulin","Injection"]
    var arrymedicine = ["No medication/diet controlled","Metformin","Sulfonylureas eg gliclazide, glibenclamide, glimepiride, tolbutamide","Meglitinides, eg nateglinide, repaglinide","Thiazolidinedione eg pioglitazone","Dipeptidylpeptidase-4 inhibitors (gliptins) eg alogliptin, linagliptin, sitagliptin","Sodium glucose co-transporter 2 inhibitors eg canagliflozin, dapagliflozin, empagliflozin","Glucagon-like peptide-1 receptor agonists eg dulaglutide, exenatide, liraglutide","Acarbose"]
    let dropDown = DropDown()
    let dropDowna = DropDown()
    let dropDownamedicineType = DropDown()
    let dropDownamedicine = DropDown()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTitle.text = strHeader
        datefrmenter.dateFormat = "yyyy-MM-dd"
        datefrmenter.locale = Locale(identifier:"en_US_POSIX")

        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[7].strValue = datefrmenter.string(from: currentDate as Date)
        
        datefrmenter1.dateFormat = "hh:mm a"
        datefrmenter1.locale = Locale(identifier:"en_US_POSIX")

        datePickerView1.datePickerMode = UIDatePicker.Mode.time
        let currentDate1 = NSDate()
        datePickerView1.maximumDate = currentDate1 as Date
        fieldData.allFields[8].strValue = datefrmenter1.string(from: currentDate1 as Date)
        datePickerView1.addTarget(self, action: #selector(self.datePickerValueChanged1), for: UIControl.Event.valueChanged)
        
        
        if strHeader == "Add Blood Glucose"{
            viewBottom.frame.size.height = 100 * _widthRatio
            fieldData.allFields[0].strValue1 = arryBloodGlucose[0]
            fieldData.allFields[1].strValue = arryMeal[0]
            fieldData.fieldData.allFields[0].strValue1 = arrymedicine[0]
            fieldData.fieldData.allFields[0].strValue = arrymedicineType[0]
        }else{
            viewBottom.frame.size.height = 0 * _widthRatio
            var t4 = SingIn()
            t4.placeholder = "Medicine"
            t4.keybord = .numberPad
            fieldData.allFields[0].strValue = bloodGlucose.blood_glucose
            fieldData.allFields[0].strValue1 = bloodGlucose.blood_glucose_unit
            fieldData.allFields[1].strValue = bloodGlucose.bloodGlucose_meal
            fieldData.allFields[5].strValue = bloodGlucose.total_carbs
            fieldData.allFields[6].strValue = bloodGlucose.total_energy
            fieldData.allFields[9].strValue = bloodGlucose.note
            fieldData.allFields[7].strValue = bloodGlucose.date
            fieldData.allFields[8].strValue = bloodGlucose.time
            for obj in bloodGlucose.arryaMedicine{
                fieldData.fieldData.allFields.append(t4)
                if obj.medicine_type == "Other"{
                    fieldData.allFields[4].strValue = obj.medicine_name
                }else{
                    t4.strValue1 = obj.medicine_name
                    t4.strValue = obj.medicine_type
                }
            }
            fieldData.fieldData.allFields.remove(at: 0)
            tableView.reloadData()
            
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[7].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 7))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[7].strValue
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker) {
        fieldData.allFields[8].strValue = datefrmenter1.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 8))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[8].strValue
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Note"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.Maven_pro_Medium(size: 14)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.textColor == UIColor.black {
            if textView.text == "Add Note"{
                textView.text = nil
                textView.font = UIFont.Maven_pro_Medium(size: 14)
            }else{
                textView.font = UIFont.Maven_pro_Medium(size: 14)
                textView.text = strPlacholder
            }
            textView.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        fieldData.allFields[9].strValue = textView.text!
    }
    
    @IBAction func textdrtf(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue1 = sender.text!
        
    }
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        if strHeader != "Add Blood Glucose"{
            return
        }
        fieldData.fieldData.allFields.remove(at: sender.tag)
        tableView.reloadData()
    }
    @IBAction func btnAddMedicialClicked(_ sender: Any) {
        if strHeader != "Add Blood Glucose"{
            return
        }
        

        
        
        var t4 = SingIn()
        t4.placeholder = "Medicine"
        t4.keybord = .numberPad
        t4.strValue1 = arrymedicine[0]
        t4.strValue = arrymedicineType[0]
        
        fieldData.fieldData.allFields.append(t4)
        tableView.reloadData()
    }
    @IBAction func btnDrop(_ sender: Any) {
        if strHeader != "Add Blood Glucose"{
            return
        }
        getdrop(ayya:arryBloodGlucose,widht : 104, sropDown: dropDowna)
        dropDowna.selectionAction = { [unowned self] (index: Int, item: String) in
            fieldData.allFields[0].strValue1 = arryBloodGlucose[index]
            self.tableView.reloadData()
        }
        self.view.endEditing(true)
        self.dropDowna.show()
    }
    
    
    @IBAction func btnMedicationClicked(_ sender: UIButton) {
        if strHeader != "Add Blood Glucose"{
            return
        }
        getdrop(ayya:arrymedicine,widht : Float(_screenSize.width), sropDown: dropDownamedicine)
        dropDownamedicine.selectionAction = { [unowned self] (index: Int, item: String) in
            fieldData.fieldData.allFields[sender.tag].strValue1 = arrymedicine[index]
            self.tableView.reloadData()
        }
        self.view.endEditing(true)
        self.dropDownamedicine.show()
    }
    
    @IBAction func btnMedicationTypeClicked(_ sender: UIButton) {
        if strHeader != "Add Blood Glucose"{
            return
        }
        getdrop(ayya:arrymedicineType,widht : 114, sropDown: dropDownamedicineType)
        dropDownamedicineType.selectionAction = { [unowned self] (index: Int, item: String) in
            fieldData.fieldData.allFields[sender.tag].strValue = arrymedicineType[index]
            self.tableView.reloadData()
        }
        self.view.endEditing(true)
        self.dropDownamedicineType.show()
    }
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        if validation().0{
            vital_addgldata()
        }else{
            showFailMessage(message : validation().1)
        }
    }
    
}
extension GlucoseAddVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if strHeader != "Add Blood Glucose"{
            return
        }
        if indexPath.section == 1{
            getdrop(ayya:arryMeal,widht : 300, sropDown: dropDown)
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[indexPath.section].strValue = arryMeal[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropDown.show()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 90 * _widthRatio
        }else if indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 || indexPath.section == 7 || indexPath.section == 8{
            return 75 * _widthRatio
        }
        if indexPath.section == 3{
            if strHeader == "Add Blood Glucose"{
                return 33 * _widthRatio
            }else{
                return 0
            }
        }
        return 150 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2{
            return fieldData.fieldData.allFields.count
        }
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Glucose") as! GlucoseCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretBottom.constant = -5
            cell.viewConstretTop.constant = 5
            cell.textDrop.isUserInteractionEnabled = false
            cell.textDrop.text = fieldData.allFields[indexPath.section].strValue1
            cell.textDrop.tag = indexPath.section
            dropDowna.anchorView = cell.textDrop
            if strHeader != "Add Blood Glucose"{
                cell.textDrop.isUserInteractionEnabled = false
                cell.btndrop.isUserInteractionEnabled = false
            }
            return cell
            
        }
        if  indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            cell.textEmail.isUserInteractionEnabled = false
            cell.textEmail.inputView = nil
            dropDown.anchorView = cell.textEmail
            if strHeader != "Add Blood Glucose"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        if  indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddItem") as! GlucoseCell
            cell.textEmail.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.fieldData.allFields[indexPath.row].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretBottom.constant = -5
            cell.viewConstretTop.constant = -5
            cell.textEmail.isUserInteractionEnabled = false
            cell.textDrop.isUserInteractionEnabled = false
            cell.textDrop.text = fieldData.fieldData.allFields[indexPath.row].strValue1
            cell.textDrop.tag = indexPath.row
            dropDownamedicineType.anchorView = cell.btnMedicationType
            dropDownamedicine.anchorView = cell.btnMedication
            cell.btnMedication.tag = indexPath.row
            cell.btnMedicationType.tag = indexPath.row
            if strHeader == "Add Blood Glucose"{
                cell.addWidhtRemove.constant = 35
            }else{
                cell.addWidhtRemove.constant = 0
            }
            return cell
            
        }
        if  indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Information") as! HomeCell
            cell.lblInformation.text = ""
            cell.btnViewAll.isHidden = false
            let FormattedText1 = NSMutableAttributedString()
            FormattedText1
                .unlink("Add Medicine")
            cell.btnViewAll.setAttributedTitle(FormattedText1, for: .normal)
            cell.viewBG.makeCornerRound(redias : 6)
            
            return cell
        }
        if  indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.textEmail.inputView = nil
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.textEmail.isUserInteractionEnabled = true
            if strHeader != "Add Blood Glucose"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        if  indexPath.section == 5 || indexPath.section == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            cell.textEmail.inputView = nil
            cell.textEmail.isUserInteractionEnabled = true
            if strHeader != "Add Blood Glucose"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            return cell
        }
        
        if indexPath.section == 7 || indexPath.section == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.section
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.section].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.section].strValue
            cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
            cell.textEmail.isUserInteractionEnabled = true
            if strHeader != "Add Blood Glucose"{
                cell.textEmail.isUserInteractionEnabled = false
            }
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.section].image
            if indexPath.section == 7{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
            }
            if indexPath.section == 8{
                if #available(iOS 13.4, *) {
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView1.preferredDatePickerStyle = .wheels
                    datePickerView1.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView1
                }else{
                    cell.textEmail.inputView = datePickerView1
                }
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textArea") as! ProfileCell
        cell.lblName.text = fieldData.allFields[indexPath.section].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = 5
        cell.textAreaNAme.tag = indexPath.section
        cell.textAreaNAme.text = fieldData.allFields[indexPath.section].strValue
        if strHeader != "Add Blood Glucose"{
            cell.textAreaNAme.isUserInteractionEnabled = false
        }
        return cell
    }
    
    func getdrop(ayya:Array<String>,widht : Float, sropDown : DropDown){
        sropDown.dataSource = ayya
        sropDown.direction = .bottom
        sropDown.bottomOffset = CGPoint(x: 0, y:(sropDown.anchorView?.plainView.bounds.height)!)
        sropDown.width = CGFloat(widht) * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 60 * _widthRatio
        
    }
}


extension GlucoseAddVC{
    func vital_addgldata(){
        var insulin = ""
        var dic = [String:Any]()
        var arryaType = [String]()
        var arryaname = [String]()
        for obj in fieldData.fieldData.allFields{
            if obj.strValue == "Insulin"{
                insulin = obj.strValue
            }
            arryaType.append(obj.strValue)
            arryaname.append(obj.strValue1)
        }
        if !String.validateStringValue(str: fieldData.allFields[4].strValue){
            arryaType.append("Other")
            arryaname.append(fieldData.allFields[4].strValue)
        }
        
        dic["patient_id"] = _currentUser.patient_id
        dic["patient_type"] = _currentUser.patient_type
        dic["blood_glucose"] = fieldData.allFields[0].strValue
        dic["unit"] = fieldData.allFields[0].strValue1
        dic["meal"] = fieldData.allFields[1].strValue
        dic["total_carbs"] = fieldData.allFields[5].strValue
        dic["total_energy"] = fieldData.allFields[6].strValue
        dic["total_carbs_unit"] = "gr"
        dic["total_energy_unit"] = "kcal"
        if fieldData.allFields[9].strValue == "Add Note"{
            fieldData.allFields[9].strValue = ""
        }
        dic["note"] = fieldData.allFields[9].strValue
        dic["date"] = fieldData.allFields[7].strValue
        dic["time"] = fieldData.allFields[8].strValue
        dic["medicine_type"] = arryaType
        dic["medicine_name"] = arryaname
        dic["food"] = ""
        dic["physical_activity"] = ""
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "vital_addgldata.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if fieldData.allFields[0].strValue1 == "mg/dL"{
                                        fieldData.allFields[0].strValue = "\((Float(fieldData.allFields[0].strValue)! *  0.0555))"
                                    }
                                    if insulin == "Insulin"{
                                        
                                        if   Float(fieldData.allFields[0].strValue)! < 4{
                                            
                                            showAlert1(title: "", msgString: "Hypogylcaemia. Eat something sugary immediately. Follow personalised plan if feeling unwell. If you feel very unwell or your levels are not coming up call 111 or 999", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("Before-Meal") && Float(fieldData.allFields[0].strValue)! >= 4 && Float(fieldData.allFields[0].strValue)! <= 7{
                                            
                                            showAlert1(title: "", msgString: "Optimal Range", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("Before-Meal") && Float(fieldData.allFields[0].strValue)! >= 7 && Float(fieldData.allFields[0].strValue)! <= 20{
                                            
                                            showAlert1(title: "", msgString: "Monitor over a couple of days. If feeling unwell, or sugars rising book urgent GP appointment.", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("90 min after the meal") && Float(fieldData.allFields[0].strValue)! <= 8.5{
                                            
                                            showAlert1(title: "", msgString: "Optimal Range", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("90 min after the meal") && Float(fieldData.allFields[0].strValue)! >= 8.5 && Float(fieldData.allFields[0].strValue)! <= 20{
                                            
                                            showAlert1(title: "", msgString: "Monitor over a couple of days. If feeling unwell, or sugars rising book urgent GP appointment.", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if Float(fieldData.allFields[0].strValue)! > 20{
                                            
                                            showAlert1(title: "Warning", msgString: "High Blood Sugars. Contact GP/111. Call 999 if feeling very unwell", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else{
                                            _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                        
                                    }else{
                                        
                                        if  Float(fieldData.allFields[0].strValue)! < 2.5{
                                            
                                            showAlert1(title: "", msgString: "Hypogyclaemia: Call 111/999 if feeling unwell. Eat something sugary immediately", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if Float(fieldData.allFields[0].strValue)! >= 2.5 && Float(fieldData.allFields[0].strValue)! <= 4{
                                            
                                            showAlert1(title: "", msgString: "Possible Hypogylcaemia. Follow personalised plan if feeling unwell. If you feel very unwell or your levels are not coming up call 111/GP or 999", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("Before-Meal") && Float(fieldData.allFields[0].strValue)! >= 4 && Float(fieldData.allFields[0].strValue)! <= 7{
                                            
                                            showAlert1(title: "", msgString: "Optimal Range", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("Before-Meal") && Float(fieldData.allFields[0].strValue)! >= 7 && Float(fieldData.allFields[0].strValue)! <= 20{
                                            
                                            showAlert1(title: "", msgString: "Monitor over a couple of days. If feeling unwell, or sugars rising book urgent GP appointment.", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("90 min after the meal") && Float(fieldData.allFields[0].strValue)! <= 8.5{
                                            
                                            showAlert1(title: "", msgString: "Optimal Range", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if fieldData.allFields[1].strValue.contains("90 min after the meal") && Float(fieldData.allFields[0].strValue)! >= 8.5 && Float(fieldData.allFields[0].strValue)! <= 20{
                                            
                                            showAlert1(title: "", msgString: "Monitor over a couple of days. If feeling unwell, or sugars gradually book urgent GP appointment.", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else if  Float(fieldData.allFields[0].strValue)! > 20{
                                            
                                            showAlert1(title: "Warning", msgString: "High Blood Sugars. Contact GP/111. Call 999 if feeling very unwell", Ohk: "OK") {
                                                _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                                _ = self.navigationController?.popViewController(animated: true)
                                            }
                                            
                                        }else{
                                            _defaultCenter.post(name: NSNotification.Name(rawValue: "GlucoesRelooadData"), object: nil)
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                        
                                    }
                                    
                                }else{
                                    showResponseMessage(dict: response)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Enter blood glucose")
        }
        return(true,"")
    }
}
