//
//  GuidelinesVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 14/04/21.
//

import UIKit
class GuidelinesCollCell: ConstrainedCollectionViewCell {
    @IBOutlet weak var lblDescption: LabelMedium!
    @IBOutlet weak var imageCancel: UIImageView!
    @IBOutlet weak var lbblNumber: LabelSemiBold!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
class GuidelinesVC: ParentViewController {
    

    @IBOutlet weak var btnRetun: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var pageCounter: UIPageControl!
    var arrZW: [[Any]] = []
    var arrUK: [[Any]] = []
    
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        arrZW.append(["zw_into1","Home","Easily book an appointment at a convenient time and view CheckUp Health's contact details."])
        arrZW.append(["zw_into2","Questions","Ask questions to get reply from doctor/nurse, depends on their interest and availability."])
        arrZW.append(["zw_into3","Appointments","Quickly view any upcoming or completed appointments"])
        arrZW.append(["zw_into4","My PHR","My Personal Health Record - to record all of your medications, medical conditions and other health information."])
        arrZW.append(["zw_into5","My Health","Record and monitor your blood pressure or blood sugars with real time interactive information about your results."])
        
        arrUK.append(["uk_into1","Home","Easily book an appointment at a convenient time and view CheckUp Health's contact details."])
        arrUK.append(["uk_into2","Appointments","Quickly view any upcoming or completed appointments"])
        arrUK.append(["uk_into3","My PHR","My Personal Health Record - to record all of your medications, medical conditions and other health information."])
        arrUK.append(["UK_into4","My Health","Record and monitor your blood pressure or blood sugars with real time interactive information about your results."])
        self.pageCounter.numberOfPages = Language() == true ? arrZW.count : arrUK.count
        btnSkip.setTitle("Skip", for: .normal)
        btnRetun.setTitle("", for: .normal)
        let image = UIImage(named: "ic_back")!
        let newImage = image.flipHorizontally()
        btnRetun.setImage(newImage?.maskWithColor(color: UIColor.colorchaupbule()), for: .normal)
        getFirebaseAnalytics(screenName : getEventName.INTRODUCTION_SLIDER,isPatient_id : true)
    }
    @IBAction func btnRetrunClicked(_ sender: Any) {
        let collectionBounds = self.myColView.bounds
        let contentOffset = CGFloat(floor(self.myColView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    

    @IBAction func btnSkipClicked(_ sender: Any) {
        let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
        self.navigationController?.pushViewController(vclogin, animated: true)
        
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.myColView.contentOffset.y ,width : self.myColView.frame.width,height : self.myColView.frame.height)
        let visiblePoint = CGPoint(x: frame.midX, y: frame.midY)
        if let visibleIndexPath = self.myColView.indexPathForItem(at: visiblePoint) {
            
            let arryWay = Language() == true ? arrZW.count : arrUK.count
            
            if visibleIndexPath.row == arryWay - 1{
                btnRetun.setTitle("Finish", for: .normal)
                btnRetun.setImage(nil, for: .normal)
            }else{
                btnRetun.setTitle("", for: .normal)
                let image = UIImage(named: "ic_back")!
                let newImage = image.flipHorizontally()
                btnRetun.setImage(newImage?.maskWithColor(color: UIColor.colorchaupbule()), for: .normal)
            }
            self.pageCounter.currentPage = visibleIndexPath.row
        }else{
            let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
            self.navigationController?.pushViewController(vclogin, animated: true)
            
        }
        self.myColView.scrollRectToVisible(frame, animated: true)
        
    }
}
extension GuidelinesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  Language() == true ? arrZW.count : arrUK.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GuidelinesCollCell
        cell.imageCancel.image = UIImage(named: Language() == true ? arrZW[indexPath.row][0] as! String : arrUK[indexPath.row][0] as! String)
        cell.lbblNumber.text = Language() == true ? arrZW[indexPath.row][1] as! String : arrUK[indexPath.row][1] as! String
        cell.lblDescption.text = Language() == true ? arrZW[indexPath.row][2] as! String : arrUK[indexPath.row][2] as! String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width , height: collectionView.frame.size.height - (view.safeAreaInsets.top + view.safeAreaInsets.bottom))
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.myColView.contentOffset, size: self.myColView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.myColView.indexPathForItem(at: visiblePoint) {
            let arryWay = Language() == true ? arrZW.count : arrUK.count

            if visibleIndexPath.row == arryWay - 1{
                btnRetun.setTitle("Finish", for: .normal)
                btnRetun.setImage(nil, for: .normal)
            }else{
                btnRetun.setTitle("", for: .normal)
                let image = UIImage(named: "ic_back")!
                let newImage = image.flipHorizontally()
                btnRetun.setImage(newImage?.maskWithColor(color: UIColor.colorchaupbule()), for: .normal)
            }
            self.pageCounter.currentPage = visibleIndexPath.row
        }
    }
     func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
      return layoutAttributes
    }
    
   
}
extension UIImage {
    func flipHorizontally() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        context.translateBy(x: self.size.width/2, y: self.size.height/2)
        context.scaleBy(x: -1.0, y: 1.0)
        context.translateBy(x: -self.size.width/2, y: -self.size.height/2)
        
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}

extension UIImage {

    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!

        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!

        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)

        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }

}
