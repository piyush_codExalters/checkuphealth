//
//  OrganizationPopVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 14/04/21.
//

import UIKit

class OrganizationPopVC: ParentViewController {
    @IBOutlet weak var viewBg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias : 6)
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   

}
