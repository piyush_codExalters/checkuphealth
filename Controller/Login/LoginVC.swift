//
//  LoginVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 08/04/21.
//

import UIKit
class LoginList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t4 = SingIn()
        t4.placeholder = "Email or Mobile number"
        t4.isSecure = false
        t4.keybord = .emailAddress
        t4.strValue = ""
        
        var t5 = SingIn()
        t5.placeholder = "Password"
        t5.isSecure = true
        t5.strValue = ""
        
        
        allFields.append(t4)
        allFields.append(t5)
        
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Enter Valid Email id or Mobile Number")
        }else  if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Password can't be blank")
        }
        return(true,"")
    }
}
class LoginCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var lblCountryCode: LabelRegular!
    @IBOutlet weak var textEmail: JPWidthTextField!
    @IBOutlet weak var viewConstretBottom: NSLayoutConstraint!
    @IBOutlet weak var viewConstretTop: NSLayoutConstraint!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblName: LabelRegular!
    @IBOutlet weak var btnContory: UIButton!
    @IBOutlet weak var btnEya: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "custText" || reuseIdentifier == "custText1" || reuseIdentifier == "custText2"{
            viewText.makeCustomRound(radius:6,bc:.black)
        }
        if reuseIdentifier == "custTextConact"{
            viewText.makeCustomRound(radius:6,bc:.black)
        }
    }
}
class LoginVC: ParentViewController {
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var btnSingup: UIButton!
    @IBOutlet weak var imageIcon: UIImageView!
    
    var fieldData = LoginList()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTop.frame.size.height = 480 * _widthRatio
        viewBottom.frame.size.height = 207 * _widthRatio
        if isWorkingCountryUK(){
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }else{
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }
        let FormattedText = NSMutableAttributedString()
        FormattedText
            .forgetpassword("Forgot password?")
        self.btnForgetPassword.setAttributedTitle(FormattedText, for: .normal)
        
        
        let FormattedText1 = NSMutableAttributedString()
        FormattedText1
            .sinup("Not a member? ")
            .forgetpassword("Sign up")
        self.btnSingup.setAttributedTitle(FormattedText1, for: .normal)
    }
    
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "forgetpaswword"{
            let vc = segue.destination as! ForgetPasswordVC
            vc.isPin = false
        }
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    @IBAction func btnForgetPasswordClicked(_ sender: Any) {
        
        self.performSegue(withIdentifier: "forgetpaswword", sender: nil)
        
    }
    @IBAction func btnSignupClicked(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func btnLoginClicekd(_ sender: Any) {
        if fieldData.validation().0{
            getsignin()
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
    }
    
}
extension LoginVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "custText") as! LoginCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
        return cell
        
    }
    
   
    
}
extension LoginVC{
    func getsignin(){
        let uuid = UUID().uuidString
        var strPush = ""
              if let push = _userDefault.value(forKey: "vpushToken") as? String{
                  strPush = push
              }
//        if fieldData.allFields[0].strValue.prefix(1) == "0"{
//            let result1 = String(fieldData.allFields[0].strValue.dropFirst())    // "ello"
//            fieldData.allFields[0].strValue = result1
//        }
        let parameters = "{\"data\":{\"email\":\"\(fieldData.allFields[0].strValue)\",\"password\":\"\(fieldData.allFields[1].strValue)\",\"fcm_token\":\"\(strPush)\", \"macAddress\":\"\(uuid)\", \"nDeviceLoginType\":\"2\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "signin.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        DispatchQueue.main.async {
                                            _currentUser = UserDetail(dic: dataname)
                                            KPWebCall.call.setClientApiKey(token: _currentUser.vApiToken)
                                            print (self.getSaveData(dataname : dataname))
                                            _userDefault.set(0, forKey: "nUserType")
                                            _userDefault.set(self.getSaveData(dataname : dataname), forKey: "userInfo")
                                            _userDefault.setValue(_currentUser.patient_id, forKey: "OldPatientId")
                                            _userDefault.setValue(dataname.getStringValue(key: "patient_referralcode"), forKey: "patient_referralcode")
                                            _userDefault.synchronize()
                                            self.getFirebaseAnalytics(screenName : self.getEventName.LOGIN,isPatient_id : true)
                                            if _currentUser.isMobileVerified == "0"{
                                                let otp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                                                
                                                self.navigationController?.pushViewController(otp, animated: true)
                                            }else if _currentUser.isOnboarding == "0"{
                                                
                                                let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                                                self.navigationController?.pushViewController(vclogin, animated: true)
//                                                let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//                                                profile.allData = self.getSaveData(dataname : dataname)
//                                                self.navigationController?.pushViewController(profile, animated: true)
                                            }else if _currentUser.nPin == ""{
                                                let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetAppLockVC") as! SetAppLockVC
                                                profile.allData = self.getSaveData(dataname : dataname)
                                                self.navigationController?.pushViewController(profile, animated: true)
                                            }else{
                                                
                                                    let vclogin = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "SwitchAccountVC") as! SwitchAccountVC
                                                    vclogin.isSetting = false
                                                    self.navigationController?.pushViewController(vclogin, animated: true)
                                                    
//                                                let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
//                                                self.navigationController?.pushViewController(vclogin, animated: true)
                                                
                                            }
                                        }
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}


