//
//  ScreenPinVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 07/05/21.
//

import UIKit

class ScreenPinVC: ParentViewController {
    
    @IBOutlet weak var textFour: JPWidthTextField!
    @IBOutlet weak var textThree: JPWidthTextField!
    @IBOutlet weak var textTwo: JPWidthTextField!
    @IBOutlet weak var textONe: JPWidthTextField!
    @IBOutlet weak var lblError: LabelSemiBold!
    @IBOutlet weak var lblsetTitlel: LabelSemiBold!
    var arryaNumber = ["1","2","3","4","5","6","7","8","9","","0",""]
    var setPin = ""
    var isAgen = false
    var allData = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblsetTitlel.text = "Enter Your Pin"
        textFour.makeCornecscrRoun1(redias : 4)
        textThree.makeCornecscrRoun1(redias : 4)
        textTwo.makeCornecscrRoun1(redias : 4)
        textONe.makeCornecscrRoun1(redias : 4)
        lblError.text = " "
    }

    @IBAction func btnNumberClickrd(_ sender: UIButton) {
        if sender.tag == 9{
            return
        }
        if sender.tag == 11{
            if !String.validateStringValue(str: textFour.text){
                textFour.text = ""
            }else if !String.validateStringValue(str: textThree.text){
                textThree.text = ""
            }else if !String.validateStringValue(str: textTwo.text){
                textTwo.text = ""
            }else if !String.validateStringValue(str: textONe.text){
                textONe.text = ""
            }
            return
        }
        if String.validateStringValue(str: textONe.text){
            textONe.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textTwo.text){
            textTwo.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textThree.text){
            textThree.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textFour.text){
            textFour.text = arryaNumber[sender.tag]
            setPin = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
            if setPin != _currentUser.nPin{
                print(_currentUser.nPin)
                lblError.text = "Wrong Pin"
            }else{
                lblError.text = " "
                let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                nav.viewControllers = [vclogin]
                _appDelegator.window?.rootViewController = nav
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "forgetpaswword"{
            let vc = segue.destination as! ForgetPasswordVC
            vc.isPin = true
        }
    }
    
    @IBAction func btnForgetPasswordClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "forgetpaswword", sender: nil)
    }
}

extension ScreenPinVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryaNumber.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 11{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CancelIcon", for: indexPath) as! PinCollCell
            cell.btnNumber.tag = indexPath.row
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Number", for: indexPath) as! PinCollCell
        cell.lbblNumber.text = arryaNumber[indexPath.row]
        cell.btnNumber.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 3 , height: 84 * _widthRatio)
    }
    
}
