//
//  OTPVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 08/04/21.
//

import UIKit
import Foundation

class otpList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t4 = SingIn()
        t4.placeholder = "Enter your OTP"
        t4.isSecure = true
        t4.keybord = .numberPad
        t4.strValue = ""
        
        
        allFields.append(t4)
        
    }
    
    
}
extension String{



  func regex(pattern: String) -> [String]?{
    do {
        let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
        let all = NSRange(location: 0, length: count)
        var matches = [String]()
        regex.enumerateMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) {
            (result : NSTextCheckingResult?, _, _) in
              if let r = result {
                    let nsstr = self as NSString
                    let result = nsstr.substring(with: r.range) as String
                    matches.append(result)
              }
        }
        return matches
    } catch {
        return nil
    }
  }
}
class OTPVC: ParentViewController {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnSingup: UIButton!
    @IBOutlet weak var lblForgot: LabelMedium!
    @IBOutlet weak var lblHeader: LabelMedium!
    @IBOutlet weak var imageIcon: UIImageView!

    var fieldData = otpList()
    var strNumber = ""
    var strOTP = ""
    var vMobileNumber = ""
    var vMobileConctroy = ""

    var isUpdateNumber = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isWorkingCountryUK(){
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }else{
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }
        viewTop.frame.size.height = 462 * _widthRatio
        viewBottom.frame.size.height = 146 * _widthRatio
        
        lblForgot.text = "Verify OTP"
        if isUpdateNumber{
            

           

            var numberNew = ""
            numberNew = vMobileNumber
            for chater in vMobileNumber {
                
                if numberNew.prefix(1) == "0"{
                    let result1 = String(numberNew.dropFirst())    // "ello"
                    numberNew = result1
                }
                
            }


            lblHeader.text = "Please enter OTP sent to \(vMobileConctroy + " " + "\(numberNew)")"

        }else{
            lblHeader.text = "Please enter OTP sent to \(_currentUser.patient_countrycode + " " + "\(_currentUser.patient_phone!)")"

        }
    }
    
    
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    
    @IBAction func btnLoginClicekd(_ sender: Any) {
        if validation().0{
            if isUpdateNumber{
                if strOTP == fieldData.allFields[0].strValue{
                    getupdateMobile(key : "newmobile",value: vMobileConctroy + "," + vMobileNumber)
                }else{
                    showFailMessage(message: "OTP Fail")
                }
            }else{
                getOtpVerification()
            }
            
        }else{
            showFailMessage(message : validation().1)
        }
    }
    
    
    
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Enter OTP")
        }else if fieldData.allFields[0].strValue.count <= 4{
            return(false,"Please enter 5 digit OTP")
        }
        return(true,"")
    }
}
extension OTPVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "custText") as! LoginCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
        return cell
        
    }
    
    
    
    
}
extension OTPVC{
    func getOtpVerification(){
        let parameters = "{\"data\":{\"patient_id\":\"\(_currentUser.patient_id!)\",\"otp_code\":\"\(fieldData.allFields[0].strValue)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "OtpVerification.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    
                                    
                                    if let dataname = Response["data"] as? NSDictionary{
                                        DispatchQueue.main.async { [self] in
                                            _currentUser = UserDetail(dic: dataname)
                                            KPWebCall.call.setClientApiKey(token: _currentUser.vApiToken)
                                            _currentUser.patient_email_address = dataname.getStringValue(key: "patient_email_address")
                                            _currentUser.patient_id = dataname.getStringValue(key: "patient_id")
                                            _currentUser.patient_phone = dataname.getStringValue(key: "mobile")
                                            _currentUser.patient_first_name = dataname.getStringValue(key: "patient_fname")
                                            _currentUser.patient_last_name = dataname.getStringValue(key: "patient_lname")
                                            _currentUser.patient_countrycode = dataname.getStringValue(key: "vCountryCode")
                                            _userDefault.set(0, forKey: "nUserType")
                                            _userDefault.setValue(dataname.getStringValue(key: "patient_referralcode"), forKey: "patient_referralcode")

                                            _userDefault.setValue(_currentUser.patient_id, forKey: "OldPatientId")

                                            print (self.getSaveData(dataname : dataname))
                                            getFirebaseAnalytics(screenName : getEventName.SIGNUP,isPatient_id : true)
                                            _userDefault.set(self.getSaveData(dataname : dataname), forKey: "userInfo")
                                            _userDefault.synchronize()
                                            let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetAppLockVC") as! SetAppLockVC
                                            profile.allData = self.getSaveData(dataname : dataname)
                                            self.navigationController?.pushViewController(profile, animated: true)
                                        }
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    func getupdateMobile(key : String,value: String){
        var strLanguage = ""
        if let obj = _userDefault.value(forKey: "Language") as? String{
            strLanguage = obj
        }
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{

                                    _currentUser.patient_type = dataname.getStringValue(key: "patient_email_address")
                                      _currentUser.patient_phone = dataname.getStringValue(key: "patient_phone")
                                      _currentUser.patient_countrycode = dataname.getStringValue(key: "patient_countrycode")
                                    _ = self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
