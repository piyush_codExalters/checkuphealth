//
//  SignupVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 08/04/21.
//

import UIKit
import SKCountryPicker
import FBSDKCoreKit
import FirebaseAnalytics
import DropDown
class SingInList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t1 = SingIn()
        t1.placeholder = "First Name"
        t1.keybord = .default
        t1.isSecure = false

        
        var t2 = SingIn()
        t2.placeholder = "Last Name"
        t2.keybord = .default
        t2.isSecure = false
        
        var t6 = SingIn()
        t6.placeholder = "Sex"
        t6.keybord = .default
        t6.image = #imageLiteral(resourceName: "ic_dropdown")
        t6.isSecure = false

        var t7 = SingIn()
        t7.placeholder = "Date of Birth"
        t7.keybord = .default
        t7.image = #imageLiteral(resourceName: "ic_calender")
        t7.isSecure = false
        
        var t20 = SingIn()
        t20.placeholder = "Country"
        t20.keybord = .default
        t20.image = #imageLiteral(resourceName: "ic_dropdown")
        t20.isSecure = false

        
        var t3 = SingIn()
        t3.placeholder = "Mobile Number"
        let country = CountryManager.shared.currentCountry
        t3.strkey = country!.countryCode
        t3.strcode = (country?.dialingCode)!
        t3.image1 = country!.flag
        t3.keybord = .numberPad
        t3.isSecure = false

        
        var t4 = SingIn()
        t4.placeholder = "Email Address"
        t4.isSecure = false
        t4.keybord = .emailAddress
        t4.strValue = ""
        
        var t5 = SingIn()
        t5.placeholder = "Password"
        t5.isSecure = true
        t5.keybord = .numbersAndPunctuation
        t5.strkey = "1"
        t5.image = #imageLiteral(resourceName: "ic_eye_close")
        t5.image1 = #imageLiteral(resourceName: "ic_eye_open")
        
       
        
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t6)
        allFields.append(t7)
        allFields.append(t20)
        allFields.append(t3)
        allFields.append(t4)
        allFields.append(t5)
        

    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please Enter First Name")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please Enter Last Name")
        }else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please Select Sex")
        }else if String.validateStringValue(str: allFields[3].strValue){
            return(false,"Please Select Birth Date")
        }else if String.validateStringValue(str: allFields[5].strValue){
            return(false,"Please Enter contact number")
        }else if String.validateStringValue(str: allFields[6].strValue){
            return(false,"Please Enter Email Address")
        }else if !allFields[6].strValue.isValidEmailAddress(){
            return(false,"Please Enter Valid Email Address")
        }else if String.validateStringValue(str: allFields[7].strValue){
            return(false,"Please Enter Password")
        }
        
        
        return(true,"")
    }
}
class SignupVC: ParentViewController {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblTreamCondition: LabelMedium!
    @IBOutlet weak var imageIcon: UIImageView!

    var fieldData = SingInList()
    var allData = NSDictionary()
    let dropCity = DropDown()
    let dropGender = DropDown()
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()

    var city = ["Zimbabwe", "United Kingdom"]
    var cityCode = ["ZW","GB"]
    var arryaGender = ["Male", "Female","Not listed","Prefer not to say"]

    override func viewDidLoad() {
        super.viewDidLoad()
        if isWorkingCountryUK(){
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }else{
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }
        viewTop.frame.size.height = 423 * _widthRatio
        viewBottom.frame.size.height = 207 * _widthRatio
        setupMultipleTapLabel()
        fieldData.allFields[4].strValue = city[1]
        fieldData.allFields[4].strkey = cityCode[1]
        _userDefault.set(cityCode[1], forKey: "Language")
        _userDefault.synchronize()
        datefrmenter.dateFormat = "dd/MM/yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)

        
        
        let FormattedText1 = NSMutableAttributedString()
        FormattedText1
            .sinup("Already a member? ")
            .forgetpassword("Login")
        self.btnLogin.setAttributedTitle(FormattedText1, for: .normal)
    }
    
    @IBAction func bnEyaClicked(_ sender: UIButton) {
        if fieldData.allFields[sender.tag].strkey == "1"{
            fieldData.allFields[sender.tag].strkey = "0"
        }else{
            fieldData.allFields[sender.tag].strkey = "1"
        }
        tableView.reloadData()
        
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[3].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 3, section: 0))as! LoginCell
        cell.textEmail.text = fieldData.allFields[3].strValue
    }
    
    
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    
    @IBAction func btnContyCodeClikced(_ sender: Any) {
        _ = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.fieldData.allFields[5].strkey = country.countryCode // IN
            self.fieldData.allFields[5].strcode = country.dialingCode! // +91
            self.tableView.reloadData()
        }
        
    }
    @IBAction func btnLoginClicked(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func btnSignupClicked(_ sender: Any) {
        if fieldData.validation().0{
            getSinup()
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OTP"{
            let vc = segue.destination as! OTPVC
            vc.strNumber = "\(self.fieldData.allFields[5].strcode)"+" "+"\(self.fieldData.allFields[5].strValue)"
        }
    }
    
    
}
extension SignupVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2{
            getgender()
            dropGender.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[2].strValue = arryaGender[index]
                self.tableView.reloadData()
                
            }
            self.view.endEditing(true)
            self.dropGender.show()
        }
        if indexPath.row == 4{
            getdropCity()
            dropCity.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[4].strValue = city[index]
                fieldData.allFields[4].strkey = cityCode[index]

                _userDefault.set(cityCode[index], forKey: "Language")
                _userDefault.synchronize()
                tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropCity.show()
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 7{
            return 110 * _widthRatio
        }
        return 60 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custText1") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
            cell.btnEya.setImage(fieldData.allFields[indexPath.row].image, for: .normal)
            cell.btnEya.isUserInteractionEnabled = false
            cell.textEmail.isUserInteractionEnabled = false
            dropGender.anchorView = cell.textEmail
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure

            return cell
        }
        if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custText1") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
            cell.btnEya.setImage(fieldData.allFields[indexPath.row].image, for: .normal)
            cell.btnEya.isUserInteractionEnabled = false
            cell.textEmail.isUserInteractionEnabled = true
            dropCity.anchorView = cell.textEmail
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            if #available(iOS 13.4, *) {
                datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                datePickerView.preferredDatePickerStyle = .wheels
                datePickerView.preferredDatePickerStyle = .wheels
                cell.textEmail.inputView = datePickerView
            }else{
                cell.textEmail.inputView = datePickerView
            }
            return cell
        }
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custText1") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
            cell.btnEya.setImage(fieldData.allFields[indexPath.row].image, for: .normal)
            cell.btnEya.isUserInteractionEnabled = false
            cell.textEmail.isUserInteractionEnabled = false
            dropCity.anchorView = cell.textEmail
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure

            return cell
        }
        
        if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custTextConact") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            cell.lblCountryCode.text = "\(fieldData.allFields[indexPath.row].strcode)"
            return cell
        }
        if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "custText2") as! LoginCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            if fieldData.allFields[indexPath.row].strkey == "1"{
                cell.textEmail.isSecureTextEntry = true
                cell.btnEya.setImage(fieldData.allFields[indexPath.row].image1, for: .normal)
            }else{
                cell.textEmail.isSecureTextEntry = false
                cell.btnEya.isSelected = true
                cell.btnEya.setImage(fieldData.allFields[indexPath.row].image, for: .normal)
            }
            cell.btnEya.isUserInteractionEnabled = true
            cell.btnEya.tag = indexPath.row
            cell.textEmail.isUserInteractionEnabled = true
            cell.textEmail.inputView = nil
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "custText") as! LoginCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
        return cell
        
    }
    
    
    
    
}
extension SignupVC{
    func setupMultipleTapLabel() {
        lblTreamCondition.text = "By creating an account, you agree to\nTerms & Conditions and Privacy Policy."
        let text = (lblTreamCondition.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms & Conditions")
        underlineAttriString.addAttribute(.font, value: UIFont.Maven_pro_Medium(size: 15 * _widthRatio), range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorchaupbule(), range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy.")
        underlineAttriString.addAttribute(.font, value: UIFont.Maven_pro_Medium(size: 15 * _widthRatio), range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorchaupbule(), range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
        lblTreamCondition.attributedText = underlineAttriString
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        lblTreamCondition.isUserInteractionEnabled = true
        lblTreamCondition.addGestureRecognizer(tapAction)
    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (lblTreamCondition.text)!
        let termsRange = (text as NSString).range(of: "Terms & Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy.")
        if gesture.didTapAttributedTextInLabel(label: lblTreamCondition, inRange: termsRange) {
            let mapVc1 = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "AboutVC")  as! AboutVC
            mapVc1.strHeader = "Terms & Conditions"
            navigationController?.pushViewController(mapVc1, animated: true)
        } else if gesture.didTapAttributedTextInLabel(label: lblTreamCondition, inRange: privacyRange) {
            let mapVc1 = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "AboutVC")  as! AboutVC
            mapVc1.strHeader = "Privacy Policy"
            navigationController?.pushViewController(mapVc1, animated: true)
        } else {
            print("Tapped none")
        }
        
    }
    func getdropCity(){
        dropCity.dataSource = city
        dropCity.direction = .bottom
        dropCity.bottomOffset = CGPoint(x: 0, y:(dropCity.anchorView?.plainView.bounds.height)!)
        dropCity.width = 263 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    func getgender(){
        dropGender.dataSource = arryaGender
        dropGender.direction = .bottom
        dropGender.bottomOffset = CGPoint(x: 0, y:(dropGender.anchorView?.plainView.bounds.height)!)
        dropGender.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
}
extension SignupVC{
    func getSinup(){
        let uuid = UUID().uuidString
        var strPush = ""
              if let push = _userDefault.value(forKey: "vpushToken") as? String{
                  strPush = push
              }
        var dic = [String:Any]()
        dic["fname"] = fieldData.allFields[0].strValue
        dic["lname"] = fieldData.allFields[1].strValue
        dic["gender"] = fieldData.allFields[2].strValue
        dic["dob"] = fieldData.allFields[3].strValue
        dic["vCountryCode"] = fieldData.allFields[4].strkey
        dic["countrycode"] = fieldData.allFields[5].strcode
        dic["mobile"] = fieldData.allFields[5].strValue
        dic["email"] = fieldData.allFields[6].strValue
        dic["password"] = fieldData.allFields[7].strValue
        dic["nDeviceLoginType"] = "2"
        dic["macAddress"] = uuid
        dic["fcm_token"] = strPush
        
//        if fieldData.allFields[3].strValue.prefix(1) == "0"{
//            let result1 = String(fieldData.allFields[3].strValue.dropFirst())    // "ello"
//            fieldData.allFields[3].strValue = result1
//        }
        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "signup.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        DispatchQueue.main.async { [self] in
                                            _currentUser = UserDetail(dic: dataname)
                                            _currentUser.patient_email_address = dataname.getStringValue(key: "email")
                                            _currentUser.patient_id = dataname.getStringValue(key: "id")
                                            _currentUser.patient_phone = dataname.getStringValue(key: "mobile")
                                            _currentUser.patient_first_name = dataname.getStringValue(key: "fname")
                                            _currentUser.patient_last_name = dataname.getStringValue(key: "lname")
                                            _currentUser.patient_countrycode = dataname.getStringValue(key: "countrycode")
                                            _currentUser.patient_dob = dataname.getStringValue(key: "dob")
                                            _userDefault.set(self.getSaveData(dataname : dataname), forKey: "userInfo")
                                            _userDefault.synchronize()
                                            getFirebaseAnalytics(screenName : getEventName.SIGNUP,isPatient_id : true)
                                            let parameters = [
                                                AppEvents.ParameterName("User ID").rawValue: "\(_currentUser.patient_id!)",
                                                AppEvents.ParameterName("User Name").rawValue: "\(_currentUser.patient_first_name!)" + " " + "\(_currentUser.patient_last_name!)",
                                                AppEvents.ParameterName("User Email").rawValue: "\(_currentUser.patient_email_address!)",
                                                AppEvents.ParameterName("User Mobile").rawValue: "\(_currentUser.patient_phone!)"
                                              
                                            ] as [String : Any]
                                            AppEvents.logEvent(.init("Registration"), parameters: parameters)
                                            
                                                let otp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                                                self.navigationController?.pushViewController(otp, animated: true)
                                        }
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
