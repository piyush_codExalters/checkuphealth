//
//  PinVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 14/04/21.
//

import UIKit
class PinCollCell: ConstrainedCollectionViewCell {
    
    @IBOutlet weak var btnNumber: UIButton!
    @IBOutlet weak var imageCancel: UIImageView!
    @IBOutlet weak var lbblNumber: LabelSemiBold!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class PinVC: ParentViewController {
    
    @IBOutlet weak var textFour: JPWidthTextField!
    @IBOutlet weak var textThree: JPWidthTextField!
    @IBOutlet weak var textTwo: JPWidthTextField!
    @IBOutlet weak var textONe: JPWidthTextField!
    @IBOutlet weak var lblError: LabelSemiBold!
    @IBOutlet weak var lblsetTitlel: LabelSemiBold!
    var arryaNumber = ["1","2","3","4","5","6","7","8","9","","0",""]
    var setPin = ""
    var setAgenPin = ""
    var isAgen = false
    var allData = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblsetTitlel.text = "Enter new pin"
        textFour.makeCornecscrRoun1(redias : 4)
        textThree.makeCornecscrRoun1(redias : 4)
        textTwo.makeCornecscrRoun1(redias : 4)
        textONe.makeCornecscrRoun1(redias : 4)
        lblError.text = " "
        getFirebaseAnalytics(screenName : getEventName.ENTER_APP_LOCK_PIN,isPatient_id : true)
    }
    
   
    
    @IBAction func btnNumberClickrd(_ sender: UIButton) {
        if sender.tag == 9{
            return
        }
        if sender.tag == 11{
            if !String.validateStringValue(str: textFour.text){
                textFour.text = ""
            }else if !String.validateStringValue(str: textThree.text){
                textThree.text = ""
            }else if !String.validateStringValue(str: textTwo.text){
                textTwo.text = ""
            }else if !String.validateStringValue(str: textONe.text){
                textONe.text = ""
            }
            return
        }
        
        if String.validateStringValue(str: textONe.text){
            textONe.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textTwo.text){
            textTwo.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textThree.text){
            textThree.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textFour.text){
            textFour.text = arryaNumber[sender.tag]
            if isAgen{
                setAgenPin = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
                if setPin != setAgenPin{
                    lblError.text = "Wrong Pin"
                }else{
                    lblError.text = " "
                    getPin()
                    
                }
            }else{
                setPin = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
                textFour.text = ""
                textThree.text = ""
                textTwo.text = ""
                textONe.text = ""
                lblsetTitlel.text = "Confirm new pin"
                getFirebaseAnalytics(screenName : getEventName.REENTER_APP_LOCK_PIN,isPatient_id : true)
                isAgen = true
            }
        }
        
    }
    
}
extension PinVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryaNumber.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 11{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CancelIcon", for: indexPath) as! PinCollCell
            cell.btnNumber.tag = indexPath.row
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Number", for: indexPath) as! PinCollCell
        cell.lbblNumber.text = arryaNumber[indexPath.row]
        cell.btnNumber.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 3 , height: 84 * _widthRatio)
    }
    
}

extension PinVC{
    func getPin(){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(setAgenPin)\",\"key\":\"nPin\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    _currentUser.nPin = setAgenPin
                                    _userDefault.set(self.getSaveData(dataname : self.allData), forKey: "userInfo")
                                    _userDefault.synchronize()
                                    
//                                        let vclogin = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "SwitchAccountVC") as! SwitchAccountVC
//                                        vclogin.isSetting = false
//                                        self.navigationController?.pushViewController(vclogin, animated: true)
                                    if  !_userDefault.bool(forKey: "Guidelines"){
                                        _userDefault.set(true, forKey: "Guidelines")
                                        _userDefault.synchronize()
                                        let vclogin = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GuidelinesVC") as! GuidelinesVC
                                        self.navigationController?.pushViewController(vclogin, animated: true)
                                        
                                    }else{
                                        let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                                        self.navigationController?.pushViewController(vclogin, animated: true)
                                        
                                    }
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
