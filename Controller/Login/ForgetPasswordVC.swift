//
//  ForgetPasswordVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 08/04/21.
//

import UIKit


class FogetList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
    var t4 = SingIn()
        t4.placeholder = "Email or Mobile number"
    t4.isSecure = false
    t4.keybord = .emailAddress
    t4.strValue = ""
    
        
    allFields.append(t4)

    }
    
   func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Enter Valid Email id or Mobile Number")
        }
        return(true,"")
    }
}

class ForgetPasswordVC: ParentViewController {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnSingup: UIButton!
    @IBOutlet weak var lblForgot: LabelMedium!
    @IBOutlet weak var lblHeader: LabelMedium!
    @IBOutlet weak var imageIcon: UIImageView!

    var fieldData = FogetList()
    var isPin = false
    var allData = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        if isWorkingCountryUK(){
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }else{
            imageIcon.image = UIImage(named: "ic_loginscreens_bg")
        }
        viewTop.frame.size.height = 500 * _widthRatio
        viewBottom.frame.size.height = 146 * _widthRatio
        if isPin{
            
            lblForgot.text = "Forgot Pin"
            lblHeader.text = "Enter registered mobile no. or your verified email to get a new pin."
        }else{
            lblForgot.text = "Forgot Password"
            lblHeader.text = "Enter registered mobile no. or your verified email to reset your password."
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnremove()
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
           fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnBackNAvigationClicked(_ sender: Any) {
        if isPin{
            initLockScreen2()
        }else{
            if (self.presentingViewController != nil){
                self.dismiss(animated: false, completion: nil)
            }else{
                _ = self.navigationController?.popViewController(animated: true)
            }
        if let _ = viewScreenLock{
            viewScreenLock.removeFromSuperview()
        }

        }

        
    }
    
    
    
    
    @IBAction func btnLoginClicekd(_ sender: Any) {
        if fieldData.validation().0{
            if isPin{
                getforgotpasswordPin()
            }else{
                getforgotpassword()
            }
           
        }else{
            showFailMessage(message : fieldData.validation().1)
        }
    }

}
extension ForgetPasswordVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "custText") as! LoginCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            return cell
                
    }
    
    
    
    
}
extension ForgetPasswordVC{
    func getforgotpassword(){
        
//        if fieldData.allFields[0].strValue.prefix(1) == "0"{
//            let result1 = String(fieldData.allFields[0].strValue.dropFirst())    // "ello"
//            fieldData.allFields[0].strValue = result1
//        }
        
        let parameters = "{\"data\":{\"email\":\"\(fieldData.allFields[0].strValue)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "forgotpassword.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                           
                        _ = self.navigationController?.popViewController(animated: true)
                                
                        }
                        self.showResponseMessage(dict: Response)
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
    func getforgotpasswordPin(){
        let patient =  _userDefault.value(forKey: "OldPatientId")

        let parameters = "{\"data\":{\"email\":\"\(fieldData.allFields[0].strValue)\",\"nPatientId\":\"\(patient!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "forgotpin.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async { [self] in
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                            _currentUser.nPin = Response.getStringValue(key: "data")
                            _userDefault.set(self.getSaveData(dataname : self.allData), forKey: "userInfo")
                            fieldData.allFields[0].strValue = ""
                            _userDefault.synchronize()
                            if isPin{
                                initLockScreen2()
                            }else{
                                if (self.presentingViewController != nil){
                                    self.dismiss(animated: false, completion: nil)
                                }else{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                            if let _ = viewScreenLock{
                                viewScreenLock.removeFromSuperview()
                            }

                            }
                            self.tableView.reloadData()

                        }
                        self.showResponseMessage(dict: Response)
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
