//
//  SplashVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 19/05/21.
//

import UIKit

class SplashVC: ParentViewController, BtnDelegate {
    @IBOutlet weak var imafeIcon: UIImageView!
    var timer : Timer!
    var strcheckAppVersionNew = ""
    var strvStoreURL = ""
    var isMaintenanceMode = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imafeIcon.image = UIImage(named: "ic_logo_forsplash")
        checkVersion()

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let _ = timer{
            timer.invalidate()
        }

    }
    @objc func eventWith() {
        if let data = _userDefault.value(forKey: "userInfo") as? NSDictionary{
            _currentUser = UserDetail(dic: data)
            KPWebCall.call.setClientApiKey(token: _currentUser.vApiToken)
            if _currentUser.isMobileVerified == "0"{
                let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                nav.viewControllers = [Information]
                _appDelegator.window?.rootViewController = nav
            }else if _currentUser.isOnboarding == "0"{
                let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                nav.viewControllers = [Information]
                _appDelegator.window?.rootViewController = nav
            }else if _currentUser.nPin == ""{
                let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                nav.viewControllers = [Information]
                _appDelegator.window?.rootViewController = nav
            }else{
                let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                let vclogin = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScreenPinVC") as! ScreenPinVC
                nav.viewControllers = [vclogin]
                _appDelegator.window?.rootViewController = nav
            }
        }else{
            let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
            let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
            nav.viewControllers = [Information]
            _appDelegator.window?.rootViewController = nav
        }
    }
    func initVersion(fore : Bool){
        if let _ = viewVersion{
            viewVersion.removeFromSuperview()
        }
        viewVersion = CheckVersion.instanceFromNib()
        viewVersion.isFore = fore
        viewVersion.checkForce()
        viewVersion.delegate = self
        viewVersion.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: _screenSize.height)
        _appDelegator.window!.addSubview(viewVersion)
    }
    func initMaintenace(){
        if let _ = viewMaintence{
            viewMaintence.removeFromSuperview()
        }
        viewMaintence = CheckMaintenance.instanceFromNib()
       
        viewMaintence.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: _screenSize.height)
        _appDelegator.window!.addSubview(viewMaintence)
    }
    func showsVersion(){
        if let _ = viewScreenLock{
            viewScreenLock.isHidden = false
        }
    }
    func showsMaintance(){
        if let _ = viewMaintence{
            viewMaintence.isHidden = false
        }
    }
    func btnNotNow() {
        if let _ = viewVersion{
            eventWith()
            viewVersion.isHidden = true
        }
    }
    func btnAppStore() {
        if let url = URL(string: self.strvStoreURL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    func checkVersion(){
        let parameters = "{\"data\":{\"nAppType\":\"2\"}}"
        KPWebCall.call.postRequestApiClinet(relPath: "getVersiondetail.php", param: parameters) { [self] (data) in
            if let response = data {
                if response.ReturnedData != nil{
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                        self.strcheckAppVersionNew  = data.getStringValue(key: "isForceUpdate")
                                        self.strvStoreURL  = data.getStringValue(key: "vStoreURL")
                                        let versionCode = data.getDoubleValue(key: "dVersionName")
                                        self.isMaintenanceMode = data.getStringValue(key: "isMaintenanceMode")
                                        if let info = Bundle.main.infoDictionary{
                                            let currentVersion = info["CFBundleShortVersionString"] as? String
                                            let doubleCurrentVersion = currentVersion?.doubleValue
                                            if self.isMaintenanceMode == "1"{
                                                // MaintenanceMode
                                                initMaintenace()
                                            }else{
                                                 if versionCode > doubleCurrentVersion!{
                                                     if self.strcheckAppVersionNew == "1"{
                                                         // isForceUpdate
                                                        initVersion(fore: true)
                                                     }else{
                                                         // Version Update
                                                         initVersion(fore: false)
                                                     }
                                                 }else{
                                                     // Home
                                                     timer = Timer.scheduledTimer(timeInterval: 3, target: self,selector: #selector(eventWith),userInfo: nil,repeats: true)
                                                 }
                                            }
                                        }
                                    }
                                }else{
                                    timer = Timer.scheduledTimer(timeInterval: 3, target: self,selector: #selector(eventWith),userInfo: nil,repeats: true)
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            }
        }
    }
}
