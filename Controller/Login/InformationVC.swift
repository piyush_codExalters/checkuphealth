//
//  ViewController.swift
//  CheckupHealth
//
//  Created by codExalters1 on 06/04/21.
//

import UIKit
import DropDown

class InformationCallCell: ConstrainedCollectionViewCell {
    @IBOutlet weak var imageOnbording: UIImageView!
    @IBOutlet weak var lblHeadderDes: JPWidthLabel!
    @IBOutlet weak var lblHedarTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}


class InformationVC: ParentViewController {
    var city = ["Zimbabwe", "United Kingdom"]
    var cityCode = ["ZW","GB"]
    @IBOutlet weak var pageOnbording: UIPageControl!
    @IBOutlet weak var btnNext: LoginButton!
    @IBOutlet weak var textDropCity: JPWidthTextField!
    @IBOutlet weak var btnSkip: LoginButton!
    @IBOutlet weak var viewDrop: UIView!
    @IBOutlet weak var btnLogin: LoginButton!
    @IBOutlet weak var btnSignUp: LoginButton!
    
    
    let dropCity = DropDown()
    var arrySliderList = [SliderList]()
    var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewDrop.makeCustomRound(radius:6,bc:.black)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(localTimeZoneAbbreviation)
        if "GMT+02:00" == localTimeZoneAbbreviation{
            textDropCity.text = city[0]
            _userDefault.set(cityCode[0], forKey: "Language")
            _userDefault.synchronize()
        }else{
            textDropCity.text = city[1]
            _userDefault.set(cityCode[1], forKey: "Language")
            _userDefault.synchronize()
        }
        dropCity.anchorView = textDropCity
        getFirebaseAnalytics(screenName : getEventName.WELCOME,isPatient_id : false)
        getdropCity()
        getslider()
    }


    @IBAction func btnDropDownClicked(_ sender: Any) {
        dropCity.selectionAction = { [unowned self] (index: Int, item: String) in
            self.textDropCity.text! = city[index]
            _userDefault.set(cityCode[index], forKey: "Language")
            _userDefault.synchronize()
        }
        self.view.endEditing(true)
        self.dropCity.show()
    }
    @IBAction func btnLoginClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "Login", sender: nil)
    }
    
    
    @IBAction func btnSingupClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "Signup", sender: nil)

    }
    
    
}


extension InformationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrySliderList.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! InformationCallCell
        cell.imageOnbording.pin_updateWithProgress = true
        cell.imageOnbording.pin_setImage(from: URL(string: arrySliderList[indexPath.row].sliderImage)!)
        cell.lblHedarTitle.textColor = UIColor.colorchaupbule()
        cell.lblHedarTitle.text = arrySliderList[indexPath.row].sliderTitle
        cell.lblHeadderDes.text = arrySliderList[indexPath.row].sliderText
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width , height: collectionView.frame.size.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.myColView.contentOffset, size: self.myColView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.myColView.indexPathForItem(at: visiblePoint) {
            self.pageOnbording.currentPage = visibleIndexPath.row
        }
    }
    
    
    func getdropCity(){
        dropCity.dataSource = city
        dropCity.direction = .bottom
        dropCity.bottomOffset = CGPoint(x: 0, y:(dropCity.anchorView?.plainView.bounds.height)!)
        dropCity.width = 263 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
}
extension InformationVC{
    func getslider(){
        showCentralSpinner()
        KPWebCall.call.getRequestApiClinet(relPath: "slider.php", param: "") { (response) in
            self.hideCentralSpinner()
            self.arrySliderList = [SliderList]()

            if let response = response {
            do {
                if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                    print(response)
                    DispatchQueue.main.async {
                    if let Response = response as? NSDictionary{
                        
                        if Response.getIntValue(key: "success") == 1{
                            if let data = Response.value(forKey: "data") as? NSArray{
                                for obj in data{
                                    self.arrySliderList.append(SliderList(dic: obj as! NSDictionary))
                                }
                            }
                            self.myColView.reloadData()
                            self.pageOnbording.numberOfPages = self.arrySliderList.count

                        }
                     }
                    }
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            }
        }
    }
}
