//
//  ProfileVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 08/04/21.
//

import UIKit
import DropDown
import CropViewController

protocol addressDelegete {
    func getAddress(str: String,address: AddressListName, index : Int)
}
class ProfileList: NSObject {
    var allFields: [SingIn] = []
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t1 = SingIn()
        t1.placeholder = "First Name"
        t1.keybord = .default
        
        var t2 = SingIn()
        t2.placeholder = "Last Name"
        t2.keybord = .default
        
        var t3 = SingIn()
        t3.placeholder = "Date of Birth"
        t3.keybord = .default
        t3.image = #imageLiteral(resourceName: "ic_calender")
        
        
        var t6 = SingIn()
        t6.placeholder = "Sex"
        t6.keybord = .default
        t6.image = #imageLiteral(resourceName: "ic_dropdown")
        
        
        var t16 = SingIn()
        t16.placeholder = "Registered Address"
        t16.keybord = .default
        t16.placeholder1 = "Address Line 1"
        
        var t17 = SingIn()
        t17.keybord = .default
        t17.placeholder1 = "Area (Optional)"
        
        var t18 = SingIn()
        t18.keybord = .default
        t18.placeholder1 = "Town/City"
        
        
        var t22 = SingIn()
        t22.placeholder1 = "District(Optional)"
        t22.keybord = .default
        
        
        var t19 = SingIn()
        t19.keybord = .default
        t19.placeholder1 = "State/Province/Region (Optional)"
        
        var t20 = SingIn()
        t20.keybord = .default
        t20.placeholder1 = "Country"
        
        
        var t21 = SingIn()
        t21.keybord = .default
        t21.placeholder1 = "Postcode"
        
        
        
        var t10 = SingIn()
        t10.placeholder = "GP Practice Name"
        t10.keybord = .default
        
        var t122 = SingIn()
        t122.placeholder = "GP Practice Email"
        t122.keybord = .emailAddress

        
        var t11 = SingIn()
        t11.placeholder = "GP Practice Address"
        t11.keybord = .default
        t11.placeholder1 = "Address Line 1"
        
        var t12 = SingIn()
        t12.placeholder1 = "Address Line 2"
        t12.keybord = .default
        
        var t13 = SingIn()
        t13.placeholder1 = "Town/City"
        t13.keybord = .default
        
        var t14 = SingIn()
        t14.placeholder1 = "Postcode"
        t14.keybord = .default
        
        var t15 = SingIn()
        t15.placeholder1 = "Telephone no"
        t15.keybord = .numberPad
        
        var t4 = SingIn()
        t4.placeholder = "Ethnicity"
        t4.keybord = .default
        t4.image = #imageLiteral(resourceName: "ic_dropdown")
        
        var t5 = SingIn()
        t5.placeholder = "Address"
        t5.keybord = .default
        t5.image = #imageLiteral(resourceName: "ic_navigation_aro")
        
        var t7 = SingIn()
        t7.placeholder = "Your Organization Code (Optional)"
        t7.keybord = .default
        
        var t8 = SingIn()
        t8.placeholder = "Is Advantage Health Taken"
        t8.keybord = .default
        t8.strkey = "0"
        
        allFields.append(t1)
        allFields.append(t2)
        allFields.append(t3)
        allFields.append(t6)
        
        allFields.append(t16)
        allFields.append(t17)
        allFields.append(t18)
        allFields.append(t22)
        allFields.append(t19)
        allFields.append(t20)
        allFields.append(t21)

        
        allFields.append(t10)
        allFields.append(t122)

        allFields.append(t11)
        allFields.append(t12)
        allFields.append(t13)
        allFields.append(t14)
        allFields.append(t15)
        
        allFields.append(t4)
        allFields.append(t5)
        allFields.append(t7)
        allFields.append(t8)
    }
    
    func validation() -> (Bool,String){
        if String.validateStringValue(str: allFields[0].strValue){
            return(false,"Please enter first name")
        }else if String.validateStringValue(str: allFields[1].strValue){
            return(false,"Please enter last name")
        }else if String.validateStringValue(str: allFields[2].strValue){
            return(false,"Please select birth date")
        }else if String.validateStringValue(str: allFields[3].strValue) || allFields[3].strValue == "Select"{
            return(false,"Please select Sex")
        }else if String.validateStringValue(str: allFields[4].strValue){
            return(false,"Please enter registered address Line 1")
        }else if String.validateStringValue(str: allFields[6].strValue){
            return(false,"Please enter registered address city")
        }else if String.validateStringValue(str: allFields[9].strValue){
            return(false,"Please enter registered address country")
        }
        return(true,"")
        
        
       
        
        
    }
}
class ProfileCell: ConstrainedTableViewCell {
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblName: LabelRegular!
    @IBOutlet weak var textEmail: JPWidthTextField!
    @IBOutlet weak var viewConstretBottom: NSLayoutConstraint!
    @IBOutlet weak var viewConstretTop: NSLayoutConstraint!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var imageCalander: UIImageView!
    @IBOutlet weak var lblisAdvatigate: LabelMedium!
    @IBOutlet weak var btnMale: JPWidthButton!
    @IBOutlet weak var btnFemale: JPWidthButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var textAreaNAme: UITextView!
    @IBOutlet weak var btnLink: JPWidthButton!
    @IBOutlet weak var imageVerifation: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if reuseIdentifier == "textName" || reuseIdentifier == "textCalander" || reuseIdentifier == "Origation" || reuseIdentifier == "Password"{
            viewBg.makeCornerRound(redias : 6)
            viewText.makeCustomRound(radius:6,bc:.black)
            
        }
        if reuseIdentifier == "Check" || reuseIdentifier == "Gender"{
            viewBg.makeCornerRound(redias : 6)
        }
        if reuseIdentifier == "textArea"{
            viewBg.makeCornerRound(redias : 6)
            viewText.makeCustomRound(radius:6,bc:.black)
        }
    }
}


class ProfileVC: ParentViewController, addressDelegete {
    
    
    
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    var fieldData = ProfileList()
    var imagePicker = UIImagePickerController()
    var imgUser : UIImage?
    let datePickerView: UIDatePicker = UIDatePicker()
    let datefrmenter = DateFormatter()
    var allData = NSDictionary()
    let dropEthnicity = DropDown()
    let dropGender = DropDown()
    var selectAddres : AddressListName!
    var indext = -1
    var arryaEthnicity = ["White", "Mixed/ multiple ethnic","Asian/ Asian British","Black/ African/ Caribbean/ African British","Other ethnic groups"]
    var arryaGender = ["Male", "Female","Not listed","Prefer not to say"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewProfile.makeCornerRound(redias : 6)
        viewTop.frame.size.height = 203 * _widthRatio
        viewBottom.frame.size.height = 95 * _widthRatio
        datefrmenter.dateFormat = "dd/MM/yyyy"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        let currentDate = NSDate()
        datePickerView.maximumDate = currentDate as Date
        imageProfile.makeCornerRound(redias : 6)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        fieldData.allFields[0].strValue = _currentUser.patient_first_name
        fieldData.allFields[1].strValue = _currentUser.patient_last_name
        fieldData.allFields[3].strValue = "Select"
        fieldData.allFields[18].strValue = arryaEthnicity[0]
        fieldData.allFields[12].strValue = _currentUser.patient_practice_email

        tableView.reloadData()
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        fieldData.allFields[2].strValue = datefrmenter.string(from: sender.date)
        let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0))as! ProfileCell
        cell.textEmail.text = fieldData.allFields[2].strValue
    }
    @IBAction func btnCameraClicked(_ sender: Any) {
        openPickerCamera()
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        if imgUser == nil{
            showFailMessage(message : "Please pick profile picture")
        }else{
            if fieldData.validation().0{
                getProfile()
            }else{
                showFailMessage(message : fieldData.validation().1)
            }
        }
        
        
    }
    @IBAction func btnOrigationClicked(_ sender: Any) {
       // self.performSegue(withIdentifier: "Organization", sender: nil)
        let alert = UIAlertController(title: "Organization Code", message: "If you are registering via your organization, please enter your organization code here.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)

        
    }
    @IBAction func btnCheckClicked(_ sender: UIButton) {
        if fieldData.allFields[20].strkey == "0"{
            fieldData.allFields[20].strkey = "1"
        }else{
            fieldData.allFields[20].strkey = "0"
        }
        tableView.reloadData()
    }
    @IBAction func btnMaleClicked(_ sender: Any) {
        fieldData.allFields[3].strkey = "1"
        tableView.reloadData()
    }
    @IBAction func btnFemaleClicekd(_ sender: Any) {
        fieldData.allFields[3].strkey = "2"
        tableView.reloadData()
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    func getAddress(str: String,address: AddressListName, index : Int) {
        fieldData.allFields[19].strValue = str
        selectAddres = address
        indext = index
        tableView.reloadData()
    }
    
}

extension ProfileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3{
            getgender()
            dropGender.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[3].strValue = arryaGender[index]
                self.tableView.reloadData()
                
            }
            self.view.endEditing(true)
            self.dropGender.show()
        }
        if indexPath.row == 18{
            getdropethnicity()
            dropEthnicity.selectionAction = { [unowned self] (index: Int, item: String) in
                fieldData.allFields[18].strValue = arryaEthnicity[index]
                self.tableView.reloadData()
            }
            self.view.endEditing(true)
            self.dropEthnicity.show()
        }
        
        if indexPath.row == 19{
            let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressVC")  as! AddressVC
            mapVc1.delegatesAddress = self
            mapVc1.isSelectionIndex = indext
            navigationController?.pushViewController(mapVc1, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90 * _widthRatio
        }
       
        if indexPath.row == 19{
            return 0
        }
        if indexPath.row == 10{
            if isWorkingCountryUK(){
                return 75 * _widthRatio
            }else{
                return 0
            }
        }
        
        
        if indexPath.row == 20{
            if isWorkingCountryUK(){
                return 90 * _widthRatio
            }else{
                return 75 * _widthRatio
            }
        }
        
        
        if indexPath.row == 21{
            if isWorkingCountryUK(){
                return 0
            }else{
                return 50 * _widthRatio

            }
        }
        if !isWorkingCountryUK(){
            if  indexPath.row == 11 || indexPath.row == 12 || indexPath.row == 13 || indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16 || indexPath.row == 17{
                return 0
            }
        }else{
            if indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 8{
                return 0
            }
        }
        return 75 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2 || indexPath.row == 19 || indexPath.row == 3 || indexPath.row == 18{
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCalander") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
            cell.imageCalander.image = fieldData.allFields[indexPath.row].image
            if indexPath.row == 2{
                if #available(iOS 13.4, *) {
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
                    datePickerView.preferredDatePickerStyle = .wheels
                    datePickerView.preferredDatePickerStyle = .wheels
                    cell.textEmail.inputView = datePickerView
                }else{
                    cell.textEmail.inputView = datePickerView
                }
                cell.textEmail.isUserInteractionEnabled = true
                cell.textEmail.placeholder = ""
            }else if indexPath.row == 3{
                cell.textEmail.isUserInteractionEnabled = false
                cell.textEmail.placeholder = ""
                dropGender.anchorView = cell.textEmail
            }else if indexPath.row == 18{
                cell.textEmail.isUserInteractionEnabled = false
                cell.textEmail.placeholder = ""
                dropEthnicity.anchorView = cell.textEmail
            }else{
                cell.textEmail.placeholder = "Select your address"
                cell.textEmail.isUserInteractionEnabled = false
                cell.textEmail.inputView = nil
            }
            
            return cell
        }
        
        if indexPath.row == 20{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Origation") as! ProfileCell
            cell.textEmail.tag = indexPath.row
            cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
            cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
            cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
            cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1
            cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
            cell.textEmail.inputView = nil
            if isWorkingCountryUK(){
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = 5
            }else{
                cell.viewConstretTop.constant = -5
                cell.viewConstretBottom.constant = -5
            }
            return cell
        }
        
        
        if indexPath.row == 21{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check") as! ProfileCell
            cell.lblisAdvatigate.text = fieldData.allFields[indexPath.row].placeholder
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = 5
            if fieldData.allFields[21].strkey == "0"{
                cell.btnCheck.isSelected = false
            }else{
                cell.btnCheck.isSelected = true
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.inputView = nil
        cell.textEmail.placeholder = fieldData.allFields[indexPath.row].placeholder1
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.textEmail.isSecureTextEntry = fieldData.allFields[indexPath.row].isSecure
        if indexPath.row == 0{
            cell.viewConstretTop.constant = 5
            cell.viewConstretBottom.constant = -5
        }else{
            cell.viewConstretTop.constant = -5
            cell.viewConstretBottom.constant = -5
        }
        return cell
        
    }
    func getdropethnicity(){
        dropEthnicity.dataSource = arryaEthnicity
        dropEthnicity.direction = .bottom
        dropEthnicity.bottomOffset = CGPoint(x: 0, y:(dropEthnicity.anchorView?.plainView.bounds.height)!)
        dropEthnicity.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    func getgender(){
        dropGender.dataSource = arryaGender
        dropGender.direction = .bottom
        dropGender.bottomOffset = CGPoint(x: 0, y:(dropGender.anchorView?.plainView.bounds.height)!)
        dropGender.width = 340 * _widthRatio
        DropDown.appearance().textColor = UIColor.black
        //  DropDown.appearance().textFont = UIFont(name: "IBMPlexSans-Medium", size: 13)!
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 40 * _widthRatio
        
    }
    
    
    
}
extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage

        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.customAspectRatio = CGSize(width: 1, height: 1)
        if picker.sourceType == .camera {
            picker.dismiss(animated: true, completion: {
                self.present(cropViewController, animated: true, completion: nil)
            })
        } else {
            picker.pushViewController(cropViewController, animated: true)
        }
    }
    
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
            // 'image' is the newly cropped version of the original image
            imageProfile.image = image
            imgUser = image
        
        dismiss(animated:true, completion: nil)

    }
  
    
    
    
    func openPickerCamera(){
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        let action2 = UIAlertAction(title: "Open Gallery", style: .default) { (action) in
            self.openLibrary()
        }
        let action3 = UIAlertAction(title: "Close", style: .cancel) { (action) in
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}
extension ProfileVC{
    func getProfile(){
        var address = String()

        
        if isWorkingCountryUK(){
            if String.validateStringValue(str: fieldData.allFields[10].strValue){
                return showFailMessage(message: "Please enter registered address postcode")
            }
        }
        
        
        if !String.validateStringValue(str: fieldData.allFields[4].strValue){
            
            address += fieldData.allFields[4].strValue
            address += ","
        }
        if !String.validateStringValue(str: fieldData.allFields[5].strValue){
            address += fieldData.allFields[5].strValue
            address += ","
        }
        if !String.validateStringValue(str: fieldData.allFields[6].strValue){
            address += fieldData.allFields[6].strValue
            address += ","
        }
        if !String.validateStringValue(str: fieldData.allFields[7].strValue){
            address += fieldData.allFields[7].strValue
            address += ","
        }
        if !String.validateStringValue(str: fieldData.allFields[8].strValue){
            address += fieldData.allFields[8].strValue
            address += ","
        }
        if !String.validateStringValue(str: fieldData.allFields[9].strValue){
            address += fieldData.allFields[9].strValue
            address += ","
        }
        
        if !String.validateStringValue(str: fieldData.allFields[10].strValue){
            address += fieldData.allFields[10].strValue
            address += "."
        }
        
        
        var isAdvant = false
        if fieldData.allFields[15].strkey == "1"{
             isAdvant = true
        }else{
             isAdvant = false
        }
        let imageResized = imgUser!.resizeWith(percentage: 1)
        let base64 = imageResized?.toBase64()
        
        
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"firstname\":\"\(fieldData.allFields[0].strValue)\",\"lastname\":\"\(fieldData.allFields[1].strValue)\",\"dob\":\"\(fieldData.allFields[2].strValue)\",\"gender\":\"\(fieldData.allFields[3].strValue)\",\"location\":\"\(address)\",\"addressline1\":\"\(fieldData.allFields[4].strValue)\",\"area\":\"\(fieldData.allFields[5].strValue)\",\"city\":\"\(fieldData.allFields[6].strValue)\",\"district\":\"\(fieldData.allFields[7].strValue)\",\"state\":\"\(fieldData.allFields[8].strValue)\",\"country\":\"\(fieldData.allFields[9].strValue)\",\"postcode\":\"\(fieldData.allFields[10].strValue)\",\"patient_profile_image\":\"\(base64!)\",\"IsAdvantageHealthTaken\":\"\(isAdvant)\",\"ethnicity\":\"\(fieldData.allFields[18].strValue)\",\"vOrganisationCode\":\"\(fieldData.allFields[20].strValue)\",\"mobile\":\"\(_currentUser.patient_phone!)\",\"practice_name\":\"\(fieldData.allFields[11].strValue)\",\"practice_email\":\"\(fieldData.allFields[12].strValue)\",\"practice_addressline1\":\"\(fieldData.allFields[13].strValue)\",\"practice_addressline2\":\"\(fieldData.allFields[14].strValue)\",\"practice_city\":\"\(fieldData.allFields[15].strValue)\",\"practice_postcode\":\"\(fieldData.allFields[16].strValue)\",\"practice_telephoneno\":\"\(fieldData.allFields[17].strValue)\",\"vReferralCode\":\"\(vPatientReferralCode)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinetImage(relPath: "profile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                    _userDefault.setValue(dataname.getStringValue(key: "patient_referralcode"), forKey: "patient_referralcode")
                                    }
                                    vPatientReferralCode = ""
                                    self.getupdate(key : "p_type",value: "1")

                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func getupdate(key : String,value: String){
        let parameters = "{\"data\":{\"id\":\"\(_currentUser.patient_id!)\",\"value\":\"\(value)\",\"key\":\"\(key)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updateuserprofile.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let dataname = Response["data"] as? NSDictionary{
                                        _currentUser.patient_type = dataname.getStringValue(key: "patient_type")
                                        _currentUser.isOnboarding = "1"
                                        _userDefault.set(self.getSaveData(dataname : self.allData), forKey: "userInfo")
                                        _userDefault.synchronize()
                                        self.performSegue(withIdentifier: "SetLock", sender: nil)
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    
//
//        func CheckPatientVerification(){
//            showCentralSpinner()
//            dic["nPatientId"] = _currentUser.patient_id
//
//            let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
//            let jsonString = String(data: jsonData!, encoding: .utf8)!
//            print(jsonString)
//
//            let parameters = "{\"data\":\(jsonString)}"
//
//            KPWebCall.call.postRequestApiClinet(relPath: "CheckPatientVerification.php", param: parameters) { (response) in
//                self.hideCentralSpinner()
//                if let response = response {
//                    do {
//                        if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
//                            print(response)
//                            DispatchQueue.main.async { [self] in
//                                if let Response = response as? NSDictionary{
//                                    if Response.getIntValue(key: "success") == 1{
//                                        _ = self.navigationController?.popViewController(animated: true)
//
//                                    }
//                                    self.showResponseMessage(dict: Response)
//                                }
//                            }
//                        }
//                    }
//                    catch let error as NSError {
//                        print(error.localizedDescription)
//                    }
//                }
//            }
//        }
}
