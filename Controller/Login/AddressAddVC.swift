//
//  AddressAddVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 30/04/21.
//

import UIKit
class AddressList: NSObject {
    
    var allFields: [SingIn] = []
    
    override init() {
        super.init()
        prepareDate()
    }
    func prepareDate(){
        
        var t6 = SingIn()
        t6.placeholder = "Address Line 1"
        t6.keybord = .default
        
        var t7 = SingIn()
        t7.placeholder = "Address Line 2(Optional)"
        t7.keybord = .default
        
        var t9 = SingIn()
        t9.placeholder = "Area(Optional)"
        t9.keybord = .default
        
        var t10 = SingIn()
        t10.placeholder = "City"
        t10.keybord = .default
        
        var t11 = SingIn()
        t11.placeholder = "District(Optional)"
        t11.keybord = .default
        
        var t12 = SingIn()
        t12.placeholder = "State/Province/Region (Optional)"
        t12.keybord = .default
        
        var t13 = SingIn()
        t13.placeholder = "Country"
        t13.keybord = .default
        
        var t14 = SingIn()
        t14.placeholder = "Postcode"
        t14.keybord = .default
        
        allFields.append(t6)
        allFields.append(t7)
        allFields.append(t9)
        allFields.append(t10)
        allFields.append(t11)
        allFields.append(t12)
        allFields.append(t13)
        allFields.append(t14)
        
        
        
    }
    
}
class AddressAddVC: ParentViewController {
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblHeaderTitle: LabelMedium!
    @IBOutlet weak var btnAdd: submitButton!
    
    var strHeader = "Add Address"
    var fieldData = AddressList()
    var arryaAddres : AddressListName!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if "Add Address" == strHeader{
            lblHeaderTitle.text = "Add Address"
            btnAdd.setTitle("Add", for: .normal)
        }else if "Add registered Address" == strHeader{
            getFirebaseAnalytics(screenName : getEventName.ADD_REGISTRATION_ADDRESS,isPatient_id : true)
            lblHeaderTitle.text = "Add registered Address"
            btnAdd.setTitle("Add", for: .normal)
            tabBarController?.tabBar.isHidden = true
        }else{
            lblHeaderTitle.text = "Edit Address"
            fieldData.allFields[0].strValue = arryaAddres.addressLine1
            fieldData.allFields[1].strValue = arryaAddres.addressLine2
            fieldData.allFields[2].strValue = arryaAddres.area
            fieldData.allFields[3].strValue = arryaAddres.city
            fieldData.allFields[4].strValue = arryaAddres.district
            fieldData.allFields[5].strValue = arryaAddres.state
            fieldData.allFields[6].strValue = arryaAddres.country
            fieldData.allFields[7].strValue = arryaAddres.postcode
            tableView.reloadData()
            btnAdd.setTitle("Update", for: .normal)
        }
    }
    
    @IBAction func textEditChange(_ sender: JPWidthTextField) {
        fieldData.allFields[sender.tag].strValue = sender.text!
    }
    
    @IBAction func btnAddressClicked(_ sender: Any) {
        if validation().0{
            if "Add Address" == strHeader{
                getaddpatientaddress()
            }else if "Add registered Address" == strHeader{
                addPatientRegisteredAddress()
            }else{
                getupdatepatientaddress()
            }
        }else{
            showFailMessage(message : validation().1)
        }
        
        
    }
    
    func validation() -> (Bool,String){
        
        if String.validateStringValue(str: fieldData.allFields[0].strValue){
            return(false,"Please Enter Address Line 1")
        }else  if String.validateStringValue(str: fieldData.allFields[3].strValue){
            return(false,"Please Enter City")
        }else  if String.validateStringValue(str: fieldData.allFields[6].strValue){
            return(false,"Please Enter Country")
        }else if isWorkingCountryUK() {
            if String.validateStringValue(str: fieldData.allFields[7].strValue){
               return(false,"Please enter valid Postcode")
           }
        }
        return(true,"")
    }
    
    
    
}
extension AddressAddVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isWorkingCountryUK() {
            if indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 5{
                return 0
            }
            if indexPath.row == 7{
                return 90 * _widthRatio
            }
        } else {
            if indexPath.row == 7{
                return 0
            }
            if indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 5{
                return 75 * _widthRatio
            }
        }
        if indexPath.row == 0 || indexPath.row == 7{
            return 90 * _widthRatio
        }
        return 75 * _widthRatio
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldData.allFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textName") as! ProfileCell
        cell.textEmail.tag = indexPath.row
        cell.textEmail.keyboardType = fieldData.allFields[indexPath.row].keybord
        cell.textEmail.text = fieldData.allFields[indexPath.row].strValue
        cell.textEmail.inputView = nil
        cell.lblName.text = fieldData.allFields[indexPath.row].placeholder
        cell.viewConstretTop.constant = -5
        cell.viewConstretBottom.constant = -5
        if indexPath.row == 0{
            cell.viewConstretTop.constant = 5
        }
        if indexPath.row == 7{
            cell.viewConstretBottom.constant = 5
        }
        return cell
    }
}
extension AddressAddVC{
    func getaddpatientaddress(){
        if isWorkingCountryUK() {
            fieldData.allFields[2].strValue = ""
            fieldData.allFields[4].strValue = ""
            fieldData.allFields[5].strValue = ""
        } else {
            fieldData.allFields[5].strValue = ""
        }
        let parameters = "{\"data\":{\"patientId\":\"\(_currentUser.patient_id!)\",\"addressLine1\":\"\(fieldData.allFields[0].strValue)\",\"addressLine2\":\"\(fieldData.allFields[1].strValue)\",\"area\":\"\(fieldData.allFields[2].strValue)\",\"city\":\"\(fieldData.allFields[3].strValue)\",\"district\":\"\(fieldData.allFields[4].strValue)\",\"state\":\"\(fieldData.allFields[5].strValue)\",\"country\":\"\(fieldData.allFields[6].strValue)\",\"postcode\":\"\(fieldData.allFields[7].strValue)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addpatientaddress.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func addPatientRegisteredAddress(){
        var location = ""
        if isWorkingCountryUK() {
            fieldData.allFields[2].strValue = ""
            fieldData.allFields[4].strValue = ""
            fieldData.allFields[5].strValue = ""
            
            location = fieldData.allFields[0].strValue + " " + fieldData.allFields[1].strValue + " " + fieldData.allFields[3].strValue + " " + fieldData.allFields[6].strValue + " " + fieldData.allFields[7].strValue
        } else {
            fieldData.allFields[5].strValue = ""
            
            location = fieldData.allFields[0].strValue + " " + fieldData.allFields[1].strValue + " " + fieldData.allFields[2].strValue + " " + fieldData.allFields[3].strValue + " " + fieldData.allFields[4].strValue + " " + fieldData.allFields[6].strValue + " " + fieldData.allFields[7].strValue
        }
       
        let parameters = "{\"data\":{\"patientId\":\"\(_currentUser.patient_id!)\",\"addressLine1\":\"\(fieldData.allFields[0].strValue)\",\"addressLine2\":\"\(fieldData.allFields[1].strValue)\",\"area\":\"\(fieldData.allFields[2].strValue)\",\"city\":\"\(fieldData.allFields[3].strValue)\",\"district\":\"\(fieldData.allFields[4].strValue)\",\"state\":\"\(fieldData.allFields[5].strValue)\",\"country\":\"\(fieldData.allFields[6].strValue)\",\"postcode\":\"\(fieldData.allFields[7].strValue)\",\"location\":\"\(location)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "addPatientRegisteredAddress.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func getupdatepatientaddress(){
        if isWorkingCountryUK() {
            fieldData.allFields[2].strValue = ""
            fieldData.allFields[4].strValue = ""
            fieldData.allFields[5].strValue = ""
        } else {
            fieldData.allFields[5].strValue = ""
        }
        let parameters = "{\"data\":{\"addressId\":\"\(arryaAddres.address_id!)\",\"patientId\":\"\(_currentUser.patient_id!)\",\"addressLine1\":\"\(fieldData.allFields[0].strValue)\",\"addressLine2\":\"\(fieldData.allFields[1].strValue)\",\"area\":\"\(fieldData.allFields[2].strValue)\",\"city\":\"\(fieldData.allFields[3].strValue)\",\"district\":\"\(fieldData.allFields[4].strValue)\",\"state\":\"\(fieldData.allFields[5].strValue)\",\"country\":\"\(fieldData.allFields[6].strValue)\",\"postcode\":\"\(fieldData.allFields[7].strValue)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "updatepatientaddress.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    self.navigationController?.popViewController(animated: true)
                                }
                                self.showResponseMessage(dict: Response)
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
