//
//  VideoViewVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 18/05/21.
//

import UIKit
import Sinch
class SINUIViewController: ParentViewController {
    private(set) var isAppearing = false
    private(set) var isDisappearing = false

    func dismiss() {
    }
}
class VideoViewVC: SINUIViewController {
    @IBOutlet weak var remoteVideoView: UIView!
    @IBOutlet weak var localVideoView: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var cameraFilepButton: UIButton!
    @IBOutlet weak var viewPauseButton: UIButton!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var lblName: JPWidthLabel!
    @IBOutlet weak var callStateLabel: JPWidthLabel!
    @IBOutlet weak var lblPuse: JPWidthLabel!
    @IBOutlet weak var lblLocalPuse: JPWidthLabel!
    
    var isIncamingCall = false
    var durationTimer: Timer?
    var call: SINCall?
    var isChanegeView = false
    private var videoPaused = false
    private var SoundPaused = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCall(call)
        self.lblPuse.text = ""
        self.lblLocalPuse.text = ""
        if isIncamingCall{
            audioController()!.stopPlayingSoundFile()
            call!.answer()
        }
        localVideoView.isHidden = true
        if self.call!.direction == SINCallDirection.incoming  {
            self.callStateLabel.text = ""
           // audioController()!.startPlayingSoundFile(self.pathForSound(string: "incoming.wav") as String, loop: true)
        } else {
            self.callStateLabel.text = "calling..."
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let user = call?.headers as NSDictionary?{
            if isIncamingCall{
                lblName.text = user.getStringValue(key: "Pro_Name_sinch")
            }else{
                lblName.text = user.getStringValue(key: "Pro_Name_Incaming")
            }
        }
    }

    @IBAction func hangup(_ sender: Any) {
        call!.hangup()
        ATCallManager.shared.endCall()
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onSwitchCamera(_ sender: Any) {
        let current = videoController()!.captureDevicePosition
        videoController()!.captureDevicePosition = SINToggleCaptureDevicePosition(current)
    }

    @IBAction func onPauseResumeVideoTapped(_ sender: Any) {
        if videoPaused {
            viewPauseButton.isSelected = false
            call!.resumeVideo()
            self.lblLocalPuse.text = ""
            videoPaused = false
        } else {
            call!.pauseVideo()
            videoPaused = true
            self.lblLocalPuse.text = "Pause"
            viewPauseButton.isSelected = true
        }
        
    }
    @IBAction func onSoundTapped(_ sender: Any) {
        if SoundPaused {
            soundButton.isSelected = false
            audioController()!.unmute()
            SoundPaused = false
        } else {
            audioController()!.mute()
            
            SoundPaused = true
            soundButton.isSelected = true
        }
    }


    }
extension VideoViewVC{
func setCall(_ call: SINCall?) {
    self.call = call
    self.call!.delegate = self
}
func audioController() -> SINAudioController? {
    return _appDelegator.client!.audioController()
}
func videoController() -> SINVideoController? {
    return _appDelegator.client!.videoController()
}
@objc func onDurationTimer(_ unused: Timer?) {
    let duration = Int(Date().timeIntervalSince(call!.details.establishedTime))
    setDuration(duration)
}
func setDuration(_ seconds: Int) {
    self.callStateLabel.text = String(format: "%02d:%02d", seconds / 60, seconds % 60)
}
@objc func internal_updateDuration(_ timer: Timer?) {
    let selector = NSSelectorFromString(timer?.userInfo as? String ?? "")
    if responds(to: selector) {
    //#pragma clang diagnostic push
    //#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        perform(selector, with: timer)
    //#pragma clang diagnostic pop
    }
}
func startCallDurationTimer(with sel: Selector) {
    let selectorAsString = NSStringFromSelector(sel)
   durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(internal_updateDuration), userInfo: selectorAsString, repeats: true)
  
}
func stopCallDurationTimer() {
    if (durationTimer != nil){
        durationTimer!.invalidate()
        durationTimer = nil
    }
}
func pathForSound(string: String) -> NSString {
    let nsSt = Bundle.main.resourcePath! as NSString

    return nsSt.appendingPathComponent(string) as NSString
}
}
extension VideoViewVC: SINCallDelegate{
func callDidProgress(_ call: SINCall!) {
    self.callStateLabel.text = "ringing..."
  //  audioController()!.startPlayingSoundFile(self.pathForSound(string: "ringback.wav") as String, loop: true)
}

func callDidEstablish(_ call: SINCall!) {
    let view = videoController()!.remoteView()
    view?.sin_isFullscreen()
    view?.contentMode = .scaleAspectFill
    view?.sin_enableFullscreen(true)
    remoteVideoView.addSubview(view!)
    localVideoView.addSubview(videoController()!.localView())
    startCallDurationTimer(with: #selector(onDurationTimer(_:)))
    remoteVideoView.isHidden = false
    localVideoView.isHidden = false
    audioController()!.stopPlayingSoundFile()
}

func callDidEnd(_ call: SINCall!) {
    audioController()!.stopPlayingSoundFile()
    stopCallDurationTimer()
    videoController()!.remoteView().removeFromSuperview()
    audioController()!.disableSpeaker()
    ATCallManager.shared.endCall()
    _ = self.navigationController?.popViewController(animated: true)
}

func callDidAddVideoTrack(_ call: SINCall!) {
   
    audioController()!.enableSpeaker()
   
}
func callDidPauseVideoTrack(_ call: SINCall!) {
    self.lblPuse.text = "Pause"
     remoteVideoView.isHidden = false
}

func callDidResumeVideoTrack(_ call: SINCall!) {
    self.lblPuse.text = ""
    remoteVideoView.isHidden = false
}

func clientDidFail(_ client: SINClient!, error: Error!) {
    audioController()!.stopPlayingSoundFile()
    stopCallDurationTimer()
    videoController()!.remoteView().removeFromSuperview()
    audioController()!.disableSpeaker()
    ATCallManager.shared.endCall()
    _ = self.navigationController?.popViewController(animated: true)
}
func clientDidStop(_ client: SINClient!) {
    audioController()!.stopPlayingSoundFile()
    stopCallDurationTimer()
    videoController()!.remoteView().removeFromSuperview()
    audioController()!.disableSpeaker()
    ATCallManager.shared.endCall()
    _ = self.navigationController?.popViewController(animated: true)
}
}
