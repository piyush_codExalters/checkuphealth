//
//  AddressVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 30/04/21.
//

import UIKit
import Alamofire

class AddressVC: ParentViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnAddNew: submitButton!
    @IBOutlet weak var lblOR: LabelRegular!
    @IBOutlet weak var hightView: NSLayoutConstraint!
    @IBOutlet weak var textPostCode: JPWidthTextField!
    @IBOutlet weak var viewBg: UIView!
    var arryaAddres = [AddressListName]()
    var delegatesAddress : ProfileVC!
    var delegatesAddress4 : NewProfileVC!
    var delegatesAddress1 : EditProfileVC!
    var delegatesAddress2 : EmergencyAddVC!
    var delegatesAddress3 : AppointmentConfirmationVC!
    var delegatesAddress5 : AppointmentDetailsVC!
    var strpostCode = ""
    var isPostCode = false
    var sessionDataTask: URLSessionDataTask!
    var arrData :[Address] = []
    var isSelectionIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.makeCornerRound(redias: 5)
        btnAddNew.isHidden = false
        if isPostCode{
            hightView.constant = 0 // 50 post code serach
            viewBg.isHidden = true // false
            //lblOR.isHidden = true // false
            lblOR.text = "" // "OR"
        }else{
            hightView.constant = 0
            viewBg.isHidden = true
           // lblOR.isHidden = true
            lblOR.text = ""
        }
        
        if let _ = delegatesAddress3 {
            lblOR.text = "Kindly provide details of where you expect you will be at the time of your appointment. This way, we will know where to send help if it is needed."
        }else if let _ = delegatesAddress5 {
            lblOR.text = "Kindly provide details of where you expect you will be at the time of your appointment. This way, we will know where to send help if it is needed."
        }else{
            lblOR.text = ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getpatientaddress()
    }
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressAddVC")  as! AddressAddVC
        mapVc1.strHeader = "Add Address"
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    @IBAction func btnEditClicked(_ sender: UIButton) {
        let mapVc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressAddVC")  as! AddressAddVC
        mapVc1.strHeader = "Edit Address"
        mapVc1.arryaAddres = arryaAddres[sender.tag]
        navigationController?.pushViewController(mapVc1, animated: true)
    }
    
    @IBAction func btnDeletedClicked(_ sender: UIButton) {
        showAlert(title: _appName, msgString: "Are you sure you want to delete address?", Ohk: "OK", cancal: "CANCEL") {
            self.getdeleteaddress(Index : sender.tag)
        }
    }
    
    @IBAction func textSerachPostCode(_ sender: UITextField) {
        strpostCode = sender.text!
        // getAddressFromLatLong(strAddress: strpostCode)
        // postcodeEnter(str: strpostCode)
        getAddressFromLatLong(StrKey: strpostCode)
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        strpostCode = ""
        arrData.removeAll()
        btnAddNew.isHidden = false
        tableView.reloadData()
        return true
    }
    
    
    
}
extension AddressVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSelectionIndex = indexPath.row
        if let _ = delegatesAddress5{
            delegatesAddress5.getAddress(addressID: arryaAddres[indexPath.row].address_id)
        }else if let _ = delegatesAddress4{
            delegatesAddress4.getAddress(str: arryaAddres[indexPath.row].address, address: arryaAddres[indexPath.row], index: isSelectionIndex)
        }else if let _ = delegatesAddress1{
            delegatesAddress1.getAddress(str: arryaAddres[indexPath.row].address, address: arryaAddres[indexPath.row], index: isSelectionIndex)
        }else if let _ = delegatesAddress2{
            delegatesAddress2.getAddress(str: arryaAddres[indexPath.row].address, address: arryaAddres[indexPath.row], index: isSelectionIndex)
        }else if let _ = delegatesAddress3{
            
            if self.arrData.count > 0{
                var addree : AddressListName!
                
                let param = ["address":arrData[indexPath.row].name,"addressLine1":"","addressLine2":"","address_id":"","area":"","city":"","country":"","district":"","postcode":arrData[indexPath.row].title,"state":""]
                
                
                addree = AddressListName(dic: param as NSDictionary)
                
                delegatesAddress3.getAddress(str: arrData[indexPath.row].name, address: addree, index: isSelectionIndex)
            }else {
                if String.validateStringValue(str: arryaAddres[indexPath.row].postcode) {
                    return showFailMessage(message: "Selected address does not have postcode!")
                }
                delegatesAddress3.getAddress(str: arryaAddres[indexPath.row].address, address: arryaAddres[indexPath.row], index: isSelectionIndex)
            }
        }else if let _ = delegatesAddress{
            delegatesAddress.getAddress(str: arryaAddres[indexPath.row].address, address: arryaAddres[indexPath.row], index: isSelectionIndex)
        }
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if String.validateStringValue(str: strpostCode){
            return arryaAddres.count
        }
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if String.validateStringValue(str: strpostCode){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name") as! HealthConditionsCell
            cell.btnEdti.tag = indexPath.row
            if arryaAddres.count == 1{
                cell.btnDeleted.isHidden = true
            }else{
                cell.btnDeleted.isHidden = false
            }
            if isSelectionIndex == indexPath.row{
                cell.btnselection.isSelected = true
            }else{
                cell.btnselection.isSelected = false
            }
            if let _ = delegatesAddress{
                cell.btnselection.isHidden = true
                cell.widhtViewLayout.constant = 0
                cell.widhtLayoutSelection.constant = 0
            }else{
                cell.btnselection.isHidden = false
                cell.widhtViewLayout.constant = 10
                cell.widhtLayoutSelection.constant = 22
            }
            cell.btnDeleted.tag = indexPath.row
            cell.lblName.text = arryaAddres[indexPath.row].address
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "post") as! HealthConditionsCell
        cell.lblName.text = arrData[indexPath.row].title + "\n" + arrData[indexPath.row].name
        if isSelectionIndex == indexPath.row{
            cell.btnselection.isSelected = true
        }else{
            cell.btnselection.isSelected = false
        }
        if let _ = delegatesAddress{
            cell.btnselection.isHidden = true
            cell.widhtViewLayout.constant = 0
            cell.widhtLayoutSelection.constant = 0
        }else{
            cell.btnselection.isHidden = false
            cell.widhtViewLayout.constant = 10
            cell.widhtLayoutSelection.constant = 22
        }
        return cell
    }
    
}
extension AddressVC{
    func getpatientaddress(){
        let parameters = "{\"data\":{\"patientId\":\"\(_currentUser.patient_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "patientaddress.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                self.arryaAddres.removeAll()
                                if Response.getIntValue(key: "success") == 1{
                                    self.tableView.backgroundView = nil
                                    if let data = Response.value(forKey: "data") as? NSArray{
                                        for obj in data{
                                            self.arryaAddres.append(AddressListName(dic: obj as! NSDictionary))
                                        }
                                    }
                                    self.tableView.reloadData()
                                }else{
                                    if self.arryaAddres.count == 0{
                                        self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "To add an address please click on add New button. You can also add multiple addresses by clicking on add New button.", image : nil)
                                    }else{
                                        self.tableView.backgroundView = nil
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func getdeleteaddress(Index : Int){
        let parameters = "{\"data\":{\"addressId\":\"\(arryaAddres[Index].address_id!)\"}}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "deletepatientaddress.php", param: parameters) { (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async {
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    self.arryaAddres.remove(at: Index)
                                }
                                self.showResponseMessage(dict: Response)
                                if self.arryaAddres.count == 0{
                                    self.tableView.backgroundView =  self.setPlaceHolder(strMessage : "To add an address please click on add New button. You can also add multiple addresses by clicking on add New button.", image : nil)
                                }else{
                                    self.tableView.backgroundView = nil
                                }
                            }
                            
                            self.tableView.reloadData()
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    func getAddressFromLatLong(StrKey : String) {
        let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(StrKey)&types=%28regions%29&key=\(googleKey)"
        // "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=amoeba&location=\(23.000566),\(72.502294)&radius=500&types=establishment&key=\(googleKey)"
        // "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=amoeba&location=37.76999%2C-122.44696&radius=500&strictbounds=true&types=establishment&key=YOUR_API_KEY"
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                self.arrData.removeAll()
                let responseJson = response.result.value! as! NSDictionary
                
                if let results = responseJson.object(forKey: "predictions")! as? NSArray {
                    if results.count > 0 {
                        for obj in results{
                            
                            if let postCode = (obj as AnyObject).value(forKey: "types") as? NSArray{
                                
                                for obj2 in postCode{
                                    print(obj2)
                                    if obj2 as! String == "postal_code"{
                                        print(obj)
                                        self.arrData.append(Address(googleData: obj as! NSDictionary))
                                    }
                                    
                                }
                            }
                        }
                        if self.arrData.count > 0{
                            self.btnAddNew.isHidden = true
                        }else {
                            self.btnAddNew.isHidden = false
                        }
                        self.tableView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
}
