//
//  SetAppLockVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 14/04/21.
//

import UIKit

class SetAppLockVC: ParentViewController {

    @IBOutlet weak var lblsetAppLock: LabelSemiBold!
    @IBOutlet weak var lblInformation: LabelMedium!
    @IBOutlet weak var btnContinue: LoginButton!
    
    var allData = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblsetAppLock.text = "Set App Lock"
        lblInformation.text = "So only you can see your health information,\nWe need you to set up this security. It’ll take\nLess than a minute."
        btnContinue.setTitle("Continue", for: .normal)
        getFirebaseAnalytics(screenName : getEventName.SET_APP_LOCK,isPatient_id : true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Pin"{
            let vc = segue.destination as! PinVC
            vc.allData = allData
        }
    }
    
    @IBAction func btnContiueClicked(_ sender: Any) {
            self.performSegue(withIdentifier: "Pin", sender: nil)

    }
    
    

}
