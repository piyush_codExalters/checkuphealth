//
//  AppDelegate.swift
//  CheckupHealth
//
//  Created by codExalters1 on 06/04/21.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseMessaging
import FirebaseInstanceID
import Firebase
import FacebookCore
import AppTrackingTransparency
import NewRelic



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var tab = TabChackup()
    var parantView = ParentViewController()
    
    lazy internal var centralActivityIndicator : UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        return act
    }()
    let viewLoader = UIView()
    let view = UIView()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        NewRelic.start(withApplicationToken:"eu01xxb1e632c800770fdc7ca74abd76d37e1c9ad5-NRMA")
        IQKeyboardManager.shared.enable = true
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "https://dl.checkuphealth.co.uk"
        FirebaseApp.configure()
        Messaging.messaging().isAutoInitEnabled = true
        Messaging.messaging().delegate = self
        registerPushNotification(application: application)
        
        
        // =============ADDED BELOW CODE IN NOTIFICATION PERMISSION POPUP BY YAMUNA======
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            //Your code
//            print("ATT Permission Called --- PIYUSH ")
//            self.requestDataPermission()
//        }
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        return true
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if #available(iOS 15.0, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                
            })
        }
    }
    func requestDataPermission() {
        
        if #available(iOS 14, *) {
            
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                switch status {
                case .authorized:
                    Settings.setAdvertiserTrackingEnabled(true)
                case .notDetermined:
                    Settings.setAdvertiserTrackingEnabled(false)
                case .restricted:
                    Settings.setAdvertiserTrackingEnabled(false)
                case .denied:
                    Settings.setAdvertiserTrackingEnabled(false)
                @unknown default:
                    Settings.setAdvertiserTrackingEnabled(false)
                }
            })
        }else{
            Settings.setAdvertiserTrackingEnabled(true)
        }
    }
    
  
    
        func gotoAppPrivacySettings() {
            guard let url = URL(string: UIApplication.openSettingsURLString),
                UIApplication.shared.canOpenURL(url) else {
                    assertionFailure("Not able to open App privacy settings")
                    return
            }

            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }

    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            
            
        )
        
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        checkScreenLock()
    }
    
}

extension AppDelegate{
    func checkScreenLock(){
        if let data = _userDefault.value(forKey: "userInfo") as? NSDictionary{
            _currentUser = UserDetail(dic: data)
            if _currentUser.isMobileVerified == "0"{
            }else if _currentUser.isOnboarding == "0"{
            }else if _currentUser.nPin == ""{
            }else{
                let nav = self.window?.rootViewController as? UINavigationController
                if nav!.viewControllers.last is ScreenPinVC{
                }else if nav!.viewControllers.last is SplashVC{
                }else{
                    parantView.initLockScreen()
                }
            }
        }
    }
    
    
    
    
}
extension AppDelegate{
    
     func application(_ application: UIApplication, open url: URL, sourceApplication: String?,annotation: Any) -> Bool {
       if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
         // Handle the deep link. For example, show the deep-linked content or
         // apply a promotional offer to the user's account.
         // [START_EXCLUDE]
         // In this sample, we just open an alert.
           
           
         handleDynamicLink(dynamicLink)
         // [END_EXCLUDE]
         return true
       }
       // [START_EXCLUDE silent]
       // Show the deep link that the app was called with.
       showDeepLinkAlertView(withMessage: "\(url)")
       // [END_EXCLUDE]
       return false
     }

     // [END openurl]
     // [START continueuseractivity]
     func application(_ application: UIApplication, continue userActivity: NSUserActivity,restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
       let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
           // [START_EXCLUDE]
           
           
           if let dynamiclink = dynamiclink {
               
               
             self.handleDynamicLink(dynamiclink)
               
               
           }
           // [END_EXCLUDE]
         }

       // [START_EXCLUDE silent]
       if !handled {
         // Show the deep link URL from userActivity.
         showDeepLinkAlertView(withMessage: userActivity.webpageURL?.absoluteString ?? "")
       }else{
           
           print(performRequest (urlString: userActivity.webpageURL?.absoluteString ?? ""))

       }
         
       // [END_EXCLUDE]
       return handled
     }

     // [END continueuseractivity]
     func handleDynamicLink(_ dynamicLink: DynamicLink) {
       
//
//       let matchConfidence: String
//       if dynamicLink.matchType == .weak {
//         matchConfidence = "Weak"
//       } else {
//         matchConfidence = "Strong"
//       }
//
//
//
//       let message = "App URL: \(dynamicLink.url?.absoluteString ?? "")\n" +
//         "Match Confidence: \(matchConfidence)\nMinimum App Version: \(dynamicLink.minimumAppVersion ?? "")"
       showDeepLinkAlertView(withMessage: dynamicLink.url?.absoluteString ?? "")
     }

     func showDeepLinkAlertView(withMessage message: String) {
         vPatientReferralCode = ""
         let component = message.components(separatedBy: "=") // 2
               if component.count > 1, let productId = component.last { // 3
                   vPatientReferralCode = productId
          }
         print(vPatientReferralCode)

//         let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            let alertController = UIAlertController(
//              title: "Deep-link Data",
//              message: message,
//              preferredStyle: .alert
//            )
//            alertController.addAction(okAction)
//         window?.rootViewController?.present(alertController, animated: true, completion: nil)

     }
    
    
    func performRequest(urlString: String){

    if let url = URL(string: urlString){
    let session = URLSession(configuration: .default)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "HEAD"
    let task = session.dataTask(with: urlRequest, completionHandler: handle(data:response:error:))
    task.resume()
    }
    }


    func handle (data: Data?, response: URLResponse?, error: Error?){
    if error != nil {
        print (error)
        return
    }
    if let safeData = data {
        let dataString = String(data: safeData, encoding: .utf8)
        let expandedURL = response?.url?.absoluteString
        print(expandedURL!)
        DispatchQueue.main.async { [self] in
        showDeepLinkAlertView(withMessage: response?.url?.absoluteString ?? "")
        }
    }
    }
    
    
}

