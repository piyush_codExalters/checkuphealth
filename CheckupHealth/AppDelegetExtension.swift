//
//  AppDelegetExtension.swift
//  CheckupHealth
//
//  Created by codExalters1 on 18/05/21.
//

import UIKit
import CallKit
import PushKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID

extension AppDelegate: MessagingDelegate,UNUserNotificationCenterDelegate{
    func registerPushNotification(application: UIApplication){
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_,_ in })
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//        application.registerForRemoteNotifications()
        
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { granted, error in
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.requestDataPermission()
                })
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        _userDefault.set(fcmToken, forKey: "vpushToken")
        _userDefault.synchronize()
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
        _userDefault.set(fcmToken, forKey: "vpushToken")
        _userDefault.synchronize()
    }
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingDelegate) {
        print("Received data message: \(remoteMessage.description)")
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let user = notification.request.content.userInfo as? NSDictionary{
            var dictOther = NSDictionary()
            let nID = user.getStringValue(key: "nId")
            let type = user.getIntValue(key: "type")
            let strOther = user.getStringValue(key: "vOther")
            var strTitle = _appName
            var strMsg = ""
            
            if let payload = user["aps"] as? NSDictionary{
                if let alert = payload["alert"] as? NSDictionary{
                    strTitle = alert.getStringValue(key: "title")
                    strMsg = alert.getStringValue(key: "body")
                }
            }
            let data = strOther.data(using: .utf8)!
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? NSDictionary
                {
                    dictOther = json
                }
            } catch let error as NSError {
                print(error)
            }
            print(dictOther)
            print(dictOther)
            print(nID)
            print(type)
            print(strOther)
            switch type{
            case 2,7:
                
                _defaultCenter.post(name: NSNotification.Name(rawValue: "appointmentDetails"), object: nil, userInfo: nil)
              //  AppointmentDetail(Id: nID, isDoctor : dictOther.getStringValue(key: "appointment_for"))
            default:
                break
            }
            completionHandler([.alert,.sound,.badge])


        }
        
    }
    
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    // MARK: - Notification Receive
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let user = response.notification.request.content.userInfo as? NSDictionary{
            
            var dictOther = NSDictionary()
            let nID = user.getStringValue(key: "id")
            let type = user.getIntValue(key: "type")
            let strOther = user.getStringValue(key: "vOther")
            var strTitle = _appName
            var strMsg = ""
            print(user)
            if let payload = user["aps"] as? NSDictionary{
                if let alert = payload["alert"] as? NSDictionary{
                    strTitle = alert.getStringValue(key: "title")
                    strMsg = alert.getStringValue(key: "body")
                }
            }
            let data = strOther.data(using: .utf8)!
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? NSDictionary
                {
                    dictOther = json
                }
            } catch let error as NSError {
                print(error)
            }
            print(dictOther)
            print(nID)
            print(type)
            print(strOther)
            switch type{
            case 0,3:
                LoginFromNewDevice()
            case 1:
                break
               // videoStart()
              //  ATCallManager.shared.incommingCall(from: _appName, delay: 0)
            case 4:
                PatientQuestion(Id: nID)
            case 2,7:
                AppointmentDetail(Id: nID, isDoctor : dictOther.getStringValue(key: "appointment_for"))
            case 6:
                PHRListScreenAccording(Id: Int(nID)!)
            default:
                break
            }
        }
        completionHandler()
    }
}
extension AppDelegate{
    func LoginFromNewDevice(){
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
        _appDelegator.window?.rootViewController = nav
        if let data = _userDefault.value(forKey: "userInfo") as? NSDictionary{
            _currentUser = UserDetail(dic: data)
            if _currentUser.isMobileVerified == "0"{
                let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                nav.viewControllers = [Information]
            }else if _currentUser.isOnboarding == "0"{
                let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                _appDelegator.window?.rootViewController = nav
            }else if _currentUser.nPin == ""{
                let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                nav.viewControllers = [Information]
            }else{
                let vclogin = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScreenPinVC") as! ScreenPinVC
                nav.viewControllers = [vclogin]
            }
        }else{
            let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
            nav.viewControllers = [Information]
        }
    }
    func PatientQuestion(Id: String){
        if let data = _userDefault.value(forKey: "userInfo") as? NSDictionary{
            let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
            let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
            let tabVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
            nav.viewControllers.append(Information)
            nav.viewControllers.append(tabVC)
            _appDelegator.window?.rootViewController = nav
            _currentUser = UserDetail(dic: data)
            if UIApplication.shared.applicationState == .inactive{
               
                DispatchQueue.main.async {
                let vc = self.tab.viewControllers?[0] as! UINavigationController
                vc.popToRootViewController(animated: false)
                let vc2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let vc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "QuestionDetailsVC") as! QuestionDetailsVC
                vc1.QuestionId = Id
                vc.viewControllers = [vc2,vc1]
                self.tab.selectedIndex = 0
                }
            }else{
                let vc = self.tab.viewControllers?[0] as! UINavigationController
                vc.popToRootViewController(animated: false)
                let vc2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let vc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "QuestionDetailsVC") as! QuestionDetailsVC
                vc1.QuestionId = Id
                vc.viewControllers = [vc2,vc1]
                self.tab.selectedIndex = 0
            }
        
        }
        
    }
    func AppointmentDetail(Id: String, isDoctor : String){
        if let data = _userDefault.value(forKey: "userInfo") as? NSDictionary{
           
            _currentUser = UserDetail(dic: data)
            if UIApplication.shared.applicationState == .inactive{
                DispatchQueue.main.async {
                    let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                    let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
                    let tabVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                    nav.viewControllers.append(Information)
                    nav.viewControllers.append(tabVC)
                    _appDelegator.window?.rootViewController = nav
                    let vc = self.tab.viewControllers?[0] as! UINavigationController
                    vc.popToRootViewController(animated: false)
                    let vc2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    let vc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDetailsVC") as! AppointmentDetailsVC
                    vc1.appoinmentID = Id
                    vc1.isFor = isDoctor
                    vc.viewControllers = [vc2,vc1]
                    self.tab.selectedIndex = 0
                    

                }
            }else{
                let vc = self.tab.viewControllers?[0] as! UINavigationController
                vc.popToRootViewController(animated: false)
                let tabVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
                let vc2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let vc1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AppointmentDetailsVC") as! AppointmentDetailsVC
                vc1.appoinmentID = Id
                vc1.isFor = isDoctor
                vc.viewControllers.removeAll()
                vc.viewControllers = [tabVC,vc2,vc1]
                self.tab.selectedIndex = 0

            }
        }
        
    }
    func PHRListScreenAccording(Id: Int){
        if let data = _userDefault.value(forKey: "userInfo") as? NSDictionary{
            let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
            let Information = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
            let tabVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TabChackup") as! TabChackup
            nav.viewControllers.append(Information)
            nav.viewControllers.append(tabVC)
            parantView.initLockScreen()
            _appDelegator.window?.rootViewController = nav
            _currentUser = UserDetail(dic: data)
            if UIApplication.shared.applicationState == .inactive{
                
                DispatchQueue.main.async {
                    let vc = self.tab.viewControllers?[0] as! UINavigationController
                    vc.popToRootViewController(animated: false)
                    let vc2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    if Id == 0{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "ManagmnetVC") as! ManagmnetVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 1{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "HealthConditionsVC") as! HealthConditionsVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 2{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "MedicationVC") as! MedicationVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 3{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AllergiesAdverseReactionVC") as! AllergiesAdverseReactionVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 4{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "VaccinationsVC") as! VaccinationsVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 5{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "MedicalReportListVC") as! MedicalReportListVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 6{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "SurgeryProcedureVC") as! SurgeryProcedureVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 7{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "LifestyleHabitsVC") as! LifestyleHabitsVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 8{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "FamilyHistoryVC") as! FamilyHistoryVC
                        vc.viewControllers = [vc2,vc1]
                    }else if Id == 9{
                        let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "EmergencyVC") as! EmergencyVC
                        vc.viewControllers = [vc2,vc1]
                    }
                    self.tab.selectedIndex = 0
                }
            }else{
                let vc = self.tab.viewControllers?[0] as! UINavigationController
                vc.popToRootViewController(animated: false)
                let vc2 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                if Id == 0{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "ManagmnetVC") as! ManagmnetVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 1{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "HealthConditionsVC") as! HealthConditionsVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 2{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "MedicationVC") as! MedicationVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 3{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "AllergiesAdverseReactionVC") as! AllergiesAdverseReactionVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 4{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "VaccinationsVC") as! VaccinationsVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 5{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "MedicalReportListVC") as! MedicalReportListVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 6{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "SurgeryProcedureVC") as! SurgeryProcedureVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 7{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "LifestyleHabitsVC") as! LifestyleHabitsVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 8{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "FamilyHistoryVC") as! FamilyHistoryVC
                    vc.viewControllers = [vc2,vc1]
                }else if Id == 9{
                    let vc1 = UIStoryboard(name: "MYPHR", bundle: nil).instantiateViewController(withIdentifier: "EmergencyVC") as! EmergencyVC
                    vc.viewControllers = [vc2,vc1]
                }
                self.tab.selectedIndex = 0
                
            }
        
        }
        
        
    }

}
