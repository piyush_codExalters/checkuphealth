//
//  TabChackup.swift
//  CheckupHealth
//
//  Created by codExalters1 on 15/04/21.
//

import UIKit
extension Notification.Name {
    static let refreshAllTabs = Notification.Name("RefreshAllTabs")
}


class TabChackup: UITabBarController, UITabBarControllerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tabBar.layer.masksToBounds = true
        self.tabBar.isTranslucent = true
        self.tabBar.layer.cornerRadius = 20
        if #available(iOS 11.0, *) {
            self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }

    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _appDelegator.tab = self
        let Lang = _userDefault.value(forKey: "Language") as? String
         if Lang == nil || Lang == "ZW"{
                let nav = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NavSettingVC") as! NavSettingVC
                nav.navigationController?.isNavigationBarHidden = false
                let vc =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "QuestionsVC") as! QuestionsVC
                nav.viewControllers = [vc]
                vc.navigationController?.isNavigationBarHidden = false
                var arrVC = _appDelegator.tab.viewControllers
                if arrVC!.count == 5{
                        return
                }
                arrVC?.insert(nav, at: 1)

                _appDelegator.tab.viewControllers = arrVC
                (_appDelegator.tab.tabBar.items)?[1].title = "Question"
                (_appDelegator.tab.tabBar.items)?[1].image = UIImage(named: "ic_questions_active")
        }else{
            var arrVc = _appDelegator.tab.viewControllers
            if arrVc!.count == 4{
                           return
                       }
                    arrVc?.remove(at: 1)
                    _appDelegator.tab.viewControllers = arrVc
           
           
            
        }
    }
    
    
  
   
}
class NavLoginVC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}
class NavSettingVC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}

//self.tabBar.unselectedItemTintColor = UIColor.white
extension UIImage{
    //Draws the top indicator by making image with filling color
    class func drawTabBarIndicator(color: UIColor, size: CGSize, onTop: Bool) -> UIImage {
        let indicatorHeight = size.height / 30
        let yPosition = onTop ? 0 : (size.height - indicatorHeight)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: yPosition, width: size.width, height: indicatorHeight))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
