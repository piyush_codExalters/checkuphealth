//
//  ImpExtentions.swift
//  SnapTag
//
//  Created by Yudiz Solutions Pvt.Ltd. on 21/04/16.
//  Copyright © 2016 Yudiz Solutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    
    func lineHeightWithConstrainedWidth() -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [NSStringDrawingOptions.usesLineFragmentOrigin], context: nil)
        return ceil(boundingBox.height)
    }
    
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [NSStringDrawingOptions.usesLineFragmentOrigin], context: nil)
        return ceil(boundingBox.height)
    }
}

// MARK: - Attributed
extension NSAttributedString {
    
    // This will give combined string with respective attributes
//    class func attributedText(texts: [String], attributes: [[String : AnyObject]]) -> NSAttributedString {
//        let attbStr = NSMutableAttributedString()
//        for (index,element) in texts.enumerated() {
//            attbStr.append(NSAttributedString(string: element, attributes: attributes[index]))
//        }
//        return attbStr
//    }
}

extension UILabel {
    
    func animateLabelAlpha( fromValue: NSNumber, toValue: NSNumber, duration: CFTimeInterval) {
        let titleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        titleAnimation.duration = duration
        titleAnimation.fromValue = fromValue
        titleAnimation.toValue = toValue
        titleAnimation.isRemovedOnCompletion = true
        layer.add(titleAnimation, forKey: "opacity")
    }
    
//    func setAttributedText(text: String, font: UIFont, color: UIColor) {
//        let mutatingAttributedString = NSMutableAttributedString(string: text)
//        mutatingAttributedString.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, text.characters.count))
//        mutatingAttributedString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, text.characters.count))
//        attributedText = mutatingAttributedString
//    }
//
    // This will give combined string with respective attributes
//    func setAttributedText(texts: [String], attributes: [[String : AnyObject]]) {
//        let attbStr = NSMutableAttributedString()
//        for (index,element) in texts.enumerated() {
//            attbStr.append(NSAttributedString(string: element, attributes: attributes[index]))
//        }
//        attributedText = attbStr
//    }
//
//    func addCharactersSpacing(spacing:CGFloat, text:String) {
//        let attributedString = NSMutableAttributedString(string: text)
//        attributedString.addAttribute(NSKernAttributeName, value: spacing * _widthRatio, range: NSMakeRange(0, text.characters.count))
//        self.attributedText = attributedString
//    }
}


extension UIAlertController {
    class func actionWithMessage(message: String?, title: String?, type: UIAlertController.Style, buttons: [String], controller: UIViewController ,block:@escaping (_ tapped: String)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: type)
        for btn in buttons {
            alert.addAction(UIAlertAction(title: btn, style: UIAlertAction.Style.default, handler: { (action) -> Void in
                block(btn)
            }))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            block("Cancel")
        }))
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func actionWithMessageDestructive(message: String?, title: String?, type: UIAlertController.Style, buttons: [String], controller: UIViewController ,block:@escaping (_ tapped: String)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: type)
        for btn in buttons {
            alert.addAction(UIAlertAction(title: btn, style: UIAlertAction.Style.destructive, handler: { (action) -> Void in
                block(btn)
            }))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            block("Cancel")
        }))
        controller.present(alert, animated: true, completion: nil)
    }
}


