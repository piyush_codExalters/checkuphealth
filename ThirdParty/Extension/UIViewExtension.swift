//
//  UIViewExtension.swift
//  manup
//
//  Created by Tom Swindell on 09/12/2015.
//  Copyright © 2015 The App Developers. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
    layer.shadowColor = UIColor.gray.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = .zero
    
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius * _heighRatio))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
       
    }
}

//MARK: - Graphics
extension UIView {

    func addDashedBorder() {
        //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.lineWidth = 2
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [2,3]

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0),
                                CGPoint(x: 354 * _widthRatio, y: 0)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
extension UIView {
    func makeCornerRoundBorderwithoutShadow(redias : Int = 15,Color : UIColor) {
        layer.cornerRadius = CGFloat(redias) * _widthRatio
        layer.borderWidth = 1 * _widthRatio
        layer.borderColor = Color.cgColor
    }
    func makeCornerRoundBorderwithoutShadow1(redias : Int = 15,Color : UIColor) {
        layer.cornerRadius = CGFloat(redias) * _widthRatio
        layer.borderWidth = 5 * _widthRatio
        layer.borderColor = Color.cgColor
    }
    
    func viewBorder(redias : Int = 15, Color : UIColor){
        layer.borderWidth = 1 * _widthRatio
        layer.borderColor = Color.cgColor
        layer.cornerRadius = CGFloat(redias) * _heighRatio
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
    }
    func maskCircle() {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height * _widthRatio  / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
    func maskCirclewe() {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height   / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius * _widthRatio, height: radius * _widthRatio))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
       // layer.cornerRadius = CGFloat(redias) * _widthRatio
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
    }
    func makeCornecscrRoun1(redias : Int = 15) {
        layer.cornerRadius = CGFloat(redias) * _heighRatio
    }
    func makeCornerRound(redias : Int = 15) {
        layer.cornerRadius = CGFloat(redias) * _widthRatio
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
    }
    func makeCornerRoundClear(redias : Int = 15) {
        layer.cornerRadius = CGFloat(redias) * _widthRatio
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOpacity = 0
        layer.shadowOffset = .zero
    }
    func makeBorderRound(redias : Int = 15) {
        layer.cornerRadius = CGFloat(redias) * _widthRatio
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
    }

    func makeCornerRoundxs(redias : Int = 15) {
        layer.cornerRadius = CGFloat(redias) * _heighRatio
        
    }
    func makeCornerRound1(redias : Int = 15) {
      //  layer.cornerRadius = CGFloat(redias) * _heighRatio
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
    }
    
    func makeCornerRoundBorder(redias : Int = 15,Color : UIColor) {
           layer.cornerRadius = CGFloat(redias) * _heighRatio
           layer.shadowColor = UIColor.gray.cgColor
           layer.shadowOpacity = 0.5
           layer.shadowOffset = .zero
           layer.borderWidth = 1 * _widthRatio
           layer.borderColor = Color.cgColor
       }
    
    func makeshadow(){
    layer.shadowColor = UIColor.gray.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = .zero
    }
     func makeRound(){
            contentMode = UIView.ContentMode.scaleAspectFill
            layer.cornerRadius = self.frame.height * _widthRatio / 2 
            layer.shadowColor = UIColor.clear.cgColor
            layer.shadowOpacity = 1
            layer.shadowOffset = .zero
         }
    func makeBorderRound(){
            contentMode = UIView.ContentMode.scaleAspectFill
            layer.cornerRadius = self.frame.height * _widthRatio / 2
            layer.borderColor = UIColor.lightGray.cgColor
            layer.borderWidth = 0.8
         }
    func makeBorderRoundWhite(){
               contentMode = UIView.ContentMode.scaleAspectFill
               layer.cornerRadius = self.frame.height * _widthRatio / 2
               layer.borderColor = UIColor.white.cgColor
               layer.borderWidth = 0.8
    }
    func makeRound1(){
       contentMode = UIView.ContentMode.scaleAspectFill
       layer.cornerRadius = self.frame.height  / 2
       layer.shadowColor = UIColor.gray.cgColor
       layer.shadowOpacity = 1
       layer.shadowOffset = .zero
    }
    
    func makeRoundBlack() {
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 0.7
        layer.cornerRadius = (self.frame.height * _widthRatio) / 2.0
        clipsToBounds = true
    }
    
    func makeShadow(radius: CGFloat){
        clipsToBounds = false
        layer.shadowColor = UIColor(white: 0.0, alpha: 0.9).cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 7 * _heighRatio

    }
    
    func makeLoginRound() {
        layer.cornerRadius = self.frame.height / 2.0
        clipsToBounds = true
    }
    
    func makeCustomRound(radius:CGFloat,bc:UIColor) {
        layer.cornerRadius = radius
        layer.borderColor = bc.cgColor
        layer.borderWidth = 0.5
        clipsToBounds = true
    }
    
    func fadeAlpha(toAlpha: CGFloat, duration time: TimeInterval) {
        UIView.animate(withDuration: time) { () -> Void in
            self.alpha = toAlpha
        }
    }
    
    // Will add mask to given image
    func mask(maskImage: UIImage) {
        let mask: CALayer = CALayer()
        mask.frame = CGRect(x: 0, y: 0, width: maskImage.size.width, height: maskImage.size.height)//CGRectMake( 0, 0, maskImage.size.width, maskImage.size.height)
        mask.contents = maskImage.cgImage
        layer.mask = mask
        layer.masksToBounds = true
    }
    
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.04
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: self.center.x - 8, y: self.center.y)
        animation.toValue = CGPoint(x: self.center.x + 8, y: self.center.y)
        self.layer.add(animation, forKey: "position")
    }
}

extension UIView {
    
    // Will take screen shot of whole screen and return image. It's working on main thread and may lag UI.
    func takeScreenShot() -> UIImage {
        UIGraphicsBeginImageContext(self.bounds.size)
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        let rec = self.bounds
        self.drawHierarchy(in: rec, afterScreenUpdates: true)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    // To give parellex effect on any view.
    func ch_addMotionEffect() {
        let axis_x_motion: UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffect.EffectType.tiltAlongHorizontalAxis)
        axis_x_motion.minimumRelativeValue = NSNumber(value: -10)
        axis_x_motion.maximumRelativeValue = NSNumber(value: 10)
        
        let axis_y_motion: UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffect.EffectType.tiltAlongVerticalAxis)
        axis_y_motion.minimumRelativeValue = NSNumber(value: -10)
        axis_y_motion.maximumRelativeValue = NSNumber(value: 10)
        
        let motionGroup : UIMotionEffectGroup = UIMotionEffectGroup()
        motionGroup.motionEffects = [axis_x_motion, axis_y_motion]
        self.addMotionEffect(motionGroup)
    }
    
    func inAnimate(){
        self.alpha = 1.0
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.values = [0.01,1.2,0.9,1]
        animation.keyTimes = [0,0.4,0.6,1]
        animation.duration = 0.5
        self.layer.add(animation, forKey: "bounce")
    }
    
    func OutAnimation(comp:@escaping ((Bool)->())){
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            comp(true)
        })
        
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.values = [1,1.2,0.9,0.01]
        animation.keyTimes = [0,0.4,0.6,1]
        animation.duration = 0.2
        self.layer.add(animation, forKey: "bounce")
        CATransaction.commit()
    }
}
class LoginButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        if let afont = titleLabel?.font {
            titleLabel?.font = afont.withSize(afont.pointSize * _widthRatio)
            titleLabel?.font =  UIFont.Maven_pro_Medium(size: 17)
            titleLabel?.text = titleLabel?.text?.localizationString
        }
        layer.cornerRadius = CGFloat(4) * _widthRatio
        layer.backgroundColor = UIColor.colorchaupbule().cgColor
        setTitleColor(.white, for: .normal)
    }
}
class submitButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        if let afont = titleLabel?.font {
            titleLabel?.font = afont.withSize(afont.pointSize * _widthRatio)
            titleLabel?.font =  UIFont.Maven_pro_Medium(size: 17)
            titleLabel?.text = titleLabel?.text?.localizationString
        }
        layer.cornerRadius = self.frame.height * _widthRatio / 2
        layer.backgroundColor = UIColor.colorchaupOrange().cgColor
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        setTitleColor(.white, for: .normal)
    }
}
class deltedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        if let afont = titleLabel?.font {
            titleLabel?.font = afont.withSize(afont.pointSize * _widthRatio)
            titleLabel?.font =  UIFont.Maven_pro_Medium(size: 17)
            titleLabel?.text = titleLabel?.text?.localizationString
        }
        layer.cornerRadius = self.frame.height * _widthRatio / 2
        layer.backgroundColor = UIColor.colorRed().cgColor
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        setTitleColor(.white, for: .normal)
    }
}
class addButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        if let afont = titleLabel?.font {
            titleLabel?.font = afont.withSize(afont.pointSize * _widthRatio)
            titleLabel?.font =  UIFont.Maven_pro_Medium(size: 17)
            titleLabel?.text = titleLabel?.text?.localizationString
        }
        layer.cornerRadius = self.frame.height * _widthRatio / 2
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        setTitleColor(UIColor.colorchaupOrange(), for: .normal)
        layer.borderWidth = 2 * _widthRatio
        layer.borderColor = UIColor.colorchaupOrange().cgColor
    }
}

class viewAllButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        if let afont = titleLabel?.font {
            titleLabel?.font = afont.withSize(afont.pointSize * _widthRatio)
            titleLabel?.font =  UIFont.Maven_pro_Medium(size: 17)
            titleLabel?.text = titleLabel?.text?.localizationString
        }
        setTitleColor(UIColor.colorchaupbule(), for: .normal)
        let FormattedText = NSMutableAttributedString()
        FormattedText
            .viewAll((titleLabel?.text)!)
        setAttributedTitle(FormattedText, for: .normal)
    }
}

class LabelMedium: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        font = font.withSize(font.pointSize * _widthRatio)
        font = UIFont.Maven_pro_Medium(size: font.pointSize * _widthRatio)
        text = text?.localizationString
    }
}
class LabelRegular: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        font = font.withSize(font.pointSize * _widthRatio)
        font = UIFont.Maven_pro_Regular(size: font.pointSize * _widthRatio)
        text = text?.localizationString
    }
}
class LabelSemiBold: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        font = font.withSize(font.pointSize * _widthRatio)
        font = UIFont.Maven_pro_SemiBold(size: font.pointSize * _widthRatio)
        text = text?.localizationString
    }
}
class GradientView: UIView {
    override open class var layerClass: AnyClass {
       return CAGradientLayer.classForKeyedUnarchiver()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [UIColor.colorchaupbule().cgColor, UIColor.colorchaupSuportBlue().cgColor]
    }
}
