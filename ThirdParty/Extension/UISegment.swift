//
//  UISegment.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/04/21.
//

import UIKit

import Foundation
extension UISegmentedControl {

    func removeBorder(selectedcolor : UIColor, normalcolor : UIColor){

        self.tintColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor : selectedcolor], for: .selected)
        self.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor : normalcolor], for: .normal)
        if #available(iOS 13.0, *) {
            self.selectedSegmentTintColor = UIColor.clear
        }

    }

    func setupSegment() {
        self.removeBorder(selectedcolor: UIColor.colorchaupbule(), normalcolor: UIColor.black)
        let segmentUnderlineWidth: CGFloat = self.bounds.width
        let segmentUnderlineHeight: CGFloat = 2.0
        let segmentUnderlineXPosition = self.bounds.minX
        let segmentUnderLineYPosition = self.bounds.size.height - 1.0
        let segmentUnderlineFrame = CGRect(x: segmentUnderlineXPosition, y: segmentUnderLineYPosition, width: segmentUnderlineWidth, height: segmentUnderlineHeight)
        let segmentUnderline = UIView(frame: segmentUnderlineFrame)
        segmentUnderline.backgroundColor = UIColor.clear

        self.addSubview(segmentUnderline)
        self.addUnderlineForSelectedSegment()
    }

    func addUnderlineForSelectedSegment(){

        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor.colorchaupbule()
        underline.tag = 1
        self.addSubview(underline)


    }

    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        underline.frame.origin.x = underlineFinalXPosition

    }
    
    func updateTintColor(selected: UIColor, normal: UIColor) {
        let views = self.subviews
        var positions = [viewPosition]()
        for (i, view) in views.enumerated() {
            let position = viewPosition(originX: view.frame.origin.x, originIndex: i)
            positions.append(position)
        }
        positions.sort(by: { $0.originX < $1.originX })

        for (i, position) in positions.enumerated() {
            let view = self.subviews[position.originIndex]
            if i == self.selectedSegmentIndex {
                view.tintColor = selected
            } else {
                view.tintColor = normal
            }
        }
    }
    struct viewPosition {
        let originX: CGFloat
        let originIndex: Int
    }
    func removeBackgroundColors() {
        self.setBackgroundImage(imageWithColor(color: .clear), for: .normal, barMetrics: .default)
        self.setBackgroundImage(imageWithColor(color: .clear), for: .selected, barMetrics: .default)
        self.setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
    
}

