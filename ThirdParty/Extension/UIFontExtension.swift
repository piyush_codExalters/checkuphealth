//
//  NSFont+ManUp.swift
//  manup
//
//  Created by Yudiz Solutions Pvt. Ltd. on 13/01/16.
//  Copyright © 2016 The App Developers. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func laColorada_bold(size: CGFloat) -> UIFont {
        //return UIFont(name: "merriweathersans_bold", size: size * _widthRatio)!
        return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    
    class func Poppins_Medium(size: CGFloat) -> UIFont {
        //return UIFont(name: "Champagne & Limousines Bold", size: size * _widthRatio)!
        return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    class func Maven_pro_SemiBold(size: CGFloat) -> UIFont {
        return UIFont(name: "MavenPro-SemiBold", size: size * _widthRatio)!
       // return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    class func Maven_pro_Medium(size: CGFloat) -> UIFont {
        return UIFont(name: "MavenPro-Medium", size: size * _widthRatio)!
       // return UIFont.systemFont(ofSize: size * _widthRatio)
    }
 
    class func Poppins_Regular(size: CGFloat) -> UIFont {
        let temp = UIFont(name: "Poppins-Regular", size: size * _widthRatio)
        if let _ = temp{
            print("+++++++++++++")
            return temp!
        }
        return UIFont.systemFont(ofSize: 20)//UIFont(name: "Poppins-Regular", size: size * _widthRatio)!
        //return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    
    class func Maven_pro_Regular(size: CGFloat) -> UIFont {
        return UIFont(name: "MavenPro-Regular", size: size * _widthRatio)!
        //return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    
    class func IBMPlexSans_Medium(size: CGFloat) -> UIFont {
        return UIFont(name: "IBMPlexSans-Medium", size: size * _widthRatio)!
        //return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    
    class func laColorada_Bold(size: CGFloat) -> UIFont {
        return UIFont(name: "MerriweatherSans-Bold", size: size * _widthRatio)!
        //return UIFont.systemFont(ofSize: size * _widthRatio)
    }
    
    class func btlTahomaRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Tahoma", size: size)!
    }
    
    class func btlTahomaBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Tahoma-Bold", size: size)!
    }
    
    class func mlSFRegular(size: CGFloat) -> UIFont{
        return UIFont.systemFont(ofSize: size)
        //return UIFont(name: "Montserrat-Regular", size: size)!
    }
    
    class func mlSFSemiBold(size: CGFloat) -> UIFont{
        return UIFont(name: "SFUIDisplay-Semibold", size: size)!
    }
    
    class func mlSFLight(size: CGFloat) -> UIFont{
        return UIFont(name: "SFUIDisplay-Light", size: size)!
    }
    
    class func mlSFMedium(size: CGFloat) -> UIFont{
        return UIFont(name: "SFUIDisplay-Medium", size: size)!
    }
}
