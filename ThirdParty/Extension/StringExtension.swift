//
//  StringExtension.swift
//  manup
//
//  Created by Tom Swindell on 07/12/2015.
//  Copyright © 2015 The App Developers. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Registration Validation
extension String {
    func setAsAmountRoundedUp(strCurrencySymbol : String, strConversionRate : Double) -> String{
           let temp = self.doubleValue! * strConversionRate
           let roundup = ceil(temp)
           return String(format: "\(strCurrencySymbol)%.f", round(roundup) )
       }
       func setAsAmount(strCurrencySymbol : String, conversionRate : Double) -> String{
           return String(format: "\(strCurrencySymbol)%.2f", self.doubleValue! * conversionRate)
       }
       func setAsAmountRounded(strCurrencySymbol : String, strConversionRate : Double) -> String{
           if self == ""{
               return ""
           }
           let temp = self.doubleValue! * strConversionRate
           return String(format: "\(strCurrencySymbol)%.f", round(temp) )
       }
       func setAsRoundedSymbol(strConversionRate : Double) -> String{
           let temp = self.doubleValue!
           return String(format: "%.f", round(temp) )
       }
       func setAsAmountWithSymbol(conversionRate : Double) -> String{
           return String(format: "%.2f", self.doubleValue! * conversionRate)
       }
    static func validateStringValue(str:String?) -> Bool{
        var strNew = ""
        if str != nil{
            strNew = str!.trimWhiteSpace(newline: true)
        }
        if str == nil || strNew == "" || strNew.count == 0  {  return true  }
        else  {  return false  }
    }
    
    static func validatePassword(str:String?) -> Bool{
        if str == nil || str == "" || str!.count < 6  {  return true  }
        else  {  return false  }
    }
    
    func isValidUsername() -> Bool {
        let usernameRegex = "[A-Z0-9a-z_]{3,20}" //^[a-zA-Z0-9_]{3,15}$
        let temp = NSPredicate(format: "SELF MATCHES %@", usernameRegex).evaluate(with: self)
        return temp
    }
    
    func isValidName() -> Bool{
        let nameRegix = "(?:[\\p{L}\\p{M}]|\\d)"
        return NSPredicate(format: "SELF MATCHES %@", nameRegix).evaluate(with: self)
    }
    
    func isValidEmailAddress() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let temp = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
        return temp
    }

    func validateContact() -> Bool{
        let contactRegEx = "^\\d{3}-\\d{3}-\\d{4}$"
        let contactTest = NSPredicate(format:"SELF MATCHES %@", contactRegEx)
        return contactTest.evaluate(with: self)
    }
    
  
}
public extension String {
    func trimStart(_ char: Character) -> String {
        return trimStart([char])
    }
    
    func trimStart(_ symbols: [Character] = [" ", "\t", "\r", "\n"]) -> String {
        var startIndex = 0
        
        for char in self {
            if symbols.contains(char) {
                startIndex += 1
            }
            else {
                break
            }
        }
        
        if startIndex == 0 {
            return self
        }
        
        return String( self.prefix(startIndex) )
    }
    
    func trimEnd(_ char: Character) -> String {
        return trimEnd([char])
    }
    
    func trimEnd(_ symbols: [Character] = [" ", "\t", "\r", "\n"]) -> String {
        var endIndex = self.count - 1
        
        for i in (0...endIndex).reversed() {
            if symbols.contains( self[i] ) {
                endIndex -= 1
            }
            else {
                break
            }
        }
        
        if endIndex == self.count {
            return self
        }
        
        return String( self.prefix(endIndex + 1) )
    }
}

/////////////////////////
/// ACCESS TO CHAR BY INDEX
////////////////////////
extension StringProtocol {
    subscript(offset: Int) -> Character { self[index(startIndex, offsetBy: offset)] }
    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }
}
// MARK: - Character check
extension String {
    
    func trimmedString() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find, options: String.CompareOptions.caseInsensitive) != nil
    }
    
    func trimWhiteSpace(newline: Bool = false) -> String {
        if newline {
            return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        } else {
            return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
        }
    }
}

// MARK: - Layout
extension String {
    
    func isEqual(str: String) -> Bool {
        if self.compare(str) == ComparisonResult.orderedSame{
            return true
        }else{
            return false
        }
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [:], context: nil)
        return boundingBox.height
    }
    
    func WidthWithNoConstrainedHeight(font: UIFont) -> CGFloat {
        let width = CGFloat(999)
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [:], context: nil)
        return boundingBox.width
    }
}


extension String{
    func hexStringToUIColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
extension String{
    var localizationString: String {
        if _currentUser == nil  { // default language
            return NSLocalizedString(self, tableName: "Spanish", bundle: Bundle.main, value: "", comment: "")
        }else{
            if _currentUser.vAppLanguage == "es"{
                return NSLocalizedString(self, tableName: "Spanish", bundle: Bundle.main, value: "", comment: "")
            }else {
                return NSLocalizedString(self, tableName: "English", bundle: Bundle.main, value: "", comment: "")
            }
        }
    }
}
