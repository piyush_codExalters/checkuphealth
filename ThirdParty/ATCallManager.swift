//
//  ATCallManager.swift
//  VideoCalling
//
//  Created by codExalters1 on 23/03/21.
//  Copyright © 2021 codExalters. All rights reserved.
//

//
//  ATCallManager.swift
//  ATCallKit
//
//  Created by Dejan on 19/05/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit
import CallKit
import PushKit

class ATCallManager: NSObject {
    
    static let shared: ATCallManager = ATCallManager()
    
    private var provider: CXProvider?
    
    
    private override init() {
        super.init()
        self.configureProvider()
    }
    
    private func configureProvider() {
        let config = CXProviderConfiguration(localizedName: "AT Call")
        config.supportsVideo = true
        config.supportedHandleTypes = [.emailAddress]
        
        provider = CXProvider(configuration: config)
        provider?.setDelegate(self, queue: DispatchQueue.main)
    }
    
    private let voipRegistry = PKPushRegistry(queue: nil)
    public func configurePushKit() {
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [.voIP]
    }
    
    public func incommingCall(from: String) {
        incommingCall(from: from, delay: 0)
    }
    
    public func incommingCall(from: String, delay: TimeInterval) {
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .emailAddress, value: from)
        update.hasVideo = true
        let bgTaskID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
       self.provider?.reportNewIncomingCall(with: UUID(), update: update, completion: { (_) in })
        UIApplication.shared.endBackgroundTask(bgTaskID)
    }
    func endCall() {
        provider?.invalidate()
    }
    public func outgoingCall(from: String, connectAfter: TimeInterval) {
        let controller = CXCallController()
        let fromHandle = CXHandle(type: .emailAddress, value: from)
        let startCallAction = CXStartCallAction(call: UUID(), handle: fromHandle)
        startCallAction.isVideo = true
        let startCallTransaction = CXTransaction(action: startCallAction)
        controller.request(startCallTransaction) { (error) in }
        
        self.provider?.reportOutgoingCall(with: startCallAction.callUUID, startedConnectingAt: nil)
        
        let bgTaskID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + connectAfter) {
            self.provider?.reportOutgoingCall(with: startCallAction.callUUID, connectedAt: nil)
            UIApplication.shared.endBackgroundTask(bgTaskID)
        }
    }
}



extension ATCallManager: PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let parts = pushCredentials.token.map { String(format: "%02.2hhx", $0) }
        let token = parts.joined()
        print("did update push credentials with token: \(token)")
    }
    @available(iOS 11.0, *)
    internal func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
            if let callerID = payload.dictionaryPayload["callerID"] as? String {
            self.incommingCall(from: callerID)
        }
    }
    
    @available(iOS, introduced: 8.0, deprecated: 11.0)
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        if let callerID = payload.dictionaryPayload["callerID"] as? String {
            
            self.incommingCall(from: callerID)
        }
    }
    
}
