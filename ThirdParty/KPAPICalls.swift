//
//  KPAPICalls.swift
//  LoactionPicker
//
//  Created by Yudiz on 1/2/17.
//  Copyright © 2017 Yudiz Solutions Pvt.Ltd. All rights reserved.
//

import Foundation
import MapKit
var googleKey = "AIzaSyAc-YPzhMs5Hv8DxuXjaq2tLrAdlKd9Ptk"

/// If google key is empty than location fetch via goecode.
var isGooleKeyFound : Bool = {
    return !googleKey.isEmpty
}()

enum ResponceType:Int{
    case success = 0
    case loading
    case noResult
    case netWorkError
}
class Address: NSObject{
    
    // For google search
    var name: String!
    var title : String = ""
    var subTitle : String = ""
    var refCode: String!
    
    // For Geocode search
    var lat: Double = 0.0
    var long: Double = 0.0
    
    init(googleData: NSDictionary) {
        name = googleData.getStringValue(key: "description")
        refCode = googleData.getStringValue(key: "reference")
        
        if let dict = googleData["structured_formatting"] as? NSDictionary{
            title = dict.getStringValue(key: "main_text")
            subTitle = dict.getStringValue(key: "secondary_text")
            
        }
        super.init()
    }
    
    init(geoCodeData: CLPlacemark) {
        refCode = ""
        name = ""
        if let addr = geoCodeData.addressDictionary{
            if let arr = addr["FormattedAddressLines"] as? NSArray{
                name = arr.componentsJoined(by: ",")
            }
        }
        
        if let loc = geoCodeData.location{
            lat = loc.coordinate.latitude
            long = loc.coordinate.longitude
        }
        super.init()
    }
}
struct FullAddress{
    var lat: Double = 0.0
    var long: Double = 0.0
    var address: String = ""
    
    init(){
    }
    
    init(lati: Double, longi: Double, add: String){
        lat = lati
        long = longi
        address = add
    }
    
    init(addObj: Address) {
        lat = addObj.lat
        long = addObj.long
        address = addObj.name
    }
}
class KPAPICalls: NSObject{
    
    static let shared = KPAPICalls()
    let geoLocation = CLGeocoder()
    
    // Google API
    /// Search location by name and returns custom address with reference code
    ///
    /// - Parameters:
    ///   - text: Search Text
    ///   - block: Returns search addresses and responce type enum like(noResult, network error)
    /// - Returns: URLSessionDataTask for cancel previous request
    func getReferenceFromSearchText(lat: Double, long: Double,text: String, block:@escaping (([Address],ResponceType)->())) -> URLSessionDataTask{
        let str = text.addingPercentEncoding(withAllowedCharacters: CharacterSet.letters)
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(str!)&sensor=false&location=\(lat),\(long)&radius=300000&strictbounds&key=\(googleKey)")!
        let req = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: req, completionHandler: { (dataObj, urlRes, err) in
            var resType = ResponceType.success
            var arrAddress: [Address] = []
            if let res = urlRes as? HTTPURLResponse, dataObj != nil{
                if res.statusCode == 200{
                    let objJson = try! JSONSerialization.jsonObject(with: dataObj!, options: JSONSerialization.ReadingOptions.allowFragments)
                    let json = objJson as! NSDictionary
                    if json["status"] as! String == "OK"{
                        if let data = json["predictions"] as? NSDictionary{
                            let add = Address(googleData: data)
                            arrAddress.append(add)
                        }else if let dataArr = json["predictions"] as? [NSDictionary]{
                            for data in dataArr{
                                let add = Address(googleData: data)
                                arrAddress.append(add)
                            }
                        }
                        resType = .success
                    }else if json["status"] as! String == "ZERO_RESULTS"{
                        resType = .noResult
                    }else{
                        resType = .success
                    }
                }else{
                    resType = .netWorkError
                }
            }else{
             if (err as! NSError).code != -999{
                    resType = .netWorkError
                }
            }
            
            DispatchQueue.main.async {
                block(arrAddress, resType)
            }
        })
        task.resume()
        return task
    }
    
    func getReferenceFromSearchText1(text: String, block:@escaping (([Address],ResponceType)->())) -> URLSessionDataTask{
        let str = text.addingPercentEncoding(withAllowedCharacters: CharacterSet.letters)
        let url = URL(string:
                        "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(23.029355),\(72.669344)&key=\(googleKey)")!
        let req = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: req, completionHandler: { (dataObj, urlRes, err) in
            var resType = ResponceType.success
            var arrAddress: [Address] = []
            if let res = urlRes as? HTTPURLResponse, dataObj != nil{
                if res.statusCode == 200{
                    let objJson = try! JSONSerialization.jsonObject(with: dataObj!, options: JSONSerialization.ReadingOptions.allowFragments)
                    let json = objJson as! NSDictionary
                    if json["status"] as! String == "OK"{
                        if let data = json["predictions"] as? NSDictionary{
                            let add = Address(googleData: data)
                            arrAddress.append(add)
                        }else if let dataArr = json["predictions"] as? [NSDictionary]{
                            for data in dataArr{
                                let add = Address(googleData: data)
                                arrAddress.append(add)
                            }
                        }
                        resType = .success
                    }else if json["status"] as! String == "ZERO_RESULTS"{
                        resType = .noResult
                    }else{
                        resType = .success
                    }
                }else{
                    resType = .netWorkError
                }
            }else{
             if (err as! NSError).code != -999{
                    resType = .netWorkError
                }
            }
            
            DispatchQueue.main.async {
                block(arrAddress, resType)
            }
        })
        task.resume()
        return task
    }
    
    func getReferenceFromSearchText2(text: String,fullAddress : FullAddress, block:@escaping (([Address],ResponceType)->())) -> URLSessionDataTask{
        let str = text.addingPercentEncoding(withAllowedCharacters: CharacterSet.letters)
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(str!)&strictbounds&&location=\(fullAddress.lat),\(fullAddress.long)&radius=30000&sensor=false&key=\(googleKey)")!
        let req = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: req, completionHandler: { (dataObj, urlRes, err) in
            var resType = ResponceType.success
            var arrAddress: [Address] = []
            if let res = urlRes as? HTTPURLResponse, dataObj != nil{
                if res.statusCode == 200{
                    let objJson = try! JSONSerialization.jsonObject(with: dataObj!, options: JSONSerialization.ReadingOptions.allowFragments)
                    let json = objJson as! NSDictionary
                    if json["status"] as! String == "OK"{
                        if let data = json["predictions"] as? NSDictionary{
                            let add = Address(googleData: data)
                            arrAddress.append(add)
                        }else if let dataArr = json["predictions"] as? [NSDictionary]{
                            for data in dataArr{
                                let add = Address(googleData: data)
                                arrAddress.append(add)
                            }
                        }
                        resType = .success
                    }else if json["status"] as! String == "ZERO_RESULTS"{
                        resType = .noResult
                    }else{
                        resType = .success
                    }
                }else{
                    resType = .netWorkError
                }
            }else{
                if (err as! NSError).code != -999{
                    resType = .netWorkError
                }
            }
            
            DispatchQueue.main.async {
                block(arrAddress, resType)
            }
        })
        task.resume()
        return task
    }
    // Google API
    /// Get Location with lat, logn and formatted address.
    ///
    /// - Parameters:
    ///   - ref: reference id of location
    ///   - block: return fullAddress structure and error.
    func getLocationFromReference(ref: String, block:@escaping ((FullAddress?, NSError?)->())){
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/details/json?reference=\(ref)&sensor=false&key=\(googleKey)")
        let req = URLRequest(url: url!)
        URLSession.shared.dataTask(with: req) { (data, resObj, error) in
            if let res = resObj as? HTTPURLResponse, data != nil{
                if res.statusCode == 200{
                    var tmpAddress = FullAddress()
                    let jsonObj = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                    let json = jsonObj as! NSDictionary
                    if let data = json["result"] as? NSDictionary{
                        tmpAddress.address = data.getStringValue(key: "formatted_address")
                        if let cordinate = data["geometry"] as? NSDictionary{
                            if let loc = cordinate["location"] as? NSDictionary{
                                tmpAddress.lat =  loc.getDoubleValue(key: "lat")
                                tmpAddress.long = loc.getDoubleValue(key: "lng")
                            }
                        }
                    }else if let data = json["result"] as? [NSDictionary]{
                        tmpAddress.address = data[0].getStringValue(key: "formatted_address")
                        if let cordinate = data[0]["geometry"] as? NSDictionary{
                            if let loc = cordinate["location"] as? NSDictionary{
                                tmpAddress.lat =  loc.getDoubleValue(key: "lat")
                                tmpAddress.long = loc.getDoubleValue(key: "lng")
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        block(tmpAddress, error as NSError?)
                    }
                }else{
                    DispatchQueue.main.async {
                        block(nil, error as NSError?)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    block(nil, error as NSError?)
                }
            }
            }.resume()
    }
    
    func getcustemLocation(lat : Double,long: Double,address: String, block:@escaping ((FullAddress)->())){
        var tmpAddress = FullAddress()
        tmpAddress.address = address
        tmpAddress.lat = lat
        tmpAddress.long = long
        block(tmpAddress)
    }
    
    
    // // Google API
    /// Fetch address from lat, long
    ///
    /// - Parameters:
    ///   - lat: latitite
    ///   - long: longitute
    ///   - block: return formatted address string
    func getAddressFromLatLong(lat: String, long: String, block:@escaping ((String)->())){
        let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(long)&sensor=true&key=\(googleKey)")
        let req = URLRequest(url: url!)
        URLSession.shared.dataTask(with: req) { (data, resObj, error) in
            if let res = resObj as? HTTPURLResponse, data != nil{
                if res.statusCode == 200{
                    var tmpAddress = FullAddress()
                    let jsonObj = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                    let json = jsonObj as! NSDictionary
                    if json["status"] as! String == "OK"{
                        if let data = json["results"] as? NSDictionary{
                            tmpAddress.address = data.getStringValue(key: "formatted_address")
                        }else if let data = json["results"] as? [NSDictionary]{
                            tmpAddress.address = data[0].getStringValue(key: "formatted_address")
                        }
                    }
                    DispatchQueue.main.async {
                        block(tmpAddress.address)
                    }
                }else{
                    DispatchQueue.main.async {
                       block("")
                    }
                }
            }else{
                DispatchQueue.main.async {
                    block("")
                }
            }
            }.resume()
    }
    
    
    // Geo Code
    /// address from CLLocation
    ///
    /// - Parameters:
    ///   - location: CLLocation object
    ///   - block: return formatted address string
    func addressFromlocation(location: CLLocation, block: @escaping (String?)->()){
        geoLocation.cancelGeocode()
        geoLocation.reverseGeocodeLocation(location, completionHandler: { (placeMarks, error) -> Void in
            if let pmark = placeMarks, pmark.count > 0 {
                let place :CLPlacemark = pmark.last! as CLPlacemark
                if let addr = place.addressDictionary {
                    var str = ""
                    if let arr = addr["FormattedAddressLines"] as? NSArray{
                        str = arr.componentsJoined(by: ",")
                    }
                    
                    DispatchQueue.main.async {
                        block(str)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    block(nil)
                }
            }
        })
    }

    // Geocode
    /// Search address with geocode
    ///
    /// - Parameters:
    ///   - str: Search string
    ///   - block: Returns search addresses and responce type enum like(noResult, network error)
    func searchAddressBygeocode(str: String, block:@escaping (([Address],ResponceType)->())){
        geoLocation.cancelGeocode()
        geoLocation.geocodeAddressString(str) { (placeMarks, error) in
            if let pmark = placeMarks, pmark.count > 0 {
                var arrPlace:[Address] = []
                for place in pmark{
                    let add = Address(geoCodeData: place)
                    arrPlace.append(add)
                }
                
                DispatchQueue.main.async {
                    block(arrPlace, ResponceType.success)
                }
            }else{
                if let err = error as? NSError{
                    if err.code != 10{
                        DispatchQueue.main.async {
                            block([], ResponceType.noResult)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        block([], ResponceType.netWorkError)
                    }
                }
            }
        }
    }
    
    // Geocode
    ///direction Routs Api
    ///
    /// - Parameters:
    ///   - str: Search direction
    ///   - block: Returns search direction and responce type enum like(noResult, network error)
    
    
}

// MARK: - Dictionaty extention for coversion.

