
import Foundation
import Alamofire
import NotificationBannerSwift


let kTokenExpire        = "Session expired - please login again."
struct ApiResponse {
    var IsSuccess : Bool = false
    var Message : String?
    var ReturnedData : Data?
    
   
    
}
class AccessTokenAdapter: RequestAdapter {
    private let accessToken: String
    
    init(accessToken: String) {
        self.accessToken = accessToken
        
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "vAuthToken")
        // urlRequest.setValue(accessToken, forHTTPHeaderField: "vAuthToken")
        return urlRequest
    }
}

//================================================================

//9898989262
//11111111
//pin: 1234



let _baseUrl = "https://beta.checkuphealth.co.uk/apinew/v17.1/patient/" // demo beta version
//let _baseUrl =  "https://app.checkuphealth.co.uk/test/apinew/v18/patient/"// Demo1
let _worldpaybaseUrl = "https://beta.checkuphealth.co.uk/worldpay/generatetoken.php"// Demo1




//let _baseUrl =  "https://app.checkuphealth.co.uk/apinew/v17.1/patient/"// Live
//let _worldpaybaseUrl = "https://app.checkuphealth.co.uk/worldpay/generatetoken.php"// Live




//=========================================================
typealias WSBlock = (_ json: AnyObject?, _ flag: Int) -> ()
typealias WSProgress = (Progress) -> ()?
typealias WSFileBlock = (_ path: String?, _ success: Bool) -> ()

class KPWebCall:NSObject{
    
    func configureWindow(_ viewController: UIViewController) {
        
        
    }
    var currentData : DataRequest!
    static var call: KPWebCall = KPWebCall()
    let manager: SessionManager
    var networkManager: NetworkReachabilityManager
    var headers: HTTPHeaders = [
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
    ]
    var ApiKey = ""
    var paramEncode: ParameterEncoding = URLEncoding.default
    var successBlock: (String, HTTPURLResponse?, AnyObject?, WSBlock) -> Void
    var errorBlock: (String, HTTPURLResponse?, NSError, WSBlock) -> Void
    
    override init() {
        manager = Alamofire.SessionManager.default
        networkManager = NetworkReachabilityManager()!
        // paramEncode = JSONEncoding.default
        
        // Will be called on success of web service calls.
        successBlock =  { (relativePath, res, respObj, block) -> Void in
            // Check for response it should be there as it had come in success block
            if let response = res{
                kprint(items: "Response Code: \(response.statusCode)")
                kprint(items: "Response(\(relativePath)): \(String(describing: respObj))")
                if response.statusCode == 200 {
                    if let dict = respObj as? NSDictionary{
                        if 401 == dict.getIntValue(key: "status"){
                            //Check navigation stacks
                            let nav = _appDelegator.window?.rootViewController as! NavLoginVC
                            
                            //Remove all
                            nav.viewControllers.removeAll()
                            //Check navigation stacks
                            
                            //Check whether the user logined or not
                            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
                            //Clear user defaults
                            _userDefault.set(nil, forKey: "userInfo")
                            _userDefault.removeObject(forKey: "isMobileVerified")
                            _userDefault.removeObject(forKey: "isOnBoardingDone")
                            _userDefault.removeObject(forKey: "latitude")
                            _userDefault.removeObject(forKey: "longitude")
                            _userDefault.removeObject(forKey: "strLocationAddress")
                            _userDefault.removeObject(forKey: "vAppLanguage")
                            _userDefault.removeObject(forKey: "token")
                            _userDefault.synchronize()
                            totalTime.invalidate()
                            _appDelegator.window?.isUserInteractionEnabled = true
                            let lvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                            _appDelegator.window?.rootViewController = lvc
                            
                            //
                            //
                            //
                            //                            let nav = _appDelegator.window?.rootViewController as! NavLoginVC
                            //                            _appDelegator.window?.isUserInteractionEnabled = true
                            //                            for viewController in nav.viewControllers {
                            //                                if viewController.isKind(of: LoginVC.self) {
                            //                                    _userDefault.set(nil, forKey: "userInfo")
                            //                                    _userDefault.removeObject(forKey: "token")
                            //                                    _userDefault.synchronize()
                            //                                    TotalTime.invalidate()
                            //                                    totalTime.invalidate()
                            //                                    let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: "Session expired login again"), style: .danger)
                            //                                    banner.duration = 1
                            //                                    banner.show(queuePosition: .back, bannerPosition: .bottom)
                            //                                    nav.popToViewController(viewController, animated: false)
                            //                                }
                            //                            }
                        }else{
                            block(respObj, response.statusCode)
                        }
                    }
                    
                } else {
                    block(respObj, response.statusCode)
                }
            } else {
                // There might me no case this can get execute
                block(nil, 404)
            }
        }
        
        // Will be called on Error during web service call
        errorBlock = { (relativePath, res, error, block) -> Void in
            // First check for the response if found check code and make decision
            if let response = res {
                kprint(items: "Response Code: \(response.statusCode)")
                kprint(items: "Error Code: \(error.code)")
                if let data = error.userInfo["com.alamofire.serialization.response.error.data"] as? NSData {
                    let errorDict = (try? JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary
                    if errorDict != nil {
                        kprint(items: "Error(\(relativePath)): \(errorDict!)")
                        block(errorDict!, response.statusCode)
                        if response.statusCode == 423{
                            //                            let nav = _appDelegator.window?.rootViewController as! KPNavigationViewController
                            //                            let pre = nav.presentedViewController
                            //                            pre?.dismissViewControllerAnimated(false, completion: nil)
                            //                            nav.popToRootViewControllerAnimated(true)
                            //                            _appDelegator.prepareToSignOut()
                            //                            ValidationToast.showStatusMessage(kTokenExpire,yCord: 20)
                        }
                    } else {
                        let code = response.statusCode
                        block(nil, code)
                    }
                } else {
                    block(nil, response.statusCode)
                }
                // If response not found rely on error code to find the issueapp
            } else if error.code == -1009  {
                kprint(items: "Error(\(relativePath)): \(error)")
                block([_appName: "NO internet"] as AnyObject, error.code)
                return
            } else if error.code == -1003  {
                kprint(items: "Error(\(relativePath)): \(error)")
                block([_appName: "Host Down"] as AnyObject, error.code)
                return
            } else if error.code == -1001  {
                kprint(items: "Error(\(relativePath)): \(error)")
                block([_appName: "Request time out"] as AnyObject, error.code)
                return
            } else {
                kprint(items: "Error(\(relativePath)): \(error)")
                block(nil, error.code)
            }
        }
        super.init()
        addInterNetListner()
    }
    
    deinit {
        networkManager.stopListening()
    }
}

// MARK: Other methods
extension KPWebCall{
    func getFullUrl(relPath : String) throws -> URL{
        do{
            if relPath.lowercased().contains("http") || relPath.lowercased().contains("www"){
                return try relPath.asURL()
            }else{
                return try (_baseUrl+relPath).asURL()
            }
        }catch let err{
            throw err
        }
    }
    
    func setAccesTokenToHeader(token:String){
        print(token)
        manager.adapter = AccessTokenAdapter(accessToken: token)
    }
    
    func removeAccessTokenFromHeader(){
        manager.adapter = nil
    }
    
    func setClientToken(token: String){
        headers["clientToken"] = token
    }
    
    func setClientApiKey(token: String){
        ApiKey = token
    }
    
    func removeClientToken(){
        headers.removeValue(forKey: "clientToken")
    }
}

// MARK: - Request, ImageUpload and Dowanload methods
extension KPWebCall{
    
    func getRequest(relPath: String, param: [String: Any]?, block: @escaping WSBlock)-> DataRequest?{
        do{
            return manager.request(try getFullUrl(relPath: relPath), method: HTTPMethod.get, parameters: param, encoding: paramEncode, headers: headers).responseJSON { (resObj) in
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            kprint(items: errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            kprint(items: error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }
    
    func postRequest(relPath: String, param: [String: Any]?, block: @escaping WSBlock)-> DataRequest? {
        do{
            print(relPath)
            if let para = param{
                print(para)
            }
            print(try getFullUrl(relPath: relPath))
            var strLanguage = ""
            let Lang = _userDefault.value(forKey: "Language") as? String
            if Lang == nil || Lang == "ZW"{
                strLanguage = "ZW"
            }else{
                strLanguage = "GB"
            }
            let headers = [
                "country": strLanguage,
                "vAuthToken": ApiKey
            ]
            
            return manager.request(try getFullUrl(relPath: relPath), method: HTTPMethod.post, parameters: param, encoding: paramEncode, headers: headers).responseJSON { (resObj) in
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            print(errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    print(err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            print(error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }
   
    public func postRequestApiClinet(relPath: String, param: String, completion: @escaping (ApiResponse?) -> ())
    {
        var apiResponse = ApiResponse()
        kprint(items: relPath)
        kprint(items: param)
        var strLanguage = ""
        let Lang = _userDefault.value(forKey: "Language") as? String
        if Lang == nil || Lang == "ZW"{
            strLanguage = "ZW"
        }else{
            strLanguage = "GB"
        }
        let headers = [
            "country": strLanguage,
            "vAuthToken": ApiKey
        ]
        let parameters = param
        let postData = parameters.data(using: .utf8)
        var request = URLRequest(url: try! getFullUrl(relPath: relPath))
        
        request.allHTTPHeaderFields = headers
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        request.addValue("PHPSESSID=n1lll0fh86bidot5hsdtp83q82", forHTTPHeaderField: "Cookie")
        request.httpMethod = "POST"
        request.httpBody = postData
        print(request)
        let task = URLSession.shared.dataTask(with: request , completionHandler : { data, response, error in
            DispatchQueue.main.async {
            if let error = error {
                // handle the transport error
                apiResponse.IsSuccess = false
                apiResponse.Message = "Error"
                completion(apiResponse)
                return
            }
//            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
//                // handle the server error
//                completion(apiResponse)
//                return
//            }
            apiResponse.ReturnedData = data
            apiResponse.IsSuccess = true
            apiResponse.Message = "Succeed"
                
                do {
                  
                      
                if let response = try JSONSerialization.jsonObject(with: apiResponse.ReturnedData!, options: []) as? NSDictionary {
                    DispatchQueue.main.async {
                        if let Response = response as? NSDictionary{
                            
                            if Response.getStringValue(key: "success") == "-1"{
                                
                               print("Session expired login again...............\(relPath)")
                                
                                
                                let nav = _appDelegator.window?.rootViewController as! NavLoginVC
                                nav.viewControllers.removeAll()
                                _userDefault.set(nil, forKey: "userInfo")
                                _userDefault.synchronize()
                                _appDelegator.window?.isUserInteractionEnabled = true
                                let lvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                                _appDelegator.window?.rootViewController = lvc

                                
                                let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: "Session expired login again"), style: .danger)
                                banner.duration = 1
                                banner.show(queuePosition: .back, bannerPosition: .top)
                                //nav.popToViewController(viewController, animated: false)
                                
                            }
                            
                            
                        }
                        
                        
                    }
                
                }
                    
                    
                }catch let error as NSError {
                    print("Done......................No")
                    print(error.localizedDescription)
                }
                
                
                
            completion(apiResponse)
            }
        })
        task.resume()
    }
    

    
    
    public func postRequestApiClinetImage(relPath: String, param: String , completion: @escaping (ApiResponse?) -> () )
    {
        var apiResponse = ApiResponse()
        kprint(items: relPath)
        kprint(items: param)
        var strLanguage = ""
        let Lang = _userDefault.value(forKey: "Language") as? String
        if Lang == nil || Lang == "ZW"{
            strLanguage = "ZW"
        }else{
            strLanguage = "GB"
        }
        let headers = [
            "country": strLanguage,
            "vAuthToken": ApiKey
        ]
       
        let parameters = param
        let postData = parameters.data(using: .utf8)
        var request = URLRequest(url: try! getFullUrl(relPath: relPath))
        request.allHTTPHeaderFields = headers
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Cookie")
        request.httpMethod = "POST"
        request.httpBody = postData
        let task = URLSession.shared.dataTask(with: request , completionHandler : { data, response, error in
            DispatchQueue.main.async {
            if let error = error {
                // handle the transport error
                apiResponse.IsSuccess = false
                apiResponse.Message = "Error"
                completion(apiResponse)
                return
            }
//            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
//                // handle the server error
//                completion(nil)
//                return
//            }
            apiResponse.ReturnedData = data
            apiResponse.IsSuccess = true
            apiResponse.Message = "Succeed"
                
                do {
                    print("Done......................check")
                        print("Done......................Yes")
                if let response = try JSONSerialization.jsonObject(with: apiResponse.ReturnedData!, options: []) as? NSDictionary {
                    DispatchQueue.main.async {
                        if let Response = response as? NSDictionary{
                            
                            if Response.getStringValue(key: "success") == "-1"{
                                
                               print("Done......................code")
                                let nav = _appDelegator.window?.rootViewController as! NavLoginVC
                                nav.viewControllers.removeAll()
                                _userDefault.set(nil, forKey: "userInfo")
                                _userDefault.synchronize()
                                _appDelegator.window?.isUserInteractionEnabled = true
                                let lvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                                _appDelegator.window?.rootViewController = lvc
                                let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: "Session expired login again"), style: .danger)
                                banner.duration = 1
                                banner.show(queuePosition: .back, bannerPosition: .top)
                                //nav.popToViewController(viewController, animated: false)
                                
                            }
                            
                            
                        }
                        
                        
                    }
                
                }
                    
                    
                }catch let error as NSError {
                    print("Done......................No")
                    print(error.localizedDescription)
                }
                
            completion(apiResponse)
            }
        })
        task.resume()
    }
    public func getRequestApiClinet(relPath: String, param: String, completion: @escaping (ApiResponse?) -> () )
    {
        var apiResponse = ApiResponse()
        kprint(items: relPath)
        kprint(items: param)
        var strLanguage = ""
        let Lang = _userDefault.value(forKey: "Language") as? String
        if Lang == nil || Lang == "ZW"{
            strLanguage = "ZW"
        }else{
            strLanguage = "GB"
        }
        let headers = [
            "country": strLanguage,
            "vAuthToken": ApiKey
        ]
        let parameters = param
        let postData = parameters.data(using: .utf8)
        var request = URLRequest(url: try! getFullUrl(relPath: relPath))
        request.allHTTPHeaderFields = headers
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        request.addValue("PHPSESSID=n1lll0fh86bidot5hsdtp83q82", forHTTPHeaderField: "Cookie")
        request.httpMethod = "GET"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request , completionHandler : { data, response, error in
            DispatchQueue.main.async {
            if let error = error {
                // handle the transport error
                apiResponse.IsSuccess = false
                apiResponse.Message = "Error"
                completion(apiResponse)
                return
            }
//            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
//                // handle the server error
//                completion(nil)
//                return
//            }
            apiResponse.ReturnedData = data
            apiResponse.IsSuccess = true
            apiResponse.Message = "Succeed"
                
                do {
                    print("Done......................check")
                        print("Done......................Yes")
                if let response = try JSONSerialization.jsonObject(with: apiResponse.ReturnedData!, options: []) as? NSDictionary {
                    DispatchQueue.main.async {
                        if let Response = response as? NSDictionary{
                            
                            if Response.getStringValue(key: "success") == "-1"{
                                
                               print("Done......................code")
                                let nav = _appDelegator.window?.rootViewController as! NavLoginVC
                                nav.viewControllers.removeAll()
                                _userDefault.set(nil, forKey: "userInfo")
                                _userDefault.synchronize()
                                _appDelegator.window?.isUserInteractionEnabled = true
                                let lvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavLoginVC") as! NavLoginVC
                                _appDelegator.window?.rootViewController = lvc
                                let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: "Session expired login again"), style: .danger)
                                banner.duration = 1
                                banner.show(queuePosition: .back, bannerPosition: .top)
                                //nav.popToViewController(viewController, animated: false)
                                
                            }
                            
                            
                        }
                        
                        
                    }
                
                }
                    
                    
                }catch let error as NSError {
                    print("Done......................No")
                    print(error.localizedDescription)
                }
                
                
            completion(apiResponse)
            }
        })
        task.resume()
    }
    
    
    
    func postRequestWithHeader(relPath: String, param: [String: Any]?, block: @escaping WSBlock)-> DataRequest? {
        do{
            kprint(items: relPath)
            if let para = param{
                kprint(items: para)
            }
            let header: HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            return manager.request(try getFullUrl(relPath: relPath), method: HTTPMethod.post, parameters: param, encoding: paramEncode, headers: header).responseJSON { (resObj) in
                
                switch resObj.result{
                case .success:
                    if let resData = resObj.data{
                        do {
                            let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                            self.successBlock(relPath, resObj.response, res, block)
                        } catch let errParse{
                            kprint(items: errParse)
                            self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, resObj.response, err as NSError, block)
                    break
                }
            }
        }catch let error{
            kprint(items: error)
            errorBlock(relPath, nil, error as NSError, block)
            return nil
        }
    }
    
    func uploadAudio(relPath: String,audio:[Data]?,file : Data?,param: [String: Any]?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            manager.upload(multipartFormData: { (formData) in
                if let obj = audio{
                    for data in obj.enumerated(){
                        formData.append(data.element, withName: "vAudioPath[\(data.offset)]", fileName: "VoiceMessage\(data.offset).m4a", mimeType: "audio/m4a")
                    }
                }
                
                if let singleAudio = file {
                    formData.append(singleAudio, withName: "vAudioPath", fileName: "VoiceMessage.m4a", mimeType: "audio/m4a")
                }
                if let _ = param{
                    for (key, value) in param!{
                        let val = value as? String
                        if key != "voiceClipArray"{
                            formData.append((val?.data(using: String.Encoding.utf8, allowLossyConversion: false)!)!, withName: key)
                        }
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: headers, encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint(items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint(items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    func uploadImage1(relPath: String,vIdProofImagePath: UIImage?,vResidenceProofImagePath: UIImage?,vSelfieImagePath: UIImage?,param: [String: Any
    ]?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            manager.upload(multipartFormData: { (formData) in
                
                if let _ = vIdProofImagePath{
                    formData.append(vIdProofImagePath!.jpegData(compressionQuality: 0.25)!, withName: "vIdProofImagePath", fileName: "imagePro.jpeg", mimeType: "Proof/jpeg")
                }
                if let _ = vResidenceProofImagePath{
                    formData.append(vResidenceProofImagePath!.jpegData(compressionQuality: 0.25)!, withName: "vResidenceProofImagePath", fileName: "Residenc.jpeg", mimeType: "image/jpeg")
                }
                if let _ = vSelfieImagePath{
                    formData.append(vSelfieImagePath!.jpegData(compressionQuality: 0.25)!, withName: "vSelfieImagePath", fileName: "Selfie.jpeg", mimeType: "image/jpeg")
                }
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
                
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: headers, encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint(items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint(items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    func uploadImage(relPath: String,imgP: UIImage?,param: [String: Any
    ]?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            manager.upload(multipartFormData: { (formData) in
                
                if let _ = imgP{
                    formData.append(imgP!.jpegData(compressionQuality: 0.25)!, withName: "vImagePath", fileName: "imagePro.jpeg", mimeType: "image/jpeg")
                }
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: headers, encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint(items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint(items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    func uploadVideo(relPath: String,video:Data?,param: [String: Any]?, block: @escaping WSBlock, progress: WSProgress?){
        do{
            manager.upload(multipartFormData: { (formData) in
                
                if let obj = video{
                    formData.append(obj, withName: "vContent", fileName: "video.mp4", mimeType: "video/mp4")
                }
                
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: [:], encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint(items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint(items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    func uploadImageStory(relPath: String,img : UIImage?,param: [String: Any]?, block: @escaping WSBlock, progress: WSProgress?){
        
        let header1: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "form-data"
        ]
        
        do{
            manager.upload(multipartFormData: { (formData) in
                
                if let _ = img{
                    formData.append(img!.jpegData(compressionQuality: 0.25)!, withName: "vContent", fileName: "image.jpeg", mimeType: "image/jpeg")
                    print(formData)
                }
                
                if let _ = param{
                    for (key, value) in param!{
                        formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                    }
                }
            }, to: try getFullUrl(relPath: relPath), method: HTTPMethod.post, headers: header1, encodingCompletion: { encoding in
                switch encoding{
                case .success(let req, _, _):
                    req.uploadProgress(closure: { (prog) in
                        progress?(prog)
                    }).responseJSON { (resObj) in
                        switch resObj.result{
                        case .success:
                            if let resData = resObj.data{
                                do {
                                    let res = try JSONSerialization.jsonObject(with: resData, options: []) as AnyObject
                                    self.successBlock(relPath, resObj.response, res, block)
                                } catch let errParse{
                                    kprint(items: errParse)
                                    self.errorBlock(relPath, resObj.response, errParse as NSError, block)
                                }
                            }
                            break
                        case .failure(let err):
                            kprint(items: err)
                            self.errorBlock(relPath, resObj.response, err as NSError, block)
                            break
                        }
                    }
                    break
                case .failure(let err):
                    kprint(items: err)
                    self.errorBlock(relPath, nil, err as NSError, block)
                    break
                }
            })
        }catch let err{
            self.errorBlock(relPath, nil, err as NSError, block)
        }
    }
    
    func dowanloadFile(relPath : String, progress: WSProgress?, block: @escaping WSFileBlock){
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("pig.png")
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        do{
            manager.download(try getFullUrl(relPath: relPath), to: destination).downloadProgress { (prog) in
                progress?(prog)
            }.response { (responce) in
                if responce.error == nil, let path = responce.destinationURL?.path{
                    block(path, true)
                }else{
                    block(nil, false)
                }
            }.resume()
            
        }catch{
            block(nil, false)
        }
    }
    
    func dowanloadMP3File(relPath : String, progress: WSProgress?, block: @escaping WSFileBlock){
        
        let destination : DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("recording.mp4")
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        do{
            manager.download(try getFullUrl(relPath: relPath), to: destination).downloadProgress { (prog) in
                progress?(prog)
            }.response { (responce) in
                
                if responce.error == nil, let path = responce.destinationURL?.path{
                    block(path, true)
                }else{
                    block(nil, false)
                }
            }.resume()
            
        }catch{
            block(nil, false)
        }
    }
    
    func postRequest(path: String, param: String, block: @escaping (Any?,String) -> ()){
        
        let customerKey = "4ddcb7ee23744c01ad822da548c7e793"
        let customerSecret = "cc44ea4b66204fa0b2297db88d9a1a64"
        let plainCredentials = customerKey + ":" + customerSecret
        
        let base64Str = plainCredentials.toBase64()
        
        let authorizationHeader = "Basic " + base64Str
        
        guard let serviceUrl = URL(string: path) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(authorizationHeader, forHTTPHeaderField: "Authorization")
        let postData = param.data(using: .utf8)
        request.httpBody = postData
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    block(json, "")
                    print(json)
                } catch {
                    print(error)
                    block(nil,error.localizedDescription)
                }
            }
        }.resume()
    }
    
    
    func getRequest(path: String, param: String, block: @escaping (Any?,String) -> ()){
        
        let customerKey = "4ddcb7ee23744c01ad822da548c7e793"
        let customerSecret = "cc44ea4b66204fa0b2297db88d9a1a64"
        let plainCredentials = customerKey + ":" + customerSecret
        
        let base64Str = plainCredentials.toBase64()
        
        let authorizationHeader = "Basic " + base64Str
        
        guard let serviceUrl = URL(string: path) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(authorizationHeader, forHTTPHeaderField: "Authorization")
        let postData = param.data(using: .utf8)
        request.httpBody = postData
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    block(json, "")
                    print(json)
                } catch {
                    print(error)
                    block(nil,error.localizedDescription)
                }
            }
        }.resume()
    }
    
}
    





// MARK: - Internet Availability
extension KPWebCall{
    func addInterNetListner(){
        networkManager.listener = { (status) in
            if status == NetworkReachabilityManager.NetworkReachabilityStatus.notReachable{
                print("No InterNet")
                let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: "No internet connection"), style: .danger)
                banner.duration = 1
                banner.show(queuePosition: .back, bannerPosition: .bottom)
            }else{
                print("Internet Avail")
                
            }
        }
        networkManager.startListening()
    }
    
    func isInternetAvailable() -> Bool {
        if networkManager.isReachable{
            return true
        }else{
            return false
        }
    }
}

// MARK: - API Call extention
extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
    
}
extension KPWebCall{
    func worldPayPayment(param : [String : Any]?,block : @escaping WSBlock ){
        _ = postRequest(relPath: "uploadPatientVerificationDocument.php", param: param, block: block)
    }
    func CheckPatientVerification(param : [String : Any]?,IdProof:UIImage,Selfie:UIImage,ResidenceProof:UIImage,block : @escaping WSBlock ){
        _ = uploadImage1(relPath: "CheckPatientVerification.php", vIdProofImagePath: IdProof, vResidenceProofImagePath: ResidenceProof, vSelfieImagePath: Selfie, param: param, block: block, progress: nil)
    }
   
}

