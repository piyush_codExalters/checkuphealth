//
//  ScreenView.swift
//  CheckupHealth
//
//  Created by codExalters1 on 11/05/21.
//

import UIKit
protocol ScreenDelegate{
    func btnForgetPassword()
    func btnremove()
}

class ScreenView: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var textFour: JPWidthTextField!
    @IBOutlet weak var textThree: JPWidthTextField!
    @IBOutlet weak var textTwo: JPWidthTextField!
    @IBOutlet weak var textONe: JPWidthTextField!
    @IBOutlet weak var lblError: LabelSemiBold!
    @IBOutlet weak var lblsetTitlel: LabelSemiBold!
    var arryaNumber = ["1","2","3","4","5","6","7","8","9","","0",""]
    var setPin = ""
    var isAgen = false
    var delegate : ScreenDelegate!

    
    func initDataupdate()  {
        lblsetTitlel.text = "Enter your Pin"
        textFour.makeCornecscrRoun1(redias : 4)
        textThree.makeCornecscrRoun1(redias : 4)
        textTwo.makeCornecscrRoun1(redias : 4)
        textONe.makeCornecscrRoun1(redias : 4)
        lblError.text = " "
        constraintUpdate()
    }
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
    }
    
    class func instanceFromNib() -> ScreenView {
        
        return UINib(nibName: "ScreenLock", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ScreenView
    }
    
    
    
    
    @objc func btnNumberClickrd(sender: UIButton) {
        if sender.tag == 9{
            return
        }
        if sender.tag == 11{
            if !String.validateStringValue(str: textFour.text){
                textFour.text = ""
            }else if !String.validateStringValue(str: textThree.text){
                textThree.text = ""
            }else if !String.validateStringValue(str: textTwo.text){
                textTwo.text = ""
            }else if !String.validateStringValue(str: textONe.text){
                textONe.text = ""
            }
            return
        }
        
        if String.validateStringValue(str: textONe.text){
            textONe.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textTwo.text){
            textTwo.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textThree.text){
            textThree.text = arryaNumber[sender.tag]
        }else if String.validateStringValue(str: textFour.text){
            textFour.text = arryaNumber[sender.tag]
            setPin = "\(textONe.text!)\(textTwo.text!)\(textThree.text!)\(textFour.text!)"
            if setPin != _currentUser.nPin{
                    lblError.text = "Wrong Pin"
                }else{
                    lblError.text = " "
                    delegate.btnremove()
                }
        }
    }
    
    @IBAction func btnForgetPasswordClicked(_ sender: Any) {
        
        delegate.btnForgetPassword()
    }
    
}
extension ScreenView{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arryaNumber.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 11{
            collectionView.register(UINib(nibName: "Cancel", bundle: nil), forCellWithReuseIdentifier: "CancelIcon")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CancelIcon", for: indexPath) as! PinCollCell
            cell.btnNumber?.addTarget(self, action: #selector(btnNumberClickrd(sender:)), for: UIControl.Event.touchUpInside)
            cell.btnNumber.tag = indexPath.row
            return cell
        }
        collectionView.register(UINib(nibName: "NumberCell", bundle: nil), forCellWithReuseIdentifier: "Number")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Number", for: indexPath) as! PinCollCell
        cell.lbblNumber.text = arryaNumber[indexPath.row]
        cell.btnNumber.tag = indexPath.row
        cell.btnNumber?.addTarget(self, action: #selector(btnNumberClickrd(sender:)), for: UIControl.Event.touchUpInside)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: _screenSize.width / 3 , height: 84 * _widthRatio)
    }
    
}
