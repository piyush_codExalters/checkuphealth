//
//  CheckVersion.swift
//  CheckupHealth
//
//  Created by codExalters1 on 02/06/21.
//

import UIKit
protocol BtnDelegate{
    func btnNotNow()
    func btnAppStore()
}
class CheckVersion: UIView {
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnNotNow: UIButton!
    var delegate : BtnDelegate!
    var isFore = false

    func initDataupdate() {
        
        constraintUpdate()
        
    }
    func checkForce(){
        btnUpdate.layer.borderColor = UIColor.white.cgColor
        btnUpdate.layer.borderWidth = 2 * _widthRatio
        btnUpdate.layer.backgroundColor = UIColor.clear.cgColor
        btnUpdate.setTitleColor(UIColor.white, for: .normal)
        btnUpdate.layer.cornerRadius = 5 * _widthRatio

        let FormattedText2 = NSMutableAttributedString()
        FormattedText2
            .viewversion("Not Now")
        btnNotNow.setAttributedTitle(FormattedText2, for: .normal)
        if isFore{
            btnNotNow.isHidden = true
        }else{
            btnNotNow.isHidden = false
        }
    }
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
    }
    @IBAction func btnUpdateClicked(_ sender: Any) {
        delegate.btnAppStore()
        
    }
    
    @IBAction func btnNotNowClicked(_ sender: Any) {
        delegate.btnNotNow()
        
    }
    class func instanceFromNib() -> CheckVersion {
        
        return UINib(nibName: "Version", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CheckVersion
    }

}
class CheckMaintenance: UIView {
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?

    
    func initDataupdate()  {
        constraintUpdate()
    }
  
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
    }
    class func instanceFromNib() -> CheckMaintenance {
        
        return UINib(nibName: "Maintenance", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CheckMaintenance
    }

}
