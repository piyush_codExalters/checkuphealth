//
//  ObjectClass.swift
//  CheckupHealth
//
//  Created by codExalters1 on 05/05/21.
//
import UIKit
extension Calendar {
    static let iso8601 = Calendar(identifier: .iso8601)
    static let gregorian = Calendar(identifier: .gregorian)
}
extension Date {
    func byAdding(component: Calendar.Component, value: Int, wrappingComponents: Bool = false, using calendar: Calendar = .current) -> Date? {
        calendar.date(byAdding: component, value: value, to: self, wrappingComponents: wrappingComponents)
    }
    func dateComponents(_ components: Set<Calendar.Component>, using calendar: Calendar = .current) -> DateComponents {
        calendar.dateComponents(components, from: self)
    }
    func startOfWeek(using calendar: Calendar = .current) -> Date {
        calendar.date(from: dateComponents([.yearForWeekOfYear, .weekOfYear], using: calendar))!
    }
    var noon: Date {
        Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    func daysOfWeek(using calendar: Calendar = .current) -> [Date] {
        let startOfWeek = self.startOfWeek(using: calendar).noon
        return (0...6).map { startOfWeek.byAdding(component: .day, value: $0, using: calendar)! }
    }
}
class WeekAvgChart{
 
    var date : String!
    var week : Int!

    var heart_rate : Double!
    var dystolic : Double!
    var systolic : Double!
    var blood_glucose : Double!
    var bmi : Double!
    var weight : Double!
    var temperature : Double!

    var arrayheart_rate = [Double]()
    var arrayDystolic = [Double]()
    var arraySystolic = [Double]()
    var arrayBlood_glucose = [Double]()
    var arrayBmi = [Double]()
    var arrayWeight = [Double]()
    var arrayTemperature = [Double]()

    init(dic : NSDictionary , arryaChart : NSArray) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let weekDay =  dateFormatter.date(from: dic.getStringValue(key: "date"))
        dateFormatter.timeZone = TimeZone.current
        let calendar = Calendar(identifier: .gregorian)

        let datewea = calendar.component(.weekOfYear, from: weekDay!)
        date = String(dic.getStringValue(key: "date"))
        week = datewea
        
            for obj in arryaChart{
                let dicna = obj as! NSDictionary
                
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatter.locale = Locale(identifier:"en_US_POSIX")
                
                let weekDay2 =  dateFormatter.date(from: dicna.getStringValue(key: "date"))
                dateFormatter.timeZone = TimeZone.current

                
                let datewe = calendar.component(.weekOfYear, from: weekDay2!)
                
                if datewea == datewe{
                    
                    if dicna.getStringValue(key: "heart_rate") != ""{
                        self.arrayheart_rate.append(dicna.getDoubleValue(key: "heart_rate"))
                    }
                    self.arrayDystolic.append(dicna.getDoubleValue(key: "dystolic"))
                    self.arraySystolic.append(dicna.getDoubleValue(key: "systolic"))
                    self.arrayBlood_glucose.append(dicna.getDoubleValue(key: "blood_glucose"))
                    self.arrayBmi.append(dicna.getDoubleValue(key: "bmi"))
                    self.arrayWeight.append(dicna.getDoubleValue(key: "weight"))
                    self.arrayTemperature.append(dicna.getDoubleValue(key: "temperature"))
                }
            }
            heart_rate = arrayheart_rate.reduce(0, { $0 + Double($1) / Double(arrayheart_rate.count) })
            dystolic = arrayDystolic.reduce(0, { $0 + Double($1) / Double(arrayDystolic.count) })
            systolic = arraySystolic.reduce(0, { $0 + Double($1) / Double(arraySystolic.count) })
            blood_glucose = arrayBlood_glucose.reduce(0, { $0 + Double($1) / Double(arrayBlood_glucose.count) })
            bmi = arrayBmi.reduce(0, { $0 + Double($1) / Double(arrayBmi.count) })
            weight = arrayWeight.reduce(0, { $0 + Double($1) / Double(arrayWeight.count) })
            temperature = arrayTemperature.reduce(0, { $0 + Double($1) / Double(arrayTemperature.count) })
    }
}
class YearAvgChart{
 
    var date : String!
    var heart_rate : Double!
    var dystolic : Double!
    var systolic : Double!
    var blood_glucose : Double!
    var bmi : Double!
    var weight : Double!
    var temperature : Double!

    var arrayheart_rate = [Double]()
    var arrayDystolic = [Double]()
    var arraySystolic = [Double]()
    var arrayBlood_glucose = [Double]()
    var arrayBmi = [Double]()
    var arrayWeight = [Double]()
    var arrayTemperature = [Double]()

    init(dic : NSDictionary , arryaChart : NSArray) {
        
        
            date = String(dic.getStringValue(key: "date").dropLast(6))
        
            for obj in arryaChart{
                let dicna = obj as! NSDictionary
                
                if String(dic.getStringValue(key: "date").dropLast(6)) == String(dicna.getStringValue(key: "date").dropLast(6)){
                    
                    if dicna.getStringValue(key: "heart_rate") != ""{
                        self.arrayheart_rate.append(dicna.getDoubleValue(key: "heart_rate"))
                    }
                    self.arrayDystolic.append(dicna.getDoubleValue(key: "dystolic"))
                    self.arraySystolic.append(dicna.getDoubleValue(key: "systolic"))
                    self.arrayBlood_glucose.append(dicna.getDoubleValue(key: "blood_glucose"))
                    self.arrayBmi.append(dicna.getDoubleValue(key: "bmi"))
                    self.arrayWeight.append(dicna.getDoubleValue(key: "weight"))
                    self.arrayTemperature.append(dicna.getDoubleValue(key: "temperature"))
                }
            }
            heart_rate = arrayheart_rate.reduce(0, { $0 + Double($1) / Double(arrayheart_rate.count) })
            dystolic = arrayDystolic.reduce(0, { $0 + Double($1) / Double(arrayDystolic.count) })
            systolic = arraySystolic.reduce(0, { $0 + Double($1) / Double(arraySystolic.count) })
            blood_glucose = arrayBlood_glucose.reduce(0, { $0 + Double($1) / Double(arrayBlood_glucose.count) })
            bmi = arrayBmi.reduce(0, { $0 + Double($1) / Double(arrayBmi.count) })
            weight = arrayWeight.reduce(0, { $0 + Double($1) / Double(arrayWeight.count) })
            temperature = arrayTemperature.reduce(0, { $0 + Double($1) / Double(arrayTemperature.count) })
    }
}
class MonthlyAvgChart{
 
    var date : String!
    var heart_rate : Double!
    var dystolic : Double!
    var systolic : Double!
    var blood_glucose : Double!
    var bmi : Double!
    var weight : Double!
    var temperature : Double!

    var arrayheart_rate = [Double]()
    var arrayDystolic = [Double]()
    var arraySystolic = [Double]()
    var arrayBlood_glucose = [Double]()
    var arrayBmi = [Double]()
    var arrayWeight = [Double]()
    var arrayTemperature = [Double]()

    init(dic : NSDictionary , arryaChart : NSArray) {
        
        
            date = String(dic.getStringValue(key: "date").dropLast(3))
        
            for obj in arryaChart{
                let dicna = obj as! NSDictionary
                
                if String(dic.getStringValue(key: "date").dropLast(3)) == String(dicna.getStringValue(key: "date").dropLast(3)){
                    
                    if dicna.getStringValue(key: "heart_rate") != ""{
                        self.arrayheart_rate.append(dicna.getDoubleValue(key: "heart_rate"))
                    }
                    self.arrayDystolic.append(dicna.getDoubleValue(key: "dystolic"))
                    self.arraySystolic.append(dicna.getDoubleValue(key: "systolic"))
                    self.arrayBlood_glucose.append(dicna.getDoubleValue(key: "blood_glucose"))
                    self.arrayBmi.append(dicna.getDoubleValue(key: "bmi"))
                    self.arrayWeight.append(dicna.getDoubleValue(key: "weight"))
                    self.arrayTemperature.append(dicna.getDoubleValue(key: "temperature"))
                }
            }
            heart_rate = arrayheart_rate.reduce(0, { $0 + Double($1) / Double(arrayheart_rate.count) })
            dystolic = arrayDystolic.reduce(0, { $0 + Double($1) / Double(arrayDystolic.count) })
            systolic = arraySystolic.reduce(0, { $0 + Double($1) / Double(arraySystolic.count) })
            blood_glucose = arrayBlood_glucose.reduce(0, { $0 + Double($1) / Double(arrayBlood_glucose.count) })
            bmi = arrayBmi.reduce(0, { $0 + Double($1) / Double(arrayBmi.count) })
            weight = arrayWeight.reduce(0, { $0 + Double($1) / Double(arrayWeight.count) })
            temperature = arrayTemperature.reduce(0, { $0 + Double($1) / Double(arrayTemperature.count) })
    }
}

class DateAvgChart{
 
    var date : String!
    var heart_rate : Double!
    var dystolic : Double!
    var systolic : Double!
    var blood_glucose : Double!
    var bmi : Double!
    var weight : Double!
    var temperature : Double!

    var arrayheart_rate = [Double]()
    var arrayDystolic = [Double]()
    var arraySystolic = [Double]()
    var arrayBlood_glucose = [Double]()
    var arrayBmi = [Double]()
    var arrayWeight = [Double]()
    var arrayTemperature = [Double]()

    init(dic : NSDictionary , arryaChart : NSArray) {
            date = dic.getStringValue(key: "date")
            for obj in arryaChart{
                let dicna = obj as! NSDictionary
                if dic.getStringValue(key: "date") == dicna.getStringValue(key: "date"){
                    if dicna.getStringValue(key: "heart_rate") != ""{
                        self.arrayheart_rate.append(dicna.getDoubleValue(key: "heart_rate"))
                    }
                    self.arrayDystolic.append(dicna.getDoubleValue(key: "dystolic"))
                    self.arraySystolic.append(dicna.getDoubleValue(key: "systolic"))
                    self.arrayBlood_glucose.append(dicna.getDoubleValue(key: "blood_glucose"))
                    self.arrayBmi.append(dicna.getDoubleValue(key: "bmi"))
                    self.arrayWeight.append(dicna.getDoubleValue(key: "weight"))
                    self.arrayTemperature.append(dicna.getDoubleValue(key: "temperature"))
                }
            }
            heart_rate = arrayheart_rate.reduce(0, { $0 + Double($1) / Double(arrayheart_rate.count) })
            dystolic = arrayDystolic.reduce(0, { $0 + Double($1) / Double(arrayDystolic.count) })
            systolic = arraySystolic.reduce(0, { $0 + Double($1) / Double(arraySystolic.count) })
            blood_glucose = arrayBlood_glucose.reduce(0, { $0 + Double($1) / Double(arrayBlood_glucose.count) })
            bmi = arrayBmi.reduce(0, { $0 + Double($1) / Double(arrayBmi.count) })
            weight = arrayWeight.reduce(0, { $0 + Double($1) / Double(arrayWeight.count) })
            temperature = arrayTemperature.reduce(0, { $0 + Double($1) / Double(arrayTemperature.count) })
    }
}






class ActivesubscriptionList {
    var dAmount: String!
    var id: String!
    var nDuration: String!
    var nNoOfConsultant: String!
    var nNoOfConsultantRemaining: String!
    var vTitle: String!
    var dExpiredDate: String!
    var nSubscriptionlogId: String!
    init(dic : NSDictionary) {
        dAmount = dic.getStringValue(key: "dAmount")
        id = dic.getStringValue(key: "id")
        nDuration = dic.getStringValue(key: "nDuration")
        nNoOfConsultantRemaining = dic.getStringValue(key: "nNoOfConsultantRemaining")
        nNoOfConsultant = dic.getStringValue(key: "nNoOfConsultant")
        vTitle = dic.getStringValue(key: "vTitle")
        dExpiredDate = dic.getStringValue(key: "dExpiredDate")
        nSubscriptionlogId = dic.getStringValue(key: "nSubscriptionlogId")

    }
    
}
class SubscriptionList {
    var dAmount: String!
    var id: String!
    var nDuration: String!
    var nNoOfConsultant: String!
    var vTitle: String!
    init(dic : NSDictionary) {
        dAmount = dic.getStringValue(key: "dAmount")
        id = dic.getStringValue(key: "id")
        nDuration = dic.getStringValue(key: "nDuration")
        nNoOfConsultant = dic.getStringValue(key: "nNoOfConsultant")
        vTitle = dic.getStringValue(key: "vTitle")
    }
}

class Subcription{
    var arryaActivesubscription = [ActivesubscriptionList]()
    var arryaSubscription = [SubscriptionList]()
    init(dic : NSDictionary) {
        if let data = dic.value(forKey: "ActivesubscriptionList") as? NSArray{
            for obj in data{
                arryaActivesubscription.append(ActivesubscriptionList(dic: obj as! NSDictionary))
            }
        }
        if let dataa = dic.value(forKey: "SubscriptionList") as? NSArray{
            for obj in dataa{
                arryaSubscription.append(SubscriptionList(dic: obj as! NSDictionary))
            }
        }
    }
}
class SliderList {
    var sliderId: Int!
    var sliderImage: String!
    var sliderText: String!
    var sliderTitle: String!
    init(dic : NSDictionary) {
        sliderId = dic.getIntValue(key: "sliderId")
        sliderImage = dic.getStringValue(key: "sliderImage")
        sliderText = dic.getStringValue(key: "sliderText")
        sliderTitle = dic.getStringValue(key: "sliderTitle")
    }
}
class AddressListName {
    var address : String!
    var addressLine1: String!
    var addressLine2 : String!
    var address_id : String!
    var area : String!
    var city : String!
    var country : String!
    var district : String!
    var postcode : String!
    var state : String!
    init(dic : NSDictionary) {
        address = dic.getStringValue(key: "address")
        addressLine1 = dic.getStringValue(key: "addressLine1")
        addressLine2 = dic.getStringValue(key: "addressLine2")
        address_id = dic.getStringValue(key: "address_id")
        area = dic.getStringValue(key: "area")
        city = dic.getStringValue(key: "city")
        country = dic.getStringValue(key: "country")
        district = dic.getStringValue(key: "district")
        postcode = dic.getStringValue(key: "postcode")
        state = dic.getStringValue(key: "state")
    }
}
class HelipListName {
    var CategoryId: String!
    var CategoryName : String!
    var arryaHelipList = [HelipList]()
    init(dic : NSDictionary) {
        CategoryId = dic.getStringValue(key: "CategoryId")
        CategoryName = dic.getStringValue(key: "CategoryName")
        if let data = dic.value(forKey: "CategoryData") as? NSArray{
            for obj in data{
                self.arryaHelipList.append(HelipList(dic: obj as! NSDictionary))
            }
        }
    }
}
class HelipList {
    var vAnswer : String!
    var vQuestion : String!
    var isSect = false
    init(dic : NSDictionary) {
        vAnswer = dic.getStringValue(key: "vAnswer")
        vQuestion = dic.getStringValue(key: "vQuestion")
        
    }
    
}
class TransactionHistory{
    var appointment_id : String!
    var appointment_order_id : String!
    var dAmount : String!
    var dCreatedDate : String!
    var doctor_degree : String!
    var doctor_first_name : String!
    var doctor_last_name : String!
    var doctor_profile_image : String!
    var doctor_specialization : String!
    var id : String!
    var isActive : String!
    var nType : String!
    var OrganizationName : String!
    var vPaymentStatus : String!
    var vTransactionId : String!
    var doctor_experience_clinic_name : String!
    var nAppointmentFor : String!
    
    init(dic : NSDictionary) {
        appointment_id = dic.getStringValue(key: "appointment_id")
        appointment_order_id = dic.getStringValue(key: "appointment_order_id")
        dAmount = dic.getStringValue(key: "dAmount")
        dCreatedDate = dic.getStringValue(key: "dCreatedDate")
        doctor_degree = dic.getStringValue(key: "doctor_degree")
        doctor_first_name = dic.getStringValue(key: "doctor_first_name")
        doctor_last_name = dic.getStringValue(key: "doctor_last_name")
        doctor_profile_image = dic.getStringValue(key: "doctor_profile_image")
        doctor_specialization = dic.getStringValue(key: "doctor_specialization")
        id = dic.getStringValue(key: "id")
        isActive = dic.getStringValue(key: "isActive")
        nType = dic.getStringValue(key: "nType")
        OrganizationName = dic.getStringValue(key: "OrganizationName")
        vPaymentStatus = dic.getStringValue(key: "vPaymentStatus")
        vTransactionId = dic.getStringValue(key: "vTransactionId")
        doctor_experience_clinic_name = dic.getStringValue(key: "doctor_experience_clinic_name")
        nAppointmentFor = dic.getStringValue(key: "nAppointmentFor")
        
    }
    
}
class CraditTransactionHistory{
    var Credit : String!
    var UserWallet : String!
    var OrganizationName : String!
    var arryaWalletHistory = [WalletHistory]()
    init(dic : NSDictionary) {
        OrganizationName = dic.getStringValue(key: "OrganizationName")
        Credit = dic.getStringValue(key: "Credit")
        UserWallet = dic.getStringValue(key: "UserWallet")
        if let data = dic.value(forKey: "WalletHistory") as? NSArray{
            for obj in data{
                self.arryaWalletHistory.append(WalletHistory(dic: obj as! NSDictionary))
            }
        }
        if let data = dic.value(forKey: "OrganizationDetails") as? NSDictionary{
            Credit = data.getStringValue(key: "Credit")
            UserWallet = data.getStringValue(key: "OrganizationName")
        }
    }
}
class WalletHistory{
    
    var OrganizationName  : String!
    var appointment_id : String!
    var appointment_order_id : String!
    var currency_vSymbol  : String!
    var dAmount : String!
    var dCreatedDate : String!
    var doctor_degree : String!
    var doctor_experience_clinic_address : String!
    var doctor_experience_clinic_name : String!
    var doctor_first_name: String!
    var doctor_last_name : String!
    var doctor_profile_image : String!
    var doctor_specialization : String!
    var id : String!
    var  isActive : String!
    var nAppointmentFor : String!
    var nType : String!
    init(dic : NSDictionary) {
        OrganizationName = dic.getStringValue(key: "OrganizationName")
        appointment_id = dic.getStringValue(key: "appointment_id")
        appointment_order_id = dic.getStringValue(key: "appointment_order_id")
        currency_vSymbol = dic.getStringValue(key: "currency_vSymbol")
        OrganizationName = dic.getStringValue(key: "OrganizationName")
        dAmount = dic.getStringValue(key: "dAmount")
        dCreatedDate = dic.getStringValue(key: "dCreatedDate")
        doctor_degree = dic.getStringValue(key: "doctor_degree")
        doctor_experience_clinic_address = dic.getStringValue(key: "doctor_experience_clinic_address")
        doctor_experience_clinic_name = dic.getStringValue(key: "doctor_experience_clinic_name")
        OrganizationName = dic.getStringValue(key: "OrganizationName")
        doctor_first_name = dic.getStringValue(key: "doctor_first_name")
        doctor_last_name = dic.getStringValue(key: "doctor_last_name")
        doctor_profile_image = dic.getStringValue(key: "doctor_profile_image")
        doctor_specialization = dic.getStringValue(key: "doctor_specialization")
        id = dic.getStringValue(key: "id")
        isActive = dic.getStringValue(key: "isActive")
        nAppointmentFor = dic.getStringValue(key: "nAppointmentFor")
        nType = dic.getStringValue(key: "nType")
        
    }
}
class PhrrequestList {
    
    var request_doctor_id : String!
    var request_doctor_image : String!
    var request_doctor_name : String!
    var request_expire_time  : String!
    var request_for : String!
    var request_id  : String!
    var request_module  : String!
    var request_patient_id  : String!
    var request_start_time  : String!
    var request_status  : String!
    var request_time  : String!
    var colorCode : UIColor!
    init(dic : NSDictionary) {
        request_doctor_id = dic.getStringValue(key: "request_doctor_id")
        request_doctor_image = dic.getStringValue(key: "request_doctor_image")
        request_doctor_name = dic.getStringValue(key: "request_doctor_name")
        request_expire_time = dic.getStringValue(key: "request_expire_time")
        request_for = dic.getStringValue(key: "request_for")
        request_id = dic.getStringValue(key: "request_id")
        request_module = dic.getStringValue(key: "request_module")
        request_patient_id = dic.getStringValue(key: "request_patient_id")
        request_start_time = dic.getStringValue(key: "request_start_time")
        if dic.getStringValue(key: "request_status") == "0"{
            request_status = ""
            colorCode = .white
        }else if dic.getStringValue(key: "request_status") == "1"{
            request_status = "Active"
            colorCode = .colorchaupGreen()
        }else if dic.getStringValue(key: "request_status") == "2"{
            request_status = "cancelled"
            colorCode = .red
        }else if dic.getStringValue(key: "request_status") == "3"{
            request_status = "Expired"
            colorCode = .colorchaupYellow()
        }
        request_time = dic.getStringValue(key: "request_time")
    }
}
class Healthlist {
    var health_diagnosed_date : String!
    var health_id : String!
    var health_name : String!
    var health_note : String!
    var health_status : String!
    var health_treated_by : String!
    var medication_name = [String]()
    init(dic : NSDictionary) {
        
        health_diagnosed_date = dic.getStringValue(key: "health_diagnosed_date")
        health_id = dic.getStringValue(key: "health_id")
        health_name = dic.getStringValue(key: "health_name")
        health_note = dic.getStringValue(key: "health_note")
        health_status = dic.getStringValue(key: "health_status")
        health_treated_by = dic.getStringValue(key: "health_treated_by")
        if let data = dic.value(forKey: "medication_name") as? NSArray{
            for obj in data{
                let str = obj as? String
                if str == nil{
                    return
                }
                str!.replacingOccurrences(of: "<null>", with: "")
                self.medication_name.append(str!)
            }
        }
    }
}
class Medicationlist{
    var dosage_how_id : String!
    var dosage_how_title : String!
    var dosage_strength_type_id : String!
    var dosage_type_id : String!
    var  dosage_type_title : String!
    var medication_by : String!
    var  medication_end_date : String!
    var  medication_for : String!
    var  medication_id : String!
    var  medication_instructions : String!
    var  medication_long_term : String!
    var  medication_name : String!
    var medication_side_effect : String!
    var medication_start_date : String!
    var medication_strength : String!
    var medication_strength_type : String!
    var arryaDosageList = [DosageList]()
    init(dic : NSDictionary) {
        
        dosage_how_id = dic.getStringValue(key: "dosage_how_id")
        dosage_how_title = dic.getStringValue(key: "dosage_how_title")
        dosage_strength_type_id = dic.getStringValue(key: "dosage_strength_type_id")
        dosage_type_id = dic.getStringValue(key: "dosage_type_id")
        dosage_type_title = dic.getStringValue(key: "dosage_type_title")
        medication_by = dic.getStringValue(key: "medication_by")
        medication_end_date = dic.getStringValue(key: "medication_end_date")
        medication_for = dic.getStringValue(key: "medication_for")
        medication_id = dic.getStringValue(key: "medication_id")
        medication_instructions = dic.getStringValue(key: "medication_instructions")
        medication_long_term = dic.getStringValue(key: "medication_long_term")
        medication_name = dic.getStringValue(key: "medication_name")
        medication_side_effect = dic.getStringValue(key: "medication_side_effect")
        medication_start_date = dic.getStringValue(key: "medication_start_date")
        medication_strength = dic.getStringValue(key: "medication_strength")
        medication_strength_type = dic.getStringValue(key: "medication_strength_type")
        if let data = dic.value(forKey: "dosage") as? NSArray{
            for obj in data{
                self.arryaDosageList.append(DosageList(dic: obj as! NSDictionary) )
            }
        }
    }
}
class DosageList {
    var dosage_time_id : String!
    var dosage_time_title : String!
    var dosage_when_id : String!
    var dosage_when_title : String!
    var medication_detail_id : String!
    var medication_detail_mid : String!
    var medication_detail_quantity : String!
    init(dic : NSDictionary) {
        
        dosage_time_id = dic.getStringValue(key: "dosage_time_id")
        dosage_time_title = dic.getStringValue(key: "dosage_time_title")
        dosage_when_id = dic.getStringValue(key: "dosage_when_id")
        dosage_when_title = dic.getStringValue(key: "dosage_when_title")
        medication_detail_id = dic.getStringValue(key: "medication_detail_id")
        medication_detail_mid = dic.getStringValue(key: "medication_detail_mid")
        medication_detail_quantity = dic.getStringValue(key: "medication_detail_quantity")
        
    }
}
class StandardizedDosingTime {
    var id : String!
    var name : String!
    var status : String!
    init(dic : NSDictionary) {
        id = dic.getStringValue(key: "id")
        name = dic.getStringValue(key: "name")
        status = dic.getStringValue(key: "status")
        
    }
    
}
class DregesNameList {
    var id : String!
    var name : String!
    var status : String!
    init(dic : NSDictionary) {
        id = dic.getStringValue(key: "id")
        name = dic.getStringValue(key: "name")
        status = dic.getStringValue(key: "status")
        
    }
    
}
class Dosage {
    var arryaStandardizedDosingTime = [StandardizedDosingTime]()
    var arryaDregesNameList = [DregesNameList]()
    init(dic : NSDictionary) {
        if let data = dic.value(forKey: "time") as? NSArray{
            for obj in data{
                self.arryaStandardizedDosingTime.append(StandardizedDosingTime(dic: obj as! NSDictionary) )
            }
        }
        if let dataa = dic.value(forKey: "type") as? NSArray{
            for obj in dataa{
                self.arryaDregesNameList.append(DregesNameList(dic: obj as! NSDictionary) )
            }
        }
    }
}
class Allergylist {
    var allergy_diagnosed_date : String!
    var allergy_id : String!
    var allergy_name : String!
    var allergy_note : String!
    var allergy_occur : String!
    var allergy_reaction : String!
    var allergy_trigger_by : String!
    var medication_name = [String]()
    init(dic : NSDictionary) {
        
        allergy_diagnosed_date = dic.getStringValue(key: "allergy_diagnosed_date")
        allergy_id = dic.getStringValue(key: "allergy_id")
        allergy_name = dic.getStringValue(key: "allergy_name")
        allergy_note = dic.getStringValue(key: "allergy_note")
        allergy_occur = dic.getStringValue(key: "allergy_occur")
        allergy_reaction = dic.getStringValue(key: "allergy_reaction")
        allergy_trigger_by = dic.getStringValue(key: "allergy_trigger_by")
        if let data = dic.value(forKey: "medication_name") as? NSArray{
            for obj in data{
                let str = obj as? String
                if str == nil{
                    return
                }
                str!.replacingOccurrences(of: "<null>", with: "")
                self.medication_name.append(str!)
            }
        }
    }
}
class Vaccinationlist {
    var vaccination_details : String!
    var vaccination_for : String!
    var vaccination_id : String!
    var vaccination_lot_number : String!
    var vaccination_name : String!
    var vaccination_note : String!
    var vaccination_taken_on : String!
    init(dic : NSDictionary) {
        
        vaccination_details = dic.getStringValue(key: "vaccination_details")
        vaccination_for = dic.getStringValue(key: "vaccination_for")
        vaccination_id = dic.getStringValue(key: "vaccination_id")
        vaccination_lot_number = dic.getStringValue(key: "vaccination_lot_number")
        vaccination_name = dic.getStringValue(key: "vaccination_name")
        vaccination_note = dic.getStringValue(key: "vaccination_note")
        vaccination_taken_on = dic.getStringValue(key: "vaccination_taken_on")
        
    }
}
var strdate = ""



class ReportlistDate{
    var medical_report_upload_by : String!
    var keyz : String!

    var arryaReport = [Reportlist]()
    var isDate = false
    
    init(dic : NSDictionary, arryaDic : NSArray) {
        
        
        
        if strdate != dic.getStringValue(key: "key"){
            
            medical_report_upload_by = dic.getStringValue(key: "medical_report_upload_by")
            keyz = dic.getStringValue(key: "medical_report_upload_by")
            
            strdate = keyz
            
            for obj in arryaDic{
                let dicna = obj as! NSDictionary
                if dic.getStringValue(key: "key") == dicna.getStringValue(key: "key"){
                    self.arryaReport.append(Reportlist(dic: obj as! NSDictionary))
                }
            }
            
            arryaReport.sort(by: { $0.rDate.compare($1.rDate) == .orderedAscending })
        }
    }
}


class Reportlist {
    var key : String!
    var medical_report_attachment : String!
    var medical_report_by : String!
    var medical_report_centre : String!
    var medical_report_date : String!
    var medical_report_id : String!
    var medical_report_name : String!
    var medical_report_note : String!
    var medical_report_result : String!
    var rDate : Date!
    var type: String!
    init(dic : NSDictionary) {
        key = dic.getStringValue(key: "key")
        medical_report_attachment = dic.getStringValue(key: "medical_report_attachment")
        medical_report_by = dic.getStringValue(key: "medical_report_by")
        medical_report_centre = dic.getStringValue(key: "medical_report_centre")
        medical_report_date = dic.getStringValue(key: "medical_report_date")
        medical_report_id = dic.getStringValue(key: "medical_report_id")
        medical_report_name = dic.getStringValue(key: "medical_report_name")
        medical_report_note = dic.getStringValue(key: "medical_report_note")
        medical_report_result = dic.getStringValue(key: "medical_report_result")
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "dd/MM/yyyy"
        rDate = dateFormatter.date(from:medical_report_date)!

        
        
        
        type = dic.getStringValue(key: "type")
    }
}
class Surgerylist{
    var surgery_by : String!
    var surgery_diagnosed_date : String!
    var surgery_id : String!
    var surgery_implants : String!
    var surgery_name : String!
    var surgery_note : String!
    init(dic : NSDictionary) {
        
        surgery_by = dic.getStringValue(key: "surgery_by")
        surgery_diagnosed_date = dic.getStringValue(key: "surgery_diagnosed_date")
        surgery_id = dic.getStringValue(key: "surgery_id")
        surgery_implants = dic.getStringValue(key: "surgery_implants")
        surgery_name = dic.getStringValue(key: "surgery_name")
        surgery_note = dic.getStringValue(key: "surgery_note")
    }
}
class Familylist{
    var family_desc : String!
    var family_id : String!
    var family_type_title : String!
    init(dic : NSDictionary) {
        family_desc = dic.getStringValue(key: "family_desc")
        family_id = dic.getStringValue(key: "family_id")
        family_type_title = dic.getStringValue(key: "family_type_title")
    }
}
class Familytypelist{
    var family_id : String!
    var family_type_title : String!
    init(dic : NSDictionary) {
        family_id = dic.getStringValue(key: "family_type_id")
        family_type_title = dic.getStringValue(key: "family_type_title")
    }
}
class EmergencyListName{
    var emergencyAddress : String!
    var emergencyAddress1 : String!
    var emergency_address : String!
    var emergency_city : String!
    var emergency_country : String!
    var emergency_countrycode : String!
    var emergency_district : String!
    var emergency_hphone : String!
    var emergency_hphone_countrycode : String!
    var emergency_id : String!
    var emergency_mobile : String!
    var emergency_name : String!
    var emergency_ophone: String!
    var emergency_ophone_countrycode : String!
    var emergency_pin : String!
    var emergency_relation : String!
    var emergency_remarks : String!
    var emergency_state : String!
    var emergency_street : String!
    init(dic : NSDictionary) {
        
        emergencyAddress = dic.getStringValue(key: "emergencyAddress")
        emergencyAddress1 = dic.getStringValue(key: "emergencyAddress1")
        emergency_address = dic.getStringValue(key: "emergency_address")
        emergency_city = dic.getStringValue(key: "emergency_city")
        emergency_country = dic.getStringValue(key: "emergency_country")
        emergency_countrycode = dic.getStringValue(key: "emergency_countrycode")
        emergency_district = dic.getStringValue(key: "emergency_district")
        emergency_hphone = dic.getStringValue(key: "emergency_hphone")
        emergency_hphone_countrycode = dic.getStringValue(key: "emergency_hphone_countrycode")
        emergency_id = dic.getStringValue(key: "emergency_id")
        emergency_mobile = dic.getStringValue(key: "emergency_mobile")
        emergency_name = dic.getStringValue(key: "emergency_name")
        emergency_ophone = dic.getStringValue(key: "emergency_ophone")
        emergency_ophone_countrycode = dic.getStringValue(key: "emergency_ophone_countrycode")
        emergency_pin = dic.getStringValue(key: "emergency_pin")
        emergency_relation = dic.getStringValue(key: "emergency_relation")
        emergency_remarks = dic.getStringValue(key: "emergency_remarks")
        emergency_state = dic.getStringValue(key: "emergency_state")
        emergency_street = dic.getStringValue(key: "emergency_street")
    }
}
var sectionIndexGlobel = 0
class ManagementList{
    var review_allergies : String!
    var review_by : String!
    var review_complaint: String!
    var review_date : String!
    var review_desc : String!
    var review_examination : String!
    var review_from : String!
    var review_historycomplaint : String!
    var review_id : String!
    var review_impression : String!
    var review_isSharenoteswithparents : String!
    var review_isSharewithGP : String!
    var review_isfollowupadvised : String!
    var review_medicalhistory : String!
    var review_other_instructions : String!
    var review_pastmedicalhistory : String!
    var review_plan : String!
    var review_referralpath : String!
    var review_sicknotepath : String!
    var review_socialhistory : String!
    var arryaDiagnosis_name = [String]()
    var strDiagnosis_name : String!
    var review_problemtype : String!
    var review_sicknotepath2 : String!
    var review_referralpath2 : String!
    var arraybloodDetails = [bloodDetails]()
    var sectionIndex = 0
    init(dic : NSDictionary) {
        review_sicknotepath2 = dic.getStringValue(key: "review_sicknotepath2")
        review_referralpath2 = dic.getStringValue(key: "review_referralpath2")
        review_problemtype = dic.getStringValue(key: "review_problemtype")
        review_allergies = dic.getStringValue(key: "review_allergies")
        review_by = dic.getStringValue(key: "review_by")
        review_complaint = dic.getStringValue(key: "review_complaint")
        review_date = dic.getStringValue(key: "review_date")
        review_desc = dic.getStringValue(key: "review_desc")
        review_examination = dic.getStringValue(key: "review_examination")
        review_from = dic.getStringValue(key: "review_from")
        review_historycomplaint = dic.getStringValue(key: "review_historycomplaint")
        review_id = dic.getStringValue(key: "review_id")
        review_impression = dic.getStringValue(key: "review_impression")
        review_isSharenoteswithparents = dic.getStringValue(key: "review_isSharenoteswithparents")
        review_isSharewithGP = dic.getStringValue(key: "review_isSharewithGP")
        review_isfollowupadvised = dic.getStringValue(key: "review_isfollowupadvised")
        review_medicalhistory = dic.getStringValue(key: "review_medicalhistory")
        review_other_instructions = dic.getStringValue(key: "review_other_instructions")
        review_pastmedicalhistory = dic.getStringValue(key: "review_pastmedicalhistory")
        review_plan = dic.getStringValue(key: "review_plan")
        review_referralpath = dic.getStringValue(key: "review_referralpath")
        review_sicknotepath = dic.getStringValue(key: "review_sicknotepath")
        review_socialhistory = dic.getStringValue(key: "review_socialhistory")
        sectionIndex = sectionIndexGlobel
        sectionIndex += 1
        if let dataa = dic.value(forKey: "bloodDetails") as? NSArray{
            for obj in dataa{
                arraybloodDetails.append(bloodDetails(dic: obj as! NSDictionary))
            }
        }
        if let dataa = dic.value(forKey: "review_diagnosis") as? NSArray{
            for obj in dataa{
                let str = obj as! NSDictionary
                arryaDiagnosis_name.append(str.getStringValue(key: "diagnosis_name"))
            }
        }
        strDiagnosis_name = arryaDiagnosis_name.joined(separator: "\n")
    }
}
class bloodDetails{
    var Tatname : String!
    var bloodName : String!
    var dSellingPrice : String!
    var pathologyName : String!
    init(dic : NSDictionary) {
        Tatname = dic.getStringValue(key: "Tatname")
        bloodName = dic.getStringValue(key: "bloodName")
        dSellingPrice = dic.getStringValue(key: "dSellingPrice")
        pathologyName = dic.getStringValue(key: "pathologyName")
    }
}

class Lifestylelist{
    var alcohol_drinkertype : String!
    var alcohol_type : String!
    var alternative_therapy : String!
    var basic_toilet : String!
    var  basic_water : String!
    var exercise : String!
    var habit : String!
    var habit_feq : String!
    var habit_since : String!
    var lifestyle_id : String!
    var religion_church : String!
    var religion_name : String!
    var religion_other : String!
    var religion_pastor : String!
    var smoking_smokernumber : String!
    var smoking_type : String!
    var type : String!
    init(dic : NSDictionary) {
        
        alcohol_drinkertype = dic.getStringValue(key: "alcohol_drinkertype")
        alcohol_type = dic.getStringValue(key: "alcohol_type")
        alternative_therapy = dic.getStringValue(key: "alternative_therapy")
        basic_toilet = dic.getStringValue(key: "basic_toilet")
        basic_water = dic.getStringValue(key: "basic_water")
        exercise = dic.getStringValue(key: "exercise")
        habit = dic.getStringValue(key: "habit")
        habit_feq = dic.getStringValue(key: "habit_feq")
        habit_since = dic.getStringValue(key: "habit_since")
        lifestyle_id = dic.getStringValue(key: "lifestyle_id")
        religion_church = dic.getStringValue(key: "religion_church")
        religion_name = dic.getStringValue(key: "religion_name")
        religion_other = dic.getStringValue(key: "religion_other")
        religion_pastor = dic.getStringValue(key: "religion_pastor")
        smoking_smokernumber = dic.getStringValue(key: "smoking_smokernumber")
        smoking_type = dic.getStringValue(key: "smoking_type")
        type = dic.getStringValue(key: "type")
        
    }
}
class Currency {
    var createdDate  : String!
    var currency_conversionRate  : String!
    var currency_currencycode  : String!
    var currency_id  : String!
    var currency_status  : String!
    var currency_vSymbol  : String!
    init(dic : NSDictionary) {
        createdDate = dic.getStringValue(key: "createdDate")
        currency_conversionRate = dic.getStringValue(key: "currency_conversionRate")
        currency_currencycode = dic.getStringValue(key: "currency_currencycode")
        currency_id = dic.getStringValue(key: "currency_id")
        currency_status = dic.getStringValue(key: "currency_status")
        currency_vSymbol = dic.getStringValue(key: "currency_vSymbol")
    }
}
class Information{
    var dCreatedDate: String!
    var id : String!
    var nStatus : String!
    var vCountryCode : String!
    var vDesc : String!
    var doctor_specialization : String!
    var doctor_specialisation_interest : String!
    var doctor_experience_clinic_name : String!
    var vImagePath : String!
    var vTitle : String!
    var vURL : String!
    init(dic : NSDictionary) {
        doctor_experience_clinic_name = dic.getStringValue(key: "doctor_experience_clinic_name")
        doctor_specialization = dic.getStringValue(key: "doctor_specialization")
        doctor_specialisation_interest = dic.getStringValue(key: "doctor_specialisation_interest")
        dCreatedDate = dic.getStringValue(key: "dCreatedDate")
        id = dic.getStringValue(key: "id")
        nStatus = dic.getStringValue(key: "nStatus")
        vCountryCode = dic.getStringValue(key: "vCountryCode")
        vDesc = dic.getStringValue(key: "vDesc")
        vImagePath = dic.getStringValue(key: "vImagePath")
        vTitle = dic.getStringValue(key: "vTitle")
        vURL = dic.getStringValue(key: "vURL")
    }
}
class GB{
    var PARAMETER_AUDIO_CONSULTATION_4 : String!
    var PARAMETER_PESCRIPTION_MODULE_1 : String!
    var PARAMETER_SERVICE_PROVIDER_3 : String!
    var PARAMETER_VIDEO_CONSULTATION_2 : String!
    init(dic : NSDictionary) {
        PARAMETER_AUDIO_CONSULTATION_4 = dic.getStringValue(key: "PARAMETER_AUDIO_CONSULTATION_4")
        PARAMETER_PESCRIPTION_MODULE_1 = dic.getStringValue(key: "PARAMETER_PESCRIPTION_MODULE_1")
        PARAMETER_SERVICE_PROVIDER_3 = dic.getStringValue(key: "PARAMETER_SERVICE_PROVIDER_3")
        PARAMETER_VIDEO_CONSULTATION_2 = dic.getStringValue(key: "PARAMETER_VIDEO_CONSULTATION_2")
    }
}
class ZW{
    var PARAMETER_AUDIO_CONSULTATION_4 : String!
    var PARAMETER_PESCRIPTION_MODULE_1 : String!
    var PARAMETER_SERVICE_PROVIDER_3 : String!
    var PARAMETER_VIDEO_CONSULTATION_2 : String!
    init(dic : NSDictionary) {
        PARAMETER_AUDIO_CONSULTATION_4 = dic.getStringValue(key: "PARAMETER_AUDIO_CONSULTATION_4")
        PARAMETER_PESCRIPTION_MODULE_1 = dic.getStringValue(key: "PARAMETER_PESCRIPTION_MODULE_1")
        PARAMETER_SERVICE_PROVIDER_3 = dic.getStringValue(key: "PARAMETER_SERVICE_PROVIDER_3")
        PARAMETER_VIDEO_CONSULTATION_2 = dic.getStringValue(key: "PARAMETER_VIDEO_CONSULTATION_2")
    }
}
class Banner{
    var dCreatedDate : String!
    var dModifiedDate : String!
    var id : String!
    var isActive : String!
    var nSequenceNo : String!
    var vImagePath : String!
    init(dic : NSDictionary) {
        dCreatedDate = dic.getStringValue(key: "dCreatedDate")
        dModifiedDate = dic.getStringValue(key: "dModifiedDate")
        id = dic.getStringValue(key: "id")
        isActive = dic.getStringValue(key: "isActive")
        nSequenceNo = dic.getStringValue(key: "nSequenceNo")
        vImagePath = dic.getStringValue(key: "vImagePath")
    }
}
class HomeList{
    var arryaCurrency = [Currency]()
    var arryaInformation = [Information]()
    var arrayBanner = [Banner]()

    var gb : GB!
    var zw : ZW!
    var isDocumentVerificationPending : String!
    var isRegisterAddressPending : String!
    var isGPLinkPending : String!
    
    var arrayLink = [String]()
    var arrayLinkImage  = [UIImage]()
    
    

    init(dic : NSDictionary) {
        isDocumentVerificationPending = dic.getStringValue(key: "isDocumentVerificationPending")
        isRegisterAddressPending = dic.getStringValue(key: "isRegisterAddressPending")
        isGPLinkPending = dic.getStringValue(key: "isGPLinkPending")
        if isRegisterAddressPending == "1"{
            arrayLink.append("Add registered address")
            arrayLinkImage.append(#imageLiteral(resourceName: "ic_address"))
        }
        if isDocumentVerificationPending == "1"{
            arrayLink.append("Upload documents")
            arrayLinkImage.append(#imageLiteral(resourceName: "ic_document"))
        }
        if isGPLinkPending == "1"{
            arrayLink.append("Link GP")
            arrayLinkImage.append(#imageLiteral(resourceName: "ic_linkgp"))
        }
        
        if let data = dic.value(forKey: "currency") as? NSArray{
            for obj in data{
                let Lang = _userDefault.value(forKey: "Language") as? String
                let cuntery = obj as! NSDictionary
                if cuntery.getStringValue(key: "currency_currencycode") == Lang{
                    _currency = Currency(dic: cuntery)
                }else if Lang == "" && cuntery.getStringValue(key: "currency_currencycode") == "GB"{
                    _currency = Currency(dic: cuntery)
                }
                arryaCurrency.append(Currency(dic: obj as! NSDictionary))
            }
        }
        if let dataa = dic.value(forKey: "Banner") as? NSArray{
            for obj in dataa{
                arrayBanner.append(Banner(dic: obj as! NSDictionary))
            }
        }
        if let dataa = dic.value(forKey: "information") as? NSArray{
            for obj in dataa{
                arryaInformation.append(Information(dic: obj as! NSDictionary))
            }
        }
        if let parame = dic.value(forKey: "parameter") as? NSDictionary{
            if let controy = parame.value(forKey: "GB") as? NSDictionary{
                gb = GB(dic: controy)
                _parameterGB = gb
            }
            if let contr = parame.value(forKey: "ZW") as? NSDictionary{
                zw = ZW(dic: contr)
                _parameterZW = zw
            }
        }
    }
}
class Appointmenthistory{
    var appoint_type : String!
    var appointment_availibility : String!
    var appointment_created_date : String!
    var appointment_for : String!
    var appointment_id : String!
    var appointment_order_id : String!
    var appointment_status : String!
    var appointment_type : String!
    var booking_date: String!
    var booking_time : String!
    var cancel_by : String!
    var cancel_reason : String!
    var cancel_status : String!
    var cancel_time : String!
    var completed_status : String!
    var completed_time : String!
    var data  : String!
    var delivery_id : String!
    var delivery_name : String!
    var description : String!
    var doctor_degree : String!
    var doctor_experience_clinic_address : String!
    var doctor_experience_clinic_name : String!
    var doctor_first_name : String!
    var doctor_id : String!
    var doctor_last_name : String!
    var doctor_latitude : String!
    var doctor_longitude : String!
    var doctor_phone : String!
    var doctor_profile_image : String!
    var doctor_specialization : String!
    var doctor_specialisation_interest : String!

    var patient_dob : String!
    var patient_email_address : String!
    var patient_first_name : String!
    var patient_gender : String!
    var patient_id : String!
    var patient_last_name : String!
    var patient_phone : String!
    var patient_profile_image : String!
    var payment_status : String!
    var pharmacy_id : String!
    var pharmacy_name : String!
    var phlebotomy_name : String!
    var referralfile : String!
    var referralfile2 : String!
    var request : String!
    var sicknotefile : String!
    var sicknotefile2 : String!
    var verification_code : String!
    var verify_status : String!
    var verify_time : String!
    var status : String!
    var triage_status : String!
    
    var patient_consultation_address_id : String!
    var patient_consultation_address : String!
    init(dic : NSDictionary) {
        referralfile2  = dic.getStringValue(key: "referralfile2")
        sicknotefile2  = dic.getStringValue(key: "sicknotefile2")
        patient_consultation_address_id  = dic.getStringValue(key: "patient_consultation_address_id")
        patient_consultation_address  = dic.getStringValue(key: "patient_consultation_address")
        
        doctor_specialisation_interest  = dic.getStringValue(key: "doctor_specialisation_interest")
        appoint_type  = dic.getStringValue(key: "appoint_type")
        appointment_availibility  = dic.getStringValue(key: "appointment_availibility")
        appointment_created_date  = dic.getStringValue(key: "appointment_created_date")
        appointment_for  = dic.getStringValue(key: "appointment_for")
        appointment_id  = dic.getStringValue(key: "appointment_id")
        appointment_order_id  = dic.getStringValue(key: "appointment_order_id")
        appointment_status  = dic.getStringValue(key: "appointment_status")
        appointment_type  = dic.getStringValue(key: "appointment_type")
        booking_date = dic.getStringValue(key: "booking_date")
        booking_time  = dic.getStringValue(key: "booking_time")
        cancel_by  = dic.getStringValue(key: "cancel_by")
        cancel_reason  = dic.getStringValue(key: "cancel_reason")
        cancel_status  = dic.getStringValue(key: "cancel_status")
        cancel_time  = dic.getStringValue(key: "cancel_time")
        completed_status  = dic.getStringValue(key: "completed_status")
        completed_time  = dic.getStringValue(key: "completed_time")
        data   = dic.getStringValue(key: "data")
        delivery_id  = dic.getStringValue(key: "delivery_id")
        delivery_name  = dic.getStringValue(key: "delivery_name")
        description  = dic.getStringValue(key: "description")
        doctor_degree  = dic.getStringValue(key: "doctor_degree")
        doctor_experience_clinic_address  = dic.getStringValue(key: "doctor_experience_clinic_address")
        doctor_experience_clinic_name  = dic.getStringValue(key: "doctor_experience_clinic_name")
        doctor_first_name  = dic.getStringValue(key: "doctor_first_name")
        doctor_id  = dic.getStringValue(key: "doctor_id")
        doctor_last_name  = dic.getStringValue(key: "doctor_last_name")
        doctor_latitude  = dic.getStringValue(key: "doctor_latitude")
        doctor_longitude  = dic.getStringValue(key: "doctor_longitude")
        doctor_phone  = dic.getStringValue(key: "doctor_phone")
        doctor_profile_image  = dic.getStringValue(key: "doctor_profile_image")
        doctor_specialization  = dic.getStringValue(key: "doctor_specialization")
        patient_dob  = dic.getStringValue(key: "patient_dob")
        patient_email_address  = dic.getStringValue(key: "patient_email_address")
        patient_first_name  = dic.getStringValue(key: "patient_first_name")
        patient_gender  = dic.getStringValue(key: "patient_gender")
        patient_id  = dic.getStringValue(key: "patient_id")
        patient_last_name  = dic.getStringValue(key: "patient_last_name")
        patient_phone  = dic.getStringValue(key: "patient_phone")
        patient_profile_image  = dic.getStringValue(key: "patient_profile_image")
        payment_status  = dic.getStringValue(key: "payment_status")
        pharmacy_id  = dic.getStringValue(key: "pharmacy_id")
        pharmacy_name  = dic.getStringValue(key: "pharmacy_name")
        phlebotomy_name  = dic.getStringValue(key: "phlebotomy_name")
        referralfile  = dic.getStringValue(key: "referralfile")
        request  = dic.getStringValue(key: "request")
        sicknotefile  = dic.getStringValue(key: "sicknotefile")
        verification_code  = dic.getStringValue(key: "verification_code")
        verify_status  = dic.getStringValue(key: "verify_status")
        verify_time  = dic.getStringValue(key: "verify_time")
        triage_status  = dic.getStringValue(key: "triage_status")
        if isWorkingCountryUK(){
            if triage_status == "1" || triage_status == "0"{
                if appointment_status == "0"{
                    status =  "Upcoming"
                }else if appointment_status == "1"{
                    status =  "Completed"
                }else if appointment_status == "2"{
                    status =  "cancelled"
                }else if appointment_status == "3"{
                    status =   "DNA"
                }else{
                    status =  "Pending"
                }
            }else if triage_status == "2"{
                status =  "Rejected"
            }else{
                status =  "Pending"
            }
        }else{
            if appointment_status == "0"{
                status =  "Upcoming"
            }else if appointment_status == "1"{
                status =  "Completed"
            }else if appointment_status == "2"{
                status =  "cancelled"
            }else if appointment_status == "3"{
                status =  "DNA"
            }else{
                status =  "Pending"
            }
        }
    }
    
    func isWorkingCountryUK() -> Bool {
        let WorkingCountryUK = _userDefault.value(forKey: "Language") as? String
        if WorkingCountryUK == nil || WorkingCountryUK == "ZW"{
            return false
        }else{
            return true
        }
    }
}
class Doctorspeciality{
    var specialityId : String!
    var specialityImage : String!
    var specialityTitle : String!
    init(dic : NSDictionary) {
        specialityId  = dic.getStringValue(key: "specialityId")
        specialityImage  = dic.getStringValue(key: "specialityImage")
        specialityTitle  = dic.getStringValue(key: "specialityTitle")
    }
}
class DocterDetailes{
    var doctor_affiliations : String!
    var doctor_banner_image : String!
    var doctor_blood : String!
    var doctor_cdisplay   : String!
    var doctor_city   : String!
    var doctor_commission   : String!
    var doctor_contact   : String!
    var doctor_contact_countrycode   : String!
    var doctor_country   : String!
    var doctor_countrycode   : String!
    var doctor_cprefix   : String!
    var doctor_created_date   : String!
    var doctor_ctype   : String!
    var doctor_degree   : String!
    var doctor_demail   : String!
    var doctor_designation   : String!
    var doctor_dob   : String!
    var doctor_edisplay   : String!
    var doctor_email_address   : String!
    var doctor_exp_year   : String!
    var doctor_experience_clinic_address   : String!
    var doctor_experience_clinic_detail   : String!
    var doctor_experience_clinic_name   : String!
    var doctor_experience_current_flag   : String!
    var doctor_experience_designation   : String!
    var doctor_experience_end   : String!
    var doctor_experience_present   : String!
    var doctor_experience_specialisation   : String!
    var doctor_experience_start   : String!
    var doctor_experience_status   : String!
    var doctor_fees   : Float!
    var doctor_first_name   : String!
    var doctor_gender   : String!
    var doctor_graduation   : String!
    var doctor_height   : String!
    var doctor_id   : String!
    var doctor_is_booking   : String!
    var doctor_last_login_date   : String!
    var doctor_last_login_ip   : String!
    var doctor_last_name   : String!
    var doctor_logo   : String!
    var doctor_makecall_countrycode   : String!
    var doctor_medical_board   : String!
    var doctor_medical_license   : String!
    var doctor_occupation   : String!
    var doctor_overview   : String!
    var doctor_password   : String!
    var doctor_phone   : String!
    var doctor_postcode   : String!
    var doctor_practice_country_isocode   : String!
    var doctor_practicename   : String!
    var doctor_preferred_type   : String!
    var doctor_prefix   : String!
    var doctor_profile_image   : String!
    var doctor_province   : String!
    var doctor_references   : String!
    var doctor_reg_key   : String!
    var doctor_signature_image   : String!
    var doctor_specialintrest   : String!
    var doctor_specialisation_doctor_id   : String!
    var doctor_specialisation_id   : String!
    var doctor_specialisation_primary   : String!
    var doctor_speciality_id   : String!
    var doctor_specialization   : String!
    var doctor_state   : String!
    var doctor_status   : String!
    var doctor_street   : String!
    var doctor_unique_id   : String!
    var doctor_verification_code   : String!
    var doctor_weight   : String!
    var doctor_working_day   : String!
    var experience   : String!
    var isListingpurpose   : String!
    var keyword   : String!
    var latitude   : Double!
    var longitude   : Double!
    var make_a_call   : String!
    var nDoctorType   : String!
    var arryaSocialMedia = [SocialMedia]()
    var arryaDoctorExperience = [DoctorExperience]()
    var friday = [String]()
    var monday = [String]()
    var saturday = [String]()
    var sunday = [String]()
    var thursday = [String]()
    var tuesday = [String]()
    var wednesday = [String]()
    var appointmentType : String!
    var time_slot : TimeList!
    var appointment_for : String!
    
    var doctor_specialisation_interest : String!
    init(dic : NSDictionary) {
        appointment_for   = dic.getStringValue(key: "appointment_for")
        doctor_specialisation_interest   = dic.getStringValue(key: "doctor_specialisation_interest")
        doctor_affiliations   = dic.getStringValue(key: "doctor_affiliations")
        doctor_banner_image   = dic.getStringValue(key: "doctor_banner_image")
        doctor_blood   = dic.getStringValue(key: "doctor_blood")
        doctor_cdisplay   = dic.getStringValue(key: "doctor_cdisplay")
        doctor_city   = dic.getStringValue(key: "doctor_city")
        doctor_commission   = dic.getStringValue(key: "doctor_commission")
        doctor_contact   = dic.getStringValue(key: "doctor_contact")
        doctor_contact_countrycode   = dic.getStringValue(key: "doctor_contact_countrycode")
        doctor_country   = dic.getStringValue(key: "doctor_country")
        doctor_countrycode   = dic.getStringValue(key: "doctor_countrycode")
        doctor_cprefix   = dic.getStringValue(key: "doctor_cprefix")
        doctor_created_date   = dic.getStringValue(key: "doctor_created_date")
        doctor_ctype   = dic.getStringValue(key: "doctor_ctype")
        doctor_degree   = dic.getStringValue(key: "doctor_degree")
        doctor_demail   = dic.getStringValue(key: "doctor_demail")
        doctor_designation   = dic.getStringValue(key: "doctor_designation")
        doctor_dob   = dic.getStringValue(key: "doctor_dob")
        doctor_edisplay   = dic.getStringValue(key: "doctor_edisplay")
        doctor_email_address   = dic.getStringValue(key: "doctor_email_address")
        doctor_exp_year   = dic.getStringValue(key: "doctor_exp_year")
        doctor_experience_clinic_address   = dic.getStringValue(key: "doctor_experience_clinic_address")
        doctor_experience_clinic_detail   = dic.getStringValue(key: "doctor_experience_clinic_detail")
        doctor_experience_clinic_name   = dic.getStringValue(key: "doctor_experience_clinic_name")
        doctor_experience_current_flag   = dic.getStringValue(key: "doctor_experience_current_flag")
        doctor_experience_designation   = dic.getStringValue(key: "doctor_experience_designation")
        doctor_experience_end   = dic.getStringValue(key: "doctor_experience_end")
        doctor_experience_present   = dic.getStringValue(key: "doctor_experience_present")
        doctor_experience_specialisation  = dic.getStringValue(key: "doctor_experience_specialisation")
        doctor_experience_start   = dic.getStringValue(key: "doctor_experience_start")
        doctor_experience_status   = dic.getStringValue(key: "doctor_experience_status")
        doctor_fees   = dic.getFloatValue(key: "doctor_fees")
        doctor_first_name   = dic.getStringValue(key: "doctor_first_name")
        doctor_gender   = dic.getStringValue(key: "doctor_gender")
        doctor_graduation   = dic.getStringValue(key: "doctor_graduation")
        doctor_height   = dic.getStringValue(key: "doctor_height")
        doctor_id   = dic.getStringValue(key: "doctor_id")
        doctor_is_booking   = dic.getStringValue(key: "doctor_is_booking")
        doctor_last_login_date   = dic.getStringValue(key: "doctor_last_login_date")
        doctor_last_login_ip   = dic.getStringValue(key: "doctor_last_login_ip")
        doctor_last_name   = dic.getStringValue(key: "doctor_last_name")
        doctor_logo   = dic.getStringValue(key: "doctor_logo")
        doctor_makecall_countrycode   = dic.getStringValue(key: "doctor_makecall_countrycode")
        doctor_medical_board   = dic.getStringValue(key: "doctor_medical_board")
        doctor_medical_license   = dic.getStringValue(key: "doctor_medical_license")
        doctor_occupation   = dic.getStringValue(key: "doctor_occupation")
        doctor_overview   = dic.getStringValue(key: "doctor_overview")
        doctor_password   = dic.getStringValue(key: "doctor_password")
        doctor_phone   = dic.getStringValue(key: "doctor_phone")
        doctor_postcode   = dic.getStringValue(key: "doctor_postcode")
        doctor_practice_country_isocode   = dic.getStringValue(key: "doctor_practice_country_isocode")
        doctor_practicename   = dic.getStringValue(key: "doctor_practicename")
        doctor_preferred_type   = dic.getStringValue(key: "doctor_preferred_type")
        doctor_prefix   = dic.getStringValue(key: "doctor_prefix")
        doctor_profile_image   = dic.getStringValue(key: "doctor_profile_image")
        doctor_province   = dic.getStringValue(key: "doctor_province")
        doctor_references   = dic.getStringValue(key: "doctor_references")
        doctor_reg_key   = dic.getStringValue(key: "doctor_reg_key")
        doctor_signature_image   = dic.getStringValue(key: "doctor_signature_image")
        doctor_specialintrest   = dic.getStringValue(key: "doctor_specialintrest")
        doctor_specialisation_doctor_id   = dic.getStringValue(key: "doctor_specialisation_doctor_id")
        doctor_specialisation_id   = dic.getStringValue(key: "doctor_specialisation_id")
        doctor_specialisation_primary   = dic.getStringValue(key: "doctor_specialisation_primary")
        doctor_speciality_id   = dic.getStringValue(key: "doctor_speciality_id")
        doctor_specialization   = dic.getStringValue(key: "doctor_specialization")
        doctor_state   = dic.getStringValue(key: "doctor_state")
        doctor_status   = dic.getStringValue(key: "doctor_status")
        doctor_street   = dic.getStringValue(key: "doctor_street")
        doctor_unique_id   = dic.getStringValue(key: "doctor_unique_id")
        doctor_verification_code   = dic.getStringValue(key: "doctor_verification_code")
        doctor_weight   = dic.getStringValue(key: "doctor_weight")
        doctor_working_day   = dic.getStringValue(key: "doctor_working_day")
        experience   = dic.getStringValue(key: "experience")
        isListingpurpose   = dic.getStringValue(key: "isListingpurpose")
        keyword   = dic.getStringValue(key: "keyword")
        latitude   = dic.getDoubleValue(key: "latitude")
        longitude   = dic.getDoubleValue(key: "longitude")
        make_a_call   = dic.getStringValue(key: "make_a_call")
        nDoctorType   = dic.getStringValue(key: "nDoctorType")
        if let data = dic.value(forKey: "doctor_experience") as? NSArray{
            for obj in data{
                arryaDoctorExperience.append(DoctorExperience(dic: obj as! NSDictionary))
            }
        }
        if let data = dic.value(forKey: "socialMedia") as? NSArray{
            for obj in data{
                arryaSocialMedia.append(SocialMedia(dic: obj as! NSDictionary))
            }
        }
        if let datas = dic.value(forKey: "time_slot") as? NSDictionary{
            time_slot = TimeList(dic: datas)
        }
        if let data = dic.value(forKey: "doctor_practice_hours") as? NSDictionary{
            if let day = data.value(forKey: "friday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.friday.append(str!)
                }
            }
            if let day = data.value(forKey: "monday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.monday.append(str!)
                }
            }
            if let day = data.value(forKey: "saturday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.saturday.append(str!)
                }
            }
            if let day = data.value(forKey: "sunday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.sunday.append(str!)
                }
            }
            if let day = data.value(forKey: "thursday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.thursday.append(str!)
                }
            }
            if let day = data.value(forKey: "tuesday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.tuesday.append(str!)
                }
            }
            if let day = data.value(forKey: "wednesday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.wednesday.append(str!)
                }
            }
        }
    }
}
class DoctorExperience{
    var expId : String!
    var exp_clinic_address : String!
    var exp_clinic_designation : String!
    var exp_clinic_detail : String!
    var exp_clinic_end : String!
    var exp_clinic_name : String!
    var exp_clinic_specialisation : String!
    var exp_clinic_start : String!
    var exp_nurseId : String!
    init(dic : NSDictionary) {
        expId   = dic.getStringValue(key: "expId")
        exp_clinic_address  = dic.getStringValue(key: "exp_clinic_address")
        exp_clinic_designation  = dic.getStringValue(key: "exp_clinic_designation")
        exp_clinic_detail  = dic.getStringValue(key: "exp_clinic_detail")
        exp_clinic_end  = dic.getStringValue(key: "exp_clinic_end")
        exp_clinic_name  = dic.getStringValue(key: "exp_clinic_name")
        exp_clinic_specialisation   = dic.getStringValue(key: "exp_clinic_specialisation")
        exp_clinic_start  = dic.getStringValue(key: "exp_clinic_start")
        exp_nurseId  = dic.getStringValue(key: "exp_nurseId")
    }
}
class NurseDetailes {
    var experience : String!
    var isListingpurpose : String!
    var nurse_address : String!
    var nurse_banner_image : String!
    var nurse_blood : String!
    var nurse_city : String!
    var nurse_commission : String!
    var nurse_contact : String!
    var nurse_contact_countrycode : String!
    var nurse_country : String!
    var nurse_countrycode : String!
    var nurse_cprefix : String!
    var nurse_created_date : String!
    var nurse_demail : String!
    var nurse_dob : String!
    var nurse_doctor_id : String!
    var nurse_education : String!
    var nurse_email_address : String!
    var nurse_fees : String!
    var nurse_first_name : String!
    var nurse_gender : String!
    var nurse_height : String!
    var nurse_id : String!
    var nurse_is_booking : String!
    var nurse_last_login_date : String!
    var nurse_last_login_ip : String!
    var nurse_last_name : String!
    var nurse_latitude : Double!
    var nurse_longitude : Double!
    var nurse_make_a_call : String!
    var nurse_makecall_countrycode : String!
    var nurse_medical_board : String!
    var nurse_medical_registration_number : String!
    var nurse_ns_id : String!
    var nurse_occupation : String!
    var nurse_overview : String!
    var nurse_password : String!
    var nurse_phone : String!
    var nurse_practice_country_isocode : String!
    var nurse_preferred_type : String!
    var nurse_prefix : String!
    var nurse_profile_image : String!
    var nurse_province : String!
    var nurse_specialization : String!
    var nurse_state : String!
    var nurse_status : String!
    var nurse_unique_id : String!
    var nurse_weight : String!
    var nurse_working_day : String!
    var appointmentType : String!
    var nurse_experience_clinic_address : String!
    var arryaDoctorExperience = [DoctorExperience]()
    var arryaSocialMedia = [SocialMedia]()
    var time_slot : TimeList!
    var friday = [String]()
    var monday = [String]()
    var saturday = [String]()
    var sunday = [String]()
    var thursday = [String]()
    var tuesday = [String]()
    var wednesday = [String]()
    init(dic : NSDictionary) {
        nurse_experience_clinic_address  = dic.getStringValue(key: "nurse_experience_clinic_address")
        
        experience  = dic.getStringValue(key: "experience")
        isListingpurpose  = dic.getStringValue(key: "isListingpurpose")
        nurse_address  = dic.getStringValue(key: "nurse_address")
        nurse_banner_image  = dic.getStringValue(key: "nurse_banner_image")
        nurse_blood  = dic.getStringValue(key: "nurse_blood")
        nurse_city  = dic.getStringValue(key: "nurse_city")
        nurse_commission  = dic.getStringValue(key: "nurse_commission")
        nurse_contact  = dic.getStringValue(key: "nurse_contact")
        nurse_contact_countrycode  = dic.getStringValue(key: "nurse_contact_countrycode")
        nurse_country  = dic.getStringValue(key: "nurse_country")
        nurse_countrycode  = dic.getStringValue(key: "nurse_countrycode")
        nurse_cprefix  = dic.getStringValue(key: "nurse_cprefix")
        nurse_created_date  = dic.getStringValue(key: "nurse_created_date")
        nurse_demail  = dic.getStringValue(key: "nurse_demail")
        nurse_dob  = dic.getStringValue(key: "nurse_dob")
        nurse_doctor_id  = dic.getStringValue(key: "nurse_doctor_id")
        nurse_education  = dic.getStringValue(key: "nurse_education")
        nurse_email_address  = dic.getStringValue(key: "nurse_email_address")
        nurse_fees  = dic.getStringValue(key: "nurse_fees")
        nurse_first_name  = dic.getStringValue(key: "nurse_first_name")
        nurse_gender  = dic.getStringValue(key: "nurse_gender")
        nurse_height  = dic.getStringValue(key: "nurse_height")
        nurse_id  = dic.getStringValue(key: "nurse_id")
        nurse_is_booking  = dic.getStringValue(key: "nurse_is_booking")
        nurse_last_login_date  = dic.getStringValue(key: "nurse_last_login_date")
        nurse_last_login_ip  = dic.getStringValue(key: "nurse_last_login_ip")
        nurse_last_name  = dic.getStringValue(key: "nurse_last_name")
        nurse_latitude  = dic.getDoubleValue(key: "nurse_latitude")
        nurse_longitude  = dic.getDoubleValue(key: "nurse_longitude")
        nurse_make_a_call  = dic.getStringValue(key: "nurse_make_a_call")
        nurse_makecall_countrycode  = dic.getStringValue(key: "nurse_makecall_countrycode")
        nurse_medical_board  = dic.getStringValue(key: "nurse_medical_board")
        nurse_medical_registration_number  = dic.getStringValue(key: "nurse_medical_registration_number")
        nurse_ns_id  = dic.getStringValue(key: "nurse_ns_id")
        nurse_occupation  = dic.getStringValue(key: "nurse_occupation")
        nurse_overview  = dic.getStringValue(key: "nurse_overview")
        nurse_password  = dic.getStringValue(key: "nurse_password")
        nurse_phone  = dic.getStringValue(key: "nurse_phone")
        nurse_practice_country_isocode  = dic.getStringValue(key: "nurse_practice_country_isocode")
        nurse_preferred_type  = dic.getStringValue(key: "nurse_preferred_type")
        nurse_prefix  = dic.getStringValue(key: "nurse_prefix")
        nurse_profile_image  = dic.getStringValue(key: "nurse_profile_image")
        nurse_province  = dic.getStringValue(key: "nurse_province")
        nurse_specialization  = dic.getStringValue(key: "nurse_specialization")
        nurse_state  = dic.getStringValue(key: "nurse_state")
        nurse_status  = dic.getStringValue(key: "nurse_status")
        nurse_unique_id  = dic.getStringValue(key: "nurse_unique_id")
        nurse_weight  = dic.getStringValue(key: "nurse_weight")
        nurse_working_day  = dic.getStringValue(key: "nurse_working_day")
        if let data = dic.value(forKey: "nurse_experience") as? NSArray{
            for obj in data{
                arryaDoctorExperience.append(DoctorExperience(dic: obj as! NSDictionary))
            }
        }
        if let data = dic.value(forKey: "socialMedia") as? NSArray{
            for obj in data{
                arryaSocialMedia.append(SocialMedia(dic: obj as! NSDictionary))
            }
        }
        if let datas = dic.value(forKey: "time_slot") as? NSDictionary{
            time_slot = TimeList(dic: datas)
        }
        if let data = dic.value(forKey: "nurse_practice_hours") as? NSDictionary{
            if let day = data.value(forKey: "friday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.friday.append(str!)
                }
            }
            if let day = data.value(forKey: "monday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.monday.append(str!)
                }
            }
            if let day = data.value(forKey: "saturday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.saturday.append(str!)
                }
            }
            if let day = data.value(forKey: "sunday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.sunday.append(str!)
                }
            }
            if let day = data.value(forKey: "thursday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.thursday.append(str!)
                }
            }
            if let day = data.value(forKey: "tuesday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.tuesday.append(str!)
                }
            }
            if let day = data.value(forKey: "wednesday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.wednesday.append(str!)
                }
            }
        }
    }
}
class SocialMedia{
    var smediaId : String!
    var smediaName : String!
    var smedia_doctorId : String!
    var smedia_icon : String!
    var smedia_url : String!
    init(dic : NSDictionary) {
        smediaId  = dic.getStringValue(key: "smediaId")
        smediaName  = dic.getStringValue(key: "smediaName")
        smedia_doctorId  = dic.getStringValue(key: "smedia_doctorId")
        smedia_icon  = dic.getStringValue(key: "smedia_icon")
        smedia_url  = dic.getStringValue(key: "smedia_url")
    }
}
class TimeSlot{
    
    var time : String!
    var time_status : String!
    var custeDate : Date!
    init(dic : NSDictionary) {
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd"
        time  = dic.getStringValue(key: "time")
        time_status  = dic.getStringValue(key: "time_status")
//        if formatter2.string(from: Date().adding(minutes: 0)) == formatter2.string(from: NextDate){
//            if isTimeEnable(strTime : time){
//                time_status  = dic.getStringValue(key: "time_status")
//            }else{
//                time_status  = "0"
//            }
//        }else{
//            time_status  = dic.getStringValue(key: "time_status")
//        }
        
        
        
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strSelectedDate = dateFormatter.string(from: Date().adding(minutes: 0))
        
        let strTemp = strSelectedDate + " " + time //  2021-07-02 11:25 PM
        let formatter4 = DateFormatter()
        formatter4.timeZone = TimeZone.current
        formatter4.dateFormat = "yyyy-MM-dd h:mm a"
        formatter4.locale = Locale(identifier:"en_US_POSIX")
        let dateSelected = formatter4.date(from: strTemp)!
        custeDate = dateSelected
        
    }
    
    
    func isTimeEnable(strTime : String) -> Bool{
        
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale(identifier:"en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let time = formatter.string(from: Date().adding(minutes: 0))  // 2021-07-02 11:50 PM
        
        let dateCurrent = formatter.date(from: time)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strSelectedDate = dateFormatter.string(from: Date())
        
        
        
        let strTemp = strSelectedDate + " " + strTime //  2021-07-02 11:25 PM
        
        let formatter4 = DateFormatter()
        formatter4.timeZone = TimeZone.current
        formatter4.locale = Locale(identifier:"en_US_POSIX")
        formatter4.dateFormat = "yyyy-MM-dd hh:mm a"
        let dateSelected = formatter4.date(from: strTemp)!
        
        if dateCurrent! > dateSelected {
            return false
        }else{
            return true
        }
        
        
    }
}
class TimeList{
    var arryaAfternoon = [TimeSlot]()
    var arryaEvening = [TimeSlot]()
    var arryaMorning = [TimeSlot]()
    var arryaNight = [TimeSlot]()
    init(dic : NSDictionary) {
        if let day = dic.value(forKey: "morning") as? NSArray{
            for obj in day{
                arryaMorning.append(TimeSlot(dic: obj as! NSDictionary))
            }
        }
        
        if let day = dic.value(forKey: "afternoon") as? NSArray{
            for obj in day{
                arryaAfternoon.append(TimeSlot(dic: obj as! NSDictionary))
            }
            
        }
        if let day = dic.value(forKey: "evening") as? NSArray{
            for obj in day{
                arryaEvening.append(TimeSlot(dic: obj as! NSDictionary))
            }
        }
        if let day = dic.value(forKey: "night") as? NSArray{
            for obj in day{
                arryaNight.append(TimeSlot(dic: obj as! NSDictionary))
            }
        }
    }
}
class Deliverylist {
    var id : String!
    var name : String!
    var price : String!
    init(dic : NSDictionary) {
        id  = dic.getStringValue(key: "id")
        name  = dic.getStringValue(key: "name")
        price  = dic.getStringValue(key: "price")
    }
}
class Pharmacies {
    var del_coll : String!
    var id : String!
    var miles : String!
    var mon_fri : String!
    var name : String!
    var saturday: String!
    var sunday : String!
    init(dic : NSDictionary) {
        del_coll  = dic.getStringValue(key: "del_coll")
        id  = dic.getStringValue(key: "id")
        miles  = dic.getStringValue(key: "miles")
        mon_fri  = dic.getStringValue(key: "mon_fri")
        name  = dic.getStringValue(key: "name")
        saturday  = dic.getStringValue(key: "saturday")
        sunday  = dic.getStringValue(key: "sunday")
    }
}
class ServiceProvideList{
    var category_id : String!
    var category_image : String!
    var category_ishasspeciality : String!
    var category_name : String!
    init(dic : NSDictionary) {
        category_id  = dic.getStringValue(key: "category_id")
        category_image  = dic.getStringValue(key: "category_image")
        category_ishasspeciality  = dic.getStringValue(key: "category_ishasspeciality")
        category_name  = dic.getStringValue(key: "category_name")
    }
}
class ServiceProvideListSpeciality{
    var specialityId : String!
    var category_image : String!
    var category_ishasspeciality : String!
    var category_name : String!
    init(dic : NSDictionary) {
        specialityId  = dic.getStringValue(key: "specialityId")
        category_image  = dic.getStringValue(key: "specialityImage")
        category_name  = dic.getStringValue(key: "specialityTitle")
    }
}
class ServiceDetails{
    var provider_Street : String!
    var provider_banner_image : String!
    var provider_category_id : String!
    var provider_city : String!
    var provider_country : String!
    var provider_countrycode : String!
    var provider_created_date : String!
    var provider_email : String!
    var provider_first_name : String!
    var provider_id : String!
    var provider_last_name : String!
    var provider_latitude : Double!
    var provider_longitude : Double!
    var provider_overview : String!
    var provider_phone : String!
    var provider_practice_country_isocode : String!
    var provider_profile_image : String!
    var provider_province : String!
    var provider_specialization : String!
    var provider_state : String!
    var provider_status : String!
    var friday = [String]()
    var monday = [String]()
    var saturday = [String]()
    var sunday = [String]()
    var thursday = [String]()
    var tuesday = [String]()
    var wednesday = [String]()
    init(dic : NSDictionary) {
        provider_Street   = dic.getStringValue(key: "provider_Street")
        provider_banner_image   = dic.getStringValue(key: "provider_banner_image")
        provider_category_id   = dic.getStringValue(key: "provider_category_id")
        provider_city   = dic.getStringValue(key: "provider_city")
        provider_country   = dic.getStringValue(key: "provider_country")
        provider_countrycode   = dic.getStringValue(key: "provider_countrycode")
        provider_created_date   = dic.getStringValue(key: "provider_created_date")
        provider_email   = dic.getStringValue(key: "provider_email")
        provider_first_name   = dic.getStringValue(key: "provider_first_name")
        provider_id   = dic.getStringValue(key: "provider_id")
        provider_last_name   = dic.getStringValue(key: "provider_last_name")
        provider_latitude   = dic.getDoubleValue(key: "provider_latitude")
        provider_longitude   = dic.getDoubleValue(key: "provider_longitude")
        provider_overview   = dic.getStringValue(key: "provider_overview")
        provider_phone   = dic.getStringValue(key: "provider_phone")
        provider_practice_country_isocode = dic.getStringValue(key: "provider_practice_country_isocode")
        provider_profile_image   = dic.getStringValue(key: "provider_profile_image")
        provider_province   = dic.getStringValue(key: "provider_province")
        provider_specialization   = dic.getStringValue(key: "provider_specialization")
        provider_state   = dic.getStringValue(key: "provider_state")
        provider_status   = dic.getStringValue(key: "provider_status")
        if let data = dic.value(forKey: "provider_practice_hours") as? NSDictionary{
            if let day = data.value(forKey: "friday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.friday.append(str!)
                }
            }
            if let day = data.value(forKey: "monday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.monday.append(str!)
                }
            }
            if let day = data.value(forKey: "saturday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.saturday.append(str!)
                }
            }
            if let day = data.value(forKey: "sunday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.sunday.append(str!)
                }
            }
            if let day = data.value(forKey: "thursday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.thursday.append(str!)
                }
            }
            if let day = data.value(forKey: "tuesday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.tuesday.append(str!)
                }
            }
            if let day = data.value(forKey: "wednesday") as? NSArray{
                for obj in day{
                    let str = obj as? String
                    self.wednesday.append(str!)
                }
            }
        }
    }
}
class AnswerLIst {
    var answerBy  : String!
    var answerDoctorCat  : String!
    var answerDoctorId  : String!
    var answerDoctorImage  : String!
    var answerDoctorName  : String!
    var answerId : String!
    var answer_title : String!
    var totalAnswer  : String!
    var answerFor : String!
    var answerRead : String!
    var answerTime : String!
    init(dic : NSDictionary) {
        answerBy   = dic.getStringValue(key: "answerBy")
        answerDoctorCat   = dic.getStringValue(key: "answerDoctorCat")
        answerDoctorId   = dic.getStringValue(key: "answerDoctorId")
        answerDoctorImage  = dic.getStringValue(key: "answerDoctorImage")
        answerDoctorName   = dic.getStringValue(key: "answerDoctorName")
        answerId  = dic.getStringValue(key: "answerId")
        answer_title  = dic.getStringValue(key: "answer_title")
        totalAnswer   = dic.getStringValue(key: "totalAnswer")
        answerFor   = dic.getStringValue(key: "answerFor")
        answerRead   = dic.getStringValue(key: "answerRead")
        answerTime   = dic.getStringValue(key: "answerTime")
    }
}
class Chat {
    var doctorId : String!
    var doctorImage : String!
    var doctorName : String!
    var doctorSpe : String!
    var arryaAnswer = [AnswerLIst]()
    init(dic : NSDictionary) {
        doctorId  = dic.getStringValue(key: "doctorId")
        doctorImage  = dic.getStringValue(key: "doctorImage")
        doctorName   = dic.getStringValue(key: "doctorName")
        doctorSpe   = dic.getStringValue(key: "doctorSpe")
        if let ans = dic.value(forKey: "answer") as? NSArray{
            for obj in ans{
                arryaAnswer.append(AnswerLIst(dic: obj as! NSDictionary))
            }
        }
    }
}
class QuestionList {
    var question  : String!
    var questionDate : String!
    var questionId : String!
    var arryaAnswer = [AnswerLIst]()
    init(dic : NSDictionary) {
        question  = dic.getStringValue(key: "question")
        questionDate  = dic.getStringValue(key: "questionDate")
        questionId   = dic.getStringValue(key: "questionId")
        if let ans = dic.value(forKey: "answer") as? NSArray{
            for obj in ans{
                arryaAnswer.append(AnswerLIst(dic: obj as! NSDictionary))
            }
        }
    }
}
class TrendingChart{
    var bp_id : String!
    var date : String!
    var dystolic : String!
    var heart_rate : String!
    var level : String!
    var mood : String!
    var note : String!
    var systolic: String!
    var time : String!
    var color : UIColor!
    var imageMood : UIImage!
    init(dic : NSDictionary) {
        bp_id   = dic.getStringValue(key: "bp_id")
        date   = dic.getStringValue(key: "date")
        dystolic   = dic.getStringValue(key: "dystolic")
        heart_rate   = dic.getStringValue(key: "heart_rate")
        level   = dic.getStringValue(key: "level")
        mood   = dic.getStringValue(key: "mood")
        note   = dic.getStringValue(key: "note")
        systolic   = dic.getStringValue(key: "systolic")
        time   = dic.getStringValue(key: "time")
        if level == "Optimal"{
            color = UIColor.colorWithRGB(r: 75, g: 192, b: 181)
        }else if level == "Normal"{
            color = UIColor.colorWithRGB(r: 79, g: 175, b: 112)
        }else if level == "Grade-1 Hypertension"{
            color = UIColor.colorWithRGB(r: 236, g: 200, b: 119)
        }else if level == "Grade-2 Hypertension"{
            color = UIColor.colorWithRGB(r: 238, g: 149, b: 139)
        }else if level == "Grade-3 Hypertension"{
            color = UIColor.colorWithRGB(r: 223, g: 121, b: 110)
        }else{
            color = UIColor.colorWithRGB(r: 228, g: 228, b: 142)
        }
        if mood == "Great"{
            imageMood = #imageLiteral(resourceName: "ic_great_orange")
        }else if mood == "Happy"{
            imageMood = #imageLiteral(resourceName: "ic_happy_orange")
        }else if mood == "Moderate"{
            imageMood = #imageLiteral(resourceName: "ic_moderate_orange")
        }else if mood == "Angry"{
            imageMood = #imageLiteral(resourceName: "ic_gift_orange")
        }else if mood == "Stressed"{
            imageMood = #imageLiteral(resourceName: "ic_stressed_orange")
        }else{
            imageMood = nil
        }
    }
}
class TemperChart{
    var mood : String!
    var temperature_id : String!
    var date : String!
    var temperature : String!
    var note : String!
    var time : String!
    var weight_id : String!
    var weight : String!
    var weightFor : String!
    var bp_id : String!
    var hr_id : String!
    var heart_rate : String!
    var color : UIColor!
    var imageMood : UIImage!
    var wekly : String!
    var Monthly : String!
    var Yearly : String!
    init(dic : NSDictionary) {
        weightFor   = dic.getStringValue(key: "weightFor")
        mood   = dic.getStringValue(key: "mood")
        temperature_id   = dic.getStringValue(key: "temperature_id")
        date   = dic.getStringValue(key: "date")
        temperature   = dic.getStringValue(key: "temperature")
        note   = dic.getStringValue(key: "note")
        time   = dic.getStringValue(key: "time")
        weight_id   = dic.getStringValue(key: "weight_id")
        weight   = dic.getStringValue(key: "weight")
        bp_id   = dic.getStringValue(key: "bp_id")
        hr_id   = dic.getStringValue(key: "hr_id")
        heart_rate   = dic.getStringValue(key: "heart_rate")
        if mood == "Great"{
            imageMood = #imageLiteral(resourceName: "ic_great_orange")
        }else if mood == "Happy"{
            imageMood = #imageLiteral(resourceName: "ic_happy_orange")
        }else if mood == "Moderate"{
            imageMood = #imageLiteral(resourceName: "ic_moderate_orange")
        }else if mood == "Angry"{
            imageMood = #imageLiteral(resourceName: "ic_gift_orange")
        }else if mood == "Stressed"{
            imageMood = #imageLiteral(resourceName: "ic_stressed_orange")
        }else{
            imageMood = nil
        }
        
    }
}
class BMList{
    var age : String!
    var bmi : Float!
    var bmi_id : String!
    var data : String!
    var date : String!
    var height : String!
    var note : String!
    var time : String!
    var weight : String!
    var color : UIColor!
    
    init(dic : NSDictionary) {
        age   = dic.getStringValue(key: "age")
        date   = dic.getStringValue(key: "date")
        bmi   = dic.getFloatValue(key: "bmi")
        note   = dic.getStringValue(key: "note")
        time   = dic.getStringValue(key: "time")
        bmi_id   = dic.getStringValue(key: "bmi_id")
        weight   = dic.getStringValue(key: "weight")
        height   = dic.getStringValue(key: "height")
        if bmi >= 35.5{
            color = UIColor.colorWithRGB(r: 239, g: 141, b: 138)
        }else if bmi >= 30{
            color = UIColor.colorWithRGB(r: 249, g: 217, b: 107)
        }else if bmi >= 25{
            color = UIColor.colorWithRGB(r: 245, g: 255, b: 189)
        }else if bmi >= 18.5{
            color = UIColor.colorWithRGB(r: 127, g: 230, b: 139)
        }else{
            color = UIColor.colorWithRGB(r: 223, g: 223, b: 223)
        }
    }
}
class BloodGlucose{
    var bloodGlucose_id : String!
    var bloodGlucose_meal : String!
    var blood_glucose : String!
    var blood_glucose_unit : String!
    var date : String!
    var time : String!
    var total_carbs : String!
    var total_carbs_unit : String!
    var total_energy : String!
    var total_energy_unit : String!
    var note : String!
    var patient_type : String!
    var color : UIColor!

    var arryaFood = [Food_data]()
    var arryaPhysical = [Physical_activity]()
    var arryaMedicine = [Medicine]()
    init(dic : NSDictionary) {
        bloodGlucose_id   = dic.getStringValue(key: "bloodGlucose_id")
        bloodGlucose_meal   = dic.getStringValue(key: "bloodGlucose_meal")
        blood_glucose   = dic.getStringValue(key: "blood_glucose")
        blood_glucose_unit   = dic.getStringValue(key: "blood_glucose_unit")
        date   = dic.getStringValue(key: "date")
        time   = dic.getStringValue(key: "time")
        total_carbs   = dic.getStringValue(key: "total_carbs")
        total_carbs_unit   = dic.getStringValue(key: "total_carbs_unit")
        total_energy   = dic.getStringValue(key: "total_energy")
        total_energy_unit   = dic.getStringValue(key: "total_energy_unit")
        note   = dic.getStringValue(key: "note")
        patient_type   = dic.getStringValue(key: "patient_type")
        if let ans = dic.value(forKey: "food_data") as? NSArray{
            for obj in ans{
                arryaFood.append(Food_data(dic: obj as! NSDictionary))
            }
        }
        if let physical = dic.value(forKey: "physical_activity") as? NSArray{
            for obj in physical{
                arryaPhysical.append(Physical_activity(dic: obj as! NSDictionary))
            }
        }
        if let medical = dic.value(forKey: "medicine") as? NSArray{
            for obj in medical{
                arryaMedicine.append(Medicine(dic: obj as! NSDictionary))
            }
        }
        color = setLevelColor(diabeticType: patient_type, bloodGlucose: blood_glucose, meal: bloodGlucose_meal, bloodGlucoseUnit: blood_glucose_unit)
        
    }
    
    func setLevelColor(diabeticType: String, bloodGlucose: String, meal: String, bloodGlucoseUnit: String) -> UIColor {
        var bg = Float(0.0)
        var isBeforeMeal = false
        var isAfterMeal = false
        var isFasting = false
        var isNinetyMinAfterMeal = false
        
        if String.validateStringValue(str: diabeticType) || String.validateStringValue(str: bloodGlucose) || String.validateStringValue(str: meal){
            return UIColor.init(hexString: "FF3336") // Red
        }
        
        if bloodGlucoseUnit != "mmol/L" {
            bg =  (Float(bloodGlucose)! * 0.0555)
            } else {
                bg = Float(bloodGlucose)!
            }
            if meal == "Before-Meal"{
                isBeforeMeal = true
            }
            if meal == "After-Meal"{
                isAfterMeal = true
            }
            if meal == "Fasting"{
                isFasting = true
            }
            if meal == "90 min after the meal"{
                isNinetyMinAfterMeal = true
            }
        
        if diabeticType == "1"{ // non- diabetic
            if isFasting{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 6.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            } else if isBeforeMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 5.9{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else if isAfterMeal || isNinetyMinAfterMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.8{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else{
                return UIColor.init(hexString: "ff01b26a") // Green
            }
            
        }else if diabeticType == "2"{ // Type2 diabetic
            
            if isFasting{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            } else if isBeforeMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else if isAfterMeal || isNinetyMinAfterMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 8.5{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else{
                return UIColor.init(hexString: "ff01b26a") // Green
            }
            
            
            
        }else if diabeticType == "3"{ // Type 1 diabetic
            
            if isFasting{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            } else if isBeforeMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else if isAfterMeal || isNinetyMinAfterMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 9.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else{
                return UIColor.init(hexString: "ff01b26a") // Green
            }
            
            
            
        }else if diabeticType == "4"{ // Type1 children diabetic
            if isFasting{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            } else if isBeforeMeal{
                if bg < 4.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 7.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else if isAfterMeal || isNinetyMinAfterMeal{
                if bg < 5.0{
                    return UIColor.init(hexString: "ff8400") // Orage
                }else if bg > 9.0{
                    return UIColor.init(hexString: "ff0000") // Red
                }else{
                    return UIColor.init(hexString: "ff01b26a") // Green
                }
            }else{
                return UIColor.init(hexString: "ff01b26a") // Green
            }
        }else{
            return UIColor.init(hexString: "ff01b26a") // Green
        }
        

    }
    
    
    
    
}
class Physical_activity{
    var act_url : String!
    var activity_name : String!
    var energy : String!
    var hrs: String!
    var intensity : String!
    var min : String!
    init(dic : NSDictionary) {
        
        act_url   = dic.getStringValue(key: "act_url")
        activity_name   = dic.getStringValue(key: "activity_name")
        energy   = dic.getStringValue(key: "energy")
        hrs   = dic.getStringValue(key: "hrs")
        intensity   = dic.getStringValue(key: "intensity")
        min   = dic.getStringValue(key: "min")
    }
}
class Medicine{
    var medicine_name : String!
    var medicine_type : String!
    init(dic : NSDictionary) {
        
        medicine_name   = dic.getStringValue(key: "medicine_name")
        medicine_type   = dic.getStringValue(key: "medicine_type")
        
    }
}
class Food_data{
    var carbs : String!
    var food_id : String!
    var food_item : String!
    var food_url : String!
    var serve_qnt : String!
    var serve_type : String!
    init(dic : NSDictionary) {
        carbs   = dic.getStringValue(key: "carbs")
        food_id   = dic.getStringValue(key: "food_id")
        food_item   = dic.getStringValue(key: "food_item")
        food_url   = dic.getStringValue(key: "food_url")
        serve_qnt   = dic.getStringValue(key: "serve_qnt")
        serve_type   = dic.getStringValue(key: "serve_type")
        
    }
}
class DiabeticList{
    var patient_type : String!
    var patient_type_id : String!
    init(dic : NSDictionary) {
        
        patient_type   = dic.getStringValue(key: "patient_type")
        patient_type_id   = dic.getStringValue(key: "patient_type_id")
        
    }
}

class ProblemList{
    var nProblemId : String!
    var vName : String!
    var vNote : String!
    init(dic : NSDictionary) {
        
        nProblemId   = dic.getStringValue(key: "id")
        vName   = dic.getStringValue(key: "vName")
        vNote   = dic.getStringValue(key: "vNote")
    }
}
class ProblemQuestionList{
    var id : String!
    var vDesc : String!
    var vDesc1 : String!
    var vQuestion : String!
    var isYes = false
    init(dic : NSDictionary) {
        id   = dic.getStringValue(key: "id")
        vDesc   = dic.getStringValue(key: "vDesc")
        vDesc1   = dic.getStringValue(key: "vDesc1")
        vQuestion   = dic.getStringValue(key: "vQuestion")
    }
}


class ParentList{
    var id : Int!
    var nDocStatus : Int!
    var nUserType : Int!
    var patient_first_name : String!
    var patient_last_name : String!
    var patient_profile_image : String!
    var isSected = false
    init(dic : NSDictionary) {
       
        id   = dic.getIntValue(key: "id")
        nDocStatus   = dic.getIntValue(key: "nDocStatus")
        nUserType   = dic.getIntValue(key: "nUserType")
        patient_first_name   = dic.getStringValue(key: "patient_first_name")
        patient_last_name   = dic.getStringValue(key: "patient_last_name")
        patient_profile_image   = dic.getStringValue(key: "patient_profile_image")
        
    }
}

class ReferralList{
    var DoctorName : String!
    var dCreatedDate : String!
    var id : String!
    var isActive : String!
    var nDoctorId : String!
    var vReferralFile : String!
    var vTreatmentandtestreconded : String!
    init(dic : NSDictionary) {
        DoctorName   = dic.getStringValue(key: "DoctorName")
        dCreatedDate   = dic.getStringValue(key: "dCreatedDate")
        id   = dic.getStringValue(key: "id")
        isActive   = dic.getStringValue(key: "isActive")
        nDoctorId   = dic.getStringValue(key: "nDoctorId")
        vReferralFile   = dic.getStringValue(key: "vReferralFile")
        vTreatmentandtestreconded   = dic.getStringValue(key: "vTreatmentandtestreconded")
    }
}

extension Date {
    
    func isEqual(to date: Date, toGranularity component: Calendar.Component, in calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: component)
    }
    
    func isInSameYear(as date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
    func isInSameMonth(as date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
    func isInSameWeek(as date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }
    
    func isInSameDay(as date: Date) -> Bool { Calendar.current.isDate(self, inSameDayAs: date) }
    
    var isInThisYear:  Bool { isInSameYear(as: Date()) }
    var isInThisMonth: Bool { isInSameMonth(as: Date()) }
    var isInThisWeek:  Bool { isInSameWeek(as: Date()) }
    
    var isInYesterday: Bool { Calendar.current.isDateInYesterday(self) }
    var isInToday:     Bool { Calendar.current.isDateInToday(self) }
    var isInTomorrow:  Bool { Calendar.current.isDateInTomorrow(self) }
    
    var isInTheFuture: Bool { self > Date() }
    var isInThePast:   Bool { self < Date() }
}
