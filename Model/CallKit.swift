//
//  CallKit.swift
//  CheckupHealth
//
//  Created by codExalters1 on 19/05/21.
//

import UIKit
import CallKit
import PushKit
import Sinch

class SINCallKitProvider: NSObject, CXProviderDelegate {
    
    static let shared: SINCallKitProvider = SINCallKitProvider()

    
    private weak var client: SINClient?
    private var provider: CXProvider?
    private var acDelegate: AudioContollerDelegate?
    private var calls: [String : SINCall?]?
    private var reportedCallKitCallIds: [UUID]!
    private var sinCallIdsToCallKitIds: [AnyHashable : Any]?
    private var callController: CXCallController?
    
    
    private override init() {
        super.init()
        acDelegate = AudioContollerDelegate()
        calls = [String : SINCall?]()
        reportedCallKitCallIds = []
        sinCallIdsToCallKitIds = [AnyHashable : Any]()
        let config = CXProviderConfiguration(localizedName: "Sinch")
        config.maximumCallGroups = 1
        config.maximumCallsPerCallGroup = 1
        callController = CXCallController()
        provider = CXProvider(configuration: config)
        provider!.setDelegate(self, queue: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(callDidEnd(_:)),name: NSNotification.Name.SINCallDidEnd,object: nil)
    }
    func setClient(_ client: SINClient?) {
//        if self.client!.audioController()!.delegate == acDelegate {
//            self.client!.audioController()!.delegate = nil
//        }
        self.client = client
        self.client!.audioController()!.delegate = acDelegate
    }
    
    
    
    func didReceivePush(withPayload payload: [AnyHashable : Any]?) {
        weak var notification = SINPushHelper.queryPushNotificationPayload(payload)
        if ((notification?.isCall()) != nil) {
            weak var callNotification = notification?.call()
            if applicationState == .active {
                print("Application state is UIApplicationStateActive, skipping reporting VoIP push to CallKit")
                return
            }
            if let callId = callNotification?.callId {
                if !hasReportedCall((sinCallIdsToCallKitIds![callId] as! UUID)) {
                            reportNewIncomingCall(withNotification: callNotification)
                        }
                    }
        }
    }
    
    
    func willReceiveIncomingCall(_ call: SINCall?) {
        add(call)
    }
    
    func startOutgoingCall(_ destination: String?) {
        let callId = UUID()
        let handle = CXHandle(type: .generic, value: destination ?? "")
        let initiateCallAction = CXStartCallAction(call: callId, handle: handle)
        initiateCallAction.isVideo = false
        let initOutgoingCall = CXTransaction(action: initiateCallAction)
        callController!.request(
                initOutgoingCall) { error in
                    if let error = error {
                        print("\(error)")
                    }
                }
    }
    
    func reportNewIncomingCall(withNotification notification: SINCallNotificationResult?) {
        assert((provider != nil), "Invalid parameter not satisfying: provider")
        let callId = UUID()
        addReportedCall(callId)
        if let callId1 = notification?.callId {
            sinCallIdsToCallKitIds![callId1] = callId
        }
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: notification?.remoteUserId ?? "")
        provider!.reportNewIncomingCall(
                with: callId,
                update: update) { [self] error in
                    if let error = error {
                        hangupCall(withId: notification?.callId)
                        print("\(error)")
                    }
                }
    }
    
    func addReportedCall(_ callId: UUID?) {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if let callId = callId {
                reportedCallKitCallIds.append(callId)
            }
        }
    }
    
    func hasReportedCall(_ callId: UUID?) -> Bool {
        if callId == nil {
            return false
        }

        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if let callId = callId {
                return reportedCallKitCallIds.contains(callId)
            }
            return false
        }
    }
    
    func add(_ call: SINCall?) {
        if let callId = call?.callId {
            print("[\(callId)] Adding call: \(callId)")
        }
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            calls![(call?.callId)!] = call
        }
    }
    
    func removeCall(withId callId: String?) {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            calls?.removeValue(forKey: callId!)
        }
    }
    func call(withId callId: String?) -> SINCall? {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            return calls![callId ?? ""] as? SINCall
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        weak var call = client!.call()!.callUser(withId: action.handle.value)
        add(call)
        if let callId = call?.callId {
            sinCallIdsToCallKitIds![callId] = action.callUUID
        }
        NotificationCenter.default.post(
                name: NSNotification.Name("SINOutgoingCallCreatedNotification"),
                object: nil,
                userInfo: [
                    "call": call as Any
                ])

        action.fulfill()
    }
    func activeCalls() -> [SINCall?]? {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            return calls!.values as? [SINCall?]
        }
    }
    
    func currentEstablishedCall() -> SINCall? {
        let calls = activeCalls()
        if calls.count > 0 && calls[0].state() == SINCallStateEstablished {
            return calls![0]
        } else {
            return nil
        }
    }
    func hangupCall(withId callId: String?) {
        call(withId: callId)!.hangup()
    }
    
    
    @objc func callDidEnd(_ notification: Notification?) {
        weak var call = notification?.userInfo?[SINCallKey] as? SINCall
        if let call = call {
            let callKitCallId = sinCallIdsToCallKitIds![call.callId]

            // incoming calls received when app is in foreground are not handled via CallKit
            if let callKitCallId = callKitCallId {
                provider!.reportCall(
                    with: callKitCallId as! UUID,
                    endedAt: call.details.endedTime,
                    reason: SINGetCallEndedReason(cause: call.details.endCause))
            }
        } else {
            print("WARNING: No Call was reported as ended on SINCallDidEndNotification")
        }
        if callExists(call?.callId) {
            print("callDidEnd, Removing call: \(call?.callId!)")
            removeCall(withId: call?.callId)
            sinCallIdsToCallKitIds!.removeValue(forKey: call?.callId)
        }

    }
    var applicationState: UIApplication.State {
        // Unsafe way of aquiring UIApplicationState on non-main thread / GCD queue
        // without triggering Main Thread Checker.
        if Thread.isMainThread {
            return UIApplication.shared.applicationState
        } else {
            var state: UIApplication.State
            DispatchQueue.main.sync(execute: {
                state = UIApplication.shared.applicationState
            })
            return state
        }
    }
    
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        if nil == client {
            print("WARNING: SINClient not assigned when audio session is activating (provider:didActivateAudioSession:)")
        }
        client!.call()!.provider(provider, didActivate: audioSession)
    }
    
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        client!.call()!.provider(provider, didActivate: audioSession)
    }
    
    func call(for action: CXCallAction?) -> SINCall? {
        let calls = sinCallIdsToCallKitIds.allKeys(for: action?.callUUID) as? [String]
        if (calls?.count ?? 0) == 1 {
            weak var call = self.call(withId: calls?[0])
            return call
        } else {
            if let callUUID = action?.callUUID {
                print("WARNING: No call found for (\(callUUID))")
            }
            return nil
        }
    }
    
    func callExists(_ callId: String?) -> Bool {
        var callIdAsString: String? = nil
            if callId is UUID {
                callIdAsString = callId
            } else if callId != nil {
                callIdAsString = callId
            } else {
                NSException(name: .invalidArgumentException, reason: "Unsupported callId typ", userInfo: nil) as! Error
            }
        for call in activeCalls()! {
            if .orderedSame == call?.callId.caseInsensitiveCompare(callIdAsString!) {
                    return true
                }
            }
            return false
    }
   
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        call(for: action)!.answer()
        client!.audioController().configureAudioSessionForCallKitCall()
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        call(for: action)!.hangup()
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        print("-[CXProviderDelegate performSetMutedCallAction:]")
        if acDelegate!.muted {
            client!.audioController().unmute()
        } else {
            client!.audioController().mute()
        }
        action.fulfill()
    }
    
 
    
    func providerDidReset(_ provider: CXProvider) {
        print("-[CXProviderDelegate providerDidReset:]")
    }
    func SINGetCallEndedReason(cause:SINCallEndCause) -> CXCallEndedReason {
        switch cause {
        case SINCallEndCause.error:
            return CXCallEndedReason.failed
        case SINCallEndCause.denied:
            return CXCallEndedReason.remoteEnded
        case SINCallEndCause.hungUp:
            return CXCallEndedReason.remoteEnded
        case SINCallEndCause.timeout:
            return CXCallEndedReason.unanswered
        case SINCallEndCause.canceled:
            return CXCallEndedReason.unanswered
        case SINCallEndCause.noAnswer:
            return CXCallEndedReason.unanswered
        case SINCallEndCause.otherDeviceAnswered:
            return CXCallEndedReason.unanswered
        default:
            break
        }
        return CXCallEndedReason.failed
    }

}


class AudioContollerDelegate: NSObject, SINAudioControllerDelegate {
    private(set) var muted = false
    
    func audioControllerMuted(_ audioController: SINAudioController?) {
        muted = true
    }

    func audioControllerUnmuted(_ audioController: SINAudioController?) {
        muted = false
    }
}
