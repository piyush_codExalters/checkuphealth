


import UIKit
import Foundation


let _screenSize     = UIScreen.main.bounds.size
let _screenFrame    = UIScreen.main.bounds
//let _facebookAppId  = "222853291417810"
	var viewScreenLock : ScreenView!
var viewVersion : CheckVersion!
var viewMaintence : CheckMaintenance!
let ADD_YOUR_GOOGLE_API_KEY = "AIzaSyCzizFxyBn9ayQsIaQKIuBvb6kGhthFXHI"
let ADD_CLIENTID_KEY = "758212646910-4nn2dtvccu3c0d25prntni6lrataaphk.apps.googleusercontent.com"

let _defaultCenter  = NotificationCenter.default
let _userDefault    = UserDefaults.standard
let _appDelegator   = UIApplication.shared.delegate! as! AppDelegate
let _parent   = UIApplication.shared.delegate! as! ParentViewController

let _application    = UIApplication.shared
let _appName = "CheckupHealth"
var vCurrency = "Bs."
var vPatientReferralCode = ""
var isSubscriptionDone = false
var data:Data?
var dLat : Double!
var dLng : Double!
var totalTime = Timer()
var isAudioRecordingGranted : Bool!

//let _fbBaseUrl      = "https://graph.facebook.com/v2.5/me/"
class CyberSourceData{
    var isCyberSourceProduction : String!
    var vCyberSourceOrgId : String!
    var vMerchantId : String!
    
}
struct Events{
 var WELCOME = "welcome"
 var SIGNUP = "signup"
 var OTP_VERIFICATION = "otp_verification"
 var SET_APP_LOCK = "set_app_lock"
 var ENTER_APP_LOCK_PIN = "enter_app_lock_pin"
 var REENTER_APP_LOCK_PIN = "reenter_app_lock_pin"
 var INTRODUCTION_SLIDER = "introduction_slider"
 var HOME = "home"
 var LOGIN = "login"
 var SELECT_PROFILE = "select_profile"
 var ADD_CHILD_PROFILE = "add_child_profile"
 var LINK_CHILD_PROFILE = "link_child_profile"
 var ADD_REGISTRATION_ADDRESS = "add_registration_address"
 var UPLOAD_VERIFICATION_DOCUMENTS = "upload_verification_documents"
 var SELECT_TIMESLOT = "select_timeslot"
 var SELECT_DOCTOR = "select_doctor"
 var SELECT_APPOINTMENT_TYPE = "select_appointment_type"
 var SELECT_CONSULTATION_ADDRESS = "select_consultation_address"
 var SELECT_PROBLEM_TYPE = "select_problem_type"
 var SELECT_PACKAGE = "select_package"
 var ADD_CARD_DETAILS = "add_card_details"
 var APPOITMENT_BOOKED = "appoitment_booked"
}
class UserDetail{
    
    var IsAdvantageHealthTaken : String!
    var dWalletAmount : String!
    var isEmailVerified : String!
    var isMobileVerified : String!
    var isOnboarding : String!
    var isSMSconsent : String!
    var isVerified : String!
    var nPin : String!
    var patient_BCN : String!
    var patient_NIN : String!
    var patient_blood : String!
    var patient_city : String!
    var patient_country : String!
    var patient_countrycode : String!
    var patient_dob : String!
    var patient_email_address : String!
    var patient_first_name : String!
    var patient_forgot : String!
    var patient_gender : String!
    var patient_height : String!
    var patient_id : String!
    var patient_last_name : String!
    var patient_location : String!
    var patient_occupation : String!
    var patient_phone : String!
    var patient_profile_image : String!
    var patient_state  : String!
    var patient_status : String!
    var patient_type : String!
    var patient_weight : String!
    var vOrganizationName : String!
    var nUserType = 0
    var patient_addressline1 : String!
    var patient_postcode : String!
    var patient_area : String!
    var patient_district : String!
    var patient_practice_email : String!
    var vApiKey : String!
    var vApiToken : String!
    var isBpOnboardingDone : String!
    
    var isHaveAlbuminuria : String!
    var isHaveDiabeticMellitus : String!
    var isHaveKidneyImpairment : String!
    var nAgeBracket : String!
    var nAlbuminCreatinineRatio : String!
    var nDiabeticMellitusType : String!
    var vAppLanguage = "es"
    init(dic :NSDictionary) {
        isHaveAlbuminuria = dic.getStringValue(key: "isHaveAlbuminuria")
        isHaveDiabeticMellitus = dic.getStringValue(key: "isHaveDiabeticMellitus")
        isHaveKidneyImpairment = dic.getStringValue(key: "isHaveKidneyImpairment")
        nAgeBracket = dic.getStringValue(key: "nAgeBracket")
        nAlbuminCreatinineRatio = dic.getStringValue(key: "nAlbuminCreatinineRatio")
        nDiabeticMellitusType = dic.getStringValue(key: "nDiabeticMellitusType")
        isBpOnboardingDone = dic.getStringValue(key: "isBpOnboardingDone")
        vApiKey = dic.getStringValue(key: "API_KEY")
        vApiToken = dic.getStringValue(key: "vApiToken")
        patient_practice_email = dic.getStringValue(key: "patient_practice_email")
        patient_district = dic.getStringValue(key: "patient_district")
        patient_addressline1 = dic.getStringValue(key: "patient_addressline1")
        patient_postcode = dic.getStringValue(key: "patient_postcode")
        patient_area = dic.getStringValue(key: "patient_area")
         IsAdvantageHealthTaken = dic.getStringValue(key: "IsAdvantageHealthTaken")
         dWalletAmount = dic.getStringValue(key: "dWalletAmount")
         isEmailVerified = dic.getStringValue(key: "isEmailVerified")
         isMobileVerified = dic.getStringValue(key: "isMobileVerified")
         isOnboarding = dic.getStringValue(key: "isOnboarding")
         isSMSconsent = dic.getStringValue(key: "isSMSconsent")
         isVerified = dic.getStringValue(key: "isVerified")
         nPin = dic.getStringValue(key: "nPin")
         patient_BCN = dic.getStringValue(key: "patient_BCN")
         patient_NIN = dic.getStringValue(key: "patient_NIN")
         patient_blood = dic.getStringValue(key: "patient_blood")
         patient_city = dic.getStringValue(key: "patient_city")
         patient_country = dic.getStringValue(key: "vCountryCode")
         patient_countrycode = dic.getStringValue(key: "patient_countrycode")
         patient_dob = dic.getStringValue(key: "patient_dob")
         patient_email_address = dic.getStringValue(key: "patient_email_address")
         patient_first_name = dic.getStringValue(key: "patient_first_name")
         patient_forgot = dic.getStringValue(key: "patient_forgot")
         patient_gender = dic.getStringValue(key: "patient_gender")
         patient_height = dic.getStringValue(key: "patient_height")
         patient_id = dic.getStringValue(key: "patient_id")
         patient_last_name = dic.getStringValue(key: "patient_last_name")
         patient_location = dic.getStringValue(key: "patient_location")
         patient_occupation = dic.getStringValue(key: "patient_occupation")
         patient_phone = dic.getStringValue(key: "patient_phone")
         patient_profile_image = dic.getStringValue(key: "patient_profile_image")
         patient_state  = dic.getStringValue(key: "patient_state")
         patient_status = dic.getStringValue(key: "patient_status")
         patient_type = dic.getStringValue(key: "patient_type")
         patient_weight = dic.getStringValue(key: "patient_weight")
         vOrganizationName = dic.getStringValue(key: "vOrganizationName")
    }
}
var _currentUser: UserDetail!
var _currency: Currency!
var _parameterGB : GB!
var _parameterZW : ZW!
let _fbMeUrl            = "https://graph.facebook.com/me"
let _facebookPermissionKeywordsLogin     = "id,first_name,last_name,gender,email,picture.height(500)"
let _facebookPermissionKeywordsFriend    = "friends{id,name,picture.height(200)}"
let _facebookPermissionKeywordsLink      = "id,name" // for link and unlink
let _twitterUrl         = "https://api.twitter.com/1.1/account/verify_credentials.json"
let _twitterFriendsUrl  = "https://api.twitter.com/1.1/friends/list.json"

// MARK: Paging Structure
struct LoadMore{
    var page: Int = 1
    var isLoading: Bool = false
    var limit: Int = 10
    var isAllLoaded = false
}

//Document access object
//let _docAccess: DocumentAccess = DocumentAccess()


//Place Holder image
let _placeImage = UIImage(named: "ic_Chamba_placeholder")
let _placeImageNoData = UIImage(named: "ic_Chamba_NODATAFound")
var isVersionControlPopupShownAlready = false

//User Related
var _userRegToken: String? = nil
var isAutoLogin = false
let _heighRatio : CGFloat = {
    let ratio = _screenSize.height/736
    return ratio
}()

let _widthRatio : CGFloat = {
    let ratio = _screenSize.width/414
    return ratio
}()
let bannerHeight : CGFloat = 120.00 * _widthRatio

let _serverFormatter: DateFormatter = {
    let df = DateFormatter()
    df.timeZone = TimeZone(secondsFromGMT: 3 * 60 * 60)
    df.dateFormat = "cccc, MMMM dd, yyyy h:m a"
    return df
}()

let _deviceFormatter: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "dd-MM-yyyy"
    return df
}()


// MARK: User Default keys
let mtlAppLanguageKey    = "mtlAppLanguageKey"

//MARK:- Observer Keys
let observerNoInternet       = "NoInternerObserver"
let observerForProfileUpdate = "UserProfileUpdateNotification"

// MARK: Important Enums
enum UIUserInterfaceIdiom : Int {
    case Unspecified
    case Phone
    case Pad
}

// MARK: Global Functions
// Comment in release mode
func kprint(items: Any...) {
    for item in items {
        print(item)
    }
}

// MARK: - Settings Version Maintenance & Localize text


func getAppVersion() -> String{
    if let _ = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
       let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
        return "Version - \(version)"
    }else{
        return ""
    }
}

func setAppSettingsBundleInformation(){
    if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
       let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
        _userDefault.set(build, forKey: "application_build")
        _userDefault.set(version, forKey: "application_version")
        _userDefault.synchronize()
    }
}
struct Device {
    // iDevice detection code
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH < 568
    static let IS_IPHONE_5 = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6 = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    
    
}
struct SingIn {
    var image : UIImage?
    var image1 : UIImage?
    var placeholder : String = ""
    var strkey: String = "0"
    var strkey1: String = ""
    var strkey2: String = ""
    var strcode: String = ""
    var isMobileVerified: Int!
    var isEmailVerified: Int!
    var title: String = ""
    var isSecure = false
    var isSecure1 = false
    var isColor = false
    var isCheck = false
    var isuserintracation = true
    var arryimage : [UIImage]?
    var arryString : [String]?
    var placeholder1 : String = ""
    var vvalaue : CGFloat = 0.0
    var strValue : String = ""
    var strValue1 : String = ""
    var strValue2 : String = ""
    var strValue3 : String = ""
    var strValue4 : String = ""
    var keybord = UIKeyboardType.default
    var keybord1 = UIKeyboardType.default
    var dLat : Double!
    var dLng : Double!
    var arryPlaceholder : [String]?
    var arryStrValue : [String]?
    var arryStrValue1 : [String]?
    
}
