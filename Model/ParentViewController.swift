


import UIKit
import PINRemoteImage
import NotificationBannerSwift
import Kingfisher
import AVFoundation
import FBSDKCoreKit
import FirebaseAnalytics

extension NSMutableAttributedString {
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        
    }
}
//import GoogleMobileAds
class ParentViewController: UIViewController, ScreenDelegate{
    
    func callNumber(phoneNumber: String) {
       guard let url = URL(string: "telprompt://\(phoneNumber)"),
           UIApplication.shared.canOpenURL(url) else {
           return
       }
       UIApplication.shared.open(url, options: [:], completionHandler: nil)
   }
    
    func strDay(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    func strMonth(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    func strYear(str : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let todaysDate = dateFormatter.string(from: str)
        return todaysDate
    }
    
   
    
   
    
    
    @IBOutlet weak var viewHeader: GradientView!
    
    
    // MARK: - Outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var myColView: UICollectionView!
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?
    @IBOutlet var verticalConstraints: [NSLayoutConstraint]?
    @IBOutlet weak var imageSideMenuProfile: UIImageView!
    @IBOutlet weak var imageHeader: UIImageView!
    
    var getEventName = Events()
    
    // MARK: - Actions
    @IBAction func parentBackAction(sender: UIButton!) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnBackStartClicked(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: InformationVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
            }
        }
        
    }
   
    
    
    func getShortName(strFullName : String) -> String{
        let stringInput = strFullName.uppercased()
        let stringInputArr = stringInput.components(separatedBy: " ")
        var stringNeed = ""
        
        for string in stringInputArr {
            if !String.validateStringValue(str: string){
                stringNeed = stringNeed + String(string.first!)
                if stringNeed.count == 2{
                    break
                }
            }
        }
        
        return stringNeed
    }
    
    
    @IBAction func btnSettingClicked(_ sender: Any) {
        let vclogin = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vclogin, animated: true)
    }
    
    //ScreenLockOpen
    
    
    func initLockScreen(){
        if let _ = viewScreenLock{
            viewScreenLock.removeFromSuperview()
        }
        viewScreenLock = ScreenView.instanceFromNib()
       
        viewScreenLock.delegate = self
        viewScreenLock.frame = CGRect(x: 0, y: 0, width: _screenSize.width, height: _screenSize.height)
        _appDelegator.window!.addSubview(viewScreenLock)
    }
   
    func initLockScreen2(){
       // showLockScreen()
        
        if (self.presentingViewController != nil){
            self.dismiss(animated: false, completion: nil)
            
        }else{
            _ = self.navigationController?.popViewController(animated: true)
            
        }
        if let _ = viewScreenLock{
            initLockScreen()
        }

    }
    
    func showLockScreen(){
        if let _ = viewScreenLock{
            viewScreenLock.isHidden = false
        }
    }
    func btnremove() {
        if let _ = viewScreenLock{
            viewScreenLock.isHidden = true
        }
    }
    func btnForgetPassword() {
        
        btnremove()
        DispatchQueue.main.async {
            let storyboard = UIStoryboard.init(name: "Home", bundle: Bundle.main)
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
            mainViewController.isPin = true
            let appDeleg = UIApplication.shared.delegate as! AppDelegate
            let root = appDeleg.window?.rootViewController as! UINavigationController
            root.pushViewController(mainViewController, animated: true)
//        let vclogin = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
//        vclogin.isPin = true
//        vclogin.modalPresentationStyle = .fullScreen
//        self.present(vclogin, animated: false, completion: nil)
        }
    }
    
    
    
    
    @IBAction func parentDismissAction(sender: UIButton!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSettingTap(_ sender: UIButton){
        let setVC = UIStoryboard(name: "Settings", bundle: nil).instantiateInitialViewController()!
        self.navigationController?.pushViewController(setVC, animated: true)
    }
    
    // MARK: - Variables for Pull to Referesh
    let referesh = UIRefreshControl()
    let pullToReferesh = UIControl()
    let viewLoader = UIView()
    var isRefereshing = false
    var imageViewLoader : UIImageView!
    
    // MARK: - iOS Life Cycle
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let _ = viewHeader{
            viewHeader.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintUpdate()
        NotificationCenter.default.addObserver(self, selector: #selector(self.titleCall), name: NSNotification.Name(rawValue: "AllLangunage"), object: nil)
        _appDelegator.parantView = self
        viewLoader.backgroundColor = UIColor.lightGray
        viewLoader.layer.cornerRadius = 4
        print("Allocated: \(self.classForCoder)")
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().isTranslucent = true
    }
    //    @objc func updateData(){
    //       if ((_userDefault.value(forKey: "userInfo") as? NSDictionary) != nil){
    //            imageSideMenuProfile.kf.setImage(with: URL(string:userData.vImagePath), placeholder: #imageLiteral(resourceName: "ic_signup_profile_ph-1"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
    //            }
    //        }
    //    }
    
    
    deinit{
        _defaultCenter.removeObserver(self)
        print("Deallocated: \(self.classForCoder)")
    }
    func getTripStatus(strID : Int) -> String{
        if strID == 0{
            return "Pending"
        }else if strID == 1{
            return "Onging"
        }else if strID == 2{
            return "Completed"
        }else{
            return "Canceled"
        }
    }
    func showAlert(title : String, msgString : String, Ohk : String, cancal : String,block : @escaping ()->()){
        let alert = UIAlertController(title: title, message: msgString, preferredStyle: .alert)
        let action = UIAlertAction(title: Ohk, style: .default) { (action) in
            block()
        }
        let action2 = UIAlertAction(title: cancal, style: .cancel) { (action) in
        }
        alert.addAction(action)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func titleCall(){
        
        
    }
    
    
    func StatusStyleChange(isChange:Bool){
        if isChange{
            UIApplication.shared.statusBarStyle = .lightContent
        }else{
            UIApplication.shared.statusBarStyle = .default
        }
    }
    func CurrentTime() -> String{
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = formatter.string(from: now)
        return dateString
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    // Set Default UI
    
    @IBAction func btnMenuClick(_ sender: Any) {
        // _appDelegator.showLeftMenu()
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy, hh:mm a"
        if date == nil{
            return  "0000/00/00"
        }else{
            return  dateFormatter.string(from: date!)
        }
    }
    func convertDateFormater1(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy, hh:mm a"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return  dateFormatter.string(from: date!)
    }
    func convertDateFormater2(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    // This will update constaints and shrunk it as device screen goes lower.
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
        if let vConst = verticalConstraints {
            for const in vConst {
                let v1 = const.constant
                let v2 = v1 * _heighRatio
                const.constant = v2
            }
        }
    }
    
    // MARK: - TextFields
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Lazy Variables
    lazy internal var activityIndicator : UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        act.color = UIColor.white
        return act
    }()
    
    
    
    lazy internal var centralActivityIndicator : UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        return act
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.lightGray
        return refreshControl
    }()
}

//MARK:- Uitility Methods
extension ParentViewController{
    
    // Observer of noInterNet
    func noInterNet(noti: NSNotification){
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    // Set Title label
    
    
    func tableViewCell(index: Int) -> UITableViewCell {
        let cell = tableView.cellForRow(at: NSIndexPath(row: index, section: 0) as IndexPath)
        return cell!
    }
    
    func scrollToIndex(index: Int, animate: Bool = false){
        let indexPath = NSIndexPath(row: index, section: 0)
        tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.none, animated: animate)
    }
    
    // Show API Error
}

func getServiceName(strID : String) -> String{
    if strID == "1"{
        return "Delivery"
    }else if strID == "2"{
        return "Pick Up"
    }else{
        return "Event catering"
    }
}

//MARK: - TableView


//MARK:- Activity Indicator
extension ParentViewController{
    
    // This will show and hide spinner. In middle of container View
    // You can pass any view here, Spinner will be placed there runtime and removed on hide.
    func showSpinnerIn(container: UIView, control: UIButton, isCenter: Bool) {
        container.addSubview(activityIndicator)
        let xConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint])
        activityIndicator.alpha = 0.0
        view.layoutIfNeeded()
        self.view.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.activityIndicator.alpha = 1.0
            if isCenter{
                control.alpha = 0.0
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func changeValue(response:NSDictionary) -> NSDictionary{
        let blankString = ""
            let myMutableDict: NSMutableDictionary = NSMutableDictionary(dictionary: response)

            var data = try? JSONSerialization.data(withJSONObject:myMutableDict, options: JSONSerialization.WritingOptions.prettyPrinted)

            var strData = NSString.init(data: data!, encoding:String.Encoding.utf8.rawValue)
            strData = strData?.replacingOccurrences(of: "<NULL>", with: blankString) as NSString?
            strData = strData?.replacingOccurrences(of: "<null>", with: blankString) as NSString?
            strData = strData?.replacingOccurrences(of: "<Null>", with: blankString) as NSString?
            strData = strData?.replacingOccurrences(of: "nil", with: blankString) as NSString?
            strData = strData?.replacingOccurrences(of: "Nil", with: blankString) as NSString?
            strData = strData?.replacingOccurrences(of: "NULL", with: blankString) as NSString?
            strData = strData?.replacingOccurrences(of: "null", with: blankString) as NSString?
            
            data = strData?.data(using: String.Encoding.utf8.rawValue)!
            var dictionary = NSDictionary()
            
            do
            {

                dictionary = try JSONSerialization.jsonObject(with: data! , options: JSONSerialization.ReadingOptions()) as! NSDictionary
                
            } catch {
                print(error.localizedDescription)
            }
            
            return dictionary as NSDictionary
    }
    func getSaveData(dataname : NSDictionary) -> NSDictionary {
        let temp = dataname.mutableCopy() as! NSMutableDictionary
        temp["patient_height"] = "\(_currentUser.patient_height!)"
        temp["patient_first_name"] = "\(_currentUser.patient_first_name!)"
        temp["patient_last_name"] = "\(_currentUser.patient_last_name!)"
        temp["patient_profile_image"] = "\(_currentUser.patient_profile_image!)"
        temp["IsAdvantageHealthTaken"] = "\(_currentUser.IsAdvantageHealthTaken!)"
        temp["isEmailVerified"] = "\(_currentUser.isEmailVerified!)"
        temp["isMobileVerified"] = "\(_currentUser.isMobileVerified!)"
        temp["isOnboarding"] = "\(_currentUser.isOnboarding!)"
        temp["isSMSconsent"] = "\(_currentUser.isSMSconsent!)"
        temp["isVerified"] = "\(_currentUser.isVerified!)"
        temp["nPin"] = "\(_currentUser.nPin!)"
        temp["patient_countrycode"] = "\(_currentUser.patient_countrycode!)"
        temp["patient_email_address"] = "\(_currentUser.patient_email_address!)"
        temp["patient_forgot"] = "\(_currentUser.patient_forgot!)"
        temp["patient_id"] = "\(_currentUser.patient_id!)"
        temp["patient_status"] = "\(_currentUser.patient_status!)"
        temp["patient_type"] = "\(_currentUser.patient_type!)"
        temp["dWalletAmount"] = "\(_currentUser.dWalletAmount!)"
        temp["patient_phone"] = "\(_currentUser.patient_phone!)"
        temp["patient_dob"] = "\(_currentUser.patient_dob!)"
        temp["patient_occupation"] = "\(_currentUser.patient_occupation!)"
        temp["patient_blood"] = "\(_currentUser.patient_blood!)"
        temp["patient_weight"] = "\(_currentUser.patient_weight!)"
        temp["patient_location"] = "\(_currentUser.patient_location!)"
        temp["vOrganizationName"] = "\(_currentUser.vOrganizationName!)"
        temp["patient_state"] = "\(_currentUser.patient_state!)"
        temp["patient_NIN"] = "\(_currentUser.patient_NIN!)"
        temp["patient_BCN"] = "\(_currentUser.patient_BCN!)"
        temp["patient_country"] = "\(_currentUser.patient_country!)"
        temp["patient_city"] = "\(_currentUser.patient_city!)"
        temp["patient_gender"] = "\(_currentUser.patient_gender!)"
        temp["patient_district"] = "\(_currentUser.patient_district!)"
        temp["patient_addressline1"] = "\(_currentUser.patient_addressline1!)"
        temp["patient_postcode"] = "\(_currentUser.patient_postcode!)"
        temp["patient_area"] = "\(_currentUser.patient_area!)"
        temp["patient_practice_email"] = "\(_currentUser.patient_practice_email!)"
        temp["API_KEY"] = "\(_currentUser.vApiKey!)"
        temp["vApiToken"] = "\(_currentUser.vApiToken!)"
        temp["isBpOnboardingDone"] = "\(_currentUser.isBpOnboardingDone!)"
        temp["isHaveAlbuminuria"] = "\(_currentUser.isHaveAlbuminuria!)"
        temp["isHaveDiabeticMellitus"] = "\(_currentUser.isHaveDiabeticMellitus!)"
        temp["isHaveKidneyImpairment"] = "\(_currentUser.isHaveKidneyImpairment!)"
        temp["nAgeBracket"] = "\(_currentUser.nAgeBracket!)"
        temp["nAlbuminCreatinineRatio"] = "\(_currentUser.nAlbuminCreatinineRatio!)"
        temp["nDiabeticMellitusType"] = "\(_currentUser.nDiabeticMellitusType!)"
        
        
        _userDefault.set(_currentUser.patient_country, forKey: "Language")
        _userDefault.synchronize()
        return temp
    }
  
    
    func hideSpinnerIn(container: UIView, control: UIButton) {
        self.view.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
        control.isSelected = false
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.activityIndicator.alpha = 0.0
            control.alpha = 1.0
        }
    }
    
    func showCentralSpinner1()
    {
        self.view.addSubview(imageViewLoader)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideCentralSpinner1(){
        imageViewLoader.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    
    func showCentralSpinner() {
        self.view.addSubview(viewLoader)
        self.view.addSubview(centralActivityIndicator)
        setConstraintsToLoderView()
        let xConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: centralActivityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        centralActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint])
        centralActivityIndicator.alpha = 0.0
        viewLoader.alpha = 0.0
        self.view.layoutIfNeeded()
        self.view.isUserInteractionEnabled = false
        _appDelegator.window?.isUserInteractionEnabled = false
        centralActivityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.centralActivityIndicator.alpha = 1.0
            self.viewLoader.alpha = 1.0
        }
    }
    // Set Default UI
    func whatappCall(phoneNumber: String){
        let phoneNumber =  "\(phoneNumber)" // you need to change this number
        let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
        if UIApplication.shared.canOpenURL(appURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL)
            }
        } else {
            // WhatsApp is not installed
        }
    }
    func setConstraintsToLoderView() {
        let xConstraint = NSLayoutConstraint(item: viewLoader, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: viewLoader, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let hei = NSLayoutConstraint(item: viewLoader, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 70)
        let wid = NSLayoutConstraint(item: viewLoader, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 70)
        viewLoader.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([xConstraint, yConstraint, hei, wid])
    }
    
    func hideCentralSpinner() {
        self.view.isUserInteractionEnabled = true
        _appDelegator.window?.isUserInteractionEnabled = true
        centralActivityIndicator.stopAnimating()
        viewLoader.removeFromSuperview()
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.centralActivityIndicator.alpha = 0.0
        }
    }
    
    
    //    func showAlert2(title : String, msgString : NSDictionary,cancal : String,block : @escaping ()->()){
    //        let msg = msgString.getStringValue(key: "message")
    //
    //        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    //
    //        let action2 = UIAlertAction(title: cancal, style: .cancel) { (action) in
    //        }
    //        alert.addAction(action2)
    //        self.present(alert, animated: true, completion: nil)
    //    }
    func showAlert1(title : String, msgString : String, Ohk : String,block : @escaping ()->()){
        let alert = UIAlertController(title: title, message: msgString, preferredStyle: .alert)
        let action = UIAlertAction(title: Ohk, style: .default) { (action) in
            block()
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    func imageStatus(Status : Int) -> UIImage{
        var image : UIImage!
        if Status == 1 {
            image = #imageLiteral(resourceName: "pending")
        }else if Status == 2 {
            image = #imageLiteral(resourceName: "accepted")
        }else{
            image = #imageLiteral(resourceName: "rejected")
        }
        return image
    }
    func QRcodeisHidden(Status : Int) -> Bool{
        var isHidden : Bool!
        if Status == 1 {
            isHidden = true
        }else if Status == 2 {
            isHidden = false
        }else{
            isHidden = true
        }
        return isHidden
    }
    
}
extension NSMutableAttributedString {
    @discardableResult func forgetpassword(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.Maven_pro_Medium(size: 17),
            NSAttributedString.Key.foregroundColor : UIColor.colorchaupbule(),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func sinup(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.Maven_pro_Medium(size: 17),
            NSAttributedString.Key.foregroundColor : UIColor.colorchaupGray(),
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func viewAll(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.Maven_pro_Medium(size: 14),
            NSAttributedString.Key.foregroundColor : UIColor.colorchaupbule(),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func viewversion(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func unlink(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.Maven_pro_Medium(size: 14),
            NSAttributedString.Key.foregroundColor : UIColor.colorchaupOrange(),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func Open(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.Maven_pro_Medium(size: 14),
            NSAttributedString.Key.foregroundColor : UIColor.black,
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func Cloase(_ text:String) -> NSMutableAttributedString {
        let attrs : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.Maven_pro_Medium(size: 14),
            NSAttributedString.Key.foregroundColor : UIColor.colorchaupGray(),
            
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
}
extension ParentViewController{
    func showSucessMessage(message : String){
        
        let banner = GrowingNotificationBanner(title: "",
                                                    subtitle: message,
                                                    style: .success)
       // banner.delegate = self
        
       // let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: message), style: .success)
        banner.duration = 1
        
        banner.show(queuePosition: .front, bannerPosition: .top, on: _appDelegator.window?.rootViewController)
    }
    func showFailMessage(message : String){
        let banner = GrowingNotificationBanner(title: "",
                                                    subtitle: message,
                                                    style: .danger)
     //   let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: message), style: .danger)
        banner.duration = 3
        banner.show(queuePosition: .front, bannerPosition: .top, on: _appDelegator.window?.rootViewController)
        
    }
    func showFailMessage1(message : String){
        let banner = GrowingNotificationBanner(title: "",
                                                    subtitle: message,
                                                    style: .danger)
        
        banner.duration = 3
        
        banner.show(queuePosition: .front, bannerPosition: .top, on: _appDelegator.window?.rootViewController)
        
    }
    func showSomethingWrong(){
        let banner = GrowingNotificationBanner(title: "",
                                                    subtitle: "Something went wrong.",
                                                    style: .danger)
       // let banner = StatusBarNotificationBanner(attributedTitle: NSAttributedString(string: "Something went wrong."), style: .danger)
        banner.duration = 2
        banner.show(queuePosition: .front, bannerPosition: .top, on: _appDelegator.window?.rootViewController)
    }
    func showResponseMessage(dict : NSDictionary){
        var msg = dict.getStringValue(key: "\(_appName)")
        if msg == ""{
            msg = dict.getStringValue(key: "message")
        }
        if 1 == dict.getIntValue(key: "success"){
            self.showSucessMessage(message: msg)
        }else if 1 == dict.getIntValue(key: "success"){
            self.showFailMessage(message: msg)
        }else{
            self.showFailMessage(message: msg)
        }
    }
    
    func getFirebaseAnalytics(screenName : String,isPatient_id : Bool)  {
        let uuid = UUID().uuidString
        var dic = [String:Any]()
        dic["deviceId"] = uuid
        if isPatient_id{
            dic["patient_id"] = _currentUser.patient_id
            dic["patientName"] = _currentUser.patient_first_name! + " " + _currentUser.patient_last_name
            dic["patientEmail"] = _currentUser.patient_email_address
            Analytics.logEvent(screenName, parameters: dic)
        }else{
            Analytics.logEvent(screenName, parameters: dic)
        }
    }
}

extension ParentViewController{
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d-MMM-yyyy"
        if dt == nil{
            return ""
        }else{
            return dateFormatter.string(from: dt!)
        }
    }
    func UTCToLocalSmart(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
       // dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d MMM yyyy h:mm a"
        if dt == nil{
            return ""
        }else{
            return dateFormatter.string(from: dt!)
        }
    }
    func UTCToLocalPro(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if dt == nil{
            return ""
        }else{
            return dateFormatter.string(from: dt!)
        }
    }
    
    func UTCToLocal1(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d MMM, yyyy hh:mm a"
        if dt == nil{
            return ""
        }else{
            return dateFormatter.string(from: dt!)
        }
    }
    func UTCToLocalDay(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd"
        if dt == nil{
            return ""
        }else{
            return dateFormatter.string(from: dt!)
        }
    }
    func UTCToLocalTimeProChat(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy, hh:mm a"
        if dt == nil{
            return ""
        }else{
            return dateFormatter.string(from: dt!)
        }
    }
    func localToUTC(date:String ,  formater : String = "MM/dd/yyyy hh:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formater
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: dt!)
    }
    func localToUTC1(date:String ,  formater : String = "MM/dd/yyyy hh:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formater
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let str = dateFormatter.string(from: dt!)
        let strf = str.replacingOccurrences(of: "18:30:00", with: "")
        return strf + "00:00:00"
    }
    func getTimeComponentString(olderDate older: Date,newerDate newer: Date) -> (String?)  {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        
        let componentsLeftTime = Calendar.current.dateComponents([.minute , .hour , .day,.month, .weekOfMonth,.year], from: older, to: newer)
        
        let year = componentsLeftTime.year ?? 0
        if  year > 0 {
            formatter.allowedUnits = [.year]
            return formatter.string(from: older, to: newer)
        }
        
        
        
        let month = componentsLeftTime.month ?? 0
        if  month > 0 {
            formatter.allowedUnits = [.month]
            return formatter.string(from: older, to: newer)
        }
        
        let weekOfMonth = componentsLeftTime.weekOfMonth ?? 0
        if  weekOfMonth > 0 {
            formatter.allowedUnits = [.weekOfMonth]
            return formatter.string(from: older, to: newer)
        }
        
        let day = componentsLeftTime.day ?? 0
        if  day > 0 {
            formatter.allowedUnits = [.day]
            return formatter.string(from: older, to: newer)
        }
        
        let hour = componentsLeftTime.hour ?? 0
        if  hour > 0 {
            formatter.allowedUnits = [.hour]
            return formatter.string(from: older, to: newer)
        }
        
        let minute = componentsLeftTime.minute ?? 0
        if  minute > 0 {
            formatter.allowedUnits = [.minute]
            return formatter.string(from: older, to: newer) ?? ""
        }
        
        return nil
    }
    
  
    
    
}
extension UIView{
    // For insert layer in Foreground
    func addBlackGradientLayerInForeground(frame: CGRect, colors:[UIColor]){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors.map{$0.cgColor}
        self.layer.addSublayer(gradient)
    }
    
    // For insert layer in background
    func addBlackGradientLayerInBackground(frame: CGRect, colors:[UIColor]){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors.map{$0.cgColor}
        self.layer.insertSublayer(gradient, at: 0)
    }
}
extension ParentViewController{
    func setTableBackGround(imgName : String) -> UIView{
        let view = UIView(frame: tableView.frame)
        let imgView = UIImageView(frame: CGRect(x: _screenSize.width/4, y: _screenSize.height/4, width: _screenSize.width/2, height: _screenSize.width/2))
        imgView.contentMode = .scaleAspectFit
        let image = UIImage(named: imgName)
        imgView.image = image
        view.addSubview(imgView)
        return view
    }
    
}

extension ParentViewController{
    //    func setTableBackGround(imgName : String) -> UIView{
    //        let view = UIView(frame: tableView.frame)
    //        let imgView = UIImageView(frame: CGRect(x: screenSize.width/4, y: screenSize.height/4, width: screenSize.width/2, height: screenSize.width/2))
    //        imgView.contentMode = .scaleAspectFit
    //        let image = UIImage(named: imgName)
    //        imgView.image = image
    //        view.addSubview(imgView)
    //        return view
    //    }
    
}
extension ParentViewController{
    func setPlaceHolder(strMessage : String, image : UIImage? = nil) -> UIView{
        let viewBG = UIView()

        let lable = JPWidthLabel(frame: CGRect(x: 0, y: self.view.center.y - 100, width: _screenSize.width, height: 100))
        lable.numberOfLines = 0
        lable.text = strMessage
        lable.textAlignment = .center
        lable.textColor = .black
        viewBG.addSubview(lable)
        return viewBG
    }
}
extension ParentViewController {
  
    
    func Language() -> Bool {
        let Lang = _userDefault.value(forKey: "Language") as? String
        if Lang == nil || Lang == "ZW"{
            return true
        }else{
            return false
        }
    }
    func isWorkingCountryUK() -> Bool {
        let WorkingCountryUK = _userDefault.value(forKey: "Language") as? String
        if WorkingCountryUK == nil || WorkingCountryUK == "ZW"{
            return false
        }else{
            return true
        }
    }
    func CheckupCurrey() -> String {
        if isWorkingCountryUK(){
           return "GBP"
        }else{
           return "USD"
        }
    }
}




extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
                                                        locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
extension ParentViewController{
    
    

    
    
    
    func check_permistion_audio()  -> String
    {
        var type = ""
        switch AVAudioSession.sharedInstance().recordPermission
        {
        case AVAudioSession.RecordPermission.granted:
            type = "1"
            print("Permission granted")
        case AVAudioSession.RecordPermission.denied:
            print("Pemission denied")
            type = "2"
        case AVAudioSession.RecordPermission.undetermined:
            print("Request permission here")
            
          //  type = "3"
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
                if granted {
                    type = "1"
                } else {
                    print("Pemission denied")
                    type = "2"
                }
            }
            )
        @unknown default:
            break
        }
        return type
    }
    
    func goto_seeting()
    {
        let alertController = UIAlertController (title: _appName, message: "Go to Settings?", preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        UIApplication.shared.windows.last?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}
