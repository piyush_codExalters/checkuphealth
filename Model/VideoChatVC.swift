//
//  VideoChatVC.swift
//  CheckupHealth
//
//  Created by codExalters1 on 27/07/21.
//

import UIKit
import AgoraRtcKit

let AppID: String = "91d4521d03454ee6ba97cf85e15918fc"
var Token: String? = ""
var vAccessTokenForAgoraRecording: String? = ""
var strAppointmentIdGlobal: String? = ""
var userrcodingAgora : String? = ""

class VideoChatVC: ParentViewController {

    @IBOutlet weak var localContainer: UIView!
    @IBOutlet weak var remoteContainer: UIView!
    @IBOutlet weak var remoteVideoMutedIndicator: UIImageView!
    @IBOutlet weak var localVideoMutedIndicator: UIView!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var lbName: LabelSemiBold!
    @IBOutlet weak var lblTimer: LabelSemiBold!
    @IBOutlet weak var lblConection: LabelSemiBold!
    @IBOutlet weak var imagenewProfile: UIImageView!
    @IBOutlet weak var hightLay: NSLayoutConstraint!
    
    
    
    var agoraKit: AgoraRtcEngineKit!
    var localVideo: AgoraRtcVideoCanvas?
    var remoteVideo: AgoraRtcVideoCanvas?
    var chanal = ""
    var strName = ""
    var isVideo = false
    var duration : Timer?
    var timeCount = 0
    var imageProfile = ""
    var resourceID = ""
    var sid = ""

    
    var isRemoteVideoRender: Bool = true {
        didSet {
            if let it = localVideo, let view = it.view {
                if view.superview == localContainer {
                    remoteVideoMutedIndicator.isHidden = isRemoteVideoRender
                    remoteContainer.isHidden = !isRemoteVideoRender
                } else if view.superview == remoteContainer {
                    localVideoMutedIndicator.isHidden = isRemoteVideoRender
                }
            }
        }
    }
    
    var isLocalVideoRender: Bool = false {
        didSet {
            if let it = localVideo, let view = it.view {
                if view.superview == localContainer {
                    localVideoMutedIndicator.isHidden = isLocalVideoRender
                } else if view.superview == remoteContainer {
                    remoteVideoMutedIndicator.isHidden = isLocalVideoRender
                }
            }
        }
    }
    
    var isStartCalling: Bool = true {
        didSet {
            if isStartCalling {
                micButton.isSelected = false
            }
            micButton.isHidden = !isStartCalling
            cameraButton.isHidden = !isStartCalling
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        remoteVideoMutedIndicator.makeRound1()
        imagenewProfile.kf.setImage(with: URL(string:imageProfile), placeholder: #imageLiteral(resourceName: "ic_user_placeholder"), options: nil, progressBlock: nil) { (img, error, catchType, url) in
        }
        
        lbName.text = strName
        lblConection.text = "Contacting...".uppercased()
        if isVideo{
            hightLay.constant = 0
            imagenewProfile.isHidden = true
            cameraButton.setImage(#imageLiteral(resourceName: "ic_flipcamera_withbg"), for: .normal)
            cameraButton.setImage(#imageLiteral(resourceName: "ic_flipcamera_withbg"), for: .selected)
            
            micButton.setImage(#imageLiteral(resourceName: "ic_micon.png"), for: .normal)
            micButton.setImage(#imageLiteral(resourceName: "ic_micoff"), for: .selected)
        }else{
            micButton.setImage(#imageLiteral(resourceName: "ic_micon.png"), for: .normal)
            micButton.setImage(#imageLiteral(resourceName: "ic_micoff"), for: .selected)
            hightLay.constant = 100
            cameraButton.isHidden = true
            imagenewProfile.isHidden = false
        }
        initializeAgoraEngine()
        setupVideo()
        setupLocalVideo()
        joinChannel()
    }
    func initializeAgoraEngine() {
        // init AgoraRtcEngineKit
        
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: AppID, delegate: self)
    }

    func setupVideo() {
        // In simple use cases, we only need to enable video capturing
        // and rendering once at the initialization step.
        // Note: audio recording and playing is enabled by default.
        if isVideo{
            agoraKit.enableVideo()
            //AgoraVideoDimension640x360
            // Set video configuration
            // Please go to this page for detailed explanation
            //https://docs.agora.io/cn/Voice/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_rtc_engine.html#af5f4de754e2c1f493096641c5c5c1d8f
            agoraKit.setVideoEncoderConfiguration(AgoraVideoEncoderConfiguration(size: AgoraVideoDimension960x720,
                                                                                 frameRate: .fps15,
                                                                                 bitrate: AgoraVideoBitrateStandard,
                                                                                 orientationMode: .adaptative))
        }else{
            agoraKit.disableVideo()
        }
       
    }
    
    func setupLocalVideo() {
        // This is used to set a local preview.
        // The steps setting local and remote view are very similar.
        // But note that if the local user do not have a uid or do
        // not care what the uid is, he can set his uid as ZERO.
        // Our server will assign one and return the uid via the block
        // callback (joinSuccessBlock) after
        // joining the channel successfully.
        if isVideo{
            let view = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: localContainer.frame.size))
            localVideo = AgoraRtcVideoCanvas()
            localVideo!.view = view
            localVideo!.renderMode = .hidden
            localVideo!.uid = 0
            localContainer.addSubview(localVideo!.view!)
            agoraKit.setupLocalVideo(localVideo)
            agoraKit.startPreview()
           
        }else{
            localVideoMutedIndicator.isHidden = true
        }
    }
  
    
    func joinChannel() {
        // Set audio route to speaker
        agoraKit.setDefaultAudioRouteToSpeakerphone(true)
        // 1. Users can only see each other after they join the
        // same channel successfully using the same app id.
        // 2. One token is only valid for the channel name that
        // you use to generate this token.
       // agoraKit.joinChannel(byUserAccount: roomEmail, token: Token, channelId: channelId, joinSuccess: nil)
        
        agoraKit?.joinChannel(byToken: Token, channelId: "CHECK" + chanal, info: nil, uid: 0, joinSuccess: { [self] (channel, uid, elapsed) in
            if isVideo{
                self.isLocalVideoRender = true
                isRemoteVideoRender = true
                startRecording(channelID: "CHECK" + chanal)

                var parent: UIView = remoteContainer
                if let it = localVideo, let view = it.view {
                    if view.superview == parent {
                        parent = localContainer
                        

                    }
                }
            }else{
                if let it = remoteVideo, it.uid == uid {
                        removeFromParent(it)
                        remoteVideo = nil
                           }
                self.isLocalVideoRender = false
                isRemoteVideoRender = false
                startRecording(channelID: "CHECK" + chanal)
            }
            
            
            })
    
      /*
        agoraKit.joinChannel(byUserAccount: _currentUser.patient_id, token: Token, channelId: "CHECK" + chanal) { [unowned self] (channel, uid, elapsed) -> Void in
            if isVideo{
                self.isLocalVideoRender = true
                isRemoteVideoRender = true
                startRecording(channelID: "CHECK" + chanal)

                var parent: UIView = remoteContainer
                if let it = localVideo, let view = it.view {
                    if view.superview == parent {
                        parent = localContainer
                        

                    }
                }
            }else{
                if let it = remoteVideo, it.uid == uid {
                        removeFromParent(it)
                        remoteVideo = nil
                           }
                self.isLocalVideoRender = false
                isRemoteVideoRender = false
                startRecording(channelID: "CHECK" + chanal)
            }
            // Did join channel "demoChannel1"
            //self.logVC?.log(type: .info, content: "did join channel")
        }
//        agoraKit.joinChannel(byToken: Token, channelId: "u6n34qa", info: nil, uid: 0) { [unowned self] (channel, uid, elapsed) -> Void in
//            // Did join channel "demoChannel1"
//            self.isLocalVideoRender = true
//            //self.logVC?.log(type: .info, content: "did join channel")
//        }
       
       */
        if isVideo{
        isStartCalling = true
        }
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    func leaveChannel() {
        // leave channel and end chat
        
        self.stopRecording(channelID: "CHECK" + chanal)

    }
    
    @IBAction func didClickHangUpButton(_ sender: UIButton) {
        sender.isSelected.toggle()
        
        if sender.isSelected {
            leaveChannel()
            duration?.invalidate()
            removeFromParent(localVideo)
            localVideo = nil
            removeFromParent(remoteVideo)
            remoteVideo = nil
        }
//        else {
//            setupLocalVideo()
//            joinChannel()
//        }
    }
    
    @IBAction func didClickMuteButton(_ sender: UIButton) {
        sender.isSelected.toggle()
        // mute local audio
        agoraKit.muteLocalAudioStream(sender.isSelected)
    }
    
    @IBAction func didClickSwitchCameraButton(_ sender: UIButton) {
        sender.isSelected.toggle()
        agoraKit.switchCamera()
    }
    
    @IBAction func didClickLocalContainer(_ sender: Any) {
        switchView(localVideo)
        switchView(remoteVideo)
    }
    
    func removeFromParent(_ canvas: AgoraRtcVideoCanvas?) -> UIView? {
        if let it = canvas, let view = it.view {
            let parent = view.superview
            if parent != nil {
                view.removeFromSuperview()
                return parent
            }
        }
        return nil
    }
    
    func switchView(_ canvas: AgoraRtcVideoCanvas?) {
        let parent = removeFromParent(canvas)
        if parent == localContainer {
            canvas!.view!.frame.size = remoteContainer.frame.size
            remoteContainer.addSubview(canvas!.view!)
        } else if parent == remoteContainer {
            canvas!.view!.frame.size = localContainer.frame.size
            localContainer.addSubview(canvas!.view!)
        }
    }
    @objc func coutDriverTime(){
        lblTimer.text = splitToComponentTime3(seconds: timeCount)
        timeCount += 1
    }
    
    func splitToComponentTime3(seconds: Int) -> String{
        let hours = seconds / 3600
        var remainder = seconds - hours * 3600
        let mins = remainder / 60
        remainder -= mins * 60
        let secs = remainder
        if hours > 0{
            return "\(hours < 10 ? "0\(hours)" : "\(hours)")" + ":" + "\(mins < 10 ? "0\(mins)" : "\(mins)")" + ":" + "\(secs < 10 ? "0\(secs)" : "\(secs)")"
        }else{
            return "\(mins < 10 ? "0\(mins)" : "\(mins)")" + ":" + "\(secs < 10 ? "0\(secs)" : "\(secs)")"
        }
    }
    
    
}

extension VideoChatVC: AgoraRtcEngineDelegate {
    
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        
        lblConection.text = "Connected".uppercased()
        duration = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(coutDriverTime), userInfo: nil, repeats: true)
        if isVideo{
            isRemoteVideoRender = true
            var parent: UIView = remoteContainer
//            if let it = localVideo, let view = it.view {
//                if view.superview == parent {
//                    parent = localContainer
//                }
//            }

            // Only one remote video view is available for this
            // tutorial. Here we check if there exists a surface
            // view tagged as this uid.
//            if remoteVideo != nil {
//                return
//            }

            let view = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: parent.frame.size))
            remoteVideo = AgoraRtcVideoCanvas()
            remoteVideo!.view = view
            remoteVideo!.renderMode = .hidden
            remoteVideo!.uid = uid
            parent.addSubview(remoteVideo!.view!)
            agoraKit.setupRemoteVideo(remoteVideo!)
        }
        
    }
    
    /// Occurs when a remote user (Communication)/host (Live Broadcast) leaves a channel.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - uid: ID of the user or host who leaves a channel or goes offline.
    ///   - reason: Reason why the user goes offline
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid:UInt, reason:AgoraUserOfflineReason) {
        
       // leaveChannel()
        lblConection.text = "Contacting...".uppercased()
        duration?.invalidate()
        isRemoteVideoRender = false
//        if isVideo{
//            isRemoteVideoRender = false
//            if let it = remoteVideo, it.uid == uid {
//                removeFromParent(it)
//                remoteVideo = nil
//            }
//        }
    }
    
    /// Occurs when a remote user’s video stream playback pauses/resumes.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - muted: YES for paused, NO for resumed.
    ///   - byUid: User ID of the remote user.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted:Bool, byUid:UInt) {
        isRemoteVideoRender = !muted
    }
    
    /// Reports a warning during SDK runtime.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - warningCode: Warning code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
       // showFailMessage(message: "did occur warning, code: \(warningCode.rawValue)")
    }
    
    /// Reports an error during SDK runtime.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - errorCode: Error code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
      //  showFailMessage(message: "did occur error, code: \(errorCode.rawValue)")
    }
    func startRecording(channelID : String){
        var dic = [String:Any]()
        dic["nAppointmentId"] = strAppointmentIdGlobal
        dic["vChannelName"] = channelID
        dic["vUserAccount"] = _currentUser.patient_id
     //   dic["nAgoraRecordingUserId"] = vAccessTokenForAgoraRecording
        dic["nRole"] = "0"

        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
    //    showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "startRecording.php", param: parameters) { (response) in
     //       self.hideCentralSpinner()
            
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                    if let data = Response.value(forKey: "data") as? NSDictionary{
                                    resourceID = data.getStringValue(key: "resourceId")
                                    sid = data.getStringValue(key: "sId")
                                    userrcodingAgora = data.getStringValue(key: "uId")
                                    }
                                }
                            }
                        }
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
//    func acquire(channelID : String){
//        KPWebCall.call.postRequest(path: "https://api.agora.io/v1/apps/91d4521d03454ee6ba97cf85e15918fc/cloud_recording/acquire", param: "{\"clientRequest\":{},\"cname\":\"\(channelID)\",\"uid\":\"\(userrcodingAgora!)\"}") { json, error in
//            if let dict = json as? NSDictionary{
//                self.resourceID = dict.getStringValue(key: "resourceId")
//                self.startRecording(channelID: channelID)
//            }
//        }
//    }
//
//    func startRecording(channelID : String){
//        let strParam = "{\"clientRequest\":{\"recordingConfig\":{\"channelType\":0,\"maxIdleTime\":20,\"streamTypes\":2},\"recordingFileConfig\":{\"avFileType\":[\"hls\",\"mp4\"]},\"storageConfig\":{\"accessKey\":\"AKIA4R2XDICRF2NVAMWI\",\"bucket\":\"checkup-agora-call-recording\",\"fileNamePrefix\":[\"CheckupHealth\",\"Test\"],\"region\":7,\"secretKey\":\"GEYu+bW+9aCvX0M0DJTZtE89f7Mihhx45kRp2FOO\",\"vendor\":1},\"token\":\"\(vAccessTokenForAgoraRecording!)\"},\"cname\":\"\(channelID)\",\"uid\":\"\(userrcodingAgora!)\"}"
//
//
//
//
//       print("{\"clientRequest\":{\"recordingConfig\":{\"channelType\":0,\"maxIdleTime\":20,\"streamTypes\":2},\"recordingFileConfig\":{\"avFileType\":[\"hls\",\"mp4\"]},\"storageConfig\":{\"accessKey\":\"AKIA4R2XDICRF2NVAMWI\",\"bucket\":\"checkup-agora-call-recording\",\"fileNamePrefix\":[\"CheckupHealth\",\"Test\"],\"region\":7,\"secretKey\":\"GEYu+bW+9aCvX0M0DJTZtE89f7Mihhx45kRp2FOO\",\"vendor\":1},\"token\":\"\(vAccessTokenForAgoraRecording!)\"},\"cname\":\"\(channelID)\",\"uid\":\"\(userrcodingAgora!)\"}")
//
//        KPWebCall.call.postRequest(path: "https://api.agora.io/v1/apps/91d4521d03454ee6ba97cf85e15918fc/cloud_recording/resourceid/\(self.resourceID)/mode/mix/start", param: strParam) { json, string in
//            if let dict = json as? NSDictionary{
//                self.resourceID = dict.getStringValue(key: "resourceId")
//                self.sid = dict.getStringValue(key: "sid")
//                self.query()
//            }
//        }
//    }
//
//
//    func query(){
//
//        KPWebCall.call.getRequest(path: "https://api.agora.io/v1/apps/91d4521d03454ee6ba97cf85e15918fc/cloud_recording/resourceid/\(self.resourceID)/sid/\(sid)/mode/mix/query", param: "") { json, string in
//            if let dict = json as? NSDictionary{
//                self.resourceID = dict.getStringValue(key: "resourceId")
//                self.sid = dict.getStringValue(key: "sid")
//            }
//        }
//    }
    

    func stopRecording(channelID : String){
        var dic = [String:Any]()
        dic["nAppointmentId"] = strAppointmentIdGlobal
        dic["vChannelName"] = channelID
        dic["uId"] = sid
        dic["sId"] = userrcodingAgora
        dic["resourceId"] = resourceID
        

        let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        let parameters = "{\"data\":\(jsonString)}"
        showCentralSpinner()
        KPWebCall.call.postRequestApiClinet(relPath: "stopRecording.php", param: parameters) { [self] (response) in
            self.hideCentralSpinner()
            if let response = response {
                do {
                    if let response = try JSONSerialization.jsonObject(with: response.ReturnedData!, options: []) as? NSDictionary {
                        print(response)
                        DispatchQueue.main.async { [self] in
                            if let Response = response as? NSDictionary{
                                if Response.getIntValue(key: "success") == 1{
                                       
                                }
                            }
                            agoraKit.leaveChannel(nil)
                            duration?.invalidate()
                            isRemoteVideoRender = false
                            isLocalVideoRender = false
                            isStartCalling = false
                            UIApplication.shared.isIdleTimerDisabled = false
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                catch let error as NSError {
                    agoraKit.leaveChannel(nil)
                    duration?.invalidate()
                    isRemoteVideoRender = false
                    isLocalVideoRender = false
                    isStartCalling = false
                    UIApplication.shared.isIdleTimerDisabled = false
                    _ = self.navigationController?.popViewController(animated: true)
                    print(error.localizedDescription)
                }
            }
        }
    }


    
    
    
}












































